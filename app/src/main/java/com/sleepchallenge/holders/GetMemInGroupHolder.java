package com.sleepchallenge.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.sleepchallenge.R;
import com.sleepchallenge.itemclicklisteners.GetMemInGroupClickListner;


/**
 * Created by admin on 12/18/2017.
 */

public class GetMemInGroupHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView memNAME;
    public ImageView memDeleteButt;
    GetMemInGroupClickListner getMemInGroupClickListner;

    public GetMemInGroupHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        memNAME = (TextView) itemView.findViewById(R.id.memNAME);
        memDeleteButt = (ImageView) itemView.findViewById(R.id.memDeleteButt);

    }

    @Override
    public void onClick(View view) {
        this.getMemInGroupClickListner.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(GetMemInGroupClickListner ic)
    {
        this.getMemInGroupClickListner =ic;
    }

}
