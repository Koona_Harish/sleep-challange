package com.sleepchallenge.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sleepchallenge.R;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;


public class MyGroupHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public ImageView group_image;
    public TextView group_ranking_text,group_name,group_totlapercentage,group_win_per,group_los_per,send_request_button,member_count;
    AllItemClickListeners allMembersItemClickListener;

    public MyGroupHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        group_image = itemView.findViewById(R.id.group_image);
        group_ranking_text = itemView.findViewById(R.id.group_ranking_text);
        group_name = itemView.findViewById(R.id.group_name);
        group_totlapercentage = itemView.findViewById(R.id.group_totlapercentage);
        group_win_per = itemView.findViewById(R.id.group_win_per);
        group_los_per = itemView.findViewById(R.id.group_los_per);
        send_request_button = itemView.findViewById(R.id.send_request_button);
        member_count = itemView.findViewById(R.id.member_count);
       // user_type = itemView.findViewById(R.id.user_type);
    }

    @Override
    public void onClick(View view) {
        this.allMembersItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AllItemClickListeners ic) {
        this.allMembersItemClickListener = ic;
    }
}
