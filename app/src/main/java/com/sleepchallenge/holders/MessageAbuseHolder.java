package com.sleepchallenge.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;

import com.sleepchallenge.R;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;


public class MessageAbuseHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public RadioButton reasone_radio_dynamic_button;
    AllItemClickListeners deactivateReasonItemClickListener;

    public MessageAbuseHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        reasone_radio_dynamic_button = itemView.findViewById(R.id.reasone_radio_dynamic_button);
    }

    @Override
    public void onClick(View view) {

        this.deactivateReasonItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AllItemClickListeners ic) {
        this.deactivateReasonItemClickListener = ic;
    }
}