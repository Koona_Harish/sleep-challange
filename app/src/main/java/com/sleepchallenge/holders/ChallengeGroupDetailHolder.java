package com.sleepchallenge.holders;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sleepchallenge.R;
import com.sleepchallenge.itemclicklisteners.ChallengeGroupDetailItemClickListener;


public class ChallengeGroupDetailHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView status,challenger_names,dateandtime,cash_prize_amount, vsText, oponent_names,winner_name,won_cash,challenge_type,
            starttime_text,wakeup_time_text;

    public ImageView activity_image,share_icon,pause_challenge;
    public LinearLayout ll_completed;
    public CardView card_view;
    public RelativeLayout timing_layout;
    ChallengeGroupDetailItemClickListener challengGroupDetailItemClickListener;

    public ChallengeGroupDetailHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        //activity_image = (ImageView)itemView.findViewById(R.id.activity_image);
        share_icon = (ImageView)itemView.findViewById(R.id.share_icon);
        pause_challenge = (ImageView)itemView.findViewById(R.id.pause_challenge);

        status = (TextView) itemView.findViewById(R.id.status);
        vsText = (TextView) itemView.findViewById(R.id.vsText);
        oponent_names = (TextView) itemView.findViewById(R.id.oponent_names);
        challenger_names = (TextView) itemView.findViewById(R.id.challenger_names);
        dateandtime = (TextView) itemView.findViewById(R.id.dateandtime);
        cash_prize_amount = (TextView) itemView.findViewById(R.id.cash_prize_amount);
        challenge_type = (TextView) itemView.findViewById(R.id.challenge_type);

        winner_name = (TextView) itemView.findViewById(R.id.winner_name);
        won_cash = (TextView) itemView.findViewById(R.id.won_cash);
        ll_completed = (LinearLayout) itemView.findViewById(R.id.ll_completed);
        card_view =  itemView.findViewById(R.id.card_view);
        starttime_text = itemView.findViewById(R.id.start_time_txt);
        wakeup_time_text = itemView.findViewById(R.id.wake_up_time_txt);
        timing_layout = itemView.findViewById(R.id.time_layout);


    }

    @Override
    public void onClick(View view) {

        this.challengGroupDetailItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(ChallengeGroupDetailItemClickListener ic)
    {
        this.challengGroupDetailItemClickListener=ic;
    }
}
