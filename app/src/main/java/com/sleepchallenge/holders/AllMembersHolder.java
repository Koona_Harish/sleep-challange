package com.sleepchallenge.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sleepchallenge.R;
import com.sleepchallenge.itemclicklisteners.AllMembersItemClickListener;


public class AllMembersHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public ImageView all_member_image;
    public TextView all_member_ranking_text,all_member_user_name,all_member_totlapercentage,all_member_win_per,all_member_los_per,send_request_button,user_type,member_usertype_text;
    AllMembersItemClickListener allMembersItemClickListener;

    public AllMembersHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        all_member_image = itemView.findViewById(R.id.all_member_image);
        all_member_ranking_text = itemView.findViewById(R.id.all_member_ranking_text);
        all_member_user_name = itemView.findViewById(R.id.all_member_user_name);
        all_member_totlapercentage = itemView.findViewById(R.id.all_member_totlapercentage);
        all_member_win_per = itemView.findViewById(R.id.all_member_win_per);
        all_member_los_per = itemView.findViewById(R.id.all_member_los_per);
        send_request_button = itemView.findViewById(R.id.send_request_button);
        user_type = itemView.findViewById(R.id.user_type);
        member_usertype_text = itemView.findViewById(R.id.member_usertype_text);
    }

    @Override
    public void onClick(View view) {
        this.allMembersItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AllMembersItemClickListener ic) {
        this.allMembersItemClickListener = ic;
    }
}
