package com.sleepchallenge.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sleepchallenge.R;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;


public class GroupListHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView group_name_text,mesage_desc_text,message_sent_time;
      public ImageView group_profile_pic;


    AllItemClickListeners groupListItemClickListener;

    public GroupListHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        group_name_text =  itemView.findViewById(R.id.group_name_text);
        mesage_desc_text =  itemView.findViewById(R.id.mesage_desc_text);
        message_sent_time = itemView.findViewById(R.id.message_sent_time);
        group_profile_pic = itemView.findViewById(R.id.group_profile_pic);
    }

    @Override
    public void onClick(View view) {
        this.groupListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AllItemClickListeners ic)
    {
        this.groupListItemClickListener =ic;
    }
}
