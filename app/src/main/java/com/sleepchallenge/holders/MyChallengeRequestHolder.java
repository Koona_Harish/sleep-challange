package com.sleepchallenge.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sleepchallenge.R;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;


public class MyChallengeRequestHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public RelativeLayout rank_rl;
    public ImageView challenger_image,challenger_rank_img;
    public TextView cancel_text,challenger_rank,challenger_req_name,challenge_start_time,challenger_req_amount_doller;
    public TextView challenge_goal_txt,challenge_evaluation_unit,challenger_type,challenge_start_text,challenge_goal_title,challenger_req_amount_title,status_text;

    AllItemClickListeners challengesListItemClickListener;

    public MyChallengeRequestHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        rank_rl =  itemView.findViewById(R.id.rank_rl);
        challenger_image =  itemView.findViewById(R.id.challenger_image);
        challenger_rank_img =  itemView.findViewById(R.id.challenger_rank_img);
        cancel_text =  itemView.findViewById(R.id.cancel_text);

        challenger_rank = itemView.findViewById(R.id.challenger_rank);
        challenger_req_name =  itemView.findViewById(R.id.challenger_req_name);
        challenge_start_time =itemView.findViewById(R.id.challenge_start_time);
        challenger_req_amount_title = itemView.findViewById(R.id.challenger_req_amount_title);
        challenger_req_amount_doller = (TextView) itemView.findViewById(R.id.challenger_req_amount_doller);
        challenge_goal_txt=itemView.findViewById(R.id.challenge_goal_txt);
        challenge_evaluation_unit=itemView.findViewById(R.id.challenge_evaluation_unit);
        challenger_type=itemView.findViewById(R.id.challenger_type);
        challenge_goal_title=itemView.findViewById(R.id.challenge_goal_title);
        challenge_start_text=itemView.findViewById(R.id.challenge_start_text);
        challenge_evaluation_unit=itemView.findViewById(R.id.challenge_evaluation_unit);
        status_text=itemView.findViewById(R.id.status_text);

    }

    @Override
    public void onClick(View view) {

        this.challengesListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AllItemClickListeners ic)
    {
        this.challengesListItemClickListener = ic;
    }
}
