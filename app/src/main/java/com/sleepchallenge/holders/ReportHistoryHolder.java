package com.sleepchallenge.holders;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sleepchallenge.R;


/**
 * Created by admin on 12/4/2017.
 */

public class ReportHistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView title_r, desc_r, date_r,type_r;
    public CardView card_view;

    public ReportHistoryHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        title_r = itemView.findViewById(R.id.title_r);
        desc_r =  itemView.findViewById(R.id.desc_r);
        date_r =itemView.findViewById(R.id.date_r);
        type_r =  itemView.findViewById(R.id.type_r);
        card_view = itemView.findViewById(R.id.card_view);


    }

    @Override
    public void onClick(View view) {

    }
}
