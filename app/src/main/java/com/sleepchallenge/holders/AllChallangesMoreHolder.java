package com.sleepchallenge.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sleepchallenge.R;
import com.sleepchallenge.itemclicklisteners.AllChalngMoreItemClickListener;

public class AllChallangesMoreHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView all_challenger_names,all_status,all_cash_prize_amount,all_dateandtime, vsText, oponent_names,winner_name,won_cash,challenge_type;

    public ImageView share_icon,all_activity_image,pause_challenge;

    public LinearLayout ll_completed;

    AllChalngMoreItemClickListener allchallangMoreItemClickListener;

    public AllChallangesMoreHolder(View itemView)
    {
        super(itemView);
        itemView.setOnClickListener(this);
        share_icon = (ImageView)itemView.findViewById(R.id.share_icon);
        all_activity_image = (ImageView)itemView.findViewById(R.id.all_activity_image);
        pause_challenge = (ImageView)itemView.findViewById(R.id.pause_challenge);

        vsText = (TextView) itemView.findViewById(R.id.vsText);
        oponent_names = (TextView) itemView.findViewById(R.id.oponent_names);
        all_challenger_names = (TextView) itemView.findViewById(R.id.all_challenger_names);
        all_status = (TextView) itemView.findViewById(R.id.all_status);
        all_cash_prize_amount = (TextView) itemView.findViewById(R.id.all_cash_prize_amount);
        all_dateandtime = (TextView) itemView.findViewById(R.id.all_dateandtime);
        challenge_type = (TextView) itemView.findViewById(R.id.challenge_type);

        winner_name = (TextView) itemView.findViewById(R.id.winner_name);
        won_cash = (TextView) itemView.findViewById(R.id.won_cash);
        ll_completed = (LinearLayout) itemView.findViewById(R.id.ll_completed);

    }

    @Override
    public void onClick(View view) {

        this.allchallangMoreItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AllChalngMoreItemClickListener ic)
    {
        this.allchallangMoreItemClickListener=ic;
    }
}
