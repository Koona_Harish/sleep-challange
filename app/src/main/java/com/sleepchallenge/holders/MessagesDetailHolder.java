package com.sleepchallenge.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sleepchallenge.R;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;

public class MessagesDetailHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public LinearLayout ll_left,ll_right;
   public TextView msg_left,msg_right,msg_right_date,msg_left_date,report_abuse_left;


    AllItemClickListeners messagesDetailItemClickListener;

    public MessagesDetailHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        msg_left =  itemView.findViewById(R.id.msg_left);
        msg_right =  itemView.findViewById(R.id.msg_right);
        msg_right_date =  itemView.findViewById(R.id.msg_right_date);
        msg_left_date =  itemView.findViewById(R.id.msg_left_date);
        report_abuse_left = itemView.findViewById(R.id.report_abuse_left);
        ll_left = itemView.findViewById(R.id.ll_left);
        ll_right =  itemView.findViewById(R.id.ll_right);


    }

    @Override
    public void onClick(View view) {
        this.messagesDetailItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AllItemClickListeners ic)
    {
        this.messagesDetailItemClickListener =ic;
    }
}
