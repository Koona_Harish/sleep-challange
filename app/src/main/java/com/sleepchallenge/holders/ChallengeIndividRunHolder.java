package com.sleepchallenge.holders;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sleepchallenge.R;
import com.sleepchallenge.itemclicklisteners.ChallengeIndividRunItemClickListener;

public class ChallengeIndividRunHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView status,challenger_names,dateandtime,cash_prize_amount, vsText, oponent_names,winner_name,won_cash,challenge_type,starttime_text,wakeup_time_text;

    public ImageView activity_image,share_icon,pause_challenge;

    public LinearLayout ll_completed;
    public CardView card_view;
     public RelativeLayout timing_layout;

    ChallengeIndividRunItemClickListener challengIndRunItemClickListener;

    public ChallengeIndividRunHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        //activity_image = (ImageView)itemView.findViewById(R.id.activity_image);
        share_icon = itemView.findViewById(R.id.share_icon);
        pause_challenge = itemView.findViewById(R.id.pause_challenge);

        status = itemView.findViewById(R.id.status);
        vsText = itemView.findViewById(R.id.vsText);
        oponent_names = itemView.findViewById(R.id.oponent_names);
        challenger_names = itemView.findViewById(R.id.challenger_names);
        dateandtime = itemView.findViewById(R.id.dateandtime);
        cash_prize_amount = itemView.findViewById(R.id.cash_prize_amount);
        challenge_type = itemView.findViewById(R.id.challenge_type);

        winner_name = itemView.findViewById(R.id.winner_name);
        won_cash = itemView.findViewById(R.id.won_cash);
        ll_completed = itemView.findViewById(R.id.ll_completed);
        card_view = itemView.findViewById(R.id.card_view);
        starttime_text = itemView.findViewById(R.id.start_time_txt);
        wakeup_time_text = itemView.findViewById(R.id.wake_up_time_txt);
        timing_layout = itemView.findViewById(R.id.time_layout);
    }

    @Override
    public void onClick(View view) {

        this.challengIndRunItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(ChallengeIndividRunItemClickListener ic)
    {
        this.challengIndRunItemClickListener=ic;
    }
}
