package com.sleepchallenge.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.holders.ReportHistoryHolder;
import com.sleepchallenge.models.ReportHistoryModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class ReportHistory extends AppCompatActivity implements View.OnClickListener {

    ImageView back_img, unav;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String device_id, access_token, user_id;
    UserSessionManager session;
    ArrayList<ReportHistoryModel> rList = new ArrayList<ReportHistoryModel>();
    RecyclerView report_history_recycler;
    LinearLayoutManager lLayMan;
    ReportHistoryAdapter rHisAdap;
    TextView title_text;
    Typeface regularFont;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_history);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("SESSIONDATA", user_id + "\n" + access_token + "\n" + device_id);
        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        report_history_recycler =  findViewById(R.id.report_history_recycler);
        rHisAdap = new ReportHistoryAdapter(ReportHistory.this, rList, R.layout.row_report_history);
        lLayMan = new LinearLayoutManager(this);
        report_history_recycler.setNestedScrollingEnabled(false);
        report_history_recycler.setLayoutManager(lLayMan);
        title_text = findViewById(R.id.title_text);
        title_text.setTypeface(regularFont);
        back_img =  findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        unav =  findViewById(R.id.unav);

        getReportHistory();
    }

    private void getReportHistory() {
        checkInternet = NetworkChecking.isConnected(ReportHistory.this);
        if (checkInternet) {
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.FEEDBACK_HISTORY + user_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("HISTORY", AppUrls.BASE_URL + "user/feedback?id=" + user_id);
                            Log.d("REPRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                    unav.setVisibility(View.GONE);
                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jArr.length(); i++) {
                                        ReportHistoryModel rhm = new ReportHistoryModel();
                                        JSONObject itemArray = jArr.getJSONObject(i);
                                        rhm.setCreated_on(itemArray.getString("created_on"));
                                        rhm.setCreated_on_txt(itemArray.getString("created_on_txt"));
                                        rhm.setDetails(itemArray.getString("details"));
                                        rhm.setDevice_name(itemArray.getString("device_name"));
                                        rhm.setDevice_platform(itemArray.getString("device_platform"));
                                        rhm.setId(itemArray.getString("id"));
                                        rhm.setTitle(itemArray.getString("title"));
                                        rhm.setType(itemArray.getString("type"));
                                        rList.add(rhm);
                                    }
                                    report_history_recycler.setAdapter(rHisAdap);
                                }
                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(ReportHistory.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                    unav.setVisibility(View.VISIBLE);
                                }
                                if (successResponceCode.equals("10300")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(ReportHistory.this, "No data Found", Toast.LENGTH_SHORT).show();
                                    unav.setVisibility(View.VISIBLE);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                          error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ReportHistory.this);
            requestQueue.add(strRe);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(ReportHistory.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private class ReportHistoryAdapter extends RecyclerView.Adapter<ReportHistoryHolder> {
        ReportHistory context;
        int resource;
        ArrayList<ReportHistoryModel> rhm;
        LayoutInflater lInfla;
        String datetime;

        public ReportHistoryAdapter(ReportHistory context, ArrayList<ReportHistoryModel> rhm, int resource) {
            this.context = context;
            this.resource = resource;
            this.rhm = rhm;
            lInfla = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public ReportHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View inf = lInfla.inflate(resource, parent, false);
            ReportHistoryHolder rHol = new ReportHistoryHolder(inf);
            return rHol;
        }

        @Override
        public void onBindViewHolder(ReportHistoryHolder holder, int position) {


            holder.card_view.setCardBackgroundColor(Color.parseColor("#E0E0E0"));

            holder.title_r.setText(rhm.get(position).getTitle());
            holder.desc_r.setText(rhm.get(position).getDetails());
            datetime=parseDateToddMMyyyy(rhm.get(position).getCreated_on());
            holder.date_r.setText(datetime);
            holder.type_r.setText(rhm.get(position).getType());
            if (rhm.get(position).getType().equals("Feedback")) {
                holder.type_r.setTextColor(Color.BLUE);
            } else if (rhm.get(position).getType().equals("Suggestion")) {
                holder.type_r.setTextColor(Color.GRAY);
            } else if (rhm.get(position).getType().equals("Complaint")) {
                holder.type_r.setTextColor(Color.RED);
            }
        }

        @Override
        public int getItemCount() {
            return rhm.size();
        }

        public String parseDateToddMMyyyy(String time) {
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            String outputPattern = "dd MMM yyyy";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            Date date = null;
            String str = null;

            try {
                date = inputFormat.parse(time);
                str = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return str;
        }
    }

    @Override
    public void onClick(View view) {
        if (view == back_img) {
            finish();
        }
    }


}
