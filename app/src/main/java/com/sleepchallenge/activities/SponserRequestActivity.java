package com.sleepchallenge.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;
import com.sleepchallenge.models.SponsorReqModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.DividerItemDecorator;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SponserRequestActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView back_img;
    TextView title_text;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String user_id, user_type, token, device_ID;
    Typeface specialFont, boldFont, thinFont, regularFont;
    SponserRequestAdapter sponserRequestAdapter;
    RecyclerView sponsor_req_recyclerView;
    ArrayList<SponsorReqModel> sponserReqList;
    RecyclerView.LayoutManager layoutManager;
    TextView no_request_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponser_request);

        Typeface regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));
        userSessionManager = new UserSessionManager(SponserRequestActivity.this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_ID = userDetails.get(UserSessionManager.DEVICE_ID);
        checkInternet = NetworkChecking.isConnected(this);
        sponsor_req_recyclerView = findViewById(R.id.sponsor_req_recyclerView);
        back_img = findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        title_text = findViewById(R.id.title_text);
        title_text.setTypeface(regularFont);
        no_request_text = findViewById(R.id.no_request_text);
        no_request_text.setTypeface(specialFont);
        getSponsorRequest();

    }

    private void getSponsorRequest() {
        checkInternet = NetworkChecking.isConnected(SponserRequestActivity.this);
        if (checkInternet) {
            Log.d("URL", AppUrls.BASE_URL + AppUrls.SPONSOR_REQUEST + user_id);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.SPONSOR_REQUEST + user_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("RERERERE", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    sponserReqList = new ArrayList<>();
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jdataobj = jarray.getJSONObject(i);
                                        SponsorReqModel sponreq = new SponsorReqModel(jdataobj);
                                        sponserReqList.add(sponreq);
                                    }
                                    sponserRequestAdapter = new SponserRequestAdapter(sponserReqList, SponserRequestActivity.this, R.layout.row_sponser_requet);
                                    layoutManager = new LinearLayoutManager(getApplicationContext());
                                    sponsor_req_recyclerView.setNestedScrollingEnabled(false);
                                    sponsor_req_recyclerView.setLayoutManager(layoutManager);
                                    RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(SponserRequestActivity.this, R.drawable.recycler_view_divider));
                                    sponsor_req_recyclerView.addItemDecoration(dividerItemDecoration);
                                    sponsor_req_recyclerView.setAdapter(sponserRequestAdapter);
                                }
                                if (response_code.equals("10200")) {
                                    no_request_text.setVisibility(View.VISIBLE);
                                    no_request_text.setText("Invalid Input..!");

                                }
                                if (response_code.equals("10300")) {
                                    no_request_text.setVisibility(View.VISIBLE);
                                    no_request_text.setText("No Sponsor Requests Found..!");

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            error.getMessage();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(SponserRequestActivity.this);
            requestQueue.add(stringRequest);

        } else {

            Toast.makeText(SponserRequestActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == back_img) {
            finish();
        }

    }

    //ADAPTER
    public class SponserRequestAdapter extends RecyclerView.Adapter<SponsorReqHolder>
    {
        public ArrayList<SponsorReqModel> sponReqListItem;
        public SponserRequestActivity context;
        LayoutInflater li;
        int resource;

        public SponserRequestAdapter(ArrayList<SponsorReqModel> sponReqListItem, SponserRequestActivity context, int resource) {
            this.sponReqListItem = sponReqListItem;
            this.context = context;
            this.resource = resource;
            li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);



        }

        @Override
        public SponsorReqHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = li.inflate(resource, null);
            SponsorReqHolder slh = new SponsorReqHolder(layout);
            return slh;
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onBindViewHolder(final SponsorReqHolder holder, final int position)
        {

            holder.sponsor_user_name.setText(sponReqListItem.get(position).sponsor_name);
            holder.sponsor_user_name.setTypeface(boldFont);

            holder.sponsor_usertype_text.setText(sponReqListItem.get(position).sponsor_type);
            holder.sponsor_usertype_text.setTypeface(regularFont);

            holder.sponsor_amount_txt.setText("$"+sponReqListItem.get(position).amount_paid);
            holder.sponsor_amount_txt.setTypeface(regularFont);



            Picasso.with(context)
                    .load(sponReqListItem.get(position).sponsor_image)
                    .placeholder(R.drawable.members_dummy)
                    .resize(60,60)
                    .into(holder.sponsor_image);

            //REJECT  SPONSOR REQ
            holder.reject_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context,R.style.MyDialogTheme);
                    alertDialog.setTitle("Are You Sure?");
                    alertDialog.setMessage("You Want To Reject This Sponsor?");
                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which)
                        {

                            rejectSponsor(sponReqListItem.get(position).amount_paid,sponReqListItem.get(position).id,sponReqListItem.get(position).sponsor_id);
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    alertDialog.show();
                }
            });


           //Accept SPONSOR REQ
            holder.accept_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    checkInternet = NetworkChecking.isConnected(context);
                    if (checkInternet) {
                        Log.d("SPREQ",AppUrls.BASE_URL + AppUrls.ACCEPT_SPONSOR);
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ACCEPT_SPONSOR,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                        Log.d("AcceptSponsor", response);

                                        try {
                                            JSONObject jsonObject = new JSONObject(response);

                                            String response_code = jsonObject.getString("response_code");
                                            if (response_code.equals("10100")) {

                                                Toast.makeText(context, "You Accepted the Sponsor Request..!", Toast.LENGTH_SHORT).show();
                                                Intent intent = new Intent(context, SponserRequestActivity.class);
                                                context.startActivity(intent);
                                                context.finish();
                                            }
                                            if (response_code.equals("10200")) {

                                                Toast.makeText(context, "Invalid Input", Toast.LENGTH_SHORT).show();
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                       error.getMessage();
                                    }
                                }) {

                            @Override
                            public Map<String, String> getHeaders()  {
                                Map<String, String> headers = new HashMap<>();
                                headers.put("x-access-token", token);
                                headers.put("x-device-id", device_ID);
                                headers.put("x-device-platform", "ANDROID");
                                Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                                return headers;
                            }

                            @Override
                            protected Map<String, String> getParams(){
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("user_id", user_id);
                                params.put("user_type",user_type);
                                params.put("amount", sponReqListItem.get(position).amount_paid);
                                params.put("sponsor_request_id", sponReqListItem.get(position).id);
                                params.put("sponsor_id", sponReqListItem.get(position).sponsor_id);
                                Log.d("ACCEPTSPONSOR", "PARMS" + params.toString());
                                return params;
                            }
                        };
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        RequestQueue requestQueue = Volley.newRequestQueue(context);
                        requestQueue.add(stringRequest);
                    } else {

                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
        @Override
        public int getItemCount() {
            return this.sponReqListItem.size();
        }
    }
        //Holder
        public class SponsorReqHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {

            ImageView sponsor_image,accept_txt,reject_text;
            TextView sponsor_user_name,sponsor_usertype_text,sponsor_amount_txt;

            AllItemClickListeners allItemClickListenersClickListner;
            private Map<String, Boolean> checkBoxStates = new HashMap<>();

            public SponsorReqHolder(View itemView) {
                super(itemView);
                itemView.setOnClickListener(this);

                sponsor_image =  itemView.findViewById(R.id.sponsor_image);
                reject_text = itemView.findViewById(R.id.reject_text);
                accept_txt =  itemView.findViewById(R.id.accept_txt);

                sponsor_user_name = itemView.findViewById(R.id.sponsor_user_name);
                sponsor_usertype_text =  itemView.findViewById(R.id.sponsor_usertype_text);
                sponsor_amount_txt = itemView.findViewById(R.id.sponsor_amount_txt);


            }

            @Override
            public void onClick(View view) {
                this.allItemClickListenersClickListner.onItemClick(view, getLayoutPosition());
            }

            public void setItemClickListener(AllItemClickListeners ic)
            {
                this.allItemClickListenersClickListner =ic;
            }
   }

    public void rejectSponsor( final String send_amount, final String send_sponsor_request_id, final String send_sponsor_id) {

        if (checkInternet) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.REJECT_SPONSOR,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // progressDialog.dismiss();
                            Log.d("RejectSponsor", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {

                                    Toast.makeText(SponserRequestActivity.this, "You Rejected the Sponsor Request..!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(SponserRequestActivity.this, SponserRequestActivity.class);
                                    startActivity(intent);

                                }
                                if (response_code.equals("10200")) {

                                    Toast.makeText(SponserRequestActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                           error.getMessage();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");

                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                   // params.put("sponsor_type", sponsor_type);  //SPONSOR
                     params.put("amount", send_amount);
                    params.put("sponsor_request_id", send_sponsor_request_id);
                    params.put("sponsor_id", send_sponsor_id);

                    Log.d("REJECTSPONSOR:", params.toString());
                    return params;


                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(SponserRequestActivity.this);
            requestQueue.add(stringRequest);
        } else {

            Toast.makeText(SponserRequestActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

}
