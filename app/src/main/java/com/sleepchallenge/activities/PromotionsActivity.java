package com.sleepchallenge.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.ImagePermissions;
import com.sleepchallenge.utils.MyMultipartEntity;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import info.hoang8f.android.segmented.SegmentedGroup;

public class PromotionsActivity extends AppCompatActivity implements View.OnClickListener,  RadioGroup.OnCheckedChangeListener {

    ImageView back_img,imgEdt,imgUploadButt;
    TextView title_text_promote,week_month_txt,text_week_month;
    Typeface regularfont;
    Button sendReqButt;
    UserSessionManager session;
    String user_id, user_type, token, device_ID,nameS,amountS,descS,time_span;
    private boolean checkInternet;
    EditText nameEdt,amountEdt,descEdt;
    SegmentedGroup span_group;
    TextInputLayout nameTil,amountTil,descTil;
    Uri outputFileUri;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";
    String selectedImagePath = "";
    String selectedSpan,startdate,enddate;
    Bitmap bitmap;
    RadioButton monthly_radio,weekly_radio;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotions);

        regularfont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        session = new UserSessionManager(this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_ID = userDetails.get(UserSessionManager.DEVICE_ID);
        checkInternet = NetworkChecking.isConnected(this);
        nameTil = findViewById(R.id.nameTil);
        amountTil = findViewById(R.id.amountTil);
        descTil = findViewById(R.id.descTil);
        back_img = findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        imgEdt= findViewById(R.id.imgEdt);
        imgUploadButt= findViewById(R.id.imgUploadButt);
        imgUploadButt.setOnClickListener(this);



        nameEdt=findViewById(R.id.nameEdt);
        amountEdt=findViewById(R.id.amountEdt);
        descEdt=findViewById(R.id.descEdt);

        span_group = findViewById(R.id.span_group);
        span_group.setOnCheckedChangeListener(this);

        title_text_promote=findViewById(R.id.title_text_promote);
        title_text_promote.setTypeface(regularfont);
        week_month_txt=findViewById(R.id.week_month_txt);
        week_month_txt.setTypeface(regularfont);

        sendReqButt=findViewById(R.id.sendReqButt);
        sendReqButt.setOnClickListener(this);
        sendReqButt.setTypeface(regularfont);

        monthly_radio=findViewById(R.id.monthly_radio);
        monthly_radio.setTypeface(regularfont);
        weekly_radio=findViewById(R.id.weekly_radio);
        weekly_radio.setTypeface(regularfont);


        text_week_month=findViewById(R.id.text_week_month);
        text_week_month.setOnClickListener(this);

        week_month_txt.setText("Select Month");
        selectedSpan="MONTHLY";


    }

    @Override
    public void onClick(View view)
    {
         if(view==back_img)
         {
             finish();
         }

         if(view==imgUploadButt)
         {
              selectOption();
         }

         if(view==sendReqButt)
         {
             checkInternet = NetworkChecking.isConnected(getApplicationContext());
             nameS = nameEdt.getText().toString().trim();
             amountS = amountEdt.getText().toString().trim();
             descS = descEdt.getText().toString().trim();
             time_span = text_week_month.getText().toString().trim();

               if(getValidation())
               {


                   if (checkInternet)
                   {
                       String url = AppUrls.BASE_URL + AppUrls.ADD_PROMOTE;
                       Log.d("ADDPROMOTE", url);
                       Log.d("PATHHHHH", selectedImagePath);
                       PromotionsActivity.this.runOnUiThread(new Runnable()
                       {
                           public void run() {

                           }
                       });
                       StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                               .permitAll().build();
                       StrictMode.setThreadPolicy(policy);

                       if(selectedImagePath.equals("")|| selectedImagePath.length()==0)
                       {
                           Toast.makeText(PromotionsActivity.this, "Please select promotion Ads..!", Toast.LENGTH_LONG).show();
                       }
                       else
                       {
                           new PromotionsActivity.HttpUpload(this, selectedImagePath).execute();
                           sendReqButt.setClickable(false);
                       }




                   } else {
                       Toast.makeText(PromotionsActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
                   }

               }

         }
         if(view==text_week_month)
         {
             Calendar c = Calendar.getInstance();
             int mYear = c.get(Calendar.YEAR);
             int mMonth = c.get(Calendar.MONTH);
             int mDay = c.get(Calendar.DAY_OF_MONTH);
             DatePickerDialog datePickerDialog = new DatePickerDialog(this,AlertDialog.THEME_HOLO_LIGHT,
                     new DatePickerDialog.OnDateSetListener() {

                         @Override
                         public void onDateSet(DatePicker view, int year,
                                               int monthOfYear, int dayOfMonth) {
                             String datestr="";
                              if(selectedSpan.equalsIgnoreCase("Monthly")){
                                  datestr=year+"-"+(monthOfYear + 1)+"-"+ dayOfMonth;
                                  text_week_month.setText((monthOfYear + 1) + "/" + year);
                                  startdate=getDateOfMonth(ConvertToDate(datestr),0);
                                  enddate=getDateOfMonth(ConvertToDate(datestr),1);
                                  Log.v("Dates","//////////"+startdate+"jjj"+enddate);

                              }else {
                                  text_week_month.setText(dayOfMonth + "/" +(monthOfYear + 1) + "/" + year);

                                  datestr=year+"-"+(monthOfYear + 1)+"-"+dayOfMonth;
                                  startdate=getDateOfMonth(ConvertToDate(datestr),0);
                                  enddate=getDateOfMonth(ConvertToDate(datestr),1);
                                  Log.v("DatesWee","//////////"+startdate+"jjj"+enddate);

                              }


                         }
                     }, mYear, mMonth, mDay);



             if(selectedSpan.equalsIgnoreCase("Monthly")){
                 ((ViewGroup) datePickerDialog.getDatePicker()).findViewById(Resources.getSystem().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
             }
             datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
             datePickerDialog.show();

         }

    }



    private void selectOption()
    {
        final Dialog dialog = new Dialog(PromotionsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.camera_dialog);
        TextView take_camera_pic =  dialog.findViewById(R.id.take_camera_pic);
        TextView take_gallery_pic =  dialog.findViewById(R.id.take_gallery_pic);
        TextView cancel =  dialog.findViewById(R.id.cancel);
        final boolean result = ImagePermissions.checkPermission(PromotionsActivity.this);
        dialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        take_camera_pic.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v)
            {

                if (result)

                    if (ContextCompat.checkSelfPermission(PromotionsActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        if (getFromPref(PromotionsActivity.this, ALLOW_KEY)) {
                            showSettingsAlert();
                        } else if (ContextCompat.checkSelfPermission(PromotionsActivity.this,
                                android.Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {

                            // Should we show an explanation?
                            if (ActivityCompat.shouldShowRequestPermissionRationale(PromotionsActivity.this,
                                    android.Manifest.permission.CAMERA)) {
                                showAlert();
                            } else {
                                // No explanation needed, we can request the permission.
                                ActivityCompat.requestPermissions(PromotionsActivity.this,
                                        new String[]{android.Manifest.permission.CAMERA},
                                        0);
                            }
                        }
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        ContentValues values = new ContentValues(1);
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                        outputFileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        startActivityForResult(intent, 0);
                    }
                dialog.cancel();

            }

        });
        take_gallery_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
                dialog.cancel();

            }

        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel(); // dismissing the popup
            }

        });
        dialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    Log.d("PPP", outputFileUri.toString());
                    if (outputFileUri != null) {
                        bitmap = decodeSampledBitmapFromUri(outputFileUri, imgEdt.getWidth(), imgEdt.getHeight());
                        if (bitmap == null) {
                            Toast.makeText(getApplicationContext(), "the image data could not be decoded" + outputFileUri.getPath(), Toast.LENGTH_LONG).show();
                        } else {
                            selectedImagePath = getRealPathFromURI(PromotionsActivity.this, outputFileUri);// outputFileUri.getPath().
                            Log.d("CAMERASELECTPATH", selectedImagePath);
                            imgEdt.setImageBitmap(bitmap);
                        }
                    }
                }
                break;

            case 1:

                if (resultCode == RESULT_OK) {
                    Uri targetUri = data.getData();
                    Log.d("TGGGGG", targetUri.toString());
                    Bitmap bitmap;
                    bitmap = decodeSampledBitmapFromUri(targetUri, imgEdt.getWidth(), imgEdt.getHeight());
                    if (bitmap == null) {
                        Toast.makeText(getApplicationContext(), "the image data could not be decoded" + targetUri.getPath(), Toast.LENGTH_LONG).show();
                    } else {
                        selectedImagePath = getPath(targetUri);// targetUri.getPath();
                        Log.d("GALLRYSSSSSSSSS", selectedImagePath);
                        imgEdt.setImageBitmap(bitmap);
                    }
                }
                break;

            default:

                break;

        }

    }

    public String getPath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null,
                null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            return cursor.getString(columnIndex);
        }

        return uri.getPath();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            if ("content".equals(contentUri.getScheme()))
            {
                String[] proj = {MediaStore.Images.Media.DATA};
                cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else {
                return contentUri.getPath();
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }

        }

    }

    public Bitmap decodeSampledBitmapFromUri(Uri uri, int reqWidth, int reqHeight)
    {
        Bitmap bm = null;
        String filePath =getPath(uri);
        // Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();


        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 516.0f;    //816 and 612
        float maxWidth = 412.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            bm = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(bm);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            bm = Bitmap.createBitmap(bm, 0, 0,
                    bm.getWidth(), bm.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = filePath;
        try {
            out = new FileOutputStream(filename);


            bm.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return bm;

    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    private void showAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(PromotionsActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(PromotionsActivity.this,
                                new String[]{android.Manifest.permission.CAMERA},
                                0);
                    }
                });
        alertDialog.show();
    }

    public static void saveToPreferences(Context context, String key, Boolean allowed) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putBoolean(key, allowed);
        prefsEditor.commit();
    }

    public static Boolean getFromPref(Context context, String key) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        return (myPrefs.getBoolean(key, false));
    }

    private void showSettingsAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(PromotionsActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startInstalledAppDetailsActivity(PromotionsActivity.this);
                    }
                });

        alertDialog.show();
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }


    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId)
    {
        if (checkedId == R.id.monthly_radio)
        {
            week_month_txt.setText("Select Month");
            selectedSpan="MONTHLY";
            text_week_month.setText("Select");
        }
        else
         {
             week_month_txt.setText("Select Week");
             selectedSpan="WEEKLY";
             text_week_month.setText("Select");
        }
    }

    public class HttpUpload extends AsyncTask<Void, Integer, Void> {

        private Context context;
        private String imgPath;

        private HttpClient client;


        private long totalSize;

        public String url = AppUrls.BASE_URL + AppUrls.ADD_PROMOTE;

        public HttpUpload(Context context, String imgPath) {
            super();
            this.context = context;
            this.imgPath = imgPath;
        }

        @Override
        protected void onPreExecute() {
            //Set timeout parameters
            int timeout = 10000;
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeout);
            HttpConnectionParams.setSoTimeout(httpParameters, timeout);

            //We'll use the DefaultHttpClient
            client = new DefaultHttpClient(httpParameters);

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                File file = new File(imgPath);

                Log.d("USERDETAIL", user_id + "//" + imgPath + "//" + file);
                //Create the POST object
                HttpPost post = new HttpPost(url);

                //Create the multipart entity object and add a progress listener
                //this is a our extended class so we can know the bytes that have been transfered
                MultipartEntity entity = new MyMultipartEntity(new MyMultipartEntity.ProgressListener() {
                    @Override
                    public void transferred(long num) {
                        //Call the onProgressUpdate method with the percent completed
                        publishProgress((int) ((num / (float) totalSize) * 100));
                        Log.d("DEBUG", num + " - " + totalSize);
                    }
                });
                //Add the file to the content's body


                ContentBody cbFile = new FileBody(file, "image/*");
                entity.addPart("file", cbFile);
                entity.addPart("user_id", new StringBody(user_id));
                entity.addPart("user_type", new StringBody(user_type));
                entity.addPart("amount", new StringBody(amountS));
                entity.addPart("description", new StringBody(descS));
                entity.addPart("time_span", new StringBody(selectedSpan));
                entity.addPart("from_on", new StringBody(startdate));
                entity.addPart("to_on", new StringBody(enddate));
                post.addHeader("x-access-token", token);
                post.addHeader("x-device-platform", device_ID);
                post.addHeader("x-device-id", "ANDROID");

                //After adding everything we get the content's lenght
                totalSize = entity.getContentLength();

                //We add the entity to the post request
                post.setEntity(entity);

                //Execute post request
                HttpResponse response = client.execute(post);
                int statusCode = response.getStatusLine().getStatusCode();

                if (statusCode == HttpStatus.SC_OK) {
                    //If everything goes ok, we can get the response
                    String fullRes = EntityUtils.toString(response.getEntity());
                    Log.d("DEBUG", fullRes);
                    finish();

                } else {
                    Log.d("DEBUG", "HTTP Fail, Response Code: " + statusCode);
                }

            } catch (ClientProtocolException e) {
                // Any error related to the Http Protocol (e.g. malformed url)
                e.printStackTrace();
            } catch (IOException e) {
                // Any IO error (e.g. File not found)
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            //Set the pertange done in the progress dialog

        }

        @Override
        protected void onPostExecute(Void result) {
            Toast.makeText(PromotionsActivity.this, "Promotion sent Successfully", Toast.LENGTH_SHORT).show();

        }
    }

    private Boolean getValidation() {
        Boolean result = true;

        if (nameS.equals("") || nameS.length() < 3)
        {
            Toast.makeText(PromotionsActivity.this, "Please Enter Name..!", Toast.LENGTH_SHORT).show();
            //nameTil.setError("Enter Valid Name");
            result = false;
        }else if (time_span.equalsIgnoreCase("Select"))
        {
            // text_week_month.setError("Select Time Span");
            Toast.makeText(PromotionsActivity.this, "Select Time Span..!", Toast.LENGTH_SHORT).show();
            result = false;
        } else if (amountS.equals("")) {
            // amountTil.setError("Enter Valid Amount");
            Toast.makeText(PromotionsActivity.this, "Please Enter Amount..!", Toast.LENGTH_SHORT).show();
            result = false;
        }else if (descS.equals("") )
        {
            //descTil.setError("Enter Valid Description");
            Toast.makeText(PromotionsActivity.this, "Please Enter Description..!", Toast.LENGTH_SHORT).show();
            result = false;
        }

        else if (!nameS.matches("^[\\p{L} .'-]+$"))
        {
            //nameTil.setError("Special characters not allowed");
            Toast.makeText(PromotionsActivity.this, "Special characters not allowed..!", Toast.LENGTH_SHORT).show();
            result = false;
        }

        else
            {

            }


        return result;
    }
    private  String getDateOfMonth(Date date,int sart){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if(selectedSpan.equalsIgnoreCase("Monthly")){
            if(sart==0)
                cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
            else
                cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        }else {
            if(sart==0)
            cal.add(Calendar.DATE  , 0);
            else
             cal.add(Calendar.DATE  , 7);
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        //to convert Date to String, use format method of SimpleDateFormat class.
        String datestr = dateFormat.format(cal.getTime());
        return datestr;
    }
    private Date ConvertToDate(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return convertedDate;
    }
}
