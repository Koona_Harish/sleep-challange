package com.sleepchallenge.activities;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.ImagePermissions;
import com.sleepchallenge.utils.MyMultipartEntity;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.ProfileDB;
import com.sleepchallenge.utils.UserSessionManager;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;

    Typeface regularFont, boldFont, thinFont, specialFont;
    UserSessionManager userSessionManager;
    TextView change_photo_txt, userName, amount, send_money, add_money_wallet, rank_no, followers_no, following_no, sponsor_no, rank_text, followers_text, following_text, sponsor_text,male_txt,female_txt,other_txt,verify_email_text,
    refCod,refer_text,gender_text;
    EditText fname_edt, lname_edt, email_edt, phone_edt,paypal_id_edt, location_edt;
    String deviceId, user_type, user_id, token, fname, lname, email, phone, profile_pic, gender, wallet_amtJ,refer_code,transaction_mailJ;
    int is_email_verifiedJ;
    ImageView profile_pic_iv,shareRefer,back_img;
    Button save_btn;
    Bitmap bitmap;
    Uri outputFileUri;
    ArrayList<String> previousdata=new ArrayList<>();
    ArrayList<String> newdata=new ArrayList<>();
    boolean result;
    android.app.Dialog optionsdialog,addMoneydialog;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";
    String selectedImagePath = "",file;
    String send_gender = "Male";
    Double rang_amount=25.00;
    Double add_wal_amount=0.00;
    ProfileDB profileDB;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        profileDB=new ProfileDB(this);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("LOGDProL", deviceId + "///" + user_type + "///" + user_id);

        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));

        checkInternet = NetworkChecking.isConnected(this);

        profile_pic_iv = findViewById(R.id.profile_pic_iv);
        back_img = findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        shareRefer = findViewById(R.id.shareRefer);
        shareRefer.setOnClickListener(this);
        save_btn=findViewById(R.id.profile_save_btn);
        save_btn.setTypeface(regularFont);
        save_btn.setOnClickListener(this);


        change_photo_txt = findViewById(R.id.change_photo_txt);
        change_photo_txt.setTypeface(thinFont);
        change_photo_txt.setOnClickListener(this);

        verify_email_text = findViewById(R.id.verify_email_text);
        verify_email_text.setTypeface(regularFont);
        verify_email_text.setOnClickListener(this);
        send_money = findViewById(R.id.send_money);
        send_money.setTypeface(regularFont);
        send_money.setOnClickListener(this);
        add_money_wallet = findViewById(R.id.add_money_wallet);
        add_money_wallet.setTypeface(regularFont);
        add_money_wallet.setOnClickListener(this);

        male_txt = findViewById(R.id.male_txt);
        male_txt.setOnClickListener(this);
        female_txt = findViewById(R.id.female_txt);
        female_txt.setOnClickListener(this);
        other_txt = findViewById(R.id.other_txt);
        other_txt.setOnClickListener(this);
        userName = findViewById(R.id.userName);
        userName.setTypeface(boldFont);
        amount = findViewById(R.id.amount);
        amount.setTypeface(specialFont);

        rank_no = findViewById(R.id.rank_no);
        rank_no.setTypeface(regularFont);
        followers_no = findViewById(R.id.followers_no);
        followers_no.setTypeface(regularFont);
        following_no = findViewById(R.id.following_no);
        following_no.setTypeface(regularFont);
        sponsor_no = findViewById(R.id.sponsor_no);
        sponsor_no.setTypeface(regularFont);

        rank_text = findViewById(R.id.rank_text);
        rank_text.setTypeface(regularFont);
        followers_text = findViewById(R.id.followers_text);
        followers_text.setTypeface(regularFont);
        following_text = findViewById(R.id.following_text);
        following_text.setTypeface(regularFont);
        sponsor_text = findViewById(R.id.sponsor_text);
        sponsor_text.setTypeface(regularFont);

        fname_edt = findViewById(R.id.fname_edt);
        lname_edt = findViewById(R.id.lname_edt);
        email_edt = findViewById(R.id.email_edt);
        phone_edt = findViewById(R.id.phone_edt);
        paypal_id_edt = findViewById(R.id.paypal_id_edt);

        refCod = findViewById(R.id.refCod);
        refCod.setTypeface(regularFont);
        refer_text= findViewById(R.id.refer_text);
        refer_text.setTypeface(regularFont);
        gender_text= findViewById(R.id.gender_text);
        gender_text.setTypeface(regularFont);


        fname_edt.setTypeface(specialFont);
        lname_edt.setTypeface(specialFont);
        email_edt.setTypeface(specialFont);
        phone_edt.setTypeface(specialFont);
        paypal_id_edt.setTypeface(specialFont);

        getProfile();
    }

    private void getProfile() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Dialog.showProgressBar(MyProfileActivity.this, "Get Profile...");
             profileDB.deleteAllRows();
            Log.d("GETproFFFILEYURL", AppUrls.BASE_URL + "user/profile?user_id=" + user_id + "&user_type=" + user_type);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "user/profile?user_id=" + user_id + "&user_type=" + user_type,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            Dialog.hideProgressBar();
                            Log.d("GETproFiRRESP", response);
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                     ContentValues values=new ContentValues();
                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    JSONObject itemArray = jArr.getJSONObject(0);
                                     fname = itemArray.getString("first_name");
                                    values.put(ProfileDB.FIRST_NAME,fname);

                                    lname = itemArray.getString("last_name");
                                    values.put(ProfileDB.LAST_NAME,lname);
                                     wallet_amtJ = itemArray.getString("wallet_amt");
                                    values.put(ProfileDB.WALLET_AMOUNT,wallet_amtJ);
                                    gender = itemArray.getString("gender");
                                    values.put(ProfileDB.Gender,gender);
                                   transaction_mailJ = itemArray.getString("paypal_registered_email");
                                    values.put(ProfileDB.PAYPAL_REGISTERED_EMAIL,transaction_mailJ);
                                     profile_pic = AppUrls.BASE_IMAGE_URL + itemArray.getString("profile_pic");
                                    values.put(ProfileDB.PROFILE_PIC,profile_pic);
                                    //    selectedImagePath = profile_pic;

                                    Log.d("fhgzfbhz", profile_pic);

                                    email = itemArray.getString("email");
                                    values.put(ProfileDB.EMAIL,email);

                                    is_email_verifiedJ = itemArray.getInt("is_email_verified");
                                    values.put(ProfileDB.IS_EMAIL_VERIFIED,is_email_verifiedJ);

                                    phone = itemArray.getString("mobile");
                                    values.put(ProfileDB.MOBILE,phone);

                                    refer_code = itemArray.getString("refer_code");

                                    values.put(ProfileDB.REFER_CODE,refer_code);
                                    String overall_rankJ = itemArray.getString("overall_rank");
                                    values.put(ProfileDB.OVERALL_RANK,overall_rankJ);

                                    String  followers_cntJ = itemArray.getString("followers_cnt");
                                    values.put(ProfileDB.FOLLOWERS_COUNT,followers_cntJ);
                                    followers_no.setText(followers_cntJ);
                                    String   following_users_cntJ = itemArray.getString("following_users_cnt");
                                    values.put(ProfileDB.FOLLOWING_USERS_COUNT,following_users_cntJ);

                                    String sponsor_by_cntJ = itemArray.getString("sponsor_by_cnt");
                                    values.put(ProfileDB.SPONSOR_BY_COUNT,sponsor_by_cntJ);

                                   profileDB.addUserData(values);
                                   getProfileFromDB();

                                }
                                if (successResponceCode.equals("10200")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(MyProfileActivity.this, "Invalid input", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(MyProfileActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Dialog.hideProgressBar();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Dialog.hideProgressBar();

                            error.getMessage();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }


            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Dialog.hideProgressBar();
            Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
            getProfileFromDB();
        }
    }

    private void getProfileFromDB(){
        String firname=profileDB.getFirstName();
        if(firname!=null && firname.length()>0){
            previousdata.clear();
            String fname=profileDB.getFirstName();
            String lname=profileDB.getLirstName();
            String gender=profileDB.getGender();
            String email=profileDB.getEmail();
            String mobileno=profileDB.getMobileNo();
            String overallrank=profileDB.getOverallRank();
            String followerscount=profileDB.getFollowersCount();
            String followingusercount=profileDB.getFollowingUsersCount();
            String walletAmount=profileDB.getWalletAmount();
            int isemailverified=profileDB.getisEmailVerifired();
            String sponsorcount=profileDB.getSponsorByCount();
            String refercode=profileDB.getReferCode();
            String paypalregisteredmail=profileDB.getPaypalRegisteredEmail();
            String profilepic=profileDB.getProfilePic();
            fname_edt.setText(fname);
            email_edt.setText(email);
            lname_edt.setText(lname);
            userName.setText(fname + " " + lname);
            rank_no.setText(overallrank);
            paypal_id_edt.setText(paypalregisteredmail);
            following_no.setText(followingusercount);
            refCod.setText(refercode);
            amount.setText("" + walletAmount + " " + "USD");
            followers_no.setText(followerscount);
            sponsor_no.setText(sponsorcount);
            phone_edt.setText(mobileno);
            phone_edt.setEnabled(false);
            previousdata.add(fname);
            previousdata.add(lname);
            previousdata.add(email);
            previousdata.add(gender);
            if (isemailverified == 1)
            {
                Drawable img = MyProfileActivity.this.getResources().getDrawable( R.drawable.ic_verified_user_black_16dp );
                email_edt.setCompoundDrawablesWithIntrinsicBounds(null , null, img, null);

            } else if (isemailverified == 0)
            {
                verify_email_text.setVisibility(View.VISIBLE);
            }

            if(gender.equals("Male"))
            {
                other_txt.setTextColor(Color.parseColor("#B0B0B0"));
                other_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
                female_txt.setTextColor(Color.parseColor("#B0B0B0"));
                female_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
                male_txt.setTextColor(Color.parseColor("#00ABF7"));
                male_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
            }
            else if(gender.equals("Female"))
            {
                male_txt.setTextColor(Color.parseColor("#B0B0B0"));
                male_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
                other_txt.setTextColor(Color.parseColor("#B0B0B0"));
                other_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
                female_txt.setTextColor(Color.parseColor("#FF1493"));
                female_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
            }
            else
            {
                male_txt.setTextColor(Color.parseColor("#B0B0B0"));
                male_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
                female_txt.setTextColor(Color.parseColor("#B0B0B0"));
                female_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
                other_txt.setTextColor(Color.parseColor("#FFFF00"));
                other_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
            }
            Double valu=Double.parseDouble(walletAmount);
            if(valu >= rang_amount )
            {
                send_money.setVisibility(View.VISIBLE);
            }
            else
            {
                send_money.setVisibility(View.GONE);
            }

            if(add_wal_amount== valu)
            {
                add_money_wallet.setVisibility(View.GONE);
            }
            else
            {
                add_money_wallet.setVisibility(View.VISIBLE);
            }

            Picasso.with(MyProfileActivity.this)
                    .load(profilepic)
                    .placeholder(R.drawable.profile_img) // img_circle_placeholder
                    .into(profile_pic_iv);

        }
    }


    @Override
    public void onClick(View view)
    {
        if (view == back_img)
        {
              finish();
        }
        if (view == change_photo_txt)
        {
           selectionOption();
        }
        if(view==send_money)
        {
            addMoneydialog=new android.app.Dialog(MyProfileActivity.this);
            addMoneydialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            addMoneydialog.setContentView(R.layout.custom_dialog_send_money);
            TextView titleDialog = addMoneydialog.findViewById(R.id.titleDialog);
            final EditText edt_send_amount = addMoneydialog.findViewById(R.id.edt_send_amount);
            Button addMoneyButton = addMoneydialog.findViewById(R.id.sendMoneyButton);
            titleDialog.setText("Send Money To Paypal");
            addMoneyButton.setText("Send Money");
            titleDialog.setTypeface(regularFont);
            addMoneyButton.setTypeface(regularFont);
            addMoneyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    String send_amount=edt_send_amount.getText().toString();
                    if(send_amount == null || send_amount.equals("") || send_amount.length() < 0)
                    {
                        edt_send_amount.setError("Please Enter Amount..!");
                    }
                    else
                    {
                       // sendWalletAmount(send_amount);
                        if(Double.parseDouble(send_amount) >= Double.parseDouble(wallet_amtJ) && Double.parseDouble(send_amount) <= rang_amount )
                        {
                            Toast.makeText(MyProfileActivity.this,"Amount should be Less than Wallet Amount...! ",Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            sendWalletAmount(send_amount);
                        }
                    }
                }
            });
            addMoneydialog.show();
        }
        if(view==add_money_wallet)
        {

             addMoneydialog=new android.app.Dialog(MyProfileActivity.this);
             addMoneydialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
             addMoneydialog.setContentView(R.layout.custom_dialog_send_money);
            TextView titleDialog = addMoneydialog.findViewById(R.id.titleDialog);
            final EditText edt_send_amount = addMoneydialog.findViewById(R.id.edt_send_amount);
            Button addMoneyButton = addMoneydialog.findViewById(R.id.sendMoneyButton);
            titleDialog.setText("Add Money To Wallet");
            addMoneyButton.setText("Add Money");
            titleDialog.setTypeface(regularFont);
            addMoneyButton.setTypeface(regularFont);
            addMoneyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    String send_amount=edt_send_amount.getText().toString();

                    if(send_amount == null || send_amount.equals("") || send_amount.length() < 0)
                    {
                        edt_send_amount.setError("Please Enter Amount..!");
                    }
                    else
                    {
                        Intent intent = new Intent(MyProfileActivity.this, PayPalActivity.class);
                        intent.putExtra("activity", "MYPROFILE_ADDWALLET");
                        intent.putExtra("myprofile_addWallet", send_amount);
                        intent.putExtra("user_id", user_id);
                        intent.putExtra("user_type", user_type);
                        Log.d("WALLETAMT",  send_amount);
                        startActivity(intent);
                    }
                }
            });
            addMoneydialog.show();
        }
        if(view==shareRefer)
        {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Sleep Challenge");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, "I'm inviting you to join SleepChallenge App. Here's my Refer Code " +refer_code+". just enter it while Register"+ "\n" +AppUrls.SHARE_APP_URL);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));

        }
        if(view==verify_email_text)
        {
            if (is_email_verifiedJ == 1) {
                Toast.makeText(MyProfileActivity.this, "Email Already Verified", Toast.LENGTH_LONG).show();
            } else {

                   emailVerify();
            }
        }
        if(view==save_btn)
        {
            newdata.clear();
            String fnamestr=fname_edt.getText().toString();
            String lnamestr=lname_edt.getText().toString();
            String emailstr=email_edt.getText().toString();
            String paypal_id=paypal_id_edt.getText().toString();
            newdata.add(fnamestr);
            newdata.add(lnamestr);
            newdata.add(emailstr);
            newdata.add(send_gender);
            if(fnamestr.length()==0){
                Toast.makeText(this,"Enter first name",Toast.LENGTH_SHORT).show();
            }else if(lnamestr.length()==0){
                Toast.makeText(this,"Enter last name",Toast.LENGTH_SHORT).show();
            }
            else if(emailstr.length()==0){
                Toast.makeText(this,"Enter email",Toast.LENGTH_SHORT).show();
            }
            else if(paypal_id.length()==0){
                Toast.makeText(this,"Enter Paypal Id",Toast.LENGTH_SHORT).show();
            }
            else{
                Log.v("Data","///"+previousdata);
                Log.v("Data1","///"+newdata);
                if(previousdata.equals(newdata)){
                    Toast.makeText(this,"Update your details before save",Toast.LENGTH_SHORT).show();
                }else {
                    updateProfile(fnamestr,lnamestr,paypal_id);
                }

            }

        }
        if(view==male_txt)
        {
            other_txt.setTextColor(Color.parseColor("#B0B0B0"));
            other_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
            female_txt.setTextColor(Color.parseColor("#B0B0B0"));
            female_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
            male_txt.setTextColor(Color.parseColor("#00ABF7"));
            male_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
            send_gender = "Male";
        }

        if(view==female_txt)
        {
            male_txt.setTextColor(Color.parseColor("#B0B0B0"));
            male_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
            other_txt.setTextColor(Color.parseColor("#B0B0B0"));
            other_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
            female_txt.setTextColor(Color.parseColor("#FF1493"));
            female_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
            send_gender = "Female";
        }
        if(view==other_txt)
        {
            male_txt.setTextColor(Color.parseColor("#B0B0B0"));
            male_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
            female_txt.setTextColor(Color.parseColor("#B0B0B0"));
            female_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
            other_txt.setTextColor(Color.parseColor("#FFFF00"));
            other_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
            send_gender = "Other";

        }

        if(view.getId()==R.id.take_camera_pic)
        {
            if (result)
                if (ContextCompat.checkSelfPermission(MyProfileActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    if (getFromPref(MyProfileActivity.this, ALLOW_KEY)) {
                        showSettingsAlert();
                    } else if (ContextCompat.checkSelfPermission(MyProfileActivity.this,
                            android.Manifest.permission.CAMERA)

                            != PackageManager.PERMISSION_GRANTED) {

                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(MyProfileActivity.this,
                                android.Manifest.permission.CAMERA)) {
                            showSettingsAlert();
                        } else {
                            // No explanation needed, we can request the permission.
                            ActivityCompat.requestPermissions(MyProfileActivity.this,
                                    new String[]{android.Manifest.permission.CAMERA},
                                    0);
                        }
                    }
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    ContentValues values = new ContentValues(1);
                    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                    outputFileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    startActivityForResult(intent, 0);
                }
            optionsdialog.cancel();
        }
        if(view.getId()==R.id.take_gallery_pic)
        {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, 1);
            optionsdialog.cancel();
        }
        if(view.getId()==R.id.cancel)
        {
            optionsdialog.cancel();
        }
    }

    private void updateProfile(final String fnamestr, final String lnamestr, final String paypal_id)
    {

        checkInternet = NetworkChecking.isConnected(this);

        if(validate())
        {
            if (checkInternet)
            {
                Log.d("UPDATE_PRO_URL", AppUrls.BASE_URL + AppUrls.UPDATE_PROFILE);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.UPDATE_PROFILE,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                 Log.d("UPDATE_PRO_RESP", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String successResponceCode = jsonObject.getString("response_code");
                                    if (successResponceCode.equals("10100")) {

                                        Toast.makeText(MyProfileActivity.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(MyProfileActivity.this, MyProfileActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                    if (successResponceCode.equals("10200")) {

                                        Toast.makeText(MyProfileActivity.this, "Invalid input", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();

                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {


                                error.getMessage();
                            }
                        }) {

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("x-access-token", token);
                        headers.put("x-device-id", deviceId);
                        headers.put("x-device-platform", "ANDROID");
                        Log.d("UPDATEproHEADER", "HEADDER " + headers.toString());
                        return headers;
                    }

                    @Override
                    protected Map<String, String> getParams()  {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id", user_id);
                        params.put("first_name", fnamestr);
                        params.put("last_name", lnamestr);
                        params.put("gender", send_gender);
                       params.put("paypal_registered_email", paypal_id);
                        Log.d("UPDATEproPARAM", params.toString());
                        return params;
                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);
            } else {

                 Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean validate() {
        boolean result = true;

        String EMAIL_REGEX ="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$";


        String email = email_edt.getText().toString().trim();
        if (!email.matches(EMAIL_REGEX)) {
           Toast.makeText(this,"Invalid email",Toast.LENGTH_SHORT).show();
            result = false;
        } else {

        }


        return result;
    }
    private void selectionOption() {
        optionsdialog = new android.app.Dialog(MyProfileActivity.this);
        optionsdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        optionsdialog.setContentView(R.layout.camera_dialog);
        TextView take_camera_pic = optionsdialog.findViewById(R.id.take_camera_pic);
        take_camera_pic.setTypeface(regularFont);
        take_camera_pic.setOnClickListener(this);
        TextView take_gallery_pic = optionsdialog.findViewById(R.id.take_gallery_pic);
        take_gallery_pic.setTypeface(regularFont);
        take_gallery_pic.setOnClickListener(this);
        TextView cancel = optionsdialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(this);
        cancel.setTypeface(regularFont);
        result = ImagePermissions.checkPermission(MyProfileActivity.this);
        optionsdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        optionsdialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        optionsdialog.show();

    }
    public static Boolean getFromPref(Context context, String key) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        return (myPrefs.getBoolean(key, false));
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case 0:
                if (resultCode == RESULT_OK) {
                    Log.d("PPP", outputFileUri.toString());

                    if (outputFileUri != null) {
                        bitmap = decodeSampledBitmapFromUri(outputFileUri, profile_pic_iv.getWidth(), profile_pic_iv.getHeight());
                        if (bitmap == null) {
                            Toast.makeText(getApplicationContext(), "the image data could not be decoded" + outputFileUri.getPath(), Toast.LENGTH_LONG).show();
                        } else {
                            selectedImagePath = getRealPathFromURI(MyProfileActivity.this, outputFileUri);// outputFileUri.getPath().
                            Log.d("CAMERASELECTPATH", selectedImagePath);
                            profile_pic_iv.setImageBitmap(bitmap);
                            updateUserPic();

                        }
                    }
                }
                break;
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri targetUri = data.getData();
                    Log.d("TGGGGG", targetUri.toString());
                    Bitmap bitmap;
                    bitmap = decodeSampledBitmapFromUri(targetUri, profile_pic_iv.getWidth(), profile_pic_iv.getHeight());
                    if (bitmap == null) {
                        Toast.makeText(getApplicationContext(), "the image data could not be decoded" + targetUri.getPath(), Toast.LENGTH_LONG).show();
                    } else {
                        selectedImagePath = getPath(targetUri);// targetUri.getPath();
                        Log.d("GALLRYSSSSSSSSS", selectedImagePath);
                        profile_pic_iv.setImageBitmap(bitmap);
                        updateUserPic();

                    }
                }
                break;
            default:
                break;
        }
    }


    private void updateUserPic() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
             StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            new MyProfileActivity.HttpUpload(this, selectedImagePath).execute();
        } else {
            Dialog.hideProgressBar();

        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            if ("content".equals(contentUri.getScheme())) {
                String[] proj = {MediaStore.Images.Media.DATA};
                cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else {
                return contentUri.getPath();
            }
        } finally {

            if (cursor != null) {
                cursor.close();
            }
        }
    }


    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null,
                null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            return cursor.getString(columnIndex);
        }
        return uri.getPath();
    }

    public Bitmap decodeSampledBitmapFromUri(Uri uri, int reqWidth, int reqHeight) {
        Bitmap bm = null;
        String filePath = getPath(uri);
        // Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 516.0f;    //816 and 612
        float maxWidth = 412.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            bm = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(bm);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            bm = Bitmap.createBitmap(bm, 0, 0,
                    bm.getWidth(), bm.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = filePath;
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            bm.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return bm;

    }
    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }
    private void showSettingsAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(MyProfileActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startInstalledAppDetailsActivity(MyProfileActivity.this);
                    }
                });
        alertDialog.show();
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    public class HttpUpload extends AsyncTask<Void, Integer, Void> {

        private Context context;
        private String imgPath;
        private HttpClient client;

        private long totalSize;
        public String url = AppUrls.BASE_URL + AppUrls.UPDATE_PROFILE_PIC;

        public HttpUpload(Context context, String imgPath) {
            super();
            this.context = context;
            this.imgPath = imgPath;
        }

        @Override
        protected void onPreExecute() {
            //Set timeout parameters
            int timeout = 10000;
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeout);
            HttpConnectionParams.setSoTimeout(httpParameters, timeout);
            //We'll use the DefaultHttpClient
            client = new DefaultHttpClient(httpParameters);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // File file = new File(imgPath);
                Bitmap bmp = BitmapFactory.decodeFile(imgPath);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 15, bos);
                InputStream in = new ByteArrayInputStream(bos.toByteArray());
                Log.d("USERDETAIL", user_id + "//" + imgPath + "//" + file);
                //Create the POST object
                HttpPost post = new HttpPost(url);
                //Create the multipart entity object and add a progress listener
                //this is a our extended class so we can know the bytes that have been transfered
                MultipartEntity entity = new MyMultipartEntity(new MyMultipartEntity.ProgressListener() {
                    @Override
                    public void transferred(long num) {
                        //Call the onProgressUpdate method with the percent completed
                        publishProgress((int) ((num / (float) totalSize) * 100));
                        Log.d("DEBUG", num + " - " + totalSize);
                    }
                });
                //Add the file to the content's body
                ContentBody cbFile = new InputStreamBody(in, "image/jpeg", "jpeg");
                entity.addPart("file", cbFile);
                entity.addPart("user_id", new StringBody(user_id));
                //After adding everything we get the content's lenght
                totalSize = entity.getContentLength();
                //We add the entity to the post request
                post.setEntity(entity);
                //Execute post request
                HttpResponse response = client.execute(post);
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == HttpStatus.SC_OK) {
                    //If everything goes ok, we can get the response
                    String fullRes = EntityUtils.toString(response.getEntity());
                    Log.d("DEBUG", fullRes);
                    finish();
                } else {
                    Log.d("DEBUG", "HTTP Fail, Response Code: " + statusCode);
                }
            } catch (ClientProtocolException e) {
                // Any error related to the Http Protocol (e.g. malformed url)
                e.printStackTrace();
            } catch (IOException e) {
                // Any IO error (e.g. File not found)
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            //Set the pertange done in the progress dialog
        }

        @Override
        protected void onPostExecute(Void result) {
            //Dismiss progress dialog
            Toast.makeText(MyProfileActivity.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(MyProfileActivity.this, MyProfileActivity.class);
            startActivity(intent);
        }
    }

    // //SEND MONEY TO PAYPAL ACCOUNT
    private void sendWalletAmount(final String amount)
    {
        if (checkInternet)
        {
            addMoneydialog.cancel();
            String send_money_url = AppUrls.BASE_URL + AppUrls.TRANSFER_AMOUNT_TO_PAYPAL_ACC;

            Log.d("SENDMONEYLURL", send_money_url);
            StringRequest strFinalDeact = new StringRequest(Request.Method.POST, send_money_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("RESPSENDMONEY", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {

                            Toast.makeText(MyProfileActivity.this, "Payment transfered successfully..", Toast.LENGTH_LONG).show();

                            Intent redirect = new Intent(MyProfileActivity.this, MyProfileActivity.class);
                            startActivity(redirect);
                            finish();
                        }
                        if (response_code.equals("10200")) {

                            Toast.makeText(MyProfileActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10300")) {

                            Toast.makeText(MyProfileActivity.this, "User not exist.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10400")) {

                            Toast.makeText(MyProfileActivity.this, "Paypal Registered Email not Found..!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10500")) {

                            Toast.makeText(MyProfileActivity.this, "Insufficient amount.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10600")) {

                            Toast.makeText(MyProfileActivity.this, "Payment transfer failed.!", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            }) {

                @Override
                protected Map<String, String> getParams()  {
                    Map<String, String> param = new HashMap<>();
                    param.put("user_id", user_id);
                    param.put("user_type", user_type);
                    param.put("amount", amount);

                    Log.d("finalDEACTPARAM", param.toString());
                    return param;
                }

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);
            requestQueue.add(strFinalDeact);
        } else {
            Toast.makeText(MyProfileActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }



    private void emailVerify() {
        checkInternet = NetworkChecking.isConnected(MyProfileActivity.this);
        if (checkInternet) {
            String emil_verify = AppUrls.BASE_URL + AppUrls.SEND_VERIFICATION_EMAIL;
            Log.d("EMVERYRPESP", emil_verify);
            StringRequest reqEmailVerify = new StringRequest(Request.Method.POST, emil_verify, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("EMVERYRPESPRPESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");
                        if (response_code.equals("10100"))
                        {

                            Toast.makeText(MyProfileActivity.this, "Verification mail has been sent, Please check your mail", Toast.LENGTH_LONG).show();
                            //   verf_email.setImageResource(R.drawable.ic_verified);
                        }
                        if (response_code.equals("10200")) {

                            Toast.makeText(MyProfileActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10300")) {

                            Toast.makeText(MyProfileActivity.this, "Email already Verified.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.getMessage();
                }
            }) {
                @Override
                protected Map<String, String> getParams()  {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    Log.d("sendemailverfyParam:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MyProfileActivity.this);
            requestQueue.add(reqEmailVerify);
        } else {
            // Toast.makeText(getApplicationContext(), "No Internet Connection Please Check Your Internet Connection..!!", Toast.LENGTH_SHORT).show();
        }
    }


}
