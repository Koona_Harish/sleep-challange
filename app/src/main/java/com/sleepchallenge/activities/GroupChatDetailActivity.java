package com.sleepchallenge.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.GroupDetailAdapter;
import com.sleepchallenge.models.MessagesDetailModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GroupChatDetailActivity extends AppCompatActivity implements View.OnClickListener
{

    ImageView back_img, group_profile_pic, grp_send_mesage;
    EditText grp_mesagetext_edt;
    TextView groupName_text;
    Typeface regularFont, boldfont;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String user_id, user_type, token, device_id, g_fromname, group_id, g_fromtype, g_profile_pic, group_type, group_conversation_type;
    RecyclerView recycler_group_chatconversn;
    GroupDetailAdapter groupDetailAdapter;
    LinearLayoutManager layoutManager;
    ArrayList<MessagesDetailModel> groupDetailModalList;
    String FROM_TYPE;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_chat_detail);

        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));


        Bundle bundle = getIntent().getExtras();
        group_id = bundle.getString("group_id");
        g_fromname = bundle.getString("group_name");
        g_profile_pic = bundle.getString("profile_pic");
        group_type = bundle.getString("GROUP_TYPE");
        group_conversation_type = bundle.getString("GROUP_CONVERSATION_TYPE");
        FROM_TYPE = bundle.getString("FROM_TYPE");
        groupName_text = findViewById(R.id.groupName_text);
        groupName_text.setText(g_fromname);
        groupName_text.setTypeface(regularFont);
        Log.d("M_DETAIL", group_id + "//" + g_fromname + "//" + group_type + "///" + group_type);
        userSessionManager = new UserSessionManager(GroupChatDetailActivity.this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("M_DETAILL", user_id + "//" + user_type + "//" + token + "//" + device_id);

        back_img = (ImageView) findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        group_profile_pic =  findViewById(R.id.group_profile_pic);
        grp_send_mesage = findViewById(R.id.grp_send_mesage);
        grp_send_mesage.setOnClickListener(this);

        grp_mesagetext_edt = findViewById(R.id.grp_mesagetext_edt);
         Picasso.with(GroupChatDetailActivity.this)
                .load(g_profile_pic)
                .placeholder(R.drawable.group_dummy)
                .into(group_profile_pic);
        recycler_group_chatconversn = findViewById(R.id.recycler_group_chatconversn);

        getGroupChatConversation();

    }

    private void getGroupChatConversation() {
        checkInternet = NetworkChecking.isConnected(GroupChatDetailActivity.this);
        if (checkInternet) {
            String urlgroup = AppUrls.BASE_URL + AppUrls.GROUP_MESSAGES_CONVERSATION + "?group_id=" + group_id + "&user_id=" + user_id + "&user_type=" + user_type + "&conversation_type=" + group_conversation_type;
            Log.d("GROUPCONVERYURL", urlgroup);
            StringRequest reqgroup = new StringRequest(Request.Method.GET, urlgroup, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("GROUPCONVRESP:", response);
                    try {
                         groupDetailModalList=new ArrayList<>();
                        JSONObject jobj = new JSONObject(response);
                        String response_code = jobj.getString("response_code");

                        if (response_code.equals("10100")) {
                            JSONArray jsonArray = jobj.getJSONArray("data");
                           for (int i = 0; i < jsonArray.length(); i++)
                            {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                MessagesDetailModel groupChatDetail = new MessagesDetailModel(jsonObject1);
                                groupDetailModalList.add(groupChatDetail);
                            }
                            groupDetailAdapter = new GroupDetailAdapter(groupDetailModalList, GroupChatDetailActivity.this, R.layout.row_group_chat_detail,FROM_TYPE);
                            layoutManager = new LinearLayoutManager(GroupChatDetailActivity.this);
                            recycler_group_chatconversn.setNestedScrollingEnabled(false);
                            recycler_group_chatconversn.setLayoutManager(layoutManager);
                            recycler_group_chatconversn.setItemAnimator(null);
                            layoutManager.setStackFromEnd(true);
                            recycler_group_chatconversn.setAdapter(groupDetailAdapter);
                        }

                        if (response_code.equals("10200")) {

                            Toast.makeText(GroupChatDetailActivity.this, "Invlid Input..!", Toast.LENGTH_SHORT).show();
                        }
                        if (response_code.equals("10300")) {
                            Toast.makeText(GroupChatDetailActivity.this, "No Data Found..!", Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException json) {
                        json.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                  error.getMessage();
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(GroupChatDetailActivity.this);
            requestQueue.add(reqgroup);
        } else
        {
            // groupChatDetailList();
            Toast.makeText(GroupChatDetailActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onClick(View view)
    {
        if(view==back_img)
        {
            finish();
        }
        if(view==grp_send_mesage)
        {
            checkInternet = NetworkChecking.isConnected(this);
            if (validate()) {
                if (checkInternet) {
                    Log.d("GRPSENDURL:", AppUrls.BASE_URL + AppUrls.GROUP_SEND_MESSAGES);
                    final String msg_text = grp_mesagetext_edt.getText().toString().trim();
                    StringRequest forgetReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GROUP_SEND_MESSAGES, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("MESRESPGRP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    Toast.makeText(getApplicationContext(), "Message sent successfully..", Toast.LENGTH_SHORT).show();

                                    getGroupChatConversation();
                                    grp_mesagetext_edt.setText("");
                                }
                                if (successResponceCode.equals("10200")) {

                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                         error.getMessage();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("group_id", group_id);
                            params.put("user_id", user_id);
                            params.put("user_type", user_type);
                            params.put("conversation_type", group_conversation_type);
                            params.put("msg", msg_text);
                            Log.d("GRPSENPARAM:", params.toString());
                            return params;

                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("x-access-token", token);
                            headers.put("x-device-id", device_id);
                            headers.put("x-device-platform", "ANDROID");
                            Log.d("MSGHEAED", headers.toString());
                            return headers;
                        }
                    };
                    forgetReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(GroupChatDetailActivity.this);
                    requestQueue.add(forgetReq);

                } else {
                    Toast.makeText(GroupChatDetailActivity.this, "No Inmtenet Connection..!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private boolean validate() {
        boolean result = true;

        String name = grp_mesagetext_edt.getText().toString().trim();
        if ((name == null || name.equals("") || name.length() < 1)) {
            grp_mesagetext_edt.setError("Minimum 3 characters required");
            result = false;
        }
        return result;
    }

}
