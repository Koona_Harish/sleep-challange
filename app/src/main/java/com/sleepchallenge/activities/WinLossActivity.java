package com.sleepchallenge.activities;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;
import com.sleepchallenge.R;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.DividerItemDecorator;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;
import com.sleepchallenge.utils.WinLossDB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WinLossActivity extends AppCompatActivity implements View.OnClickListener {


    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String user_id, user_type, token, device_ID;
    ImageView back_img, dot_red, dot_green;
    TextView title_text, green_text, red_text;
    Typeface regularFont;
    RecyclerView win_loss_recyclerView;
    WinLossAdapter winlossAdapter;
    ArrayList<WinLossModel> winlossList=new ArrayList<>();
    RecyclerView.LayoutManager layoutManager;
    String type = "";
    WinLossDB winLossDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_win_loss);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));

        type = "ALL";
        winLossDB=new WinLossDB(this);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_ID = userDetails.get(UserSessionManager.DEVICE_ID);
        checkInternet = NetworkChecking.isConnected(this);

        win_loss_recyclerView = findViewById(R.id.win_loss_recyclerView);


        dot_red = findViewById(R.id.dot_red);
        dot_red.setBackgroundResource(R.color.losscolor);
        dot_green = findViewById(R.id.dot_green);
        dot_green.setBackgroundResource(R.color.wincolor);
        back_img = findViewById(R.id.back_img);
        back_img.setOnClickListener(this);

        green_text = findViewById(R.id.green_text);
        red_text = findViewById(R.id.red_text);
        title_text = findViewById(R.id.title_text);
        title_text.setTypeface(regularFont);
        green_text.setTypeface(regularFont);
        red_text.setTypeface(regularFont);

        getWinLossData();
    }

    private void getWinLossData() {
        if (checkInternet) {
            winLossDB.clearDatabase();
            Dialog.showProgressBar(WinLossActivity.this, "Loading");
            String winlossUrl = AppUrls.BASE_URL + AppUrls.WIN_LOSS_CHALLENGES + "?user_id=" + user_id + "&user_type=" + user_type + AppUrls.TYPE + type;
            Log.d("WINLOSSURL", winlossUrl);
            StringRequest strWLReq = new StringRequest(Request.Method.GET, winlossUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Dialog.hideProgressBar();
                    Log.d("ALLMEMBERESP", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100")) {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            ContentValues values=new ContentValues();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                String body = Base64.encodeToString(jdataobj.toString().getBytes("UTF-8"), Base64.NO_WRAP);
                                values.put(WinLossDB.CHALLENGE_DATA,body);
                                winLossDB.addData(values);
                               getWinLossDataFRomDB();
                            }


                        }

                        if (successResponceCode.equals("10200")) {
                            Toast.makeText(WinLossActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                        }
                        if (successResponceCode.equals("10300")) {
                            Toast.makeText(WinLossActivity.this, "No Data Found..!", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.getMessage();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strWLReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strWLReq);

        } else {
            getWinLossDataFRomDB();
            Toast.makeText(WinLossActivity.this, "'No Internet Connection..!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == back_img) {
            finish();
        }
    }


    //ADAPTER CLASS
    public class WinLossAdapter extends RecyclerView.Adapter<WinLossActivity.WinLossHolder> {

        public ArrayList<WinLossModel> winlossModelsList;
        WinLossActivity context;
        LayoutInflater li;
        int resource;
        Typeface thinFont, regularFont, boldFont, specialFont;

        public WinLossAdapter(ArrayList<WinLossModel> winlossModelsList, WinLossActivity context, int resource) {
            this.winlossModelsList = winlossModelsList;
            this.context = context;
            this.resource = resource;
            li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regularFont));
            boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.boldFont));
            thinFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.thinFont));
            specialFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.specialFont));

        }

        @Override
        public WinLossActivity.WinLossHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = li.inflate(resource, parent, false);
            WinLossActivity.WinLossHolder slh = new WinLossActivity.WinLossHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(final WinLossActivity.WinLossHolder holder, final int position) {

            String str = winlossModelsList.get(position).opponent_name;
            String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
            holder.opp_name.setText(Html.fromHtml(converted_string));

            holder.date.setText(winlossModelsList.get(position).completed_on_txt);

            String winning_status = winlossModelsList.get(position).winning_status;


            if (winning_status.equals("won")) {
                holder.win_status_text.setText("Won");
               // holder.cash_text.setText("Cash Won");
                holder.amount_text.setText(Html.fromHtml("&#36;" + winlossModelsList.get(position).winning_amount));
                holder.amount_text.setTextColor(Color.parseColor("#38ac59"));
            } else {
                holder.win_status_text.setText("Lost");
           //     holder.cash_text.setText("Cash Lost");
                holder.amount_text.setText(Html.fromHtml("&#36;" + winlossModelsList.get(position).amount));
                holder.amount_text.setTextColor(Color.parseColor("#d83515"));

            }

            holder.opp_name_title_text.setTypeface(regularFont);
            holder.opp_name.setTypeface(regularFont);
            holder.win_status_title.setTypeface(regularFont);
            holder.win_status_text.setTypeface(regularFont);
            holder.date_title.setTypeface(regularFont);
            holder.date.setTypeface(regularFont);
            holder.amount_text.setTypeface(boldFont);
         //   holder.cash_text.setTypeface(regularFont);


            holder.setItemClickListener(new AllItemClickListeners() {
                @Override
                public void onItemClick(View v, int pos) {

                }
            });

        }

        @Override
        public int getItemCount() {
            return this.winlossModelsList.size();
        }
    }

    //HOLDER CLASS

    public class WinLossHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView opp_name_title_text, opp_name, win_status_title, win_status_text, date_title, date, amount_text, cash_text;
        public CardView card;


        AllItemClickListeners winLossItemClickListener;

        public WinLossHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            // card = (CardView) itemView.findViewById(R.id.card);
            opp_name_title_text = itemView.findViewById(R.id.opp_name_title_text);
            opp_name = itemView.findViewById(R.id.opp_name);
            win_status_title = itemView.findViewById(R.id.win_status_title);
            win_status_text = itemView.findViewById(R.id.win_status_text);
            date_title = itemView.findViewById(R.id.date_title);
            date = itemView.findViewById(R.id.date);
            amount_text = itemView.findViewById(R.id.amount_text);
          //  cash_text = itemView.findViewById(R.id.cash_text);

        }

        @Override
        public void onClick(View view) {

            this.winLossItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(AllItemClickListeners ic) {
            this.winLossItemClickListener = ic;
        }


    }

   //MODEL CLASS
    public class WinLossModel {

        public String challenge_id, opponent_name, winning_status, status, amount, winning_amount, challenge_goal, completed_on_txt;

        public WinLossModel(JSONObject jsonObject) {
            try {
                if (jsonObject.has("challenge_id") && !jsonObject.isNull("challenge_id"))
                    this.challenge_id = jsonObject.getString("challenge_id");

                if (jsonObject.has("opponent_name") && !jsonObject.isNull("opponent_name"))
                    this.opponent_name = jsonObject.getString("opponent_name");

                if (jsonObject.has("status") && !jsonObject.isNull("status"))
                    this.status = jsonObject.getString("status");

                if (jsonObject.has("winning_status") && !jsonObject.isNull("winning_status"))
                    this.winning_status = jsonObject.getString("winning_status");

                if (jsonObject.has("amount") && !jsonObject.isNull("amount"))
                    this.amount = jsonObject.getString("amount");

                if (jsonObject.has("winning_amount") && !jsonObject.isNull("winning_amount"))
                    this.winning_amount = jsonObject.getString("winning_amount");

                if (jsonObject.has("challenge_goal") && !jsonObject.isNull("challenge_goal"))
                    this.challenge_goal = jsonObject.getString("challenge_goal");

                if (jsonObject.has("completed_on_txt") && !jsonObject.isNull("completed_on_txt"))
                    this.completed_on_txt = jsonObject.getString("completed_on_txt");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //Get the database
    private void getWinLossDataFRomDB() {
        List<String> data=winLossDB.getChallengeData();
        if(data.size()>0){
            for(int i=0;i<data.size();i++){
                String basestr=data.get(i);
                byte[] bytes = Base64.decode(basestr, Base64.DEFAULT);

                try {
                    String str = new String(bytes, "UTF-8");
                    JSONObject jsonObject = new JSONObject(str);
                    WinLossModel winlos = new WinLossModel(jsonObject);
                    winlossList.add(winlos);
                } catch (JSONException e) {
                    e.printStackTrace();
                }catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                winlossAdapter = new WinLossAdapter(winlossList, WinLossActivity.this, R.layout.row_win_loss_data);
                layoutManager = new LinearLayoutManager(getApplicationContext());
                win_loss_recyclerView.setNestedScrollingEnabled(false);
                win_loss_recyclerView.setLayoutManager(layoutManager);
                RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(WinLossActivity.this, R.drawable.recycler_view_divider));
                win_loss_recyclerView.addItemDecoration(dividerItemDecoration);
                win_loss_recyclerView.setAdapter(winlossAdapter);
            }
        }
    }
}
