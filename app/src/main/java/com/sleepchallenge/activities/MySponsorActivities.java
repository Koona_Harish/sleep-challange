package com.sleepchallenge.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sleepchallenge.R;
import com.sleepchallenge.adapters.SleepChllengesViewPagerAdapter;
import com.sleepchallenge.fragments.MeAsASponsorFragment;
import com.sleepchallenge.fragments.MySponsoringFragment;

public class MySponsorActivities extends AppCompatActivity implements View.OnClickListener {

    ViewPager view_pager_me_asA_sponsor;
    ImageView back_img;
    TextView title_text;
    TabLayout tab;
    SleepChllengesViewPagerAdapter me_as_a_sponsor_adapter;
    Typeface regularFont;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_sponsor_activities);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));

         title_text=findViewById(R.id.title_text);
         title_text.setTypeface(regularFont);

          back_img =  findViewById(R.id.back_img);
          back_img.setOnClickListener(this);

        tab =  findViewById(R.id.tabLayout_me_asA_sponsor);
        view_pager_me_asA_sponsor =  findViewById(R.id.view_pager_me_asA_sponsor);
        setupViewPager(view_pager_me_asA_sponsor);
        tab.setupWithViewPager(view_pager_me_asA_sponsor);

    }

    private void setupViewPager(ViewPager view_pager_me_asA_sponsor)
    {
        me_as_a_sponsor_adapter = new SleepChllengesViewPagerAdapter(getSupportFragmentManager());

        me_as_a_sponsor_adapter.addFrag(new MeAsASponsorFragment(), "Me As A SPONSOR");
        me_as_a_sponsor_adapter.addFrag(new MySponsoringFragment(), "MY SPONSORING");


        view_pager_me_asA_sponsor.setAdapter(me_as_a_sponsor_adapter);

        tab.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(view_pager_me_asA_sponsor) {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab)
                    {
                        super.onTabSelected(tab);


                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }

                }
        );
    }

    @Override
    public void onClick(View view)
    {
       if(view==back_img)
       {finish();}
    }
}
