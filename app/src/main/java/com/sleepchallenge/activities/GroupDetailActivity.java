package com.sleepchallenge.activities;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.AllGroupCompletedChallengesAdapter;
import com.sleepchallenge.adapters.GetMembersInGroupDetailAdapter;
import com.sleepchallenge.models.ChallengeGroupDetailModel;
import com.sleepchallenge.models.GetMemberinGroupDetailModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.DividerItemDecorator;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GroupDetailActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView back_img, send_message, groupprofile_pic_iv, edit_group, group_admin_image;
    TextView groupName, win, loss, total_per, follow_text, sponsor_text, block_text, group_ranking_text, send_mail, make_call, memb_title, group_member_more_text, grp_member_count_text,
            text_exit, text_delete, group_adminranking_text, group_admin_name, admint_text, group_admin_win_per, group_admin_los_per, group_admin_totlapercentage;
    String user_id, user_type, token, device_id, grp_id, grp_name_, grp_admin_id, conver_flat_type, group_id_G, has_running_challenges, group_name_G, group_pic_G, call_mobile_no, country_code, admin_mail_id;
    int block_status;
    UserSessionManager sessionManager;
    private boolean checkInternet;
    RecyclerView recycler_member_in_groupdetail;
    RelativeLayout ll_member_text_title;
    Typeface regularFont, boldFont;
    ArrayList<GetMemberinGroupDetailModel> grpMemList;
    RecyclerView.LayoutManager layoutManager;
    RelativeLayout exitGrpButt, deletGrpButt;
    GetMembersInGroupDetailAdapter getMemberInGroupDetailAdapter;
    android.app.Dialog addMoneydialog;
    int overall_rank_G;
    AllGroupCompletedChallengesAdapter challengGroupCompletAdpter;
    ArrayList<ChallengeGroupDetailModel> challengGroupCompletcomList = new ArrayList<ChallengeGroupDetailModel>();
    LinearLayoutManager completedlayoutmanger;
    LinearLayout completed_layout;
    TextView completed_text;
    RecyclerView grouprecycler_completed;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_detail);

        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        checkInternet = NetworkChecking.isConnected(this);

        sessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        Bundle bundle = getIntent().getExtras();
        grp_id = bundle.getString("grp_id");
        grp_name_ = bundle.getString("grp_name");
        grp_admin_id = bundle.getString("grp_admin_id");
        conver_flat_type = getIntent().getExtras().getString("GROUP_CONVERSATION_TYPE");
        Log.d("GRP", grp_id + "//" + grp_name_ + "//" + grp_admin_id);

        ll_member_text_title = findViewById(R.id.ll_member_text_title);

        exitGrpButt = findViewById(R.id.exitGrpButt);
        exitGrpButt.setOnClickListener(this);

        deletGrpButt = findViewById(R.id.deletGrpButt);
        deletGrpButt.setOnClickListener(this);

        back_img = findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        send_message = findViewById(R.id.send_message);
        send_message.setOnClickListener(this);
        groupprofile_pic_iv = findViewById(R.id.groupprofile_pic_iv);
        group_admin_image = findViewById(R.id.group_admin_image);
        edit_group = findViewById(R.id.edit_group);
        edit_group.setOnClickListener(this);

        groupName = findViewById(R.id.groupName);
        groupName.setTypeface(boldFont);
        group_ranking_text = findViewById(R.id.group_ranking_text);
        group_ranking_text.setTypeface(regularFont);
        grp_member_count_text = findViewById(R.id.grp_member_count_text);
        grp_member_count_text.setTypeface(regularFont);
        win = findViewById(R.id.win);
        win.setTypeface(regularFont);
        loss = findViewById(R.id.loss);
        loss.setTypeface(regularFont);
        total_per = findViewById(R.id.total_per);
        total_per.setTypeface(regularFont);
        text_exit = findViewById(R.id.text_exit);
        text_exit.setTypeface(regularFont);
        text_delete = findViewById(R.id.text_delete);
        text_delete.setTypeface(regularFont);


        group_admin_name = findViewById(R.id.group_admin_name);
        group_admin_name.setTypeface(boldFont);
        group_adminranking_text = findViewById(R.id.group_adminranking_text);
        group_adminranking_text.setTypeface(regularFont);
        admint_text = findViewById(R.id.admint_text);
        admint_text.setTypeface(regularFont);
        group_admin_win_per = findViewById(R.id.group_admin_win_per);
        group_admin_win_per.setTypeface(regularFont);
        group_admin_los_per = findViewById(R.id.group_admin_los_per);
        group_admin_los_per.setTypeface(regularFont);
        group_admin_totlapercentage = findViewById(R.id.group_admin_totlapercentage);
        group_admin_totlapercentage.setTypeface(regularFont);

        grouprecycler_completed=findViewById(R.id.group_recycler_completed);
        completed_text = findViewById(R.id.completed_text);
        completed_text.setTypeface(boldFont);
        completed_layout=findViewById(R.id.ll_completed);

        follow_text = findViewById(R.id.follow_text);
        follow_text.setOnClickListener(this);
        follow_text.setTypeface(regularFont);
        sponsor_text = findViewById(R.id.sponsor_text);
        sponsor_text.setOnClickListener(this);
        sponsor_text.setTypeface(regularFont);
        block_text = findViewById(R.id.block_text);
        block_text.setOnClickListener(this);
        block_text.setTypeface(regularFont);

        send_mail = findViewById(R.id.send_mail);
        send_mail.setTypeface(regularFont);
        send_mail.setOnClickListener(this);
        make_call = findViewById(R.id.make_call);
        make_call.setOnClickListener(this);
        make_call.setTypeface(regularFont);

        memb_title = findViewById(R.id.memb_title);
        make_call.setTypeface(boldFont);
        group_member_more_text = findViewById(R.id.group_member_more_text);
        group_member_more_text.setOnClickListener(this);
        group_member_more_text.setTypeface(boldFont);
        recycler_member_in_groupdetail = findViewById(R.id.recycler_member_in_groupdetail);
        challengGroupCompletAdpter = new AllGroupCompletedChallengesAdapter(challengGroupCompletcomList, GroupDetailActivity.this, R.layout.row_my_challenges_group_detail);
        completedlayoutmanger = new LinearLayoutManager(GroupDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
        grouprecycler_completed.setNestedScrollingEnabled(false);
        grouprecycler_completed.setLayoutManager(completedlayoutmanger);

        if (grp_admin_id.equals(user_id)) {
            edit_group.setVisibility(View.VISIBLE);
            deletGrpButt.setVisibility(View.VISIBLE);
            conver_flat_type = "GROUP_INTERNAL";
        }
        if (user_type.equals("SPONSOR")) {
            follow_text.setVisibility(View.GONE);
            send_message.setVisibility(View.GONE);
        } else {

        }

        getGroupDetail();
        getGroupFollowingStatus();
        getGroupChallenges();

    }

    public void getGroupDetail() {
        // grpMemList.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Dialog.showProgressBar(GroupDetailActivity.this, "Loading");
            Log.d("GroupDetailsUrl", AppUrls.BASE_URL + "group/get_group_details?user_id=" + user_id + "&group_id=" + grp_id);
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "group/get_group_details?user_id=" + user_id + "&group_id=" + grp_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Dialog.hideProgressBar();
                            Log.d("GroupDetailsResp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    JSONObject getData = jsonObject.getJSONObject("data");
                                    has_running_challenges = getData.getString("has_running_challenges");
                                    if (has_running_challenges.equals("1")) {
                                        edit_group.setVisibility(View.GONE);
                                    }
                                    else
                                        {
                                        if (grp_admin_id.equals(user_id))
                                        {
                                            edit_group.setVisibility(View.VISIBLE);
                                        }
                                        else
                                            {
                                            edit_group.setVisibility(View.GONE);
                                        }
                                    }

                                    int grp_member_count = getData.getInt("member_count");
                                    if (grp_member_count > 2) {
                                        group_member_more_text.setVisibility(View.VISIBLE);
                                    }
                                    //GET_GROUP_SECTION
                                    JSONObject getGroupData = getData.getJSONObject("group_details");
                                    group_id_G = getGroupData.getString("group_id");
                                    group_name_G = getGroupData.getString("group_name");
                                    String str = group_name_G;
                                    String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
                                    groupName.setText("" + converted_string);
                                    group_pic_G = AppUrls.BASE_IMAGE_URL + getGroupData.getString("group_pic");
                                    Log.d("PPPP:", group_pic_G);
                                    Picasso.with(GroupDetailActivity.this)
                                            .load(group_pic_G)
                                            .placeholder(R.drawable.group_dummy)
                                            //   .resize(60,60)
                                            .into(groupprofile_pic_iv);
                                    overall_rank_G = getGroupData.getInt("overall_rank");
                                    group_ranking_text.setText(String.valueOf(overall_rank_G));
                                    int win_challenges_G = getGroupData.getInt("win_challenges");
                                    win.setText(String.valueOf(win_challenges_G));
                                    int lost_challenges_G = getGroupData.getInt("lost_challenges");
                                    loss.setText(String.valueOf(lost_challenges_G));
                                    int winning_percentage_G = getGroupData.getInt("winning_percentage");
                                    total_per.setText(String.valueOf(winning_percentage_G) + " " + "%");
                                    int members_cnt_G = getGroupData.getInt("members_cnt");
                                    grp_member_count_text.setText(String.valueOf(members_cnt_G) + " " + "members");
                                    String ismember = getGroupData.getString("is_member_of_group");
                                    if (ismember.equals("0")) {
                                        exitGrpButt.setVisibility(View.GONE);
                                        deletGrpButt.setVisibility(View.GONE);
                                    } else {
                                        if (grp_admin_id.equals(user_id)) {
                                            deletGrpButt.setVisibility(View.VISIBLE);
                                        } else {
                                            exitGrpButt.setVisibility(View.VISIBLE);
                                        }
                                    }
                                    //GET_ADMIN_SECTION
                                    JSONObject getAdmData = getData.getJSONArray("group_admin_details").getJSONObject(0);
                                    String group_user_type_A = getAdmData.getString("group_user_type");
                                    admint_text.setText(String.valueOf(group_user_type_A));
                                    String id_A = getAdmData.getString("id");
                                    String user_name_A = getAdmData.getString("user_name");
                                    String converted_string_name = user_name_A.substring(0, 1).toUpperCase() + user_name_A.substring(1);
                                    group_admin_name.setText(String.valueOf(converted_string_name));
                                    int user_rank_A = getAdmData.getInt("user_rank");
                                    group_adminranking_text.setText(String.valueOf(user_rank_A));
                                    int win_challenges_A = getAdmData.getInt("win_challenges");
                                    group_admin_win_per.setText(String.valueOf(win_challenges_A));
                                    int lost_challenges_A = getAdmData.getInt("lost_challenges");
                                    group_admin_los_per.setText(String.valueOf(lost_challenges_A));
                                    int winning_percentage_A = getAdmData.getInt("winning_percentage");
                                    group_admin_totlapercentage.setText(String.valueOf(winning_percentage_A + " %"));
                                    String adm_img_A = AppUrls.BASE_IMAGE_URL + getAdmData.getString("profile_pic");
                                    Picasso.with(GroupDetailActivity.this)
                                            .load(adm_img_A)
                                            .placeholder(R.drawable.members_dummy)
                                            .into(group_admin_image);

                                    call_mobile_no = getAdmData.getString("mobile");
                                    country_code = getAdmData.getString("country_code");
                                    admin_mail_id = getAdmData.getString("email");


                                    JSONArray getMemData = getData.getJSONArray("member_details");
                                    Log.d("JARRRRARY", getMemData.toString());
                                    grpMemList = new ArrayList<>();
                                    for (int i = 0; i < getMemData.length(); i++) {
                                        JSONObject jdataobj = getMemData.getJSONObject(i);
                                        GetMemberinGroupDetailModel allmember = new GetMemberinGroupDetailModel(jdataobj);
                                        grpMemList.add(allmember);
                                    }
                                    getMemberInGroupDetailAdapter = new GetMembersInGroupDetailAdapter(grpMemList, GroupDetailActivity.this, R.layout.row_allmember);
                                    layoutManager = new LinearLayoutManager(getApplicationContext());
                                    recycler_member_in_groupdetail.setNestedScrollingEnabled(false);
                                    recycler_member_in_groupdetail.setLayoutManager(layoutManager);
                                    RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(GroupDetailActivity.this, R.drawable.recycler_view_divider));
                                    recycler_member_in_groupdetail.addItemDecoration(dividerItemDecoration);

                                    recycler_member_in_groupdetail.setAdapter(getMemberInGroupDetailAdapter);


                                }
                                if (successResponceCode.equals("10200")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(GroupDetailActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();

                                }
                                if (successResponceCode.equals("10300")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(GroupDetailActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Dialog.hideProgressBar();

                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GroupDetails_Header", "HEADER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strRe);

        } else {

            Toast.makeText(GroupDetailActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }


    }

    private void getGroupFollowingStatus() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.GROUP_FOLLOWING_STATUS;
            Log.d("GROUPFOLLOWSTATUSURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("GroupFOLOWSTATURESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String status = jobcode.getString("status");
                        String response_code = jobcode.getString("response_code");
                        if (status.equals("1")) {
                            follow_text.setText("UnFollow");
                            follow_text.setPadding(10,10,15,10);
                            follow_text.setTextColor(getResources().getColor(R.color.auth_text_color));
                            follow_text.setBackgroundResource(R.drawable.member_detail_textview_background);
                            follow_text.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getUnFollowGroup();
                                }
                            });

                        } else {
                            follow_text.setText("Follow");
                            follow_text.setPadding(10,10,15,10);
                            follow_text.setTextColor(getResources().getColor(R.color.gray));
                            follow_text.setBackgroundResource(R.drawable.member_detail_textview_background);
                            follow_text.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getFollowGroup();
                                }
                            });
                        }


                        if (response_code.equals("10200")) {
                            follow_text.setText("UnFollow");
                            follow_text.setPadding(10,10,15,10);
                            follow_text.setTextColor(getResources().getColor(R.color.auth_text_color));
                            follow_text.setBackgroundResource(R.drawable.member_detail_textview_background);
                        }
                        if (response_code.equals("10100")) {
                            follow_text.setText("UnFollow");
                            follow_text.setPadding(10,10,15,10);
                            follow_text.setTextColor(getResources().getColor(R.color.auth_text_color));
                            follow_text.setBackgroundResource(R.drawable.member_detail_textview_background);
                        }
                        if (response_code.equals("10300")) {
                            follow_text.setText("Follow");
                            follow_text.setPadding(10,10,15,10);
                            follow_text.setTextColor(getResources().getColor(R.color.gray));
                            follow_text.setBackgroundResource(R.drawable.member_detail_textview_background);


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    error.getMessage();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", user_id);
                    params.put("group_id", grp_id);
                    Log.d("GRPFOLLOWPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(GroupDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(GroupDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    private void getUnFollowGroup() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.GROUP_UNFOLLOW;
            Log.d("UNGRPFOLLOWURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("UNGRPFOLOWRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");
                        if (response_code.equals("10100")) {
                            //JSONObject jobj=jobcode.getJSONObject("data");
                            Toast.makeText(GroupDetailActivity.this, "UnFollow Group successfully", Toast.LENGTH_LONG).show();
                            getGroupFollowingStatus();
                        }
                        if (response_code.equals("10200")) {

                            Toast.makeText(GroupDetailActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    error.getMessage();
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", user_id);
                    params.put("group_id", grp_id);
                    Log.d("UNFGRPOLLOWPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(GroupDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(GroupDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    private void getFollowGroup() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.GROUP_FOLLOW;
            Log.d("GRPFOLLOWURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("FOLOWGRPESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                            // JSONObject jobj=jobcode.getJSONObject("data");
                            Toast.makeText(GroupDetailActivity.this, "Following group successfully", Toast.LENGTH_LONG).show();
                            getGroupFollowingStatus();
                        }
                        if (response_code.equals("10200")) {

                            Toast.makeText(GroupDetailActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    error.getMessage();
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", user_id);
                    params.put("group_id", grp_id);
                    Log.d("FOLLOWFGRPPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(GroupDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(GroupDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    public void getGroupChallenges() {
        checkInternet = NetworkChecking.isConnected(this);
        challengGroupCompletcomList.clear();
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.MY_CHALLENGES + "user_id=" + user_id + "&user_type=USER&type=" + "GROUP";
            Log.d("URLLLLL",url);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("response_code");
                        if (status.equals("10100")) {
                            JSONObject jObjData = jsonObject.getJSONObject("data");
                            int running_cnt = jObjData.getInt("running_cnt");
                            int upcomming_cnt = jObjData.getInt("upcomming_cnt");

                            int completed_cnt = jObjData.getInt("completed_cnt");
                            if (completed_cnt != 0) {
                                completed_layout.setVisibility(View.VISIBLE);
                                ContentValues values = new ContentValues();
                                JSONArray jsonArraycomplet = jObjData.getJSONArray("completed");
                                for (int k = 0; k < jsonArraycomplet.length(); k++) {
                                    JSONObject jsonObject1 = jsonArraycomplet.getJSONObject(k);
                                    ChallengeGroupDetailModel am_runn = new ChallengeGroupDetailModel();
                                    am_runn.setChallenge_id(jsonObject1.getString("challenge_id"));

                                    am_runn.setUser_name(jsonObject1.getString("user_name"));
                                    am_runn.setOpponent_name(jsonObject1.getString("opponent_name"));
                                    am_runn.setWinning_status( jsonObject1.getString("winning_status"));
                                    am_runn.setIs_group_admin( jsonObject1.getString("is_group_admin"));
                                    am_runn.setStatus(jsonObject1.getString("status"));
                                    am_runn.setAmount(jsonObject1.getString("amount"));
                                    am_runn.setWinning_amount(jsonObject1.getString("winning_amount"));
                                    am_runn.setStart_on(jsonObject1.getString("start_on"));
                                    am_runn.setPaused_on(jsonObject1.getString("paused_on"));
                                    am_runn.setResume_on( jsonObject1.getString("resume_on"));
                                    am_runn.setCompleted_on_txt(jsonObject1.getString("completed_on_txt"));
                                    am_runn.setCompleted_on(jsonObject1.getString("completed_on"));

                                    am_runn.setWinner(jsonObject1.getString("winner"));
                                    am_runn.setPaused_by(jsonObject1.getString("paused_by"));
                                    am_runn.setChallenge_type(jsonObject1.getString("challenge_type"));
                                    am_runn.setWinning_reward_type(jsonObject1.getString("winning_reward_type"));
                                    am_runn.setWinning_reward_value( jsonObject1.getString("winning_reward_value"));
                                    am_runn.setIs_scratched(jsonObject1.getString("is_scratched"));
                                    am_runn.setPause_access(jsonObject1.getString("pause_access"));

                                    challengGroupCompletcomList.add(am_runn);
                                    Log.d("sdgsdfdgjsdfg", challengGroupCompletcomList.toString());

                                }
                                grouprecycler_completed.setAdapter(challengGroupCompletAdpter);
                            }

                        }
                        if (status.equals("10200")) {
                            Toast.makeText(GroupDetailActivity.this, "No Challenges Found...!", Toast.LENGTH_LONG).show();
                        }
                        if (status.equals("10300")) {
                            Toast.makeText(GroupDetailActivity.this, "No data Found..!", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {

            //Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {

        if (view == back_img) {
            finish();
        }
        if (view == sponsor_text)
        {
            addMoneydialog = new android.app.Dialog(GroupDetailActivity.this);
            addMoneydialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            addMoneydialog.setContentView(R.layout.custom_dialolg_sponsor_request);
             TextView dummy_text =  addMoneydialog.findViewById(R.id.dummy_text);
            dummy_text.setTypeface(regularFont);

            TextView header =  addMoneydialog.findViewById(R.id.header);
            header.setText("Group Sponsor Details");

            TextView group_name_text =  addMoneydialog.findViewById(R.id.member_name_text);
            TextView group_rank_text =  addMoneydialog.findViewById(R.id.member_rank_text);

            TextView member_name_title_text =  addMoneydialog.findViewById(R.id.member_name_title_text);
            TextView rank_text_title =  addMoneydialog.findViewById(R.id.rank_text_title);

            final EditText group_sponsor_amount_edt = addMoneydialog.findViewById(R.id.member_sponsor_amount_edt);
            Button sponsor_submit_btn =  addMoneydialog.findViewById(R.id.sponsor_submit_btn);
            group_name_text.setText(group_name_G);
            group_name_text.setTypeface(regularFont);

            group_rank_text.setText(String.valueOf(overall_rank_G));
            group_rank_text.setTypeface(regularFont);
            header.setTypeface(regularFont);
            member_name_title_text.setText("Group Name");
            member_name_title_text.setTypeface(regularFont);
            rank_text_title.setTypeface(regularFont);




            sponsor_submit_btn.setOnClickListener(new View.OnClickListener() {

                @Override

                public void onClick(View v) {
                    String amt_value = group_sponsor_amount_edt.getText().toString();
                    if (amt_value.equals("") || amt_value.equals("0")) {
                        Toast.makeText(GroupDetailActivity.this, "Please enter valid amount..!", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(GroupDetailActivity.this, PaymentDetailsActivity.class);
                        intent.putExtra("activty", "GroupDetail");
                        intent.putExtra("group_id", group_id_G);
                        intent.putExtra("group_user_type", "GROUP");
                        intent.putExtra("sponsorAmount", amt_value);
                      //  Log.d("GROUPSPONSOR", group_id_G + "\n" + group_user_type_A + "\n" + amt_value);
                        startActivity(intent);
                    }
                }

            });
            addMoneydialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            addMoneydialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
            addMoneydialog.show();
        }

        if (view == send_message)
        {
            Intent ingrpdetail = new Intent(GroupDetailActivity.this, GroupChatDetailActivity.class);
            ingrpdetail.putExtra("group_id", grp_id);
            ingrpdetail.putExtra("group_name", grp_name_);
            ingrpdetail.putExtra("profile_pic", group_pic_G);
            ingrpdetail.putExtra("condition", "GROUP_NOT_USER");
            ingrpdetail.putExtra("GROUP_CONVERSATION_TYPE", conver_flat_type);
            ingrpdetail.putExtra("FROM_TYPE", "GROUP");
            startActivity(ingrpdetail);
            Log.d("PARAMMMM", grp_id + "//" + grp_name_ + "//" + group_pic_G + "//" + conver_flat_type);
        }
        if (view == group_member_more_text)
        {
           Intent it=new Intent(GroupDetailActivity.this,AllMembersActivity.class);
           it.putExtra("more","details");
           it.putExtra("group_id", grp_id);
           startActivity(it);
        }

        if (view == send_mail) {
            String[] recipients = new String[]{admin_mail_id, ""};
            Intent testIntent = new Intent(android.content.Intent.ACTION_SEND);
            testIntent.setType("message/rfc822");
            testIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
            testIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
            testIntent.putExtra(android.content.Intent.EXTRA_EMAIL, recipients);
            startActivity(testIntent);
        }

        if (view == make_call) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + country_code + call_mobile_no));
            if (ActivityCompat.checkSelfPermission(GroupDetailActivity.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            startActivity(callIntent);
        }
        if (view == edit_group)
        {
            Intent craetefroup = new Intent(GroupDetailActivity.this, UpdateGroupActivity.class);
            craetefroup.putExtra("GROUPEDIT", "editgroup");
            craetefroup.putExtra("GroupId", grp_id);
            startActivity(craetefroup);

        }

    }
}
