package com.sleepchallenge.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.NetworkChecking;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener,RadioGroup.OnCheckedChangeListener{
    RadioGroup mobile_email_radio_group;
    private boolean checkInternet;
    RadioButton mobile_radio,email_radio;
    EditText forgotEt;
    Button submit_btn;
    String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
    String  sendVia;
    String forgotstr;
    Typeface regularFont,boldFont,thinFont,specialFont;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mobile_email_radio_group = findViewById(R.id.mobile_email_radio_group);
        submit_btn=findViewById(R.id.submit_btn);
        forgotEt=findViewById(R.id.forgot_pwd_et);
        mobile_radio=findViewById(R.id.mobile_radio);
        email_radio=findViewById(R.id.email_radio);
        submit_btn.setOnClickListener(this);
        mobile_email_radio_group.setOnCheckedChangeListener(this);

        sendVia="Mobile";
        forgotEt.setInputType(InputType.TYPE_CLASS_NUMBER);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));
        mobile_radio.setTypeface(regularFont);
        email_radio.setTypeface(regularFont);
        forgotEt.setTypeface(specialFont);
        submit_btn.setTypeface(boldFont);
    }

    @Override
    public void onClick(View v)
    {
        if (v == submit_btn)
        {
            forgotstr = forgotEt.getText().toString();
            if (sendVia.equalsIgnoreCase("Mobile"))
            {
                if (forgotstr.length() == 0)
                {
                    Toast.makeText(this, "Enter Mobile No", Toast.LENGTH_SHORT).show();
                }
                else
                    {

                      forgotPwdServiceCall();
                    }
            }
           else if (sendVia.equalsIgnoreCase("Email"))
           {
               if (forgotstr.length() == 0)
                {
                    Toast.makeText(this, "Enter email address", Toast.LENGTH_SHORT).show();
                }
                else
                    {

                    forgotPwdServiceCall();
                }
            }
      }

        }

    private void  forgotPwdServiceCall(){
        checkInternet = NetworkChecking.isConnected(this);
        if(checkInternet)
        {
            if (checkInternet)
            {
                Log.d("FORGETURL", AppUrls.BASE_URL + AppUrls.FORGOTPASSWORD);
                  StringRequest forgetReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FORGOTPASSWORD, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("FORGETRESP", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String successResponceCode = jsonObject.getString("response_code");
                            if (successResponceCode.equals("10100")) {
                                Dialog.hideProgressBar();
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                 String user_id=jsonObject1.getString("user_id");

                                Intent i = new Intent(ForgotPassword.this, ForgetPassOtpActivity.class);
                                  i.putExtra("USERID",user_id);
                                startActivity(i);
                            }

                            if (successResponceCode.equals("10200")) {
                                Dialog.hideProgressBar();
                                Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                            }
                            if (successResponceCode.equals("10300")) {
                                Dialog.hideProgressBar();
                                Toast.makeText(getApplicationContext(), "User does not Exist..", Toast.LENGTH_SHORT).show();
                            }
                            if (successResponceCode.equals("10400")) {
                                Dialog.hideProgressBar();
                                Toast.makeText(getApplicationContext(), "Unable to Send OTP..", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialog.hideProgressBar();
                        error.getMessage();

                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();

                        Log.d("SENDVIA", sendVia);
                        if (sendVia.equals("Mobile"))
                        {
                            params.put("username", forgotstr);


                            Log.d("FORGETPARAmobile:", params.toString());
                        }
                        if (sendVia.equals("Email"))
                        {
                            params.put("username", forgotstr);

                            Log.d("FORGETPARAemail:", params.toString());
                        }
                        Log.d("FORGETPARAM:", params.toString());
                        return params;

                    }
                };
                forgetReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(ForgotPassword.this);
                requestQueue.add(forgetReq);

            } else {
                Toast.makeText(ForgotPassword.this, "No Inmtenet Connection..!", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.mobile_radio:
                sendVia="Mobile";
                forgotEt.setInputType(InputType.TYPE_CLASS_NUMBER);
                forgotEt.setHint("Enter Mobile No");
                forgotEt.setText("");
                break;
            case R.id.email_radio:
                sendVia="Email";
                forgotEt.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                forgotEt.setHint("Enter valid email address");
                forgotEt.setText("");
                break;
        }
    }
}
