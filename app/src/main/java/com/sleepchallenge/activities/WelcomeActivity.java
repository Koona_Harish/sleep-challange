package com.sleepchallenge.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    TextView welcome_txt, normal_txt;
    Button btn_fbLogin, btn_gplusLogin;
    RadioGroup login_rg;
    RadioButton user_rb, sponsor_rb;
    Button login_btn, register_btn;
    Typeface regularFont, boldFont, thinFont;
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 007;
    CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;
    String nm = "", lnm = "", deviceId;
    String sendFBAccessToken, sendFBUserID, token = "", verified_data, fb_user_id_data, email_data, name_data, gender_data, user_type;
    private boolean checkInternet;
    UserSessionManager userSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);

        user_type = "USER";

        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));
        checkInternet = NetworkChecking.isConnected(this);
        welcome_txt = findViewById(R.id.welcome_txt);
        welcome_txt.setTypeface(boldFont);
        normal_txt = findViewById(R.id.normal_txt);
        normal_txt.setTypeface(thinFont);


        login_rg = findViewById(R.id.login_rg);

        user_rb = findViewById(R.id.user_rb);
        user_rb.setTypeface(thinFont);
        sponsor_rb = findViewById(R.id.sponsor_rb);
        sponsor_rb.setTypeface(thinFont);

        login_btn = findViewById(R.id.login_btn);
        login_btn.setTypeface(thinFont);
        register_btn = findViewById(R.id.register_btn);
        register_btn.setTypeface(thinFont);

        btn_gplusLogin = findViewById(R.id.btn_gplusLogin);
        btn_gplusLogin.setTypeface(regularFont);
        btn_fbLogin = findViewById(R.id.btn_fbLogin);
        btn_fbLogin.setTypeface(regularFont);


        login_btn.setOnClickListener(this);
        register_btn.setOnClickListener(this);
        btn_gplusLogin.setOnClickListener(this);
        btn_fbLogin.setOnClickListener(this);

        login_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton rb = (RadioButton) findViewById(checkedId);
                if (checkedId == R.id.user_rb) {
                    user_type = "USER";

                } else if (checkedId == R.id.sponsor_rb) {
                    user_type = "SPONSOR";
                }
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        initializeFacebookSettings();
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.sleepchallange", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//5wI9pDN/WQ0ALdS/+TqIPIWhtt4=
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        Log.d("rerere", "asdfdsf");
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREFERENCE), Context.MODE_PRIVATE);
        token = sharedPreferences.getString(getString(R.string.FCM_TOKEN), "");
        Log.d("TOKEVELUE", token);


       /* LoginManager.getInstance().logInWithReadPermissions(
                this,
                Arrays.asList("user_friends", "email", "public_profile"));*/
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("asdfdsff", "asdfdsf");
                GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        JSONObject json = response.getJSONObject();
                        Log.d("LOGFBBB", json.toString());
                        if (json != null) {
                            try {
                                sendFBAccessToken = AccessToken.getCurrentAccessToken().getToken();
                                AccessToken token1 = AccessToken.getCurrentAccessToken();
                                if (token1 != null) {
                                    Log.d("ACCESSESTOKE", AccessToken.getCurrentAccessToken().getToken());
                                }
                                //  Log.d("ACCESSESTOKE", AccessToken.getCurrentAccessToken().getToken());

                                //  verified_data = json.getString("verified");
                                fb_user_id_data = json.getString("id");
                                sendFBUserID = fb_user_id_data;
                                email_data = json.getString("email");
                                name_data = json.getString("name");
                                //  gender_data = json.getString("gender");
                                String profile_ptah = "http://graph.facebook.com/" + fb_user_id_data + "/picture?type=large";
                                String provider = "Facebook";


                                String nm = "", lnm = "";

                                if (name_data.split("\\w+").length > 1) {

                                    lnm = name_data.substring(name_data.lastIndexOf(" ") + 1);
                                    nm = name_data.substring(0, name_data.lastIndexOf(' '));
                                } else {
                                    //  firstName = name;
                                }
                                Log.d("FACEBOOKPROFILE", nm + "   " + lnm + "    " + name_data + "    " + verified_data + "," + fb_user_id_data + "," + sendFBUserID + ", " + email_data + ", " + email_data + ", " + gender_data + ",," + profile_ptah);

                                getSocialLoginStatus(fb_user_id_data, email_data, provider, lnm, nm);


                              /*  Intent i = new Intent(WelcomeActivity.this, RegistrationActivity.class);
                                i.putExtra("USERTYPE", user_type);
                                i.putExtra("user_data_id", fb_user_id_data);
                                i.putExtra("name", nm);
                                i.putExtra("lastname", lnm);
                                i.putExtra("email", email_data);
                               // i.putExtra("gender", gender_data);
                                i.putExtra("provider", "Facebook");
                                i.putExtra("LOGIN", "Facebook");
                                Log.d("FBBB", user_type + "//" + nm + "//" + lnm + "//" + fb_user_id_data + "//" + email_data + "//" + gender_data+"///");
                                startActivity(i);*/


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,gender,link,email,verified,birthday");
                //  parameters.putString("fields", "id,email,first_name,last_name,gender");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(WelcomeActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException exception) {
                Log.d("EXCEPTION", exception.toString());
                Toast.makeText(WelcomeActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult" + result.isSuccess());
        if (result.isSuccess()) {

            GoogleSignInAccount acct = result.getSignInAccount();

            Log.d("detailaname", "" + acct.getDisplayName());

            String personName = acct.getDisplayName();
            String email = acct.getEmail();
            String id = acct.getId();
            String profilePicPath = "";
            if (acct != null) {
                if (acct.getPhotoUrl() != null) {
                    profilePicPath = acct.getPhotoUrl().getPath();
                }
            }
            if (personName.split("\\w+").length > 1) {
                lnm = personName.substring(personName.lastIndexOf(" ") + 1);
                nm = personName.substring(0, personName.lastIndexOf(' '));
            } else {
                //  firstName = name;
            }
            String provider = "Google";

            getSocialLoginStatus(id, email, provider, lnm, nm);
/*
            Intent i = new Intent(WelcomeActivity.this, RegistrationActivity.class);
            i.putExtra("USERTYPE", user_type);
            i.putExtra("name", nm);
            i.putExtra("lastname", lnm);
            i.putExtra("email", acct.getEmail());
            i.putExtra("user_gmail_id", acct.getId());
            i.putExtra("provider", "Google");
            i.putExtra("LOGIN", "Google");
            Log.d("GPLUS", user_type + "//" + nm + "//" + lnm + "//" + acct.getEmail() + "//" + acct.getId());
            startActivity(i);*/


            Log.e("infoooo", "" + personName + ", email: " + email + ", Image: " + profilePicPath);

        } else {
            // Signed out, show unauthenticated UI.

        }
    }

    @Override
    public void onClick(View v) {

        if (v == login_btn) {
            Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
            intent.putExtra("USERTYPE", user_type);
            startActivity(intent);
        }

        if (v == register_btn) {
            Intent intent = new Intent(WelcomeActivity.this, RegistrationActivity.class);
            intent.putExtra("USERTYPE", user_type);
            intent.putExtra("LOGIN", "Manual");
            startActivity(intent);
        }

        if (v == btn_gplusLogin) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            } else {
                Toast.makeText(WelcomeActivity.this, "No Internet Connetcion...!", Toast.LENGTH_LONG).show();
            }
        }

        if (v == btn_fbLogin) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));
            } else {
                Toast.makeText(WelcomeActivity.this, "No Internet Connetcion...!", Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d(TAG, "RESULLLL" + result);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void initializeFacebookSettings() {
        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(getApplicationContext());

    }

    private void getSocialLoginStatus(final String id, final String email, final String provider, final String l_name, final String f_name) {

        if (checkInternet) {
            Log.d("SOCIALSTATUSURL", AppUrls.BASE_URL + AppUrls.SOCIAL_LOGIN_STATUS);
            StringRequest strsocialLReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SOCIAL_LOGIN_STATUS, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("SOCIALSTATURESP", response);
                    try {
                        String mobile = "", country_code = "";
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100")) {
                            Dialog.hideProgressBar();
                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                            String token = jsonObject1.getString("token");
                            String user_id = jsonObject1.getString("user_id");
                            String user_type = jsonObject1.getString("user_type");
                            String is_email_verified = jsonObject1.getString("is_email_verified");
                            String is_mobile_verified = jsonObject1.getString("is_mobile_verified");
                            String initial_payment = jsonObject1.getString("initial_payment");
                            Log.d("JWTMANN", token + "//" + initial_payment);
                            String[] parts = token.split("\\.");
                            byte[] dataDec = Base64.decode(parts[1], Base64.DEFAULT);
                            String decodedString = "";
                            try {
                                decodedString = new String(dataDec, "UTF-8");
                                Log.d("TOKENSMANN", decodedString);
                                JSONObject jsonObject2 = new JSONObject(decodedString);
                                Log.d("JSONDATMANN", jsonObject2.toString());
                                String username = jsonObject2.getString("name");
                                String email = jsonObject2.getString("email");
                                mobile = jsonObject2.getString("mobile");
                                country_code = jsonObject2.getString("country_code");
                                String status = jsonObject2.getString("status");

                                //set value in session

                                userSessionManager.createUserLoginSession(user_id, user_type, token, username, email, mobile, country_code);


                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            if (is_mobile_verified.equals("0")) {
                                Intent itootp = new Intent(WelcomeActivity.this, AccountVerificationActivity.class);
                                itootp.putExtra("mobile", mobile);
                                itootp.putExtra("countryCode", "+" + country_code);
                                itootp.putExtra("user_id", user_id);

                                startActivity(itootp);
                            } else if (initial_payment.equals("0")) {
                                Intent iacc_page = new Intent(WelcomeActivity.this, PayPalActivity.class);
                                iacc_page.putExtra("activity", "Welcome");
                                iacc_page.putExtra("paymentAmount", "1");
                                startActivity(iacc_page);
                                finish();
                            } else {
                                Intent imain = new Intent(WelcomeActivity.this, MainActivity.class);
                                imain.putExtra("USERTYPE", user_type);
                                startActivity(imain);
                            }
                        } else if (successResponceCode.equals("10200")) {

                            Toast.makeText(getApplicationContext(), "Invalid input..!", Toast.LENGTH_SHORT).show();
                        } else if (successResponceCode.equals("10300")) {

                            Toast.makeText(getApplicationContext(), "User not exist..!", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(WelcomeActivity.this, RegistrationActivity.class);
                            i.putExtra("USERTYPE", user_type);
                            i.putExtra("name", f_name);
                            i.putExtra("lastname", l_name);
                            i.putExtra("email", email);
                            i.putExtra("user_fb_gmail_id", id);
                            i.putExtra("provider", provider);
                            i.putExtra("LOGIN", provider);
                            Log.d("FBGPLUS", user_type + "//" + nm + "//" + lnm + "//" + email + "//" + id + "///" + provider);
                            startActivity(i);

                        } else if (successResponceCode.equals("10400")) {

                            Toast.makeText(getApplicationContext(), "Account is deactivated, please contact Admin.!", Toast.LENGTH_SHORT).show();
                        } else if (successResponceCode.equals("10500")) {

                            Toast.makeText(getApplicationContext(), "Account is blocked, please contact Admin.!", Toast.LENGTH_SHORT).show();
                        } else if (successResponceCode.equals("10600")) {

                            Toast.makeText(getApplicationContext(), "Invalid Login...!", Toast.LENGTH_SHORT).show();
                        } else if (successResponceCode.equals("10700")) {

                            Toast.makeText(getApplicationContext(), "It seems you are login from different device...!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Dialog.hideProgressBar();
                    error.getMessage();
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    Log.d("DATATTT", user_type + "//" + email + "//" + deviceId + "//" + provider + "//" + id);


                    params.put("user_type", user_type);
                    params.put("email", email);
                    params.put("device_id", deviceId);
                    params.put("provider", provider);
                    params.put("identifier", id);
                    params.put("device_platform", "ANDROID");
                    params.put("force_login", "1");
                    Log.d("REGPARAMManual", params.toString());

                    return params;
                }
            };

            strsocialLReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(WelcomeActivity.this);
            requestQueue.add(strsocialLReq);
        } else {

            Toast.makeText(getApplicationContext(), "No Internet Connection Please Check Your Internet Connection..!!", Toast.LENGTH_SHORT).show();

        }


    }

}
