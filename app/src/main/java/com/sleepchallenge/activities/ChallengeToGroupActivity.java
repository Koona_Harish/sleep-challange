package com.sleepchallenge.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.ChallengeToGroupAdapter;
import com.sleepchallenge.models.MyGroupModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.DividerItemDecorator;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ChallengeToGroupActivity extends AppCompatActivity implements View.OnClickListener{
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    RecyclerView.LayoutManager layoutManager;
    Typeface thinFont,regularFont;
    String user_id, user_type, token, device_ID;
    RecyclerView all_groups_recyclerView;
    SearchView all_group_search;
    ChallengeToGroupAdapter allGroupAdapter;
    ArrayList<MyGroupModel> allGroupList;
    String challengedate="",noofdays="",hours="",timestr="",challengetype="",remainderstr="";
    LinearLayout linear_sponsor, linear_chllenge, linear_messges;
    TextView count_sponsor_text, count_challenge_text, count_mesages_text;
    ImageView close_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_to_group);
        thinFont = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.thinFont));
        regularFont = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.regularFont));
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_ID = userDetails.get(UserSessionManager.DEVICE_ID);
        checkInternet = NetworkChecking.isConnected(this);

        challengedate=getIntent().getStringExtra("challengedate");
        hours=getIntent().getStringExtra("hours");
        timestr=getIntent().getStringExtra("time");
        challengetype=getIntent().getStringExtra("challengetype");
        remainderstr=getIntent().getStringExtra("remainder");
        if(challengetype.equalsIgnoreCase("Weekly"))
        {
            noofdays=getIntent().getStringExtra("noofdays");
        }

        Log.v("Noof days","/////"+challengetype+"daysstr"+noofdays);

        count_sponsor_text = findViewById(R.id.count_sponsor_text);
        count_challenge_text = findViewById(R.id.count_challenge_text);
        count_mesages_text = findViewById(R.id.count_mesages_text);
        linear_sponsor = findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge = findViewById(R.id.linear_chllenge);
        close_btn=findViewById(R.id.close);
        close_btn.setOnClickListener(this);
        linear_chllenge.setOnClickListener(this);
        linear_messges = findViewById(R.id.linear_messges);
        View view = findViewById(R.id.custom_tab);
        linear_messges.setOnClickListener(this);

        all_group_search= findViewById(R.id.all_group_search);
        all_groups_recyclerView = findViewById(R.id.recycler_all_groups);

        all_group_search.setIconified(false);
        all_group_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                all_group_search.setIconified(false);
            }
        });
        all_group_search.clearFocus();
        all_group_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                allGroupAdapter.getFilter().filter(query);
                return false;
            }
        });
        getAllGroups();
        getCount();
    }

    private void getAllGroups() {
        if (checkInternet) {
            Dialog.showProgressBar(this, "Loading");

            String allGroupurl =   AppUrls.BASE_URL + AppUrls.GET_GROUPS_FOR_CHALLENGE+"?user_id="+user_id+"&user_type="+user_type;
            Log.d("ALLGROUPURL", allGroupurl);

            StringRequest strAllMember = new StringRequest(Request.Method.GET, allGroupurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Dialog.hideProgressBar();
                    Log.d("ALLGROUPRESP", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100"))
                        {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            allGroupList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                MyGroupModel mygroup = new MyGroupModel(jdataobj);
                                allGroupList.add(mygroup);
                            }
                            allGroupAdapter = new ChallengeToGroupAdapter(allGroupList, ChallengeToGroupActivity.this, R.layout.row_my_group,challengedate,challengetype,hours,timestr,noofdays,remainderstr);
                            layoutManager = new LinearLayoutManager(ChallengeToGroupActivity.this);
                            all_groups_recyclerView.setNestedScrollingEnabled(false);
                            all_groups_recyclerView.setLayoutManager(layoutManager);
                            RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(ChallengeToGroupActivity.this, R.drawable.recycler_view_divider));
                            all_groups_recyclerView.addItemDecoration(dividerItemDecoration);

                            all_groups_recyclerView.setAdapter(allGroupAdapter);
                        }
                        if (successResponceCode.equals("10200"))
                        {
                            Toast.makeText(ChallengeToGroupActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                        }
                        if (successResponceCode.equals("10300"))
                        {
                            Toast.makeText(ChallengeToGroupActivity.this, "No Data Found..!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Dialog.hideProgressBar();
                    Log.e("Error", error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strAllMember.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ChallengeToGroupActivity.this);
            requestQueue.add(strAllMember);
        } else {
            Toast.makeText(ChallengeToGroupActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getCount() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("CountUrl", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("COUNTRESP", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    int call_weekly_loop_ws = jsonArray.getInt("call_weekly_loop_ws");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    int call_special_event_ws = jsonArray.getInt("call_special_event_ws");

                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);


                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(ChallengeToGroupActivity.this);
            requestQueue.add(stringRequest);
        } else {
            // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == linear_chllenge) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
                Intent sponsor = new Intent(ChallengeToGroupActivity.this, ChallengeRequestActivity.class);
                startActivity(sponsor);

            } else {
                // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }

        if (view == linear_messges) {

        }
        if (view == linear_sponsor) {

        }
        if(view==close_btn){
            finish();
        }
    }
}
