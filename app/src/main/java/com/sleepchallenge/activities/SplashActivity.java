package com.sleepchallenge.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 4000;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    VideoView videoView;
    String device_id;
    UserSessionManager sessionManager;
    private boolean checkInternet;
    String user_id = "", country_code, mobile;


    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        sessionManager = new UserSessionManager(SplashActivity.this);
        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        sessionManager.createDeviceId(device_id);

        videoView = findViewById(R.id.videoView);
        String path = "android.resource://" + getPackageName() + "/" + R.raw.splash_screen;
        Uri uri = Uri.parse(path);
        videoView.setVideoURI(uri);
        videoView.requestFocus();
        videoView.start();

        if (checkLocationPermission()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!sessionManager.isUserLoggedIn()) {
                        Intent intent = new Intent(SplashActivity.this, WelcomeActivity.class);
                        startActivity(intent);
                    } else {
                        getStatus();
                    }
                }
            }, SPLASH_TIME_OUT);
        }
    }

    private void getStatus() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_STATUS + user_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String userid = jsonObject1.getString("user_id");
                                    String user_type = jsonObject1.getString("user_type");
                                    String email = jsonObject1.getString("email");
                                    country_code = jsonObject1.getString("country_code");
                                    mobile = jsonObject1.getString("mobile");
                                    String status = jsonObject1.getString("status");
                                    String is_mobile_verified = jsonObject1.getString("is_mobile_verified");
                                    String is_email_verified = jsonObject1.getString("is_email_verified");
                                    String payment_info = jsonObject1.getString("payment_info");

                                    if (is_mobile_verified.equals("0")) {
                                        getOTP();
                                    } else if (status.equals("DEACTIVATED")) {
                                        Intent i = new Intent(SplashActivity.this, WelcomeActivity.class);
                                        startActivity(i);
                                        finish();
                                    } else if (status.equals("BLOCKED_ADMIN")) {
                                        Toast.makeText(getApplicationContext(), "Your Account is Blocked,Please Contact Admin..!", Toast.LENGTH_SHORT).show();
                                    } else if (status.equals("DISABLED")) {
                                        Toast.makeText(getApplicationContext(), "Your Account is Disabled,Please Contact Admin...!", Toast.LENGTH_SHORT).show();
                                    } else if (payment_info.equals("0") && !email.equals("")) {
                                        Intent intent = new Intent(SplashActivity.this, PayPalActivity.class);
                                        intent.putExtra("activity", "Login");
                                        intent.putExtra("paymentAmount", "1");
                                        startActivity(intent);
                                    } else if (userid.equals("")) {
                                        Intent i = new Intent(SplashActivity.this, WelcomeActivity.class);
                                        startActivity(i);
                                        finish();
                                    } else if (email.equals("") && payment_info.equals("0") && !user_type.equals("")) {
                                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                    } else if (user_type.equals("")) {
                                        Intent intent = new Intent(SplashActivity.this, WelcomeActivity.class);
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                                        intent.putExtra("USERTYPE", user_type);
                                        startActivity(intent);

                                    }
                                }
                                if (successResponceCode.equals("10200")) {
                                    Toast.makeText(SplashActivity.this, "Invalid input", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    Toast.makeText(SplashActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            if (!sessionManager.isUserLoggedIn()) {
                Intent intent = new Intent(SplashActivity.this, WelcomeActivity.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
            }
            //Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    private void getOTP() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Dialog.showProgressBar(this, "Loading...");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SENDOTP,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Dialog.hideProgressBar();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    Toast.makeText(SplashActivity.this, "OTP has been Sent to your registered Mobile Number", Toast.LENGTH_SHORT).show();
                                    Dialog.hideProgressBar();
                                    Intent i = new Intent(SplashActivity.this, AccountVerificationActivity.class);
                                    i.putExtra("mobile", mobile);
                                    i.putExtra("countryCode", "+" + country_code);
                                    i.putExtra("user_id", user_id);
                                    startActivity(i);
                                    finish();
                                }
                                if (successResponceCode.equals("10200")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(getApplicationContext(), "User Not Exist..", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10400")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(getApplicationContext(), "Unable to Send Otp..", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Dialog.hideProgressBar();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Dialog.hideProgressBar();
                            error.getMessage();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("country_code", country_code);
                    params.put("mobile", mobile);
                    params.put("device_id", device_id);
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(SplashActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Check Your Internet Connection.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private boolean checkLocationPermission() {
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int storagePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int audioPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
       int telephonePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
      //  int sms = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (audioPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO);
        }
        if (telephonePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
       /* if (sms != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }*/
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
//                Log.d("DFDFDSFDSAFF", grantResults.toString());
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (!sessionManager.isUserLoggedIn()) {
                                    Intent intent = new Intent(SplashActivity.this, WelcomeActivity.class);
                                    startActivity(intent);
                                } else {
                                    getStatus();
                                }
                            }
                        }, SPLASH_TIME_OUT);
                    } else {

                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            showDialogOK(new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkLocationPermission();
                                            break;

                                        case DialogInterface.BUTTON_NEGATIVE:
                                            break;
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(this, "Goto Settings And Enable Permissions", Toast.LENGTH_SHORT).show();
                            checkLocationPermission();
                        }
                    }
                }
            }
        }
    }

    private void showDialogOK(DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this).setMessage("External Storage And Location Permissions Required For This App").setPositiveButton("OK", okListener).create().show();
    }

}
