package com.sleepchallenge.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.ChallengeRequestAdapter;
import com.sleepchallenge.models.ChallegneRequestModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.DividerItemDecorator;
import com.sleepchallenge.utils.MovableFloatingActionButton;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ChallengeRequestActivity extends AppCompatActivity  implements View.OnClickListener
{
    ImageView back_img;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    RecyclerView challegneReq_recyclerView;
    ArrayList<ChallegneRequestModel> challegneReqList;
    ChallengeRequestAdapter challengeReqAdapter;
    RecyclerView.LayoutManager layoutManager;
    String user_id, user_type, token, device_ID;
    TextView no_challenge_text;
    Typeface specialFont, boldFont,thinFont,regularFont;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_request);
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        userSessionManager = new UserSessionManager(ChallengeRequestActivity.this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_ID = userDetails.get(UserSessionManager.DEVICE_ID);
        checkInternet = NetworkChecking.isConnected(this);
        challegneReq_recyclerView =  findViewById(R.id.challegneReq_recyclerView);
        TextView my_request=findViewById(R.id.my_request);
        Typeface regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        my_request.setTypeface(regularFont);
        back_img=findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        MovableFloatingActionButton fab = findViewById(R.id.fab);
        CoordinatorLayout.LayoutParams lp  = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
        fab.setCoordinatorLayout(lp);
        fab.setOnClickListener(this);
        TextView title_text=findViewById(R.id.title_text);
        title_text.setTypeface(boldFont);
        no_challenge_text=findViewById(R.id.no_challenge_text);
        no_challenge_text.setTypeface(specialFont);
      getChallengeRequest();
    }

    private void getChallengeRequest()
    {
        if (checkInternet)
        {
            Dialog.showProgressBar(ChallengeRequestActivity.this, "Loading");
            String chllegneReqUrl= AppUrls.BASE_URL+AppUrls.GET_CHALLENGE_REQUEST+"?id="+user_id;
            Log.d("CHALLREQUESTURL",chllegneReqUrl);
            StringRequest strAllMember = new StringRequest(Request.Method.GET, chllegneReqUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Dialog.hideProgressBar();
                    Log.d("CHALLQUESTRESP", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100"))
                        {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            challegneReqList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++)
                            {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                ChallegneRequestModel challegneRequst = new ChallegneRequestModel(jdataobj);
                                challegneReqList.add(challegneRequst);
                            }
                            challengeReqAdapter = new ChallengeRequestAdapter(challegneReqList, ChallengeRequestActivity.this, R.layout.row_challegne_request);
                            layoutManager = new LinearLayoutManager(getApplicationContext());
                            challegneReq_recyclerView.setNestedScrollingEnabled(false);
                            challegneReq_recyclerView.setLayoutManager(layoutManager);
                            RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(ChallengeRequestActivity.this, R.drawable.recycler_view_divider));
                            challegneReq_recyclerView.addItemDecoration(dividerItemDecoration);

                           challegneReq_recyclerView.setAdapter(challengeReqAdapter);
                        }
                        if (successResponceCode.equals("10200"))
                        {
                            no_challenge_text.setVisibility(View.VISIBLE);
                            no_challenge_text.setText("No Requests Found..!");

                        }
                        if (successResponceCode.equals("10300"))
                        {
                            no_challenge_text.setVisibility(View.VISIBLE);
                            no_challenge_text.setText("Invalid Input..!");

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Dialog.hideProgressBar();
                    Log.e("Error", error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strAllMember.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strAllMember);
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view)
    {
      if(view==back_img)
      {
          finish();
      }

      if(view.getId()==R.id.fab)
    {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            Intent intent = new Intent(ChallengeRequestActivity.this, MyRequestChallenge.class);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }
    }
}
