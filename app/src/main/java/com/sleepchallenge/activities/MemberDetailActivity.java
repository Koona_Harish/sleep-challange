package com.sleepchallenge.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.AllChallangCompletedAdapter;
import com.sleepchallenge.adapters.GrpInMemberDetailAdapter;
import com.sleepchallenge.models.AllChallengesStatusModel;
import com.sleepchallenge.models.GrpInMemberDetailModel;
import com.sleepchallenge.models.MySponsoringModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.DividerItemDecorator;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MemberDetailActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView back_img, send_message, profile_pic_iv;
    TextView userName, member_ranking_text, win, loss, total_per, follow_text, sponsor_text, block_text, send_mail, make_call, group_title, member_grp_more_text;
    UserSessionManager sessionManager;
    private boolean checkInternet;
    String user_id, user_type, device_id, token, member_id, member_name, member_user_type, profile_pic, md_user_name, overall_rank, member_mail_id,
            member_mobile_call, member_country_code_call;
    int block_status;
    Typeface regularFont, boldFont,specialFont;
    RelativeLayout ll_grp_text_title,ll_sponsor_text_title;
    RecyclerView recycler_grp_in_memberdetail;
    GrpInMemberDetailAdapter grpInMemberDetailAdapter;
    ArrayList<GrpInMemberDetailModel> allGroupList;
    RecyclerView.LayoutManager layoutManager;
    android.app.Dialog addMoneydialog;
    RecyclerView recycler_completed;
    AllChallangCompletedAdapter allChallangCompletAdapter;
    ArrayList<AllChallengesStatusModel> allchlngComplet = new ArrayList<AllChallengesStatusModel>();
    LinearLayoutManager completedlayout;
    LinearLayout completed_layout;
    TextView completed_text,sponsor_title;

    //Sponsor login and  Detail for sponser
    ArrayList<MySponsoringModel> mySponsoringList;
    RecyclerView my_sponsoring_recyclerView;
    SponsoringAdapter sponsorAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_detail);

        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));
        checkInternet = NetworkChecking.isConnected(this);

        sessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        recycler_completed=findViewById(R.id.recycler_completed);
        my_sponsoring_recyclerView=findViewById(R.id.my_sponsoring_recyclerView);
        completed_text = findViewById(R.id.completed_text);
        completed_text.setTypeface(boldFont);
        sponsor_title = findViewById(R.id.sponsor_title);
        sponsor_title.setTypeface(specialFont);
        completed_layout=findViewById(R.id.ll_completed);
        Bundle bundle = getIntent().getExtras();
        member_id = bundle.getString("MEMBER_ID");
        member_name = bundle.getString("MEMBER_NAME");
        member_user_type = bundle.getString("member_user_type");
        Log.d("MEMID", member_id + "\n" + member_user_type);

        ll_sponsor_text_title=findViewById(R.id.ll_sponsor_text_title);

        if(member_user_type.equalsIgnoreCase("SPONSOR"))
        {
            ll_sponsor_text_title.setVisibility(View.VISIBLE);
               getspopnSponsoring();
        }

        userName = findViewById(R.id.userName);
        userName.setTypeface(boldFont);
        member_ranking_text = findViewById(R.id.member_ranking_text);
        member_ranking_text.setTypeface(boldFont);
        win = findViewById(R.id.win);
        win.setTypeface(regularFont);
        loss = findViewById(R.id.loss);
        loss.setTypeface(regularFont);
        total_per = findViewById(R.id.total_per);
        total_per.setTypeface(specialFont);
        follow_text = findViewById(R.id.follow_text);
        follow_text.setOnClickListener(this);
        follow_text.setTypeface(regularFont);
        sponsor_text = findViewById(R.id.sponsor_text);
        sponsor_text.setOnClickListener(this);
        sponsor_text.setTypeface(regularFont);
        block_text = findViewById(R.id.block_text);
        block_text.setOnClickListener(this);
        block_text.setTypeface(regularFont);
        send_mail = findViewById(R.id.send_mail);
        send_mail.setOnClickListener(this);
        send_mail.setTypeface(regularFont);
        make_call = findViewById(R.id.make_call);
        make_call.setOnClickListener(this);
        make_call.setTypeface(regularFont);

        group_title = findViewById(R.id.group_title);
        group_title.setTypeface(boldFont);
        member_grp_more_text = findViewById(R.id.member_grp_more_text);
        member_grp_more_text.setTypeface(boldFont);
        member_grp_more_text.setOnClickListener(this);
        ll_grp_text_title = findViewById(R.id.ll_grp_text_title);


        back_img = findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        send_message = findViewById(R.id.send_message);
        send_message.setOnClickListener(this);
        profile_pic_iv = findViewById(R.id.profile_pic_iv);
        profile_pic_iv.setOnClickListener(this);

        recycler_grp_in_memberdetail = findViewById(R.id.recycler_grp_in_memberdetail);
        recycler_completed.setHasFixedSize(true);
        completedlayout=new LinearLayoutManager(MemberDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recycler_completed.setLayoutManager(completedlayout);
        allchlngComplet.clear();
        allChallangCompletAdapter = new AllChallangCompletedAdapter(allchlngComplet, MemberDetailActivity.this, R.layout.row_my_challenges,"Individual");

        getMemberDetail();
        getFollowingStatus();
       if(member_user_type.equalsIgnoreCase("USER"))
        getAllCompletChallangesMore(0);

    }

    private void getMemberDetail() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Dialog.showProgressBar(MemberDetailActivity.this, "Loading Data...");
            String user_url = AppUrls.BASE_URL + AppUrls.GET_USER_MEMBER_DETAIL + "?user_id=" + user_id + "&profile_id=" + member_id;
            Log.d("USERDetailsUrl", user_url);
            StringRequest strRe = new StringRequest(Request.Method.GET, user_url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Dialog.hideProgressBar();

                            Log.d("USERDetailsResp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    JSONObject getData = jsonObject.getJSONObject("data");
                                    block_status = getData.getInt("is_blocked");
                                    JSONObject bsicInfo = getData.getJSONObject("basic_info");
                                    member_id = bsicInfo.getString("id");
                                    member_user_type = bsicInfo.getString("user_type");
                                    md_user_name = bsicInfo.getString("user_name");
                                    String convert_user_name = firstLetterCaps(md_user_name);
                                    userName.setText(convert_user_name);
                                    overall_rank = bsicInfo.getString("overall_rank");
                                    member_ranking_text.setText(overall_rank);
                                    String win_challenges = bsicInfo.getString("win_challenges");
                                    win.setText(win_challenges);
                                    String rank_lost_challenges = bsicInfo.getString("rank_lost_challenges");
                                    loss.setText(rank_lost_challenges);
                                    member_mail_id = bsicInfo.getString("email");
                                    member_mobile_call = bsicInfo.getString("mobile");
                                    member_country_code_call = bsicInfo.getString("country_code");
                                    //user_block
                                    if (block_status == 1) {
                                        block_text.setText("UnBlock");
                                        block_text.setTextColor(Color.parseColor("#ffffff"));
                                        //   block_text.setBackgroundResource(R.color.days_incomplet);
                                        block_text.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                getUnBlockUser();
                                            }
                                        });
                                    } else {
                                        block_text.setText("Block");
                                        //  block_text.setBackgroundResource(R.drawable.background_for_block);
                                        block_text.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                getBlockUser();
                                            }
                                        });
                                    }

                                    String winning_percentage = bsicInfo.getString("winning_percentage");
                                    if (winning_percentage.equals(null) || winning_percentage.equals("null")) {
                                        total_per.setText(Html.fromHtml("0%"));
                                    } else {
                                        total_per.setText(Html.fromHtml(winning_percentage + "%"));
                                    }
                                    profile_pic = AppUrls.BASE_IMAGE_URL + bsicInfo.getString("profile_pic");
                                    Picasso.with(MemberDetailActivity.this)
                                            .load(profile_pic)
                                            .placeholder(R.drawable.members_dummy)
                                            .into(profile_pic_iv);

                                    //Group SECTION//
                                    JSONArray jgrpArray = getData.getJSONArray("group_details");
                                    int member_grp_count = getData.getInt("group_counts");
                                   /* if(member_grp_count==0)
                                    {
                                        ll_grp_text_title.setVisibility(View.GONE);
                                    }*/
                                    Log.d("group_counts", String.valueOf(member_grp_count));
                                    if (member_grp_count > 2) {
                                        ll_grp_text_title.setVisibility(View.VISIBLE);
                                        member_grp_more_text.setVisibility(View.VISIBLE);
                                    } else if (member_grp_count > 0) {
                                        ll_grp_text_title.setVisibility(View.VISIBLE);
                                    }
                                    Log.d("GROUPARRAY", jgrpArray.toString());
                                    allGroupList = new ArrayList<>();
                                    for (int i = 0; i < jgrpArray.length(); i++) {
                                        JSONObject jdataobj = jgrpArray.getJSONObject(i);
                                        GrpInMemberDetailModel grpInMemberDetailModel = new GrpInMemberDetailModel(jdataobj);
                                        allGroupList.add(grpInMemberDetailModel);
                                    }
                                    grpInMemberDetailAdapter = new GrpInMemberDetailAdapter(allGroupList, MemberDetailActivity.this, R.layout.row_my_group);
                                    layoutManager = new LinearLayoutManager(MemberDetailActivity.this);
                                    recycler_grp_in_memberdetail.setNestedScrollingEnabled(false);
                                    recycler_grp_in_memberdetail.setLayoutManager(layoutManager);
                                    RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(MemberDetailActivity.this, R.drawable.recycler_view_divider));
                                    recycler_grp_in_memberdetail.addItemDecoration(dividerItemDecoration);
                                    recycler_grp_in_memberdetail.setAdapter(grpInMemberDetailAdapter);
                                }
                                if (successResponceCode.equals("10200")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(MemberDetailActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Dialog.hideProgressBar();
                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("USERDetails_Header", "HEADER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strRe);
        } else {
            Dialog.hideProgressBar();
            Toast.makeText(MemberDetailActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void getFollowingStatus() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.MEMBER_FOLLOWING_STATUS;
            Log.d("FOLLOWSTATUSURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("FOLOWSTATUSMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String status = jobcode.getString("status");
                        String response_code = jobcode.getString("response_code");
                        if (status.equals("1")) {
                            follow_text.setText("UnFollow");
                            follow_text.setPadding(10,10,15,10);
                            follow_text.setTextColor(getResources().getColor(R.color.auth_text_color));
                            follow_text.setBackgroundResource(R.drawable.member_detail_textview_background);
                            follow_text.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getUnFollowUser();
                                }
                            });
                        } else {

                            follow_text.setText("Follow");
                            follow_text.setPadding(10,10,15,10);
                            follow_text.setTextColor(getResources().getColor(R.color.gray));
                            follow_text.setBackgroundResource(R.drawable.member_detail_textview_background);
                            follow_text.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getFollowUser();
                                }
                            });
                        }
                        if (response_code.equals("10200")) {
                            follow_text.setText("UnFollow");
                            follow_text.setPadding(10,10,15,10);
                            follow_text.setTextColor(getResources().getColor(R.color.auth_text_color));
                            follow_text.setBackgroundResource(R.drawable.member_detail_textview_background);

                        }
                        if (response_code.equals("10100")) {
                            follow_text.setText("Follow");
                            follow_text.setPadding(10,10,15,10);
                            follow_text.setTextColor(getResources().getColor(R.color.gray));
                            follow_text.setBackgroundResource(R.drawable.member_detail_textview_background);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    error.getMessage();
                }
            }) {
                @Override
                protected Map<String, String> getParams()  {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("from_user_id", user_id);
                    params.put("to_user_id", member_id);
                    Log.d("FOLLOWPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MemberDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(MemberDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    private void getFollowUser() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.FOLLOW_USER;
            Log.d("FOLLOWURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("FOLOWMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                            JSONObject jobj = jobcode.getJSONObject("data");
                            Toast.makeText(MemberDetailActivity.this, "Followed successfully", Toast.LENGTH_LONG).show();
                            getFollowingStatus();
                        }
                        if (response_code.equals("10200")) {

                            Toast.makeText(MemberDetailActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    error.getMessage();
                }
            }) {

                @Override
                protected Map<String, String> getParams()  {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("from_user_id", user_id);
                    params.put("to_user_id", member_id);
                    Log.d("FOLLOWPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MemberDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(MemberDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    public void getUnFollowUser() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.UNFOLLOW_USER;
            Log.d("UNFOLLOWURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("UNFOLOWMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");
                        if (response_code.equals("10100")) {
                            JSONObject jobj = jobcode.getJSONObject("data");
                            Toast.makeText(MemberDetailActivity.this, "UnFollow successfully", Toast.LENGTH_LONG).show();
                            getFollowingStatus();
                        }
                        if (response_code.equals("10200")) {

                            Toast.makeText(MemberDetailActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.getMessage();
                }
            }) {

                @Override
                protected Map<String, String> getParams()  {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("from_user_id", user_id);
                    params.put("to_user_id", member_id);
                    Log.d("UNFOLLOWPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MemberDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(MemberDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    private void getBlockUser() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.BLOCK_USER;
            Log.d("BLOCKURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("BLOCKMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                            //  JSONObject jobj = jobcode.getJSONObject("data");
                            Toast.makeText(MemberDetailActivity.this, "Blocked successfully", Toast.LENGTH_LONG).show();
                            getMemberDetail();
                        }
                        if (response_code.equals("10200")) {

                            Toast.makeText(MemberDetailActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    error.getMessage();
                }
            }) {

                @Override
                protected Map<String, String> getParams()  {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("blocked_user_id", member_id);
                    Log.d("blockPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MemberDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(MemberDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    private void getUnBlockUser() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.UNBLOCK_USER;
            Log.d("UNBLOCKURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("UNBLOCKMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {

                            Toast.makeText(MemberDetailActivity.this, "UnBlocked successfully", Toast.LENGTH_LONG).show();
                            getMemberDetail();
                        }
                        if (response_code.equals("10200")) {

                            Toast.makeText(MemberDetailActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.getMessage();
                }
            }) {

                @Override
                protected Map<String, String> getParams()  {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("blocked_user_id", member_id);
                    Log.d("unblockPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MemberDetailActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(MemberDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }


    private void getAllCompletChallangesMore(int defaultPageNo) {

        checkInternet = NetworkChecking.isConnected(MemberDetailActivity.this);
        if (checkInternet) {
            allchlngComplet.clear();

            String url = AppUrls.BASE_URL + AppUrls.MORECHALLENGES + "?user_id=" + member_id + "&status=COMPLETED&page=" + defaultPageNo + "&type=INDIVIDUAL";
            Log.d("COMPURL",url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                Log.d("COMPRESP",response);
                                JSONObject jsonObject = new JSONObject(response);

                                String responceCode = jsonObject.getString("response_code");
                                if (responceCode.equals("10100")) {
                                    completed_layout.setVisibility(View.VISIBLE);
                                    Dialog.hideProgressBar();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("challenges");
                                   for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        AllChallengesStatusModel itemRunnAll = new AllChallengesStatusModel();
                                        itemRunnAll.setChallenge_id(jsonObject2.getString("challenge_id"));
                                        itemRunnAll.setUser_id(jsonObject2.getString("user_id"));
                                        itemRunnAll.setUser_type(jsonObject2.getString("user_type"));
                                        itemRunnAll.setOpponent_id(jsonObject2.getString("opponent_id"));
                                        itemRunnAll.setOpponent_type(jsonObject2.getString("opponent_type"));
                                        itemRunnAll.setRemainder(jsonObject2.getString("remainder"));
                                        itemRunnAll.setStart_time(jsonObject2.getString("start_time"));
                                        itemRunnAll.setChallenge_goal(jsonObject2.getString("challenge_goal"));
                                        itemRunnAll.setChallenger_group_id(jsonObject2.getString("challenger_group_id"));
                                        itemRunnAll.setPause_access(jsonObject2.getString("pause_access"));
                                        itemRunnAll.setIs_group_admin(jsonObject2.getString("is_group_admin"));
                                        itemRunnAll.setUser_name(jsonObject2.getString("user_name"));
                                        itemRunnAll.setOpponent_name(jsonObject2.getString("opponent_name"));
                                        itemRunnAll.setWinning_status(jsonObject2.getString("winning_status"));
                                        itemRunnAll.setWinning_amount(jsonObject2.getString("winning_amount"));
                                        itemRunnAll.setWinner(jsonObject2.getString("winner"));
                                        itemRunnAll.setStatus(jsonObject2.getString("status"));
                                        itemRunnAll.setAmount(jsonObject2.getString("amount"));
                                        itemRunnAll.setChallenge_type(jsonObject2.getString("challenge_type"));
                                        itemRunnAll.setStart_on(jsonObject2.getString("start_on"));
                                        itemRunnAll.setPaused_on(jsonObject2.getString("paused_on"));
                                        itemRunnAll.setPaused_by(jsonObject2.getString("paused_by"));
                                        itemRunnAll.setResume_on(jsonObject2.getString("resume_on"));
                                        itemRunnAll.setCompleted_on_txt(jsonObject2.getString("completed_on_txt"));
                                        itemRunnAll.setCompleted_on(jsonObject2.getString("completed_on"));
                                        itemRunnAll.setWinning_reward_type(jsonObject2.getString("winning_reward_type"));
                                        itemRunnAll.setWinning_reward_value(jsonObject2.getString("winning_reward_value"));
                                        itemRunnAll.setIs_scratched(jsonObject2.getString("is_scratched"));

                                        allchlngComplet.add(itemRunnAll);
                                    }

                                    recycler_completed.setAdapter(allChallangCompletAdapter);

                                }

                                if (responceCode.equals("10200")) {
                                    completed_layout.setVisibility(View.GONE);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    error.getMessage();
                }
            }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GGGGG_CHALL_HEAD", headers.toString());
                    return headers;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(MemberDetailActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(MemberDetailActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == back_img) {
            finish();
        }
        if (view == send_message)
        {
            Intent inten = new Intent(MemberDetailActivity.this, MessageDetailActivity.class);
            inten.putExtra("FROM_ID", member_id); // friend id
            inten.putExtra("FROM_NAME", member_name); // friend name
            inten.putExtra("FROM_TYPE", member_user_type);   // friend type
            inten.putExtra("FROM_PROFILEPIC", profile_pic); //progile pic
            inten.putExtra("FROM_TYPE", "USER"); //progile pic
            startActivity(inten);

        }

        if (view == sponsor_text)
        {
            addMoneydialog = new android.app.Dialog(MemberDetailActivity.this);
            addMoneydialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            addMoneydialog.setContentView(R.layout.custom_dialolg_sponsor_request);

            TextView dummy_text =  addMoneydialog.findViewById(R.id.dummy_text);
            dummy_text.setTypeface(regularFont);
            TextView member_name_title_text =  addMoneydialog.findViewById(R.id.member_name_title_text);
            TextView rank_text_title = addMoneydialog.findViewById(R.id.rank_text_title);
            TextView header =  addMoneydialog.findViewById(R.id.header);
            header.setText("Member Sponsor Details");
            header.setTypeface(regularFont);
            TextView member_name_text = addMoneydialog.findViewById(R.id.member_name_text);
            TextView member_rank_text =  addMoneydialog.findViewById(R.id.member_rank_text);
            final EditText member_sponsor_amount_edt = addMoneydialog.findViewById(R.id.member_sponsor_amount_edt);
            Button sponsor_submit_btn =  addMoneydialog.findViewById(R.id.sponsor_submit_btn);
            member_name_text.setText(md_user_name);
            member_name_text.setTypeface(regularFont);
            member_rank_text.setText(overall_rank);
            member_rank_text.setTypeface(regularFont);
            member_name_title_text.setText("Member Name");
            member_name_title_text.setTypeface(regularFont);
            rank_text_title.setTypeface(regularFont);
            sponsor_submit_btn.setTypeface(regularFont);
            addMoneydialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            addMoneydialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
            sponsor_submit_btn.setOnClickListener(new View.OnClickListener() {

                @Override

                public void onClick(View v) {
                    String amt_value = member_sponsor_amount_edt.getText().toString();
                    if (amt_value.equals("") || amt_value.equals("0")) {
                        Toast.makeText(MemberDetailActivity.this, "Please enter valid amount..!", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(MemberDetailActivity.this, PaymentDetailsActivity.class);
                        intent.putExtra("activty", "MemberDetail");
                        intent.putExtra("member_id", member_id);
                        intent.putExtra("member_user_type", member_user_type);
                        intent.putExtra("sponsorAmount", amt_value);
                        Log.d("SPONSOR", member_id + "\n" + member_user_type + "\n" + amt_value);
                        startActivity(intent);
                    }
                }

            });
            addMoneydialog.show();

        }
        if (view == send_mail) {
            String[] recipients = new String[]{member_mail_id, ""};
            Intent testIntent = new Intent(android.content.Intent.ACTION_SEND);
            testIntent.setType("message/rfc822");
            testIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
            testIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
            testIntent.putExtra(android.content.Intent.EXTRA_EMAIL, recipients);
            startActivity(testIntent);
        }
        if (view == make_call) {
            Log.d("call----", member_country_code_call + member_mobile_call);

            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + member_country_code_call + member_mobile_call));
            if (ActivityCompat.checkSelfPermission(MemberDetailActivity.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            startActivity(callIntent);
        }
        if(view==member_grp_more_text){
            Intent it=new Intent(this,AllGroupsActivity.class);
            it.putExtra("member_id", member_id);
            startActivity(it);
        }

    }

    static public String firstLetterCaps(String data) {
        String firstLetter = data.substring(0, 1).toUpperCase();
        String restLetters = data.substring(1).toLowerCase();
        return firstLetter + restLetters;
    }


    private void getspopnSponsoring()
    {
        checkInternet = NetworkChecking.isConnected(MemberDetailActivity.this);
        if (checkInternet)
        {
           String sponsorUrl=AppUrls.BASE_URL + AppUrls.MY_SPONSORINGS +"?user_id="+member_id+ AppUrls.USER_TYPE + member_user_type+"&status=ACCEPT";
           Log.d("SPPOURL",sponsorUrl);

            StringRequest strRe = new StringRequest(Request.Method.GET, sponsorUrl,new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            Log.d("SPURLREPRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100"))
                                {
                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    mySponsoringList = new ArrayList<>();
                                    for (int i = 0; i < jArr.length(); i++)
                                    {
                                        JSONObject jdataobj = jArr.getJSONObject(i);
                                        MySponsoringModel sponreq = new MySponsoringModel(jdataobj);
                                        mySponsoringList.add(sponreq);
                                    }
                                    sponsorAdapter = new SponsoringAdapter(MemberDetailActivity.this,mySponsoringList,  R.layout.row_my_sponsorings);
                                    layoutManager = new LinearLayoutManager(MemberDetailActivity.this);
                                    my_sponsoring_recyclerView.setNestedScrollingEnabled(false);
                                    my_sponsoring_recyclerView.setLayoutManager(layoutManager);
//                                    RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.recycler_view_divider));
//                                    my_sponsoring_recyclerView.addItemDecoration(dividerItemDecoration);
                                    my_sponsoring_recyclerView.setAdapter(sponsorAdapter);
                                }
                                if (successResponceCode.equals("10200")) {

                                    Toast.makeText(MemberDetailActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {

                                    Toast.makeText(MemberDetailActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(MemberDetailActivity.this);
            requestQueue.add(strRe);
        } else {

            Toast.makeText(MemberDetailActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }


    //ADAPTER

    class SponsoringAdapter extends RecyclerView.Adapter<MySponsoringHistoryHolder> {
        MemberDetailActivity context;
        int resource;
        ArrayList<MySponsoringModel> mysponingList;
        LayoutInflater lInfla;

        public SponsoringAdapter(MemberDetailActivity context, ArrayList<MySponsoringModel> mysponingList, int resource) {
            this.context = context;
            this.resource = resource;
            this.mysponingList = mysponingList;
            lInfla = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public MySponsoringHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View inf = lInfla.inflate(resource, parent, false);
            MySponsoringHistoryHolder rHol = new MySponsoringHistoryHolder(inf);
            return rHol;
        }

        @Override
        public void onBindViewHolder(MySponsoringHistoryHolder holder, int position) {

            holder.card.setCardBackgroundColor(Color.parseColor("#E0E0E0"));

            String sponsertypeto=mysponingList.get(position).to_sponsor_type;
            Log.d("sponsertypeto",mysponingList.get(position).to_sponsor_type);

            holder.name.setText(mysponingList.get(position).name);
            holder.name.setTypeface(boldFont);

            Log.d("PPPTAH",mysponingList.get(position).profile_pic);
            Picasso.with(context)
                    .load(mysponingList.get(position).profile_pic)
                    .placeholder(R.drawable.members_dummy)
                    .into(holder.sponcering_image);

            holder.status.setText(mysponingList.get(position).status);
            holder.status.setTypeface(regularFont);
            holder.amount.setText("$"+mysponingList.get(position).amount);
            holder.amount.setTypeface(regularFont);

            String timedate=parseDateToddMMyyyy(mysponingList.get(position).date);

            holder.date.setText(Html.fromHtml(timedate));
            holder.date.setTypeface(regularFont);

            holder.sponser_type_to.setText(sponsertypeto);
            holder.sponser_type_to.setTypeface(regularFont);

            if(sponsertypeto.equals("USER"))
            {
                //  holder.sponser_type_to.setText("Type : USER");
                Picasso.with(context)
                        .load(mysponingList.get(position).profile_pic)
                        .placeholder(R.drawable.members_dummy)
                        .into(holder.sponcering_image);
            }
            else
            {
                //  holder.sponser_type_to.setText("Type : GROUP");
                Picasso.with(context)
                        .load(mysponingList.get(position).profile_pic)
                        .placeholder(R.drawable.group_dummy)
                        .into(holder.sponcering_image);
            }

            if (mysponingList.get(position).status.equals("ACCEPT")) {
                holder.status.setTextColor(Color.parseColor("#38ac59"));
            } else if (mysponingList.get(position).status.equals("PENDING")) {
                holder.status.setTextColor(Color.parseColor("#FF933A3A"));
            } else if (mysponingList.get(position).status.equals("REJECT")) {
                holder.status.setTextColor(Color.RED);
            }
        }

        @Override
        public int getItemCount() {
            return mysponingList.size();
        }
    }


    //Holder

    public class MySponsoringHistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public TextView name, status, amount,date,sponser_type_to;
        public CircleImageView sponcering_image;
        public CardView card;

        public MySponsoringHistoryHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            name =  itemView.findViewById(R.id.name);
            status = itemView.findViewById(R.id.status);
            amount =  itemView.findViewById(R.id.amount);
            date = itemView.findViewById(R.id.date);
            sponser_type_to =  itemView.findViewById(R.id.sponser_type_to);
            sponcering_image =  itemView.findViewById(R.id.sponcering_image);
            card =  itemView.findViewById(R.id.card);


        }
        @Override
        public void onClick(View view) {

        }
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
