package com.sleepchallenge.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class PaymentDetailsActivity extends AppCompatActivity implements View.OnClickListener,RadioGroup.OnCheckedChangeListener{
     TextView payable_text,payment_text,challenge_date_label,challenge_date_txt,challenge_with_label,challenge_with_text,sleep_time_label,sleep_time_txt,
                sleep_start_time_label,sleep_start_time_txt,no_of_days_label,no_of_days_txt,note_label;
     RadioGroup payment_rg;
     Button pay_btn;
     ImageView close_btn;
     EditText note_et;
     int timeinsecs;
     private boolean checkInternet;
     RadioButton wallet_radio,paypal_radio;
    Typeface regularFont, boldFont, thinFont,specialFont;
    LinearLayout noofdays_layout,date_layout,challenge_with_layout,sleep_time_layout,sleepstart_layout,note_txt_ll;
    UserSessionManager userSessionManager;
    String deviceId, user_type, user_id, token;
    String walletAmount,paymentAmount, paymentAmount_sendChallenge = "";
    double wallet, challenge, result;
    String sendhours,sendtime,senddate,remainderstr;
    String member_id, member_name, member_user_type,group_id,group_user_type;
    String challengedate="",noofdays="",challengeid="",hours="",timestr="",challengetype="",challengerName="",payamount="",opponetId,opponenttype,span,paymentmode,activty="", promotion_id, promotionAmount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));
        TextView title = findViewById(R.id.title_text);
        title.setTypeface(boldFont);
         initilizeUI();
        activty=getIntent().getStringExtra("activty");
        if(activty.equalsIgnoreCase("MeAsaSponsorAPP"))
        {
            payamount=getIntent().getStringExtra("sponsorAmount");
            setVisibity(View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE);

        }
        else if(activty.equalsIgnoreCase("MemberDetail"))
        {
            payamount=getIntent().getStringExtra("sponsorAmount");
            member_id=getIntent().getStringExtra("member_id");
            member_user_type=getIntent().getStringExtra("member_user_type");
            setVisibity(View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE);
        }
        else if(activty.equalsIgnoreCase("GroupDetail"))
        {
            payamount=getIntent().getStringExtra("sponsorAmount");
            group_id=getIntent().getStringExtra("group_id");
            group_user_type=getIntent().getStringExtra("group_user_type");
            setVisibity(View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE);
        }
        else if(activty.equalsIgnoreCase("MyPromotionsActivity"))
        {
            promotion_id = getIntent().getStringExtra("promotion_id");
            payamount = getIntent().getStringExtra("promotionAmount");
            setVisibity(View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE);
        }
        else {
            setVisibity(View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE);
            String datestr=getIntent().getStringExtra("challengedate");
            String date[]=datestr.split(" ");
            challengedate=date[0];
            hours=getIntent().getStringExtra("hours");
            challengeid=getIntent().getStringExtra("challengeid");
            timestr=getIntent().getStringExtra("time");
            challengetype=getIntent().getStringExtra("challengetype");
            opponetId=getIntent().getStringExtra("challengerid");
            opponenttype=getIntent().getStringExtra("challengertype");
            if(challengetype.equalsIgnoreCase("Weekly"))
            {
                noofdays=getIntent().getStringExtra("noofdays");
                noofdays_layout.setVisibility(View.VISIBLE);
                no_of_days_txt.setText(noofdays);
            }
            challengerName=getIntent().getStringExtra("challengername");
            payamount=getIntent().getStringExtra("payamout");
            remainderstr=getIntent().getStringExtra("remainder");
            challenge_date_txt.setText(challengedate);
            challenge_with_text.setText(challengerName);
            sleep_time_txt.setText(hours);
            sleep_start_time_txt.setText(timestr);
        }
        payment_text.setText(payamount);
        //get Time zone
        TimeZone tz = TimeZone.getDefault();
        Calendar cal = GregorianCalendar.getInstance(tz);
        timeinsecs = (tz.getOffset(cal.getTimeInMillis()))/1000;
        Dialog.showProgressBar(this,"Getting Details");
        getWalletAmount();
    }

    @Override
    public void onClick(View v) {
        if(v==close_btn) {
            finish();
        }
        if(v==pay_btn){
            if(activty.equalsIgnoreCase("challengeto") || activty.equalsIgnoreCase("AcceptChallengePayment")){
                String hoursplit[] =hours.split(" ");
                String timesplit[] =timestr.split(":");
                sendhours=hoursplit[0];
                int time=Integer.parseInt(timesplit[0]);
                if(time==0 || time<10){
                    sendtime="0"+timesplit[0]+":"+timesplit[1];
                }else {
                    sendtime=timesplit[0]+":"+timesplit[1];
                }

                if(challengetype.equalsIgnoreCase("Weekly")){
                    String days[]=noofdays.split(" ");
                    span=days[0];

                }else{
                    span="1";

                }
                senddate=challengedate;

            }

            result = challenge - wallet;
            paymentAmount = String.valueOf(result);
            paymentAmount_sendChallenge=payamount;
            if (!paymentAmount_sendChallenge.equals("0"))
            {
                if (paypal_radio.isChecked())
                {
                    Intent intent = new Intent(PaymentDetailsActivity.this, PayPalActivity.class);
                    intent.putExtra("activity", activty);
                    if (wallet_radio.isChecked())
                    {
                        Log.v("amount","??===="+paymentAmount);
                        if (wallet > challenge) {
                            intent.putExtra("paymentAmount", "0");
                            intent.putExtra("walletAmount", paymentAmount_sendChallenge);
                            Log.v("amount","??00"+paymentAmount);
                        } else if (wallet < challenge) {
                            intent.putExtra("paymentAmount", paymentAmount);
                            intent.putExtra("walletAmount", walletAmount);
                            Log.v("amount","??"+paymentAmount);
                        } else if (wallet == challenge) {
                            intent.putExtra("paymentAmount", "0");
                            intent.putExtra("walletAmount", paymentAmount_sendChallenge);
                            Log.v("amount","?"+paymentAmount);
                        }
                    }
                    else
                    {
                        intent.putExtra("paymentAmount", paymentAmount_sendChallenge);
                        intent.putExtra("walletAmount", "0");
                        Log.v("amount","?@@@"+paymentAmount_sendChallenge);
                    }
                    if(activty.equalsIgnoreCase("challengeto")||activty.equalsIgnoreCase("AcceptChallengePayment")){
                        intent.putExtra("user_id", user_id);
                        intent.putExtra("user_type",user_type);
                        intent.putExtra("challenge_goal", sendhours);
                        intent.putExtra("challenge_type", challengetype);
                        intent.putExtra("challenge_date", senddate);
                        intent.putExtra("amount", payamount);
                        intent.putExtra("span", span);
                        intent.putExtra("opponent_id", opponetId);
                        intent.putExtra("opponent_type", opponenttype);
                        intent.putExtra("sleeptime", sendtime);
                        intent.putExtra("remainder", remainderstr);
                        intent.putExtra("activity", activty);
                        intent.putExtra("challengeid", challengeid);
                        Log.v("Values","//"+user_id+"//"+user_type+"//"+user_type+"//"+challengetype+"//"+opponenttype+"//"+opponetId+"//"+remainderstr+"//"+sendtime+"//"+sendhours+"//"+senddate+"//"+span);
                  }else if(activty.equalsIgnoreCase("MemberDetail")){
                        intent.putExtra("member_id", member_id);
                        intent.putExtra("member_user_type", member_user_type);
                        intent.putExtra("paymentAmount", paymentAmount_sendChallenge);
                        intent.putExtra("walletAmount", "0");
                    }else if(activty.equalsIgnoreCase("GroupDetail")){
                        intent.putExtra("group_id", group_id);
                        intent.putExtra("group_user_type", group_user_type);
                        intent.putExtra("paymentAmount", paymentAmount_sendChallenge);
                        intent.putExtra("walletAmount", "0");
                    }else if(activty.equalsIgnoreCase("MeAsaSponsorAPP")){
                        intent.putExtra("member_id", member_id);
                        intent.putExtra("member_user_type", "ACTIVITY");
                        intent.putExtra("paymentAmount", paymentAmount_sendChallenge);
                        intent.putExtra("walletAmount", "0");
                    }
                    else if(activty.equalsIgnoreCase("MyPromotionsActivity")){
                        intent.putExtra("promotionid", promotion_id);
                        intent.putExtra("paymentAmount", paymentAmount_sendChallenge);
                        intent.putExtra("walletAmount", "0");
                    }

                    startActivity(intent);

                } else if (wallet_radio.isChecked())
                {
                    Toast.makeText(this, "Paying Through Wallet..!", Toast.LENGTH_SHORT).show();
                    result = challenge - wallet;
                    paymentAmount = String.valueOf(result);
                    if (wallet > challenge || wallet == challenge) {
                        if(activty.equalsIgnoreCase("challengeto")){
                            sendChallenge();
                        }
                        else if(activty.equalsIgnoreCase("AcceptChallengePayment")){
                            acceptChallenge();
                        }else {
                            sponsorPay();
                        }
                    } else {
                        Intent intent = new Intent(PaymentDetailsActivity.this, PayPalActivity.class);
                        intent.putExtra("activity", activty);
                        if (wallet_radio.isChecked()) {
                            if (wallet > challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", paymentAmount_sendChallenge);
                            } else if (wallet < challenge) {
                                intent.putExtra("paymentAmount", paymentAmount);
                                intent.putExtra("walletAmount", walletAmount);
                            } else if (wallet == challenge) {
                                intent.putExtra("paymentAmount", "0");
                                intent.putExtra("walletAmount", paymentAmount_sendChallenge);
                            }
                        } else {
                            intent.putExtra("paymentAmount", paymentAmount_sendChallenge);
                            intent.putExtra("walletAmount", "0");
                        }
                        if(activty.equalsIgnoreCase("challengeto")||activty.equalsIgnoreCase("AcceptChallengePayment")){

                            intent.putExtra("user_id", user_id);
                            intent.putExtra("user_type", user_type);
                            intent.putExtra("challenge_goal", sendhours);
                            intent.putExtra("span", span);
                            intent.putExtra("date", senddate);
                            intent.putExtra("opponent_id", opponetId);
                            intent.putExtra("opponent_type", opponenttype);
                            intent.putExtra("sleeptime", sendtime);
                            intent.putExtra("remainder", remainderstr);
                            intent.putExtra("activity", activty);
                            intent.putExtra("challengeid", challengeid);
                        }else if(activty.equalsIgnoreCase("MemberDetail")){
                            intent.putExtra("member_id", member_id);
                            intent.putExtra("member_user_type", member_user_type);
                        }else if(activty.equalsIgnoreCase("GroupDetail")){
                            intent.putExtra("group_id", group_id);
                            intent.putExtra("group_user_type", group_user_type);
                        }else if(activty.equalsIgnoreCase("MeAsaSponsorAPP")){
                            intent.putExtra("member_id", member_id);
                            intent.putExtra("member_user_type", "ACTIVITY");
                        }
                        else if(activty.equalsIgnoreCase("MyPromotionsActivity")){
                            intent.putExtra("promotionid", promotion_id);
                            intent.putExtra("paymentAmount", paymentAmount_sendChallenge);
                        }

                        startActivity(intent);
                        Log.v("Values","//"+challengeid+"//"+user_id+"//"+user_type+"//"+user_type+"//"+challengetype+"//"+opponenttype+"//"+opponetId+"//"+remainderstr+"//"+sendtime+"//"+sendhours+"//"+senddate+"//"+span);
                    }
                }
            } else {
                Toast.makeText(this, "Invalid Amount..!", Toast.LENGTH_SHORT).show();
            }
           }


    }


    private void acceptChallenge() {
        checkInternet = NetworkChecking.isConnected(PaymentDetailsActivity.this);
        if (checkInternet) {
            Log.d("URL", AppUrls.BASE_URL + AppUrls.ACEPT_CHALLENGE_REQUEST);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ACEPT_CHALLENGE_REQUEST,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //  progressDialog.dismiss();
                            Log.d("AcceptChallenge", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    // progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String challenge_id = jsonObject1.getString("challenge_id");
                                    Toast.makeText(PaymentDetailsActivity.this,"Wallet payment successfully",Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(PaymentDetailsActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                if (response_code.equals("10200")) {
                                    // progressDialog.dismiss();
                                    Toast.makeText(PaymentDetailsActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                // progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //  progressDialog.cancel();
                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("challenge_id", challengeid);

                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("gateway_response", "");
                    params.put("amount", paymentAmount_sendChallenge);
                    params.put("payment_id", "");
                    params.put("payment_mode", "WALLET");
                    params.put("amount_wallet", paymentAmount_sendChallenge);
                    params.put("amount_paypal", "0");
                    params.put("timezone_in_sec",String.valueOf(timeinsecs));

                    Log.d("ACCEPT", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(PaymentDetailsActivity.this);
            requestQueue.add(stringRequest);
        } else {
            // progressDialog.cancel();
            Toast.makeText(PaymentDetailsActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendChallenge() {
        checkInternet = NetworkChecking.isConnected(PaymentDetailsActivity.this);
        if (checkInternet) {


            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SEND_CHALLENGES,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("CHALLENGE_RESPONSE", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {

                                    Toast.makeText(PaymentDetailsActivity.this, "Challenge Request Sent", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(PaymentDetailsActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();

                                }
                                if (successResponceCode.equals("10200")) {

                                    Toast.makeText(PaymentDetailsActivity.this, "Challenge Request Faild", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {

                                    Toast.makeText(PaymentDetailsActivity.this, "Invalid Amount", Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams()  {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("challenge_goal", sendhours);
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("challenge_type", challengetype);
                    params.put("challenge_date", senddate);
                    params.put("amount", paymentAmount_sendChallenge);
                    params.put("opponent_id", opponetId);
                    params.put("opponent_type", opponenttype);
                    params.put("day_span", span);
                    params.put("gateway_response", "");
                    params.put("payment_id", "");
                    params.put("payment_mode", "WALLET");
                    params.put("amount_wallet", paymentAmount_sendChallenge);
                    params.put("amount_paypal", "0");
                    params.put("timezone_in_sec", String.valueOf(timeinsecs));
                    params.put("start_time", sendtime);
                    params.put("remainder", remainderstr);


                    Log.d("REPORT_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(PaymentDetailsActivity.this);
            requestQueue.add(stringRequest);

        } else {

            Toast.makeText(PaymentDetailsActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }



    private void getWalletAmount()
    {
        checkInternet = NetworkChecking.isConnected(PaymentDetailsActivity.this);
        if (checkInternet) {
            Log.d("WALLETAMOUNT", AppUrls.BASE_URL + AppUrls.WALLET_AMOUNT + user_id + "&user_type=" + user_type);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.WALLET_AMOUNT + user_id + "&user_type=" + user_type,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("WalletResponce", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    Dialog.hideProgressBar();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    walletAmount = jsonObject1.getString("wallet_amt");
                                    if (walletAmount.equals("0")) {
                                        wallet_radio.setVisibility(View.GONE);
                                        payment_rg.check(paypal_radio.getId());
                                    }
                                    wallet = Double.parseDouble(walletAmount);

                                    wallet_radio.setText("Wallet " + "($ " + walletAmount + " USD)");
                                }

                                if (response_code.equals("10200")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(PaymentDetailsActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Dialog.hideProgressBar();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Dialog.hideProgressBar();
                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(PaymentDetailsActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(PaymentDetailsActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }


    private void sponsorPay() {
        checkInternet = NetworkChecking.isConnected(PaymentDetailsActivity.this);
        if (checkInternet) {
            String url="";
            if(activty.equalsIgnoreCase("MyPromotionsActivity")){
                url=AppUrls.BASE_URL + AppUrls.PROMOTION_PAY;
            }else {
                url=AppUrls.BASE_URL + AppUrls.SPONSOR_PAY;
            }
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("SPONSORRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {

                                    Toast.makeText(PaymentDetailsActivity.this, "Transcation Uploaded to server..!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(PaymentDetailsActivity.this, MainActivity.class);
                                    startActivity(intent);
                                }
                                if (response_code.equals("10200")) {

                                    Toast.makeText(PaymentDetailsActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    if (activty.equals("MemberDetail")){
                        params.put("to_user_id", member_id);
                        params.put("to_user_type", member_user_type);
                    }else if (activty.equals("GroupDetail")){
                        params.put("to_user_id", group_id);
                        params.put("to_user_type", group_user_type);
                    }
                    else if (activty.equals("MeAsaSponsorAPP")){
                        params.put("to_user_id", "59def9dce25e4081ac0f1093");
                        params.put("to_user_type", "APP");
                    }
                    else if (activty.equals("MeAsSponsorActivity")){
                        params.put("to_user_id", group_id);
                        params.put("to_user_type", "ACTIVITY");
                    }else if(activty.equalsIgnoreCase("MyPromotionsActivity")){
                        params.put("promotion_id", promotion_id);
                    }

                    params.put("payment_id", "");
                    params.put("amount", paymentAmount_sendChallenge);
                    params.put("payment_mode", "WALLET");
                    params.put("amount_wallet", paymentAmount_sendChallenge);
                    params.put("amount_paypal", "0");
                    if (wallet_radio.isChecked())
                    {
                        if (wallet > challenge) {
                            if (wallet_radio.isChecked()) {
                                params.put("amount_wallet", paymentAmount_sendChallenge);
                                params.put("amount_paypal", "0");
                                params.put("payment_mode", "WALLET");
                            } else if (paypal_radio.isChecked()) {
                                params.put("amount_wallet", "0");
                                params.put("amount_paypal", paymentAmount_sendChallenge);
                                params.put("payment_mode", "PAYPAL");
                            }

                        } else if (wallet < challenge) {
                            if (wallet_radio.isChecked()) {
                                params.put("amount_wallet", walletAmount);
                                params.put("amount_paypal", paymentAmount);
                                params.put("payment_mode", "WALLET_PAYPAL");
                            } else if (paypal_radio.isChecked()) {
                                params.put("amount_wallet", "0");
                                params.put("amount_paypal", paymentAmount_sendChallenge);
                                params.put("payment_mode", "PAYPAL");
                            }
                        } else if (wallet == challenge) {
                            if (wallet_radio.isChecked()) {
                                params.put("amount_wallet", walletAmount);
                                params.put("amount_paypal", "0");
                                params.put("payment_mode", "WALLET");
                            } else if (paypal_radio.isChecked()) {
                                params.put("amount_wallet", "0");
                                params.put("amount_paypal", paymentAmount_sendChallenge);
                                params.put("payment_mode", "PAYPAL");
                            }
                        }
                    }
                    else {
                        params.put("amount_wallet", "0");
                        params.put("amount_paypal", paymentAmount_sendChallenge);
                    }
                    Log.d("PAY_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(PaymentDetailsActivity.this);
            requestQueue.add(stringRequest);
        } else {

            Toast.makeText(PaymentDetailsActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }



    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if(checkedId==R.id.wallet_rb){
            wallet_radio.setChecked(true);
            paymentmode="WALLET";
        }else {
            paypal_radio.setChecked(true);
            paymentmode="PAYPAL";
        }
    }

    private void initilizeUI(){
        noofdays_layout=findViewById(R.id.noof_days_layout);
        date_layout=findViewById(R.id.date_layout);
        challenge_with_layout=findViewById(R.id.challenge_with_layout);
        sleep_time_layout=findViewById(R.id.sleep_time_layout);
        sleepstart_layout=findViewById(R.id.sleepstart_layout);
        note_txt_ll=findViewById(R.id.note_txt_ll);
        payment_text=findViewById(R.id.price_txt);
        payment_text.setText(payamount);
        payment_text.setTypeface(regularFont);
        payable_text=findViewById(R.id.payable_text);
        payable_text.setTypeface(boldFont);
        challenge_date_label=findViewById(R.id.challenge_date_lbl);
        challenge_date_label.setTypeface(regularFont);
        challenge_date_txt=findViewById(R.id.challenge_date_txt);
        challenge_date_txt.setText(challengedate);
        challenge_date_txt.setTypeface(regularFont);
        challenge_with_label=findViewById(R.id.challenge_with_lbl);
        challenge_with_label.setTypeface(regularFont);
        challenge_with_text=findViewById(R.id.challenge_with_txt);
        challenge_with_text.setText(challengerName);
        challenge_with_text.setTypeface(regularFont);
        sleep_time_label=findViewById(R.id.sleep_time_lbl);
        sleep_time_label.setTypeface(regularFont);
        sleep_time_txt=findViewById(R.id.sleep_time_txt);
        sleep_time_txt.setText(hours);
        sleep_time_txt.setTypeface(regularFont);
        sleep_start_time_label=findViewById(R.id.sleep_start_time_lbl);
        sleep_start_time_label.setTypeface(regularFont);
        sleep_start_time_txt=findViewById(R.id.sleep_start_time_txt);
        sleep_start_time_txt.setText(timestr);
        sleep_start_time_txt.setTypeface(regularFont);
        no_of_days_label=findViewById(R.id.noof_days_lbl);
        no_of_days_label.setTypeface(regularFont);
        no_of_days_txt=findViewById(R.id.noof_days_txt);
        if(challengetype.equalsIgnoreCase("Weekly")) {
            no_of_days_txt.setText(noofdays);
        }
        no_of_days_txt.setTypeface(regularFont);
        note_label=findViewById(R.id.note_lbl);
        note_label.setTypeface(regularFont);
        note_et=findViewById(R.id.note_et);
        note_et.setTypeface(regularFont);
        payment_rg=findViewById(R.id.payment_rg);
        payment_rg.setOnCheckedChangeListener(this);
        wallet_radio=findViewById(R.id.wallet_rb);
        wallet_radio.setTypeface(specialFont);
        paypal_radio=findViewById(R.id.paypal_rb);
        note_et.setTypeface(regularFont);
        pay_btn=findViewById(R.id.pay_details_btn);
        paypal_radio.setTypeface(regularFont);
        pay_btn.setOnClickListener(this);
        close_btn=findViewById(R.id.back_img);
        close_btn.setOnClickListener(this);
    }

    private void setVisibity(int noofdays,int date,int challenge,int sleep_time,int sleepstart,int note){
        if(challengetype.equalsIgnoreCase("Weekly"))
        {
            noofdays_layout.setVisibility(View.VISIBLE);
            no_of_days_txt.setText(noofdays);
        }

        date_layout.setVisibility(date);
        challenge_with_layout.setVisibility(challenge);
        sleep_time_layout.setVisibility(sleep_time);
        sleepstart_layout.setVisibility(sleepstart);
        note_txt_ll.setVisibility(note);
    }

}
