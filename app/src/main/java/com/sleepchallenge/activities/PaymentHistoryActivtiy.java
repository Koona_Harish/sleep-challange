package com.sleepchallenge.activities;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;
import com.sleepchallenge.models.PaymentHistoryModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class PaymentHistoryActivtiy extends AppCompatActivity implements View.OnClickListener
{
     private boolean checkIntenet;
     UserSessionManager session;
     ImageView back_img;
     TextView title_text;
    Typeface regularfont;
    String user_id, user_type, token, device_ID;
    ArrayList<PaymentHistoryModel>payHistoryList;
    RecyclerView   payment_detail_recyclerView;
    PaymentHistoryAdapter paymentHistAdapter;
    RecyclerView.LayoutManager layoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_detail_activtiy);

        regularfont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));

        session = new UserSessionManager(this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_ID = userDetails.get(UserSessionManager.DEVICE_ID);
        checkIntenet = NetworkChecking.isConnected(this);

        back_img=findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        title_text=findViewById(R.id.title_text);
        title_text.setTypeface(regularfont);

        payment_detail_recyclerView=findViewById(R.id.payment_detail_recyclerView);

       getPaymentHisory();
  }

   public void getPaymentHisory()
   {
       if(checkIntenet)
       {
           Dialog.showProgressBar(PaymentHistoryActivtiy.this,"Loading");
           String payhistoryURL= AppUrls.BASE_URL+AppUrls.RECENT_PAYMETN_HISTORY+"?user_id="+user_id+"&user_type="+user_type;
           Log.d("PAYHISTURL",payhistoryURL);
           StringRequest payHist=new StringRequest(Request.Method.GET, payhistoryURL, new Response.Listener<String>() {
               @Override
               public void onResponse(String response)
               {
                   Dialog.hideProgressBar();
                   Log.d("PAYHISTRESP", response);

                   try
                   {
                       JSONObject jsonObject = new JSONObject(response);
                       String responseCode=jsonObject.getString("response_code");
                       if(responseCode.equals("10100"))
                       {

                           payHistoryList = new ArrayList<>();
                           JSONArray hisJsonArray=jsonObject.getJSONArray("data");
                           for (int i = 0; i < hisJsonArray.length(); i++)
                           {
                               JSONObject hisJOBJ=hisJsonArray.getJSONObject(i);
                               PaymentHistoryModel payHisModel=new PaymentHistoryModel(hisJOBJ);
                               payHistoryList.add(payHisModel);
                           }

                           paymentHistAdapter = new PaymentHistoryAdapter(payHistoryList, PaymentHistoryActivtiy.this, R.layout.row_recent_history);
                           layoutManager = new LinearLayoutManager(getApplicationContext());
                           payment_detail_recyclerView.setNestedScrollingEnabled(false);
                           payment_detail_recyclerView.setLayoutManager(layoutManager);
                           payment_detail_recyclerView.setAdapter(paymentHistAdapter);
                       }
                       if(responseCode.equals("10200"))
                       {
                           Toast.makeText(PaymentHistoryActivtiy.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                       }
                       if(responseCode.equals("10300"))
                       {
                           Toast.makeText(PaymentHistoryActivtiy.this, "No Data Found.!", Toast.LENGTH_SHORT).show();
                       }
                   }
                   catch (JSONException exception)
                   {
                       exception.printStackTrace();
                   }
               }
           }, new Response.ErrorListener()
           {
               @Override
               public void onErrorResponse(VolleyError error)
               {
                   Dialog.hideProgressBar();
                   Log.e("Error", error.getMessage());
               }
           }) {
               @Override
               public Map<String, String> getHeaders(){
                   Map<String, String> headers = new HashMap<>();
                   headers.put("x-access-token", token);
                   headers.put("x-device-id", device_ID);
                   headers.put("x-device-platform", "ANDROID");
                   Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                   return headers;
               }
           };
           payHist.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
           RequestQueue requestQueue = Volley.newRequestQueue(this);
           requestQueue.add(payHist);
       }
       else
       {
              Toast.makeText(PaymentHistoryActivtiy.this,"No Internet Connection..!",Toast.LENGTH_SHORT).show();
       }
   }
    @Override
    public void onClick(View view)
    {
       if(view==back_img)
        {
            finish();
        }
    }

    //ADAPTER CLASS
    public class    PaymentHistoryAdapter extends RecyclerView.Adapter<PayHistoryHolder>
    {

        public ArrayList<PaymentHistoryModel> payHistoryModelsList;
        PaymentHistoryActivtiy context;
        LayoutInflater li;
        int resource;
        private boolean checkInternet;
        UserSessionManager userSessionManager;
        Typeface thinFont, regularFont, boldFont, specialFont;

        public PaymentHistoryAdapter(ArrayList<PaymentHistoryModel> payHistoryModelsList, PaymentHistoryActivtiy context, int resource) {
            this.payHistoryModelsList = payHistoryModelsList;
            this.context = context;
            this.resource = resource;
            li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regularFont));
            boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.boldFont));
            thinFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.thinFont));
            specialFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.specialFont));

        }
        @Override
        public PayHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = li.inflate(resource, parent, false);
            PayHistoryHolder slh = new PayHistoryHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(final PayHistoryHolder holder, final int position)
        {

            holder.cardView.setCardBackgroundColor(Color.parseColor("#E0E0E0"));

            String str = payHistoryModelsList.get(position).title;
            String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
            holder.title_txt.setText(Html.fromHtml(converted_string));
            holder.title_txt.setTypeface(boldFont);

            holder.amount_txt.setText(" "+payHistoryModelsList.get(position).amount+" "+"USD");
            holder.amount_txt.setTypeface(specialFont);

            String txn_flag=payHistoryModelsList.get(position).txn_flag;
            if(txn_flag.equals("SCRATCH_CARD_WEEKLY"))
            {
                if (!payHistoryModelsList.get(position).for_span.equals(""))
                {
                    holder.txn_flag_txt.setVisibility(View.VISIBLE);
                    holder.txn_flag_txt.setText(payHistoryModelsList.get(position).for_span);
                    holder.txn_flag_txt.setTypeface(regularFont);
                }
                else {
                    holder.txn_flag_txt.setVisibility(View.GONE);
                }
            }



            //   holder.txn_flag_txt.setText(txn_flag);
            holder.txn_type_txt.setText(payHistoryModelsList.get(position).txn_type);
            if (payHistoryModelsList.get(position).txn_type.equals("DEBIT")){
                holder.back_layout.setBackgroundColor(Color.parseColor("#DD6B55"));
                holder.date_text.setText("Debited On");
                holder.date_text.setTypeface(regularFont);
            }
            else if (payHistoryModelsList.get(position).txn_type.equals("CREDIT")){
                holder.back_layout.setBackgroundColor(Color.parseColor("#239843"));
                holder.date_text.setText("Credited on");
                holder.date_text.setTypeface(regularFont);
            }


            holder.status.setText(payHistoryModelsList.get(position).txn_status);
            holder.status.setTypeface(regularFont);

             String createdate=parseDateToddMMyyyy(payHistoryModelsList.get(position).created_on);
             Log.d("DATATAT",createdate);
            holder.time_txt.setText(createdate);
            holder.time_txt.setTypeface(regularFont);

            holder.type_title.setTypeface(regularFont);
            holder.status_title.setTypeface(regularFont);
            holder.amount_title.setTypeface(regularFont);


            holder.setItemClickListener(new AllItemClickListeners() {
                @Override
                public void onItemClick(View v, int pos) {

                }
            });

        }

        @Override
        public int getItemCount() {
            return this.payHistoryModelsList.size();
        }

        public String parseDateToddMMyyyy(String time) {
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            String outputPattern = "dd MMM yyyy";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            Date date = null;
            String str = null;

            try {
                date = inputFormat.parse(time);
                str = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return str;
        }
    }

    //HOLDER CLASS

    public class PayHistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView amount_txt,title_txt,txn_flag_txt,txn_type_txt,time_txt,updated_time_txt,status,type_title,status_title,amount_title,date_text;
        public CardView cardView;
        public LinearLayout back_layout;

        AllItemClickListeners payHistoryItemClickListener;

        public PayHistoryHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            cardView =   itemView.findViewById(R.id.cardView);
            amount_txt =  itemView.findViewById(R.id.amount_txt);
            title_txt =  itemView.findViewById(R.id.title_txt);
            txn_flag_txt =  itemView.findViewById(R.id.txn_flag_txt);
            txn_type_txt =  itemView.findViewById(R.id.txn_type_txt);
            time_txt = itemView.findViewById(R.id.time_txt);
            updated_time_txt =  itemView.findViewById(R.id.updated_time_txt);
            status =  itemView.findViewById(R.id.status);
            back_layout =  itemView.findViewById(R.id.back_layout);

            type_title=itemView.findViewById(R.id.type_title);
            status_title=itemView.findViewById(R.id.status_title);
            amount_title=itemView.findViewById(R.id.amount_title);
            date_text=itemView.findViewById(R.id.date_text);


        }

        @Override
        public void onClick(View view) {

            this.payHistoryItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(AllItemClickListeners ic) {
            this.payHistoryItemClickListener = ic;
        }


    }

}
