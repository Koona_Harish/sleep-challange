package com.sleepchallenge.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.AllChallangRunningAdapter;
import com.sleepchallenge.models.AllChallengesStatusModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AllRunningChallengesActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    ProgressDialog progressDialog;
    String device_id, token, user_id;
    UserSessionManager session;
    LinearLayoutManager layoutManager;
    RecyclerView recycler_allmore_running;
    ImageView close, refresh;
    private boolean userScrolled = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int defaultPageNo = 0;
    private static int displayedposition = 0;
    int total_number_of_items = 0;
    private boolean loading = true;
    int type_of_request = 0;
    String from_chalng_fragment, sendStatus;
    RelativeLayout bottomLayout;
    AllChallangRunningAdapter allChallangRunningAdapter;
    ArrayList<AllChallengesStatusModel> allchlngRunn = new ArrayList<AllChallengesStatusModel>();
    Intent myGpsService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_running_challenges);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Bundle b = new Bundle();
        from_chalng_fragment = getIntent().getExtras().getString("condition");

        progressDialog = new ProgressDialog(AllRunningChallengesActivity.this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        bottomLayout = (RelativeLayout) findViewById(R.id.loadItemsLayout_recyclerView);
        recycler_allmore_running = (RecyclerView) findViewById(R.id.recycler_allmore_running);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        refresh = (ImageView) findViewById(R.id.refresh);
        refresh.setOnClickListener(this);
        recycler_allmore_running.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(AllRunningChallengesActivity.this);
        recycler_allmore_running.setLayoutManager(layoutManager);
        allChallangRunningAdapter = new AllChallangRunningAdapter(allchlngRunn, AllRunningChallengesActivity.this, R.layout.row_all_challenges_more);

        allchlngRunn.clear();

        getAllRunChallangesMore(0);
    }

    private void getAllRunChallangesMore(int defaultPageNo) {
        checkInternet = NetworkChecking.isConnected(AllRunningChallengesActivity.this);
        if (checkInternet) {

            allchlngRunn.clear();
            String url = AppUrls.BASE_URL + AppUrls.MORECHALLENGES + "?user_id=" + user_id + "&status=RUNNING&page=" + defaultPageNo + "&type=" + from_chalng_fragment;
            Log.d("RUNNLISTURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("response_code");
                                if (responceCode.equals("10100")) {
                                    progressDialog.dismiss();

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                    JSONArray jsonArray = jsonObject1.getJSONArray("challenges");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        AllChallengesStatusModel itemRunnAll = new AllChallengesStatusModel();
                                        itemRunnAll.setChallenge_id(jsonObject2.getString("challenge_id"));
                                        itemRunnAll.setUser_id(jsonObject2.getString("user_id"));
                                        itemRunnAll.setUser_type(jsonObject2.getString("user_type"));
                                        itemRunnAll.setOpponent_id(jsonObject2.getString("opponent_id"));
                                        itemRunnAll.setOpponent_type(jsonObject2.getString("opponent_type"));
                                        itemRunnAll.setRemainder(jsonObject2.getString("remainder"));
                                        itemRunnAll.setStart_time(jsonObject2.getString("start_time"));
                                        itemRunnAll.setChallenge_goal(jsonObject2.getString("challenge_goal"));
                                        itemRunnAll.setChallenger_group_id(jsonObject2.getString("challenger_group_id"));
                                        itemRunnAll.setPause_access(jsonObject2.getString("pause_access"));
                                        itemRunnAll.setIs_group_admin(jsonObject2.getString("is_group_admin"));
                                        itemRunnAll.setUser_name(jsonObject2.getString("user_name"));
                                        itemRunnAll.setOpponent_name(jsonObject2.getString("opponent_name"));
                                        itemRunnAll.setWinning_status(jsonObject2.getString("winning_status"));
                                        itemRunnAll.setWinning_amount(jsonObject2.getString("winning_amount"));
                                        itemRunnAll.setWinner(jsonObject2.getString("winner"));
                                        itemRunnAll.setStatus(jsonObject2.getString("status"));
                                        itemRunnAll.setAmount(jsonObject2.getString("amount"));
                                        itemRunnAll.setChallenge_type(jsonObject2.getString("challenge_type"));
                                        itemRunnAll.setStart_on(jsonObject2.getString("start_on"));
                                        itemRunnAll.setPaused_on(jsonObject2.getString("paused_on"));
                                        itemRunnAll.setPaused_by(jsonObject2.getString("paused_by"));
                                        itemRunnAll.setResume_on(jsonObject2.getString("resume_on"));
                                        itemRunnAll.setCompleted_on_txt(jsonObject2.getString("completed_on_txt"));
                                        itemRunnAll.setCompleted_on(jsonObject2.getString("completed_on"));
                                        itemRunnAll.setWinning_reward_type(jsonObject2.getString("winning_reward_type"));
                                        itemRunnAll.setWinning_reward_value(jsonObject2.getString("winning_reward_value"));
                                        itemRunnAll.setIs_scratched(jsonObject2.getString("is_scratched"));

                                        allchlngRunn.add(itemRunnAll);
                                    }

                                    recycler_allmore_running.setAdapter(allChallangRunningAdapter);

                                    bottomLayout.setVisibility(View.GONE);
                                }
                                if (responceCode.equals("10200")) {
                                    recycler_allmore_running.setVisibility(View.GONE);
                                    progressDialog.dismiss();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GGGGG_CHALL_HEAD", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(AllRunningChallengesActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Check your Internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(View v) {
        if (v == close) {
            finish();
        }
        if (v == refresh) {
            allchlngRunn.clear();
            getAllRunChallangesMore(defaultPageNo);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(myGpsService!=null)
            stopService(myGpsService);
    }
}
