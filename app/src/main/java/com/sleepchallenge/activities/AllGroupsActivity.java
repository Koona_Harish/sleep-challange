package com.sleepchallenge.activities;

import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.AllMoreGroupsAdapter;
import com.sleepchallenge.adapters.MyGroupsAdapter;
import com.sleepchallenge.fragments.MyGroupsFragment;
import com.sleepchallenge.models.MyGroupModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.DividerItemDecorator;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AllGroupsActivity extends AppCompatActivity implements View.OnClickListener{
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String user_id, user_type, token, device_ID;
    RecyclerView my_groups_recyclerView;
    ArrayList<MyGroupModel> allMyGroupList;
    AllMoreGroupsAdapter myGroupAdapter;
    RecyclerView.LayoutManager layoutManager;
    Typeface thinFont,regularFont,boldFont;
    SearchView group_search;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_groups);
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_ID = userDetails.get(UserSessionManager.DEVICE_ID);
        checkInternet = NetworkChecking.isConnected(this);
        group_search= findViewById(R.id.group_search);
        String memberid=getIntent().getStringExtra("member_id");
        my_groups_recyclerView =  findViewById(R.id.my_groups_recyclerView);
        ImageView back_img = findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        TextView title= findViewById(R.id.title);
        TextView title_text=findViewById(R.id.title_text);
        title_text.setTypeface(boldFont);


        group_search.setIconified(true);
        group_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                group_search.setIconified(false);
            }
        });
        group_search.clearFocus();
        group_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if(myGroupAdapter!=null)
                    myGroupAdapter.getFilter().filter(query);
                return false;
            }
        });
        getMyGroups(memberid);
    }

    private void getMyGroups(String id) {
        if (checkInternet) {
            Dialog.showProgressBar(this, "Loading");

            String myGroupurl = AppUrls.BASE_URL + "group/user?user_id=" + id + "&user_type=" + user_type;
            Log.d("MYGROUPURL", myGroupurl);

            StringRequest strAllMember = new StringRequest(Request.Method.GET, myGroupurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Dialog.hideProgressBar();
                    Log.d("MYGROUPRESP", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100"))
                        {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            allMyGroupList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                MyGroupModel mygroup = new MyGroupModel(jdataobj);
                                allMyGroupList.add(mygroup);
                            }
                            myGroupAdapter = new AllMoreGroupsAdapter(allMyGroupList, AllGroupsActivity.this, R.layout.row_my_group);
                            layoutManager = new LinearLayoutManager(AllGroupsActivity.this);
                            my_groups_recyclerView.setNestedScrollingEnabled(false);
                            my_groups_recyclerView.setLayoutManager(layoutManager);
                            RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(AllGroupsActivity.this, R.drawable.recycler_view_divider));
                            my_groups_recyclerView.addItemDecoration(dividerItemDecoration);

                            my_groups_recyclerView.setAdapter(myGroupAdapter);
                        }
                        if (successResponceCode.equals("10200"))
                        {
                            Toast.makeText(AllGroupsActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                        }
                        if (successResponceCode.equals("10300"))
                        {
                            Toast.makeText(AllGroupsActivity.this, "No Data Found..!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Dialog.hideProgressBar();
                    Log.e("Error", error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strAllMember.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strAllMember);
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.back_img){
            finish();
        }
    }
}
