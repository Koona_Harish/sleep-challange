package com.sleepchallenge.activities;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.GPSTracker;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class ConfirmationActivity extends AppCompatActivity implements View.OnClickListener {

    TextView username_txt, status, payment_id;
    Button next_btn;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String activity, amount, paymentStatus, paymentId, ref_txn_id = "", user_id, user_type, username;
    String  challenge_goal = "", token = "", span = "", date = "",deviceId="",challengetype="",challengeid="",paymentAmount="",walletAmount="",
            opponent_id = "", opponent_type = "",sendtime="",sendremainder="",member_id = "", member_user_type = "",group_id = "", group_user_type = "",promotion_id="";
    int timeinsecs;
    GPSTracker gps;
    Geocoder geocoder;
    List<Address> addresses;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);
        gps=new GPSTracker(this);
        geocoder = new Geocoder(this, Locale.getDefault());
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        username = userDetails.get(UserSessionManager.USER_NAME);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        //get Time zone
        TimeZone tz = TimeZone.getDefault();
        Calendar cal = GregorianCalendar.getInstance(tz);
        timeinsecs = (tz.getOffset(cal.getTimeInMillis()))/1000;
        Bundle bundle = getIntent().getExtras();
        activity = bundle.getString("activity");
        if (activity.equals("AccountVerification")) {
            amount = bundle.getString("PaymentAmount");
        } else if (activity.equals("Login")) {
            amount = bundle.getString("PaymentAmount");
        }else if(activity.equalsIgnoreCase("challengeto")){
            amount = bundle.getString("PaymentAmount");
            user_id = bundle.getString("user_id");
            user_type = bundle.getString("user_type");
            challenge_goal = bundle.getString("challenge_goal");
            challengetype = bundle.getString("challenge_type");
            span = bundle.getString("span");
            date = bundle.getString("challenge_date");
            sendtime = bundle.getString("sleeptime");
            sendremainder = bundle.getString("remainder");
            opponent_id = bundle.getString("opponent_id");
            opponent_type = bundle.getString("opponent_type");
        }

        else if(activity.equalsIgnoreCase("MYPROFILE_ADDWALLET"))
        {
            amount = bundle.getString("PaymentAmount");
            user_id = bundle.getString("user_id");
            user_type = bundle.getString("user_type");

        }else if(activity.equalsIgnoreCase("AcceptChallengePayment")){
            amount = bundle.getString("PaymentAmount");

            challenge_goal = bundle.getString("challenge_goal");
            challengetype = bundle.getString("challenge_type");
            span = bundle.getString("span");
            date = bundle.getString("challenge_date");
            sendtime = bundle.getString("sleeptime");
            sendremainder = bundle.getString("remainder");
             challengeid = bundle.getString("challengeid");
        }else if(activity.equalsIgnoreCase("MemberDetail")){
            member_id=getIntent().getStringExtra("member_id");
            member_user_type=getIntent().getStringExtra("member_user_type");
            amount = bundle.getString("PaymentAmount");
            walletAmount = bundle.getString("walletAmount");
        }else if(activity.equalsIgnoreCase("GroupDetail")){
            amount = bundle.getString("PaymentAmount");
            walletAmount = bundle.getString("walletAmount");
            group_id=getIntent().getStringExtra("group_id");
            group_user_type=getIntent().getStringExtra("group_user_type");
        }else if(activity.equalsIgnoreCase("MeAsaSponsorAPP")){
            amount = bundle.getString("PaymentAmount");
            walletAmount = bundle.getString("walletAmount");
            group_id = bundle.getString("member_id");
            group_user_type = bundle.getString("member_user_type");
        }else if(activity.equalsIgnoreCase("MyPromotionsActivity")){
            amount = bundle.getString("PaymentAmount");
            walletAmount = bundle.getString("walletAmount");
            promotion_id = bundle.getString("promotionid");

        }

        Intent intent = getIntent();
        try {
            JSONObject jsonDetails = new JSONObject(intent.getStringExtra("PaymentDetails"));
            showDetails(jsonDetails.getJSONObject("response"), intent.getStringExtra("PaymentAmount"));
        } catch (JSONException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void showDetails(JSONObject jsonDetails, String paymentAmount) throws JSONException {
        username_txt = findViewById(R.id.username_txt);
        username_txt.setText("Hi," + username);

        payment_id = findViewById(R.id.payment_id);
        status = findViewById(R.id.status);
        next_btn = findViewById(R.id.next_btn);
        next_btn.setOnClickListener(this);
        paymentId = jsonDetails.getString("id");
        paymentStatus = jsonDetails.getString("state");
        payment_id.setText(paymentId);
        status.setText("$ " + paymentAmount + " USD" + "\t" + "Payment Completed");
        Log.d("PAYMENTDETAILS", paymentId + "\n" + paymentStatus);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (activity.equals("Login"))
                {
                    initialPay();
                }
                else if (activity.equals("AccountVerification"))
                {
                    initialPay();
                }
                else if(activity.equalsIgnoreCase("challengeto")){
                  sendChallenge();
                }
                else if(activity.equalsIgnoreCase("MYPROFILE_ADDWALLET")){
                    profilewalletPay();
                }else if(activity.equalsIgnoreCase("AcceptChallengePayment")){
                    acceptChallenge();
                }else{
                    sponsorPay();
                }
            }
        }, 2000);

    }

    private void profilewalletPay()
    {
        checkInternet = NetworkChecking.isConnected(ConfirmationActivity.this);
        if(checkInternet)
        {
            Log.d("WALLETURL",AppUrls.BASE_URL + AppUrls.USER_WALLET_PAY);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.USER_WALLET_PAY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("WALLETRESP", response);


                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100"))
                                {

                                    Toast.makeText(ConfirmationActivity.this, "Amount Sent successfully.!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ConfirmationActivity.this, MainActivity.class);
                                    startActivity(intent);
                                }
                                if (response_code.equals("10200")) {

                                    Toast.makeText(ConfirmationActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                           error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("amount", amount);
                    params.put("payment_id", paymentId);
                    Log.d("PAYWALLETPARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationActivity.this);
            requestQueue.add(stringRequest);
        }
        else
        {

            Toast.makeText(ConfirmationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void sponsorPay() {
        checkInternet = NetworkChecking.isConnected(ConfirmationActivity.this);
        if (checkInternet) {
            String url="";
            if(activity.equalsIgnoreCase("MyPromotionsActivity")){
                url=AppUrls.BASE_URL + AppUrls.PROMOTION_PAY;
            }else {
                url=AppUrls.BASE_URL + AppUrls.SPONSOR_PAY;
            }
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("SPONSORRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {

                                    Toast.makeText(ConfirmationActivity.this, "Transcation Uploaded to server..!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ConfirmationActivity.this, MainActivity.class);
                                    startActivity(intent);
                                }
                                if (response_code.equals("10200")) {

                                    Toast.makeText(ConfirmationActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams()  {
                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    if (activity.equals("MemberDetail")){
                        params.put("to_user_id", member_id);
                        params.put("to_user_type", member_user_type);
                    }else if (activity.equals("GroupDetail")){
                        params.put("to_user_id", group_id);
                        params.put("to_user_type", group_user_type);
                    }
                    else if (activity.equals("MeAsaSponsorAPP")){
                        params.put("to_user_id", "59def9dce25e4081ac0f1093");
                        params.put("to_user_type", "APP");
                    }
                    else if (activity.equals("MeAsSponsorActivity")){
                        params.put("to_user_id", group_id);
                        params.put("to_user_type", "ACTIVITY");
                    }else if (activity.equals("MyPromotionsActivity")){
                        params.put("promotion_id", promotion_id);

                    }

                    params.put("gateway_response", paymentStatus);
                    params.put("amount", amount);
                    params.put("payment_id", paymentId);
                    if (walletAmount.equals("0") || walletAmount.equals("")) {
                        params.put("payment_mode", "PAYPAL");
                    } else {
                        params.put("payment_mode", "WALLET_PAYPAL");
                    }
                    params.put("amount_wallet", walletAmount);
                    params.put("amount_paypal", amount);
                    Log.d("PAY_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationActivity.this);
            requestQueue.add(stringRequest);
        } else {

            Toast.makeText(ConfirmationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendChallenge() {
        checkInternet = NetworkChecking.isConnected(ConfirmationActivity.this);
        if (checkInternet) {


            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SEND_CHALLENGES,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("CHALLENGE_RESPONSE", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {

                                    Toast.makeText(ConfirmationActivity.this, "Challenge Request Sent", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ConfirmationActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();

                                }
                                if (successResponceCode.equals("10200")) {

                                    Toast.makeText(ConfirmationActivity.this, "Challenge Request Faild", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {

                                    Toast.makeText(ConfirmationActivity.this, "Invalid Amount", Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams()  {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("challenge_goal", challenge_goal);
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("challenge_type", challengetype);
                    params.put("challenge_date", date);
                    params.put("amount", amount);
                    params.put("opponent_id", opponent_id);
                    params.put("opponent_type", opponent_type);
                    params.put("day_span", span);
                    params.put("gateway_response", "");
                    params.put("payment_id", paymentId);
                    params.put("payment_mode", "PAYPAL");
                    params.put("amount_wallet", "0");
                    params.put("amount_paypal", amount);
                    params.put("timezone_in_sec", String.valueOf(timeinsecs));
                    params.put("start_time", sendtime);
                    params.put("remainder", sendremainder);


                    Log.d("REPORT_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationActivity.this);
            requestQueue.add(stringRequest);

        } else {

            Toast.makeText(ConfirmationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void initialPay() {
        checkInternet = NetworkChecking.isConnected(ConfirmationActivity.this);
        if (checkInternet) {
            Dialog.showProgressBar(this, "Registering...");
            Log.d("INTIALPAYURL", AppUrls.BASE_URL + AppUrls.INITIAL_PAYMENT);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.INITIAL_PAYMENT,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("initialREPORTRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    Dialog.hideProgressBar();
                                    JSONObject jobj = jsonObject.getJSONObject("data");
                                    ref_txn_id = jobj.getString("ref_txn_id");
                                    String user_typ = jobj.getString("user_type");
                                    String user_id = jobj.getString("user_id");
                                    String first_name = jobj.getString("first_name");
                                    String last_name = jobj.getString("last_name");
                                    String email = jobj.getString("email");
                                    String country_code = jobj.getString("country_code");
                                    String mobile = jobj.getString("mobile");
                                    String status = jobj.getString("status");
                                    String provided_acc_info = jobj.getString("provided_acc_info");
                                    String username = first_name + " " + last_name;
                                    Log.d("", ref_txn_id + "" + user_typ + "" + username + "" + email + "" + country_code + "" + mobile + "" + status + "" + provided_acc_info);
                                    Toast.makeText(ConfirmationActivity.this, "Transcation Uploaded to server..!", Toast.LENGTH_SHORT).show();
                                   // userSessionManager.createUserLoginSession(user_id, user_type, token, username, email, mobile, country_code);
                                     sendUserLocation();
                                    /*Intent intent = new Intent(ConfirmationActivity.this, MainActivity.class);
                                    startActivity(intent);*/
                                }
                                if (response_code.equals("10200")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(ConfirmationActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                                }

                                if (response_code.equals("10300")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(ConfirmationActivity.this, "Invalid Payment..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Dialog.hideProgressBar();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Dialog.hideProgressBar();
                            error.getMessage();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams()  {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    if (paymentStatus.equals("approved")) {
                        params.put("txn_flag", "COMPLETED");
                    } else {
                        params.put("txn_flag", paymentStatus);
                    }
                    params.put("amount", amount);
                    params.put("payment_id", paymentId);

                    Log.d("PAY_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Dialog.hideProgressBar();
            Toast.makeText(ConfirmationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {

    }

    private void sendUserLocation(){
        if(gps.canGetLocation()){
            double latitude = gps.getLatitude();
            String lat = String.valueOf(latitude);
            double longitude = gps.getLongitude();
            String lng = String.valueOf(longitude);
            Log.d("sdjgbs", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String place = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();// Here 1 represent max location result to returned, by documents it recommended 1 to 5
                sendUserData(user_id, lat, lng, city, country, state, place);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

}

    public void sendUserData(final String user_id, final String lat, final String lng, final String city, final String country, final String state, final String place) {
        checkInternet = NetworkChecking.isConnected(ConfirmationActivity.this);
        if (checkInternet) {
            Log.d("URL", AppUrls.BASE_URL + AppUrls.SET_USER_LOCATION);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SET_USER_LOCATION,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("LOCATIONRES", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    Intent intent = new Intent(ConfirmationActivity.this, MainActivity.class);
                                    startActivity(intent);
                                }
                                if (successResponceCode.equals("10200")) {
                                    Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("lat", lat);
                    params.put("long", lng);
                    params.put("city", city);
                    params.put("country", country);
                    params.put("state", state);
                    params.put("place", place);
                    Log.d("REPORT_PARAM:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationActivity.this);
            requestQueue.add(stringRequest);
        } else {
            // Toast.makeText(MainActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }


    private void acceptChallenge() {
        checkInternet = NetworkChecking.isConnected(ConfirmationActivity.this);
        if (checkInternet) {
            Log.d("URL", AppUrls.BASE_URL + AppUrls.ACEPT_CHALLENGE_REQUEST);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ACEPT_CHALLENGE_REQUEST,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //  progressDialog.dismiss();
                            Log.d("AcceptChallenge", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {
                                    // progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String challenge_id = jsonObject1.getString("challenge_id");
                                    Toast.makeText(ConfirmationActivity.this,"Payment successfully",Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ConfirmationActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                if (response_code.equals("10200")) {
                                    // progressDialog.dismiss();
                                    Toast.makeText(ConfirmationActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                // progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //  progressDialog.cancel();
                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("challenge_id", challengeid);

                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("gateway_response", paymentStatus);
                    params.put("amount", amount);
                    params.put("payment_id", paymentId);
                    params.put("payment_mode", "PAYPAL");
                    params.put("amount_wallet", "0");
                    params.put("amount_paypal", amount);
                    params.put("timezone_in_sec",String.valueOf(timeinsecs));

                    Log.d("ACCEPT:", "PARMS" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationActivity.this);
            requestQueue.add(stringRequest);
        } else {
            // progressDialog.cancel();
            Toast.makeText(ConfirmationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }
}
