package com.sleepchallenge.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.MovableFloatingActionButton;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MyPromotionsActivity extends AppCompatActivity implements View.OnClickListener
{
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String user_id, user_type, token, device_ID;
    ImageView back_img;
    TextView title_text_my,no_promotions_text;
    Typeface regularFont,boldFont,specialFont;
    RecyclerView recyclerview_my_promot;
    ArrayList<PromotionModel> promotList;
    PromotionAdapter promoAdap;
    RecyclerView.LayoutManager layoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_promotions);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_ID = userDetails.get(UserSessionManager.DEVICE_ID);
        checkInternet = NetworkChecking.isConnected(this);

        MovableFloatingActionButton fab = findViewById(R.id.fab);
        CoordinatorLayout.LayoutParams lp  = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
        fab.setCoordinatorLayout(lp);
        fab.setOnClickListener(this);
        TextView my_request=findViewById(R.id.my_promotios);
        my_request.setTypeface(regularFont);


        back_img=findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        title_text_my=findViewById(R.id.title_text_my);
        title_text_my.setTypeface(regularFont);
        recyclerview_my_promot=findViewById(R.id.recyclerview_my_promot);
        no_promotions_text=findViewById(R.id.no_promotions_text);
        no_promotions_text.setTypeface(specialFont);

        getMyPromotions();
    }

    private void getMyPromotions()
    {

        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_myPromo = AppUrls.BASE_URL+AppUrls.MY_PROMOTIONS+user_id+"&type="+user_type;
            Log.d("MYPROMOURL", url_myPromo);
            StringRequest req_members = new StringRequest(Request.Method.GET, url_myPromo, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("MYPROMORESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100"))
                        {
                            promotList = new ArrayList<>();
                            JSONArray jsonArray = jobcode.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++)
                            {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                PromotionModel dact = new PromotionModel(jdataobj);
                                promotList.add(dact);
                            }
                            promoAdap = new PromotionAdapter(promotList, MyPromotionsActivity.this, R.layout.row_my_promotions);
                            layoutManager = new LinearLayoutManager(getApplicationContext());
                            recyclerview_my_promot.setNestedScrollingEnabled(false);
                            recyclerview_my_promot.setLayoutManager(layoutManager);
                            recyclerview_my_promot.setAdapter(promoAdap);
                        }
                        if (response_code.equals("10200"))
                        {
                            no_promotions_text.setVisibility(View.VISIBLE);
                            no_promotions_text.setText("No Promotions Found..!");

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    error.getMessage();
                }
            }) {
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("MYPROMOEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MyPromotionsActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(MyPromotionsActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
     public void onClick(View view)
    {
        if(view==back_img)
        {
            finish();
        }

        if(view.getId()==R.id.fab)
        {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
                Intent promo = new Intent(MyPromotionsActivity.this, PromotionsActivity.class);
                startActivity(promo);

            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }

        }
    }

     //ADAPTER
     public class PromotionAdapter extends RecyclerView.Adapter<PromotionHolder> {
         MyPromotionsActivity context;
         public ArrayList<PromotionModel> promoModels;
         LayoutInflater li;
         int resource;
         String datetime;

         public PromotionAdapter(ArrayList<PromotionModel> promoModels, MyPromotionsActivity context, int resource) {
             this.context = context;
             this.promoModels = promoModels;
             this.resource =resource;
             li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         }


         @Override
         public PromotionHolder onCreateViewHolder(ViewGroup parent, int viewType)
         {
             View layout = li.inflate(resource,parent,false);
             PromotionHolder slh = new PromotionHolder(layout);
             return slh;
         }

         @SuppressLint("ResourceAsColor")
         @Override
         public void onBindViewHolder(PromotionHolder holder, final int position) {

             holder.cardview.setCardBackgroundColor(Color.parseColor("#E0E0E0"));
             holder.amount.setText(String.valueOf(promoModels.get(position).amount+" "+"\nUSD"));

             datetime =parseDateToddMMyyyy(promoModels.get(position).created_on);
             holder.cre_date.setText(datetime);

             holder.name.setText(promoModels.get(position).user_name);
             holder.promoCode.setText(promoModels.get(position).promotion_code);

             holder.status.setText(promoModels.get(position).status);
             if (holder.status.getText().equals("APPROVED"))
             {
                 holder.pay_txt.setVisibility(View.VISIBLE);
                 holder.status.setTextColor(Color.parseColor("#28df5a"));
             }
             else if (holder.status.getText().equals("PAID"))
             {
                 holder.status.setTextColor(Color.parseColor("#28df5a"));
             }else {
                 holder.status.setTextColor(Color.parseColor("#b3270c"));
             }

             holder.pay_txt.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view)
                 {
                     Intent intent = new Intent(MyPromotionsActivity.this, PaymentDetailsActivity.class);
                     intent.putExtra("promotionAmount",String.valueOf(promoModels.get(position).amount));
                     intent.putExtra("promotion_id",promoModels.get(position).promotion_id);
                     intent.putExtra("activty","MyPromotionsActivity");
                     Log.d("SendingData", String.valueOf(promoModels.get(position).amount));
                     context.startActivity(intent);
                 }
             });

             holder.name_title.setTypeface(regularFont);
             holder.promo_code_title.setTypeface(regularFont);
             holder.status_title.setTypeface(regularFont);

             holder.pay_txt.setTypeface(regularFont);
             holder.amount.setTypeface(boldFont);
             holder.name.setTypeface(regularFont);
             holder.status.setTypeface(regularFont);
             holder.cre_date.setTypeface(regularFont);
         }

         @Override
         public int getItemCount() {
             return promoModels.size();
         }

         public String parseDateToddMMyyyy(String time) {
             String inputPattern = "yyyy-MM-dd HH:mm:ss";
             String outputPattern = "dd MMM yyyy ";
             SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
             SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

             Date date = null;
             String str = null;

             try {
                 date = inputFormat.parse(time);
                 str = outputFormat.format(date);
             } catch (ParseException e) {
                 e.printStackTrace();
             }
             return str;
         }
     }


    //HOLDER

    public class PromotionHolder extends RecyclerView.ViewHolder  {

        public TextView name, userTyp, promoCode, status, cre_date, amount,pay_txt,name_title,promo_code_title,status_title;
        public CardView cardview;
        public PromotionHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            userTyp = itemView.findViewById(R.id.userTyp);
            promoCode =  itemView.findViewById(R.id.promoCode);
            status =  itemView.findViewById(R.id.status);
            cre_date =  itemView.findViewById(R.id.cre_date);
            amount =  itemView.findViewById(R.id.amount);
            pay_txt =  itemView.findViewById(R.id.pay_txt);

            cardview =  itemView.findViewById(R.id.cardview);

            name_title =  itemView.findViewById(R.id.name_title);
            promo_code_title =  itemView.findViewById(R.id.promo_code_title);
            status_title =  itemView.findViewById(R.id.status_title);





        }

    }

    //MODEL
     public class PromotionModel {

         public String promotion_id,status,amount,banner_path,promotion_code,created_on,from_on,to_on,user_name;
         public PromotionModel(JSONObject jsonObject) {

             try {
                 if (jsonObject.has("promotion_id") && !jsonObject.isNull("promotion_id"))
                     this.promotion_id = jsonObject.getString("promotion_id");
                 if (jsonObject.has("status") && !jsonObject.isNull("status"))
                     this.status = jsonObject.getString("status");
                 if (jsonObject.has("amount") && !jsonObject.isNull("amount"))
                     this.amount = jsonObject.getString("amount");
                 if (jsonObject.has("banner_path") && !jsonObject.isNull("banner_path"))
                     this.banner_path = AppUrls.BASE_IMAGE_URL + jsonObject.getString("banner_path");
                 if (jsonObject.has("promotion_code") && !jsonObject.isNull("promotion_code"))
                     this.promotion_code = jsonObject.getString("promotion_code");
                 if (jsonObject.has("created_on") && !jsonObject.isNull("created_on"))
                     this.created_on = jsonObject.getString("created_on");
                 if (jsonObject.has("from_on") && !jsonObject.isNull("from_on"))
                     this.from_on = jsonObject.getString("from_on");
                 if (jsonObject.has("to_on") && !jsonObject.isNull("to_on"))
                     this.to_on = jsonObject.getString("to_on");
                 if (jsonObject.has("user_name") && !jsonObject.isNull("user_name"))
                     this.user_name = jsonObject.getString("user_name");


             } catch (Exception e) {
                 e.printStackTrace();
             }
         }
     }

}
