package com.sleepchallenge.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.GetMemUpdateGroupAdapter;
import com.sleepchallenge.adapters.GetMemUpdateStatusGroupAdapter;
import com.sleepchallenge.models.GetGroupMembersModel;
import com.sleepchallenge.models.GetMemUpdtStatusModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.ImagePermissions;
import com.sleepchallenge.utils.MyMultipartEntity;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UpdateGroupActivity extends AppCompatActivity implements View.OnClickListener {

    UserSessionManager userSessionManager;
    private boolean checkInternet;

    TextView addMemButt, update_grp_name, add_mem_statubut, update_grp_name_save, up_addMemButt_text;
    Button updateGrouButt;
    ImageView up_camera_click, close, up_group_user_image, up_addMemButt;
    String fetch_group_id_G, fetch_group_name_G, fetch_group_pic_G;
    String user_id, user_type, device_id, token, v, update_key, update_group_id;
    String selected_gmNames, selected_gmIds;
    EditText up_groupNamET;
    SearchView searchupdateV;
    RecyclerView up_getaddMemRecycler;
    LinearLayoutManager lLMan;
    GetMemUpdateGroupAdapter getMemAdapup;
    ArrayList<GetGroupMembersModel> getmemberslist = new ArrayList<GetGroupMembersModel>();
    ArrayList<String> getmembIDDt = new ArrayList<String>();
    RecyclerView memListupdatestatusRecycler;
    GetMemUpdateStatusGroupAdapter getMemUpdateStatusGroupAdapter;
    ArrayList<GetMemUpdtStatusModel> getmembersStatuslist = new ArrayList<>();
    Uri outputFileUri;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";
    String selectedImagePath = "", sendGRPName;
    Bitmap bitmap;
    Typeface regularFont, boldFont, thinFont, specialFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_group);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        update_key = getIntent().getStringExtra("GROUPEDIT");
        update_group_id = getIntent().getStringExtra("GroupId");
        Log.d("DDDDDDDD", update_key + "///" + update_group_id);
        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        up_camera_click = findViewById(R.id.up_camera_click);
        up_camera_click.setOnClickListener(this);
        up_group_user_image = findViewById(R.id.up_group_user_image);
        up_groupNamET = findViewById(R.id.up_groupNamET);
        update_grp_name_save = findViewById(R.id.update_grp_name_save);
        update_grp_name_save.setTypeface(regularFont);
        update_grp_name_save.setOnClickListener(this);
        up_addMemButt_text = findViewById(R.id.up_addMemButt_text);
        up_addMemButt_text.setTypeface(boldFont);
        up_addMemButt = findViewById(R.id.up_addMemButt);
        up_addMemButt.setOnClickListener(this);
        updateGrouButt = findViewById(R.id.updateGrouButt);
        updateGrouButt.setOnClickListener(this);
        up_getaddMemRecycler = findViewById(R.id.up_getaddMemRecycler);
        getMemAdapup = new GetMemUpdateGroupAdapter(getmemberslist, UpdateGroupActivity.this, R.layout.row_get_mem_in_group, update_group_id);
        lLMan = new LinearLayoutManager(this);
        up_getaddMemRecycler.setNestedScrollingEnabled(false);
        up_getaddMemRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        getDataForUpdate();
    }

    public void getDataForUpdate() {
        getmemberslist.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Log.d("GroupUPDATEDetailsUrl", AppUrls.BASE_URL + "group/get_group_details?user_id=" + user_id + "&group_id=" + update_group_id + "&action=UPDATEGROUP");
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "group/get_group_details?user_id=" + user_id + "&group_id=" + update_group_id + "&action=UPDATEGROUP",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {


                            Log.d("UPDATEDetailsResp", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {

                                    // Toast.makeText(UpdateGroupActivity.this, "Data Fetched MyGroup", Toast.LENGTH_SHORT).show();
                                    JSONObject getData = jsonObject.getJSONObject("data");

                                    String group_ids = getData.getString("member_ids");
                                    selected_gmIds = group_ids;

                                    //GET_GROUP_SECTION
                                    JSONObject getGroupData = getData.getJSONObject("group_details");
                                    fetch_group_id_G = getGroupData.getString("group_id");
                                    fetch_group_name_G = getGroupData.getString("group_name");
                                    up_groupNamET.setText(fetch_group_name_G);
                                    fetch_group_pic_G = AppUrls.BASE_IMAGE_URL + getGroupData.getString("group_pic");
                                    Picasso.with(UpdateGroupActivity.this)
                                            .load(fetch_group_pic_G)
                                            .placeholder(R.drawable.group_dummy)
                                            .into(up_group_user_image);
                                    //GET_MEMBERS_SECTION
                                    JSONArray getMemData = getData.getJSONArray("member_details");
                                    for (int i = 0; i < getMemData.length(); i++) {
                                        GetGroupMembersModel gMemberList = new GetGroupMembersModel();
                                        JSONObject jMemArr = getMemData.getJSONObject(i);
                                        gMemberList.setId_M(jMemArr.getString("id"));
                                        gMemberList.setUser_name_M(jMemArr.getString("user_name"));
                                        getmemberslist.add(gMemberList);
                                        String gm_ids = jMemArr.getString("id");
                                        getmembIDDt.add(gm_ids);
                                        Log.d("LKLKLK", getmemberslist.toString());
                                    }
                                    up_getaddMemRecycler.setAdapter(getMemAdapup);
                                }
                                if (successResponceCode.equals("10200")) {

                                    Toast.makeText(UpdateGroupActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {

                                    Toast.makeText(UpdateGroupActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                          error.getMessage();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GroupDetails_Header", "HEADER " + headers.toString());
                    return headers;
                }
            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strRe);

        } else {

            Toast.makeText(UpdateGroupActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == up_camera_click) {
            selectionOption();
        }
        if (view == up_addMemButt) {

            getmembersStatuslist.clear();
            final Dialog dialog = new Dialog(UpdateGroupActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.add_member_update_status_list);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);
            add_mem_statubut = dialog.findViewById(R.id.add_mem_statubut);
            searchupdateV = dialog.findViewById(R.id.searchupdateV);
            memListupdatestatusRecycler = dialog.findViewById(R.id.memListupdatestatusRecycler);
            getMemUpdateStatusGroupAdapter = new GetMemUpdateStatusGroupAdapter(getmembersStatuslist, UpdateGroupActivity.this, R.layout.add_memebers_status_dialog_row);
            lLMan = new LinearLayoutManager(this);
            memListupdatestatusRecycler.setNestedScrollingEnabled(false);
            memListupdatestatusRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            add_mem_statubut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                    //  Toast.makeText(UpdateGroupActivity.this, "Members Added", Toast.LENGTH_SHORT).show();
                }
            });
            searchupdateV.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String query) {

                    getMemUpdateStatusGroupAdapter.getFilter().filter(query);
                    return false;
                }
            });

            getMemberStatus();
            dialog.show();
        }
        if (view == updateGrouButt) {
            //  groupProfilePicUpdate();
            groupDataUpdate();
        }
        if (view == update_grp_name_save) {
            String namevalue = up_groupNamET.getText().toString();
            updateGroupName(namevalue);
        }
    }
    private void updateGroupName(final String namevalue) {
        checkInternet = NetworkChecking.isConnected(UpdateGroupActivity.this);
        if (checkInternet) {
            String updtNameUrl = AppUrls.BASE_URL + AppUrls.GROUP_UPDATE_NAME;
            Log.d("update", updtNameUrl);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, updtNameUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("nameUPtRESsp", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100")) {
                            JSONObject getdataObj = jsonObject.getJSONObject("data");
                            // Toast.makeText(UpdateGroupActivity.this, "Group Name Updated", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(UpdateGroupActivity.this, GroupsActivity.class);
                            startActivity(intent);
                        }
                        if (successResponceCode.equals("10200")) {
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                   error.getMessage();
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("group_id", update_group_id);
                    params.put("group_name", namevalue);
                    Log.d("UPDATEPARAM", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GroupDetails_Header", "HEADER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(UpdateGroupActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    private void getMemberStatus() {
        checkInternet = NetworkChecking.isConnected(UpdateGroupActivity.this);
        if (checkInternet) {//http://192.168.1.61:8090/api/v1/group/get_members_status?user_id=59f03caf1f3c56e80e000029&user_type=USER&group_id=5a2fc6180c6eff045c4a4476
            String memberstatus_url = AppUrls.BASE_URL + AppUrls.GET_GROUP_MEMEBER_STATUS + "?user_id=" + user_id + "&user_type=" + user_type + "&group_id=" + update_group_id;
            Log.d("asdfdf", memberstatus_url);
            StringRequest strStatus = new StringRequest(Request.Method.GET, memberstatus_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("StatusRESsp", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100")) {

                            //  Toast.makeText(UpdateGroupActivity.this, "Data Fetched Successfully", Toast.LENGTH_SHORT).show();
                            JSONArray getArrayData = jsonObject.getJSONArray("data");
                            for (int i = 0; i < getArrayData.length(); i++) {
                                GetMemUpdtStatusModel datastatus = new GetMemUpdtStatusModel();
                                JSONObject itemArray = getArrayData.getJSONObject(i);
                                datastatus.setId(itemArray.getString("id"));
                                datastatus.setName(itemArray.getString("name"));
                                datastatus.setStatus(itemArray.getString("status"));
                                getmembersStatuslist.add(datastatus);
                            }
                            memListupdatestatusRecycler.setAdapter(getMemUpdateStatusGroupAdapter);
                        }
                        if (successResponceCode.equals("10200")) {
                            Toast.makeText(UpdateGroupActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                        }
                        if (successResponceCode.equals("10300")) {
                            Toast.makeText(UpdateGroupActivity.this, "No Data Found..!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                        error.getMessage();
                }
            }) {
                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GroupDetails_Header", "HEADER " + headers.toString());
                    return headers;
                }
            };
            strStatus.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strStatus);
        } else {
            Toast.makeText(UpdateGroupActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    public void setGmName(ArrayList<String> gm_ids, ArrayList<String> gm_names) {
        selected_gmIds = gm_ids.toString();
        // ,getmemberslist
        getmemberslist.clear();
        for (int i = 0; i < gm_names.size(); i++) {
            GetGroupMembersModel getMemL = new GetGroupMembersModel();
            selected_gmNames = gm_names.get(i);
            getMemL.setUser_name_M(selected_gmNames);
            getmemberslist.add(getMemL);
            Log.d("GetMemName", selected_gmNames);
            Log.d("GetMemId", gm_ids.get(i));
        }
        up_getaddMemRecycler.setAdapter(getMemAdapup);
        // getMemAdapup.notifyDataSetChanged();
    }

    private void selectionOption() {
        final Dialog dialog = new Dialog(UpdateGroupActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.camera_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView take_camera_pic = dialog.findViewById(R.id.take_camera_pic);
        TextView take_gallery_pic = dialog.findViewById(R.id.take_gallery_pic);
        TextView cancel = dialog.findViewById(R.id.cancel);
        final boolean result = ImagePermissions.checkPermission(UpdateGroupActivity.this);
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);
        take_camera_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (result)
                    if (ContextCompat.checkSelfPermission(UpdateGroupActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        if (getFromPref(UpdateGroupActivity.this, ALLOW_KEY)) {
                            showSettingsAlert();
                        } else if (ContextCompat.checkSelfPermission(UpdateGroupActivity.this,
                                android.Manifest.permission.CAMERA)

                                != PackageManager.PERMISSION_GRANTED) {

                            // Should we show an explanation?
                            if (ActivityCompat.shouldShowRequestPermissionRationale(UpdateGroupActivity.this,
                                    android.Manifest.permission.CAMERA)) {
                                showAlert();
                            } else {
                                // No explanation needed, we can request the permission.
                                ActivityCompat.requestPermissions(UpdateGroupActivity.this,
                                        new String[]{android.Manifest.permission.CAMERA},
                                        0);
                            }
                        }
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        ContentValues values = new ContentValues(1);
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                        outputFileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        startActivityForResult(intent, 0);
                    }
                dialog.cancel();
            }

        });
        take_gallery_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
                dialog.cancel();

            }

        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel(); // dismissing the popup
            }

        });

        dialog.show();

    }

    public static Boolean getFromPref(Context context, String key) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        return (myPrefs.getBoolean(key, false));
    }

    private void showSettingsAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(UpdateGroupActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startInstalledAppDetailsActivity(UpdateGroupActivity.this);
                    }
                });
        alertDialog.show();
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    private void showAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(UpdateGroupActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");
        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(UpdateGroupActivity.this,
                                new String[]{android.Manifest.permission.CAMERA},
                                0);
                    }
                });
        alertDialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case 0:
                if (resultCode == RESULT_OK) {
                    Log.d("PPP", outputFileUri.toString());

                    if (outputFileUri != null) {
                        bitmap = decodeSampledBitmapFromUri(outputFileUri, up_group_user_image.getWidth(), up_group_user_image.getHeight());
                        if (bitmap == null) {
                            Toast.makeText(getApplicationContext(), "the image data could not be decoded" + outputFileUri.getPath(), Toast.LENGTH_LONG).show();
                        } else {
                            selectedImagePath = getRealPathFromURI(UpdateGroupActivity.this, outputFileUri);// outputFileUri.getPath().
                            Log.d("CAMERASELECTPATH", selectedImagePath);
                            up_group_user_image.setImageBitmap(bitmap);
                            groupProfilePicUpdate();
                        }
                    }
                }
                break;
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri targetUri = data.getData();
                    Log.d("TGGGGG", targetUri.toString());
                    Bitmap bitmap;
                    bitmap = decodeSampledBitmapFromUri(targetUri, up_group_user_image.getWidth(), up_group_user_image.getHeight());
                    if (bitmap == null) {
                        Toast.makeText(getApplicationContext(), "the image data could not be decoded" + targetUri.getPath(), Toast.LENGTH_LONG).show();
                    } else {
                        selectedImagePath = getPath(targetUri);// targetUri.getPath();
                        Log.d("GALLRYSSSSSSSSS", selectedImagePath);
                        up_group_user_image.setImageBitmap(bitmap);
                        groupProfilePicUpdate();
                    }
                }
                break;
            default:
                break;
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null,
                null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            return cursor.getString(columnIndex);
        }
        return uri.getPath();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            if ("content".equals(contentUri.getScheme())) {
                String[] proj = {MediaStore.Images.Media.DATA};
                cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else {
                return contentUri.getPath();
            }
        } finally {

            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public Bitmap decodeSampledBitmapFromUri(Uri uri, int reqWidth, int reqHeight) {
        Bitmap bm = null;
        String filePath = getPath(uri);
        // Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 516.0f;    //816 and 612
        float maxWidth = 412.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            bm = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(bm);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            bm = Bitmap.createBitmap(bm, 0, 0,
                    bm.getWidth(), bm.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = filePath;
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            bm.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return bm;
      }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }


    private void groupDataUpdate() {
        checkInternet = NetworkChecking.isConnected(UpdateGroupActivity.this);
        if (getCreateGroupValidation()) {
            if (checkInternet) {

                Log.d("WITHOUTUPDT", selected_gmIds);
                final String val1 = selected_gmIds;
                sendGRPName = up_groupNamET.getText().toString();
                String va = val1.toString();
                String strWithoutSpace1 = va.replaceAll(" ", "");
                int mem = strWithoutSpace1.lastIndexOf(",");
                if (mem > 0)
                    strWithoutSpace1 = new StringBuilder(strWithoutSpace1).replace(mem, mem + 1, "").toString();

                Log.d("GRPUPDATE", sendGRPName + " ///// " + val1);
                Log.d("GRPUPDATE", AppUrls.BASE_URL + AppUrls.GROUP_UPDATE);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GROUP_UPDATE, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("GRPUPDATERESP", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String successResponceCode = jsonObject.getString("response_code");
                            if (successResponceCode.equals("10100")) {
                                JSONObject jobj = jsonObject.getJSONObject("data");
                                String grp_id = jobj.getString("group_id");
                                Intent intent = new Intent(UpdateGroupActivity.this, GroupsActivity.class);
                                startActivity(intent);
                            }
                            if (successResponceCode.equals("10200")) {

                                Toast.makeText(getApplicationContext(), "Invalid Input..!", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        error.getMessage();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("group_id", update_group_id);
                        params.put("group_name", sendGRPName);
                        params.put("members", removeCharacter(val1.toString()));
                        Log.d("UPDATEPARAM", params.toString());
                        return params;
                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(UpdateGroupActivity.this);
                requestQueue.add(stringRequest);

            } else {

                Toast.makeText(getApplicationContext(), "No Internet Connection ..!!", Toast.LENGTH_SHORT).show();

            }
        }
    }

    private void groupProfilePicUpdate() {
        checkInternet = NetworkChecking.isConnected(this);

        if (checkInternet) {

            String urllllll = AppUrls.BASE_URL + AppUrls.GROUP_UPDATE_PROFILE_PIC;
            Log.d("GROUPUPNURL:", urllllll);
            Log.d("UPDPATHHHHH", selectedImagePath);
            UpdateGroupActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    //  if (progressDialog != null)
                    //   progressDialog.show();
                }
            });
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            new HttpUpload(this, selectedImagePath).execute();
        } else {

            Toast.makeText(UpdateGroupActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    public class HttpUpload extends AsyncTask<Void, Integer, Void> {

        private Context context;
        private String imgPath;
        private HttpClient client;

        private long totalSize;
        public String url = AppUrls.BASE_URL + AppUrls.GROUP_UPDATE_PROFILE_PIC;

        public HttpUpload(Context context, String imgPath) {
            super();
            this.context = context;
            this.imgPath = imgPath;
        }

        @Override
        protected void onPreExecute() {
            //Set timeout parameters
            int timeout = 10000;
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeout);
            HttpConnectionParams.setSoTimeout(httpParameters, timeout);
            //We'll use the DefaultHttpClient
            client = new DefaultHttpClient(httpParameters);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                Bitmap bmp = BitmapFactory.decodeFile(imgPath);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 70, bos);
                InputStream in = new ByteArrayInputStream(bos.toByteArray());

                String grp_name = up_groupNamET.getText().toString();
                HttpPost post = new HttpPost(url);

                MultipartEntity entity = new MyMultipartEntity(new MyMultipartEntity.ProgressListener() {
                    @Override
                    public void transferred(long num) {
                        //Call the onProgressUpdate method with the percent completed
                        publishProgress((int) ((num / (float) totalSize) * 100));
                        Log.d("DEBUG", num + " - " + totalSize);
                    }
                });


                ContentBody cbFile = new InputStreamBody(in, "image/jpeg", "jpeg");
                // ContentBody cbFile = new FileBody( file, "image/*" );
                entity.addPart("file", cbFile);
                //  entity.addPart("group_name",new StringBody(grp_name));
                entity.addPart("group_id", new StringBody(update_group_id));
                // entity.addPart("members",new StringBody(removeCharacter(val1.toString())));

                //After adding everything we get the content's lenght
                totalSize = entity.getContentLength();

                //We add the entity to the post request
                post.setEntity(entity);

                //Execute post request
                HttpResponse response = client.execute(post);
                int statusCode = response.getStatusLine().getStatusCode();

                if (statusCode == HttpStatus.SC_OK) {
                    //If everything goes ok, we can get the response
                    String fullRes = EntityUtils.toString(response.getEntity());
                    Log.d("DEBUG", fullRes);
                    finish();

                } else {
                    Log.d("DEBUG", "HTTP Fail, Response Code: " + statusCode);
                }

            } catch (ClientProtocolException e) {
                // Any error related to the Http Protocol (e.g. malformed url)
                e.printStackTrace();
            } catch (IOException e) {
                // Any IO error (e.g. File not found)
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            //Set the pertange done in the progress dialog

        }

        @Override
        protected void onPostExecute(Void result) {
            //Dismiss progress dialog
            //   Toast.makeText(context, "Sucess", Toast.LENGTH_SHORT).show();
            Log.d("resultsssssssssss", "" + result);

        }

    }

    private String removeCharacter(String word) {
        String[] specialCharacters = {"-", " ", "(", ")", "[", "]", "}", "."};
        StringBuilder sb = new StringBuilder(word);
        for (int i = 0; i < sb.toString().length() - 1; i++) {
            for (String specialChar : specialCharacters) {
                if (sb.toString().contains(specialChar)) {
                    int index = sb.indexOf(specialChar);
                    sb.deleteCharAt(index);
                }
            }
        }
        return sb.toString();
    }


    private Boolean getCreateGroupValidation() {
        Boolean result = true;
        String name = up_groupNamET.getText().toString();

        if (name.equals("") || name.length() < 3) {
            up_groupNamET.setError("Enter Valid Name");
            result = false;
        } else if (!name.matches("^[\\p{L} .'-]+$")) {
            up_groupNamET.setError("Special characters not allowed");
            result = false;
        } else if (getmemberslist.size() <= 0) {
            up_groupNamET.setError("Please add minimum group members");
        } else {
            up_groupNamET.setError(null);
        }

        return result;
    }

}
