package com.sleepchallenge.activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.sleepchallenge.R;
import com.sleepchallenge.adapters.SleepChllengesViewPagerAdapter;
import com.sleepchallenge.fragments.EulaRulesFragment;
import com.sleepchallenge.fragments.FaqFragment;
import com.sleepchallenge.fragments.TermsandConditionFragment;

public class FaqTermsConditionActivity extends AppCompatActivity implements View.OnClickListener
{
    ViewPager view_pager_faq_termscondition;
    TabLayout tab;
     ImageView back_img,notification;
     String type;
    SleepChllengesViewPagerAdapter faq_temsCodition_adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq_terms_condition);

        back_img=findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        notification=findViewById(R.id.notification);
        notification.setOnClickListener(this);

        Bundle b=getIntent().getExtras();

        type=b.getString("condition");

        tab =  findViewById(R.id.tabLayout_faq_termscondition);
        view_pager_faq_termscondition =  findViewById(R.id.view_pager_faq_termscondition);
        setupViewPager(view_pager_faq_termscondition);
        tab.setupWithViewPager(view_pager_faq_termscondition);

    }

    private void setupViewPager(ViewPager view_pager_suggest)
    {
        faq_temsCodition_adapter = new SleepChllengesViewPagerAdapter(getSupportFragmentManager());


        if(type.equals("NORMAL"))
        {
            view_pager_suggest.setCurrentItem(1);
            faq_temsCodition_adapter.addFrag(new FaqFragment(), "FAQ");
            faq_temsCodition_adapter.addFrag(new TermsandConditionFragment(), "Terms And Codnitions");
            faq_temsCodition_adapter.addFrag(new EulaRulesFragment(), "EULA Rules");
        }

        if(type.equals("TermsandConditions"))
        {
            view_pager_suggest.setCurrentItem(2);
            faq_temsCodition_adapter.addFrag(new TermsandConditionFragment(), "Terms And Codnitions");
            faq_temsCodition_adapter.addFrag(new FaqFragment(), "FAQ");
            faq_temsCodition_adapter.addFrag(new EulaRulesFragment(), "EULA Rules");
        }
        if(type.equals("EULA"))
        {
            view_pager_suggest.setCurrentItem(3);
            faq_temsCodition_adapter.addFrag(new EulaRulesFragment(), "EULA Rules");
            faq_temsCodition_adapter.addFrag(new FaqFragment(), "FAQ");
            faq_temsCodition_adapter.addFrag(new TermsandConditionFragment(), "Terms And Codnitions");

        }




        view_pager_suggest.setAdapter(faq_temsCodition_adapter);

        tab.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(view_pager_faq_termscondition) {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab)
                    {
                        super.onTabSelected(tab);


                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }

                }
        );
    }

    @Override
    public void onClick(View view)
    {
        if(view==back_img)
        {finish();}

        if(view==notification)
        {
            Intent notif = new Intent(FaqTermsConditionActivity.this, NotificationActivity.class);
            startActivity(notif);
        }
    }
}
