package com.sleepchallenge.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;
import com.sleepchallenge.models.DeactivateReasonModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DeactivateActivity extends AppCompatActivity implements View.OnClickListener{

    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String user_id, user_type, token, device_ID,send_check_Value,running_chalnge, upcoming_chalnge, pending_chalnge, paused_chalnge, wallet_amt;
    ImageView back_img;
    TextView title_text,help_text_title,deactivate_proceed;
    Typeface regularFont,specialFont;
    RadioButton  radio_other;
    EditText reason_other_desc_edt;
    LinearLayout ll_description;
    RecyclerView recyclerview_radio_group;
    DeactivateAccAdapter  deactivateAccAdapter;
    ArrayList<DeactivateReasonModel> reasonmodelList;
    RecyclerView.LayoutManager layoutManager;
    private RadioButton lastCheckedRB = null;
    Dialog dialog,dialog2;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deactivate);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_ID = userDetails.get(UserSessionManager.DEVICE_ID);
        checkInternet = NetworkChecking.isConnected(this);

        back_img=findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        title_text=findViewById(R.id.title_text);
        title_text.setTypeface(regularFont);
        help_text_title=findViewById(R.id.help_text_title);
        help_text_title.setTypeface(regularFont);
        radio_other = findViewById(R.id.radio_other);
        reason_other_desc_edt=findViewById(R.id.reason_other_desc_edt);
        ll_description =  findViewById(R.id.ll_description);

        deactivate_proceed=findViewById(R.id.deactivate_proceed);
        deactivate_proceed.setOnClickListener(this);
        deactivate_proceed.setTypeface(regularFont);
        recyclerview_radio_group = findViewById(R.id.recyclerview_radio_group);

        getReasonsForDeactiv();

        radio_other.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (radio_other.isChecked())
                {
                    ll_description.setVisibility(View.VISIBLE);

                }

            }
        });

    }

    private void getReasonsForDeactiv() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_deactivate = AppUrls.BASE_URL + AppUrls.USER_DEACTIVATE_REASON;
            Log.d("DEACTURL", url_deactivate);
            StringRequest req_members = new StringRequest(Request.Method.GET, url_deactivate, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("DECTRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100"))
                        {
                            reasonmodelList = new ArrayList<>();
                            JSONArray jsonArray = jobcode.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++)
                            {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                DeactivateReasonModel dact = new DeactivateReasonModel(jdataobj);
                                reasonmodelList.add(dact);
                            }
                            deactivateAccAdapter = new DeactivateAccAdapter(reasonmodelList, DeactivateActivity.this, R.layout.row_deactivate_reason_list);
                            layoutManager = new LinearLayoutManager(getApplicationContext());
                            recyclerview_radio_group.setNestedScrollingEnabled(false);
                            recyclerview_radio_group.setLayoutManager(layoutManager);
                            recyclerview_radio_group.setAdapter(deactivateAccAdapter);
                            recyclerview_radio_group.setAdapter(deactivateAccAdapter);
                        }
                        if (response_code.equals("10200"))
                        {

                            Toast.makeText(DeactivateActivity.this, "No Data Found..!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                  error.getMessage();
                }
            }) {
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(DeactivateActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(DeactivateActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onClick(View view)
    {
       if(view==back_img)
       {
           finish();
       }

       if(view==deactivate_proceed)
       {
           getDeactivateDetail();
       }
    }
    private void getDeactivateDetail() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_all_members = AppUrls.BASE_URL + AppUrls.DEACTIVATE_USER_DETAIL + "?user_id=" + user_id + "&user_type=" + user_type;
            Log.d("DDUSEDETAIURL", url_all_members);
            StringRequest req_members = new StringRequest(Request.Method.GET, url_all_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("DDUSERDETRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                            JSONObject dataObj = jobcode.getJSONObject("data");
                            running_chalnge = dataObj.getString("running_challenges");
                            upcoming_chalnge = dataObj.getString("upcoming_challenges");
                            pending_chalnge = dataObj.getString("pending_challenges");
                            paused_chalnge = dataObj.getString("paused_challenges");
                            wallet_amt = dataObj.getString("wallet_amt");
                            //my_groups = dataObj.getString("my_groups");
                            // Log.d("DDDDDDD",running_chalnge+"//"+upcoming_chalnge+"//"+pending_chalnge+"//"+paused_chalnge+"//"+wallet_amt+"//"+my_groups);
                        }
                        if (response_code.equals("10200")) {

                            Toast.makeText(DeactivateActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                        getDialogDetail();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                   error.getMessage();
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(DeactivateActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(DeactivateActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }

    }

    private void getDialogDetail()
    {
        dialog = new Dialog(DeactivateActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.deactivate_detail_custom_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        //title
        TextView pending_title_text =  dialog.findViewById(R.id.pending_title_text);
        TextView pausedtitle_text =  dialog.findViewById(R.id.pausedtitle_text);
        TextView upcoming_title_text =  dialog.findViewById(R.id.upcoming_title_text);
        TextView running_title_text =  dialog.findViewById(R.id.running_title_text);
        TextView deactivate_title =  dialog.findViewById(R.id.deactivate_title);

        //value text
        TextView okdialogbtn =  dialog.findViewById(R.id.okdialogbtn);
        final TextView sendmoneydialogbtn =  dialog.findViewById(R.id.sendmoneydialogbtn);
        TextView pending_text = dialog.findViewById(R.id.pending_text);
        TextView paused_text =  dialog.findViewById(R.id.paused_text);
        TextView upcoming_text =  dialog.findViewById(R.id.upcoming_text);
        TextView running_text = dialog.findViewById(R.id.running_text);
        TextView wallet_amount_text = dialog.findViewById(R.id.wallet_amount_text);
        TextView mygroups_text =  dialog.findViewById(R.id.mygroups_text);

        okdialogbtn.setTypeface(regularFont);
        sendmoneydialogbtn.setTypeface(regularFont);
        pending_text.setTypeface(regularFont);
        paused_text.setTypeface(regularFont);
        upcoming_text.setTypeface(regularFont);
        running_text.setTypeface(regularFont);
        wallet_amount_text.setTypeface(specialFont);
        pending_title_text.setTypeface(regularFont);
        pausedtitle_text.setTypeface(regularFont);
        upcoming_title_text.setTypeface(regularFont);
        running_title_text.setTypeface(regularFont);
        deactivate_title.setTypeface(regularFont);

        if(user_type.equals("SPONSOR"))
        {
            //texttitle
            pending_title_text.setVisibility(View.GONE);
            pausedtitle_text.setVisibility(View.GONE);
            upcoming_title_text.setVisibility(View.GONE);
            running_title_text.setVisibility(View.GONE);
            //value
            pending_text.setVisibility(View.GONE);
            paused_text.setVisibility(View.GONE);
            upcoming_text.setVisibility(View.GONE);
            running_text.setVisibility(View.GONE);
        }
        pending_text.setText(pending_chalnge);
        paused_text.setText(paused_chalnge);
        upcoming_text.setText(upcoming_chalnge);
        running_text.setText(running_chalnge);
        wallet_amount_text.setText(Html.fromHtml("&#36;" + "<b>" + wallet_amt + "</b>"));
       // mygroups_text.setText(my_groups);
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);
        okdialogbtn.setOnClickListener(new View.OnClickListener()
        {

            @Override

            public void onClick(View v) {
                // dialog.cancel();
                double parse_wallet_amt=Double.parseDouble(wallet_amt);
                if (parse_wallet_amt!=0)
                {
                    Toast.makeText(DeactivateActivity.this, "You Can't Deactivate Account...!  Please Transfer Amount to your PayPal Account.", Toast.LENGTH_LONG).show();
                    sendmoneydialogbtn.setVisibility(View.VISIBLE);
                }
                else if(!running_chalnge.equals("0"))
                {
                    Toast.makeText(DeactivateActivity.this, "You Can't Deactivate Account...!  Please Complete Running Challnge.", Toast.LENGTH_LONG).show();
                }
                else if(!upcoming_chalnge.equals("0"))
                {
                    Toast.makeText(DeactivateActivity.this, "You Can't Deactivate Account...!  Please Complete Upcoming Challegne.", Toast.LENGTH_LONG).show();
                }
                else if(!pending_chalnge.equals("0"))
                {
                    Toast.makeText(DeactivateActivity.this, "You Can't Deactivate Account...!  Please Complete Pending Challenge.", Toast.LENGTH_LONG).show();
                }
                else if(!paused_chalnge.equals("0"))
                {
                    Toast.makeText(DeactivateActivity.this, "You Can't Deactivate Account...!  Please Complete Paused Challenge.", Toast.LENGTH_LONG).show();
                }
                else
                {
                    finalDeactivationAcc();
                }
            }

        });
        sendmoneydialogbtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {// Toast.makeText(DeactivateAccountActivity.this, "SEND MONEY", Toast.LENGTH_LONG).show();
                dialog2 = new Dialog(DeactivateActivity.this);
                dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog2.setContentView(R.layout.custom_dialog_send_money);
                TextView titleDialog = dialog2.findViewById(R.id.titleDialog);
                final EditText edt_send_amount = dialog2.findViewById(R.id.edt_send_amount);
                final Button sendMoneyButton = dialog2.findViewById(R.id.sendMoneyButton);
                titleDialog.setText("Send Money To Paypal");
                sendMoneyButton.setText("Send Money");
                titleDialog.setTypeface(regularFont);
                sendMoneyButton.setTypeface(regularFont);

                sendMoneyButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {
                        String send_amount=edt_send_amount.getText().toString();

                        if(send_amount == null || send_amount.equals("") || send_amount.length() < 0)
                        {
                            edt_send_amount.setError("Please Enter Amount..!");
                        }
                        else
                        {
                            sendWalletAmount(send_amount);

                        }
                    }
                });

                dialog2.show();

            }
        });
        dialog.show();
    }


  //Reason Adapter
  public class DeactivateAccAdapter extends RecyclerView.Adapter<DeactivateReasonHolder> {
      public ArrayList<DeactivateReasonModel> reasonlistModels;
      DeactivateActivity context;
      LayoutInflater li;
      int resource;
      UserSessionManager session;
      String user_name;

      public DeactivateAccAdapter(ArrayList<DeactivateReasonModel> reasonlistModels, DeactivateActivity context, int resource) {
          this.reasonlistModels = reasonlistModels;
          this.context = context;
          this.resource = resource;
          li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

          session = new UserSessionManager(context);
          HashMap<String, String> userDetails = session.getUserDetails();
          user_name = userDetails.get(UserSessionManager.USER_NAME);

      }

      @Override
      public DeactivateReasonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
          View layout = li.inflate(resource, parent, false);
          DeactivateReasonHolder slh = new DeactivateReasonHolder(layout);
          return slh;
      }

      @Override
      public void onBindViewHolder(final DeactivateReasonHolder holder, final int position) {
          String str_reason = reasonlistModels.get(position).reason;
          String converted_string = str_reason.substring(0, 1).toUpperCase() + str_reason.substring(1);
          holder.reasone_radio_dynamic_button.setText(converted_string);
          holder.reasone_radio_dynamic_button.setTypeface(specialFont);

          View.OnClickListener rbClick = new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  RadioButton checked_rb = (RadioButton) v;
                  if (lastCheckedRB != null) {
                      lastCheckedRB.setChecked(false);
                      radio_other.setChecked(false);
                      ll_description.setVisibility(View.GONE);
                      send_check_Value = holder.reasone_radio_dynamic_button.getText().toString();
                  }
                  lastCheckedRB = checked_rb;
                  radio_other.setChecked(false);
                  ll_description.setVisibility(View.GONE);
                  send_check_Value = holder.reasone_radio_dynamic_button.getText().toString();
              }
          };
          holder.reasone_radio_dynamic_button.setOnClickListener(rbClick);
          radio_other.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
              @Override
              public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                  if (radio_other.isChecked()) {
                      ll_description.setVisibility(View.VISIBLE);
                      reasonmodelList.clear();
                      getReasonsForDeactiv();
                  }
              }
          });

      }

      @Override
      public int getItemCount() {
          return this.reasonlistModels.size();
      }
  }
  //Reason Holder

    public class DeactivateReasonHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RadioButton reasone_radio_dynamic_button;
        AllItemClickListeners allItemClickListener;

        public DeactivateReasonHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            reasone_radio_dynamic_button = itemView.findViewById(R.id.reasone_radio_dynamic_button);
        }

        @Override
        public void onClick(View view) {

            this.allItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(AllItemClickListeners ic) {
            this.allItemClickListener = ic;
        }
    }



    private void sendWalletAmount(final String amount)
    {
        checkInternet = NetworkChecking.isConnected(DeactivateActivity.this);

        if (checkInternet)
        {
            dialog.cancel();
            String send_money_url = AppUrls.BASE_URL + AppUrls.TRANSFER_AMOUNT_TO_PAYPAL_ACC;


            Log.d("SENDMONEYLURL", send_money_url);
            StringRequest strFinalDeact = new StringRequest(Request.Method.POST, send_money_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("RESPSENDMONEY", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {

                            Toast.makeText(DeactivateActivity.this, "Payment transfered successfully..", Toast.LENGTH_LONG).show();

                            Intent redirect = new Intent(DeactivateActivity.this, DeactivateActivity.class);
                            startActivity(redirect);
                            finish();
                        }
                        if (response_code.equals("10200")) {

                            Toast.makeText(DeactivateActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10300")) {

                            Toast.makeText(DeactivateActivity.this, "User not exist.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10400")) {

                            Toast.makeText(DeactivateActivity.this, "Paypal Registered Email not Found..!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10500")) {

                            Toast.makeText(DeactivateActivity.this, "Insufficient amount.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10600")) {

                            Toast.makeText(DeactivateActivity.this, "Payment transfer failed.!", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            }) {

                @Override
                protected Map<String, String> getParams()  {
                    Map<String, String> param = new HashMap<>();
                    param.put("user_id", user_id);
                    param.put("user_type", user_type);
                    param.put("amount", amount);
                    Log.d("finalDEACTPARAM", param.toString());
                    return param;
                }

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(DeactivateActivity.this);
            requestQueue.add(strFinalDeact);
        } else {
            Toast.makeText(DeactivateActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    private void finalDeactivationAcc() {

        checkInternet = NetworkChecking.isConnected(DeactivateActivity.this);
        if (checkInternet) {
            String finaldeact_user_url = AppUrls.BASE_URL + AppUrls.DEACTIVATE_USER;

            final String send_reason_other = reason_other_desc_edt.getText().toString();
            Log.d("DEACTFIALURL", finaldeact_user_url + "//" + send_reason_other);
            StringRequest strFinalDeact = new StringRequest(Request.Method.POST, finaldeact_user_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("FinalDeactivatRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                            // JSONObject dataObj = jobcode.getJSONObject("message");
                            Toast.makeText(DeactivateActivity.this, "Deactivated Successfully..", Toast.LENGTH_LONG).show();
                            dialog.cancel();
                            Intent redirect = new Intent(DeactivateActivity.this, WelcomeActivity.class);
                            startActivity(redirect);
                        }
                        if (response_code.equals("10200")) {

                            Toast.makeText(DeactivateActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            }) {

                @Override
                protected Map<String, String> getParams()  {
                    Map<String, String> param = new HashMap<>();
                    param.put("user_id", user_id);
                    param.put("user_type", user_type);
                    if (radio_other.isChecked()) {
                        param.put("reason", send_reason_other);
                    } else {
                        param.put("reason", send_check_Value);
                    }

                    Log.d("finalDEACTPARAM", param.toString());
                    return param;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(DeactivateActivity.this);
            requestQueue.add(strFinalDeact);
        } else {
            Toast.makeText(DeactivateActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }
}
