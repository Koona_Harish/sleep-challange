package com.sleepchallenge.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.MyChallengeRequestAdapter;
import com.sleepchallenge.models.SendMyChallegneRequestModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.DividerItemDecorator;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyRequestChallenge extends AppCompatActivity implements View.OnClickListener{


     ImageView back_img;
    String user_id, user_type, token, device_ID;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    RecyclerView sendchallngeRequat_recyclerView;
    ArrayList<SendMyChallegneRequestModel> sendchallngeRequatList;
    MyChallengeRequestAdapter myChallengeRequestAdapter;
    RecyclerView.LayoutManager layoutManager;
    Typeface  boldFont;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_request_challenge);
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        userSessionManager = new UserSessionManager(MyRequestChallenge.this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_ID = userDetails.get(UserSessionManager.DEVICE_ID);
        checkInternet = NetworkChecking.isConnected(this);
        sendchallngeRequat_recyclerView =  findViewById(R.id.sendchallngeRequat_recyclerView);
        TextView title_text=findViewById(R.id.title_text);
        title_text.setTypeface(boldFont);

        back_img=findViewById(R.id.back_img);
        back_img.setOnClickListener(this);


         getSendMyRequest();

    }

      public void getSendMyRequest()
      {
          if (checkInternet)
          {
              Dialog.showProgressBar(MyRequestChallenge.this, "Loading");

              String sendchllegneReqUrl= AppUrls.BASE_URL+AppUrls.MY_SEND_CHALLENGE_REQUEST+"?id="+user_id+"&type="+user_type;
              Log.d("SENDCHALLReqSTURL",sendchllegneReqUrl);
              StringRequest strAllMember = new StringRequest(Request.Method.GET, sendchllegneReqUrl, new Response.Listener<String>() {
                  @Override
                  public void onResponse(String response) {
                      Dialog.hideProgressBar();
                      Log.d("SENDCHALLReqSRESP", response);

                      try {
                          JSONObject jsonObject = new JSONObject(response);
                          String successResponceCode = jsonObject.getString("response_code");
                          if (successResponceCode.equals("10100"))
                          {

                              JSONArray jsonArray = jsonObject.getJSONArray("data");
                              sendchallngeRequatList = new ArrayList<>();
                              for (int i = 0; i < jsonArray.length(); i++)
                              {
                                  JSONObject jdataobj = jsonArray.getJSONObject(i);
                                  SendMyChallegneRequestModel challegneRequst = new SendMyChallegneRequestModel(jdataobj);
                                  sendchallngeRequatList.add(challegneRequst);
                              }

                              myChallengeRequestAdapter = new MyChallengeRequestAdapter(sendchallngeRequatList, MyRequestChallenge.this, R.layout.row_sendchallegne_request);
                              layoutManager = new LinearLayoutManager(getApplicationContext());
                              sendchallngeRequat_recyclerView.setNestedScrollingEnabled(false);
                              sendchallngeRequat_recyclerView.setLayoutManager(layoutManager);
                              RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(MyRequestChallenge.this, R.drawable.recycler_view_divider));
                              sendchallngeRequat_recyclerView.addItemDecoration(dividerItemDecoration);
                              sendchallngeRequat_recyclerView.setAdapter(myChallengeRequestAdapter);
                          }
                          if (successResponceCode.equals("10200"))
                          {
                              Toast.makeText(MyRequestChallenge.this, "No Data Found..!", Toast.LENGTH_SHORT).show();
                          }
                          if (successResponceCode.equals("10300"))
                          {
                              Toast.makeText(MyRequestChallenge.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                          }
                      } catch (JSONException e) {
                          e.printStackTrace();
                      }
                  }
              }, new Response.ErrorListener() {
                  @Override
                  public void onErrorResponse(VolleyError error) {
                      Dialog.hideProgressBar();
                      error.getMessage();
                  }
              }) {
                  @Override
                  public Map<String, String> getHeaders(){
                      Map<String, String> headers = new HashMap<>();
                      headers.put("x-access-token", token);
                      headers.put("x-device-id", device_ID);
                      headers.put("x-device-platform", "ANDROID");
                      Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                      return headers;
                  }
              };
              strAllMember.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
              RequestQueue requestQueue = Volley.newRequestQueue(this);
              requestQueue.add(strAllMember);
          } else {
              Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
          }
      }



    @Override
    public void onClick(View view)
    {
         if(view==back_img){
             finish();
         }
    }
}
