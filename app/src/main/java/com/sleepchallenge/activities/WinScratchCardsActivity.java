package com.sleepchallenge.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.sleepchallenge.R;
import com.sleepchallenge.itemclicklisteners.AllMembersItemClickListener;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class WinScratchCardsActivity extends AppCompatActivity implements View.OnClickListener{


    UserSessionManager session;
    private boolean checkInternet;
   Typeface regularFont,specialFont;
    ImageView back_img;
    private InterstitialAd mInterstitialAd;
    TextView total_amount,reward_text;
    String user_id, user_type, token, devive_id;
    RecyclerView recycler_card_view_history;
    ScaratchCardAdapter scratch_card_Adapter;
    ArrayList<ScratchCardModel> allScratchListModel;
  //  LinearLayoutManager layoutManager;
    GridLayoutManager gridLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_win_scratch_cards);

        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));

        MobileAds.initialize(WinScratchCardsActivity.this, getString(R.string.admob_app_id));
        mInterstitialAd = new InterstitialAd(WinScratchCardsActivity.this);


        checkInternet= NetworkChecking.isConnected(WinScratchCardsActivity.this);
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        devive_id = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        recycler_card_view_history=findViewById(R.id.recycler_card_view_history);

        back_img=findViewById(R.id.back_img);
        back_img.setOnClickListener(this);

        total_amount=findViewById(R.id.total_amount);
        reward_text=findViewById(R.id.reward_text);
        total_amount.setTypeface(specialFont);
        reward_text.setTypeface(regularFont);


        initializeInterstitialAd();
        loadInterstitialAd();
        getWinningCards();

    }

    private void getWinningCards()
    {
           if(checkInternet)
           {
               String scratch_url= AppUrls.BASE_URL+AppUrls.GET_SCRATCHCARDS+"?user_id="+user_id+"&user_type="+user_type;
               Log.d("SCATCHURL",scratch_url);
               StringRequest scratchREquestch=new StringRequest(Request.Method.GET, scratch_url, new Response.Listener<String>() {
                   @Override
                   public void onResponse(String response)
                   {
                       Log.d("DATARESP", response);
                        try
                        {
                            JSONObject jsonObject = new JSONObject(response);
                            String successResponceCode = jsonObject.getString("response_code");
                            String totlaAmount = jsonObject.getString("total_amount");
                            total_amount.setText(Html.fromHtml("&#36;" + "<b>" + totlaAmount + "</b>"));

                            if (successResponceCode.equals("10100"))
                            {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                allScratchListModel = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++)
                                {
                                    JSONObject jdataobj = jsonArray.getJSONObject(i);
                                    ScratchCardModel allscartchCard = new ScratchCardModel(jdataobj);
                                    allScratchListModel.add(allscartchCard);
                                }
                                scratch_card_Adapter = new ScaratchCardAdapter(allScratchListModel, WinScratchCardsActivity.this, R.layout.row_scratch_history);
                                gridLayoutManager = new GridLayoutManager(WinScratchCardsActivity.this, 2);
                                recycler_card_view_history.setLayoutManager(gridLayoutManager);
                                recycler_card_view_history.setAdapter(scratch_card_Adapter);
                            }
                            if (successResponceCode.equals("10200"))
                            {
                                Toast.makeText(WinScratchCardsActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                            }

                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                   }
               }, new Response.ErrorListener() {
                   @Override
                   public void onErrorResponse(VolleyError error)
                   {
                      error.getMessage();
                   }
               }) {
                   @Override
                   public Map<String, String> getHeaders(){
                       Map<String, String> headers = new HashMap<>();
                       headers.put("x-access-token", token);
                       headers.put("x-device-id", devive_id);
                       headers.put("x-device-platform", "ANDROID");
                       Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                       return headers;
                   }
               };
               scratchREquestch.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
               RequestQueue requestQueue = Volley.newRequestQueue(this);
               requestQueue.add(scratchREquestch);
           }
           else
           {
               Toast.makeText(WinScratchCardsActivity.this,"No Internet Connection",Toast.LENGTH_LONG).show();
           }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view)
    {
        if(view==back_img)
        {
            finish();
        }
    }

    //ADAPTER
    public class  ScaratchCardAdapter extends RecyclerView.Adapter<ScratchCardHolder>
    {
        public ArrayList<ScratchCardModel> allscratchMoldelList;
        WinScratchCardsActivity context;
        LayoutInflater li;
        int resource;
        Typeface regularFont, specialFont;
        String id,amount,is_scratched,scratching_date,view_scratch;


        public ScaratchCardAdapter(ArrayList<ScratchCardModel> allscratchMoldelList, WinScratchCardsActivity context, int resource) {
            this.allscratchMoldelList = allscratchMoldelList;
            this.context = context;
            this.resource = resource;
            li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regularFont));
            specialFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.specialFont));
          }

        @NonNull
        @Override
        public ScratchCardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View layout = li.inflate(resource, parent, false);
            ScratchCardHolder slh = new ScratchCardHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(@NonNull ScratchCardHolder holder, final int position)
        {

        //     holder.frame_week.setBackground(getResources().getDrawable(R.drawable.app_icon));
            is_scratched=allscratchMoldelList.get(position).is_scratched;
            scratching_date=parseDateToddMMyyyy(allscratchMoldelList.get(position).scratching_date);
            view_scratch=allscratchMoldelList.get(position).view;
            amount=allscratchMoldelList.get(position).amount;

            holder.availabel_date_text.setTypeface(specialFont);
            holder.is_scratch_view_amount.setTypeface(specialFont);
            holder.you_won_text.setTypeface(specialFont);

            if(is_scratched.equals("0")) ////already scratch done or not condition check  if 0 then not scratch if 1 then already scratch
            {
                if (view_scratch.equals("0"))
                {
                   holder.availabel_date_text.setVisibility(View.VISIBLE);
                    holder.availabel_date_text.setText("Available on \n" + scratching_date);
                    holder.frame_week.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    });
                } else {
                    holder.frame_week.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {

                            if (mInterstitialAd.isLoaded())
                            {
                                mInterstitialAd.show();
                                mInterstitialAd.setAdListener(new AdListener()
                                {
                                    // Listen for when user closes ad
                                    public void onAdClosed() {
                                        Intent sc_history = new Intent(getApplicationContext(), CardPageActivity.class);
                                        sc_history.putExtra("AMOUNT", allscratchMoldelList.get(position).amount);
                                        sc_history.putExtra("SCRATCH_ID", allscratchMoldelList.get(position).id);
                                        startActivity(sc_history);
                                    }
                                });

                            } else {
                                Log.d("TAG", "The interstitial wasn't loaded yet.");
                                Intent sc_history = new Intent(getApplicationContext(), CardPageActivity.class);
                                sc_history.putExtra("AMOUNT", allscratchMoldelList.get(position).amount);
                                sc_history.putExtra("SCRATCH_ID", allscratchMoldelList.get(position).id);
                                startActivity(sc_history);
                            }

                        }
                    });

                }
            }
            else
            {
                holder.is_scratch_view_amount.setVisibility(View.VISIBLE);
                holder.you_won_text.setVisibility(View.VISIBLE);
                holder.img_win_cup.setVisibility(View.VISIBLE);
                holder.is_scratch_view_amount.setText(Html.fromHtml("&#36;" + "<b>" + amount + "</b>"));
                holder.frame_week.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
            }

        }
        @Override
        public int getItemCount()
        {
            return this.allscratchMoldelList.size();
        }
    }
    //Holder

    public class ScratchCardHolder extends  RecyclerView.ViewHolder implements View.OnClickListener
    {

        public TextView availabel_date_text,is_scratch_view_amount,you_won_text;
        public ImageView img_win_cup;
        public CardView frame_week;

        AllMembersItemClickListener allMembersItemClickListener;
        public ScratchCardHolder(View itemView)
        {
            super(itemView);
            itemView.setOnClickListener(this);
            availabel_date_text = itemView.findViewById(R.id.availabel_date_text);
            is_scratch_view_amount = itemView.findViewById(R.id.is_scratch_view_amount);
            img_win_cup = itemView.findViewById(R.id.img_win_cup);
            frame_week = itemView.findViewById(R.id.frame_week);
            you_won_text = itemView.findViewById(R.id.you_won_text);
        }

        @Override
        public void onClick(View view) {
            this.allMembersItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(AllMembersItemClickListener ic) {
            this.allMembersItemClickListener = ic;
        }
    }

    //Model Class
    public class ScratchCardModel
    {


        public String id,amount,type,type_value,is_scratched,scratching_date,view;

        public ScratchCardModel(JSONObject jsonObject)
        {
            try
            {
                if(jsonObject.has("id") && !jsonObject.isNull("id"))
                    this.id = jsonObject.getString("id");

                if(jsonObject.has("amount") && !jsonObject.isNull("amount"))
                    this.amount = jsonObject.getString("amount");

                if(jsonObject.has("type") && !jsonObject.isNull("type"))
                    this.type = jsonObject.getString("type");

                if(jsonObject.has("type_value") && !jsonObject.isNull("type_value"))
                    this.type_value = jsonObject.getString("type_value");

                if(jsonObject.has("is_scratched") && !jsonObject.isNull("is_scratched"))
                    this.is_scratched = jsonObject.getString("is_scratched");

                if(jsonObject.has("scratching_date") && !jsonObject.isNull("scratching_date"))
                    this.scratching_date = jsonObject.getString("scratching_date");

                if(jsonObject.has("view") && !jsonObject.isNull("view"))
                    this.view = jsonObject.getString("view");

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void loadInterstitialAd() {
        Log.d("LAAAAa","LAAAAa");


        mInterstitialAd.loadAd(new AdRequest.Builder().build());

    }

    private void initializeInterstitialAd() {
        mInterstitialAd = new InterstitialAd(WinScratchCardsActivity.this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded()
            {
                Log.d("XXXXAD","XXXXAD");
                loadInterstitialAd();
            }

            @Override
            public void onAdFailedToLoad(int errorCode)
            {
                Log.d("FAILED","FAILED");
            }

            @Override
            public void onAdOpened() {
                Log.d("AdOpened","AdOpened");
            }

            @Override
            public void onAdLeftApplication() {
                Log.d("AdLeftApp","AdLeftApp");
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(WinScratchCardsActivity.this, "Interstitial Ad Closed",Toast.LENGTH_SHORT).show();

                loadInterstitialAd();

            }
        });
    }



}
