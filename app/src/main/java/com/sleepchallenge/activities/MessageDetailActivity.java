package com.sleepchallenge.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.MessagesDetailAdapter;
import com.sleepchallenge.models.MessagesDetailModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MessageDetailActivity extends AppCompatActivity implements View.OnClickListener
{
    ImageView back_img, member_profile_pic;
    TextView userName_text;
    Typeface regularfont;
    UserSessionManager userSessionManager;

    private boolean checkInternet;
    EditText mesagetext_edt;
    ImageView send_mesage;
    String user_id, user_type, token, device_id, m_fromname, m_fromid, m_fromtype, m_profile_pic;
    RecyclerView recycler_list_chat;
    MessagesDetailAdapter messageDetailAdapter;
    LinearLayoutManager layoutManager;
    ArrayList<MessagesDetailModel> msgDetailModalList;
    String FROM_TYPE;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_detail);
        regularfont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        Bundle bundle = getIntent().getExtras();
        m_fromid = bundle.getString("FROM_ID");
        m_fromname = bundle.getString("FROM_NAME");
        m_fromtype = bundle.getString("FROM_TYPE");
        m_profile_pic = bundle.getString("FROM_PROFILEPIC");
        FROM_TYPE = bundle.getString("FROM_TYPE");//Abusing

        userName_text =findViewById(R.id.userName_text);
        userName_text.setText(m_fromname);
        userName_text.setTypeface(regularfont);
        checkInternet = NetworkChecking.isConnected(this);
        Log.d("M_DETAIL", m_fromid + "//" + m_fromname + "//" + m_fromtype + "//" + m_profile_pic);
        userSessionManager = new UserSessionManager(MessageDetailActivity.this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        Log.d("M_DETAILL", user_id + "//" + user_type + "//" + token + "//" + device_id);

        back_img =findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        member_profile_pic =findViewById(R.id.member_profile_pic);
        send_mesage =  findViewById(R.id.send_mesage);
        send_mesage.setOnClickListener(this);
        mesagetext_edt =  findViewById(R.id.mesagetext_edt);
        mesagetext_edt.setOnClickListener(this);
        recycler_list_chat = findViewById(R.id.recycler_list_chat);
        Picasso.with(MessageDetailActivity.this)
                .load(m_profile_pic)
                .placeholder(R.drawable.members_dummy)
                .into(member_profile_pic);

        getMessageDetail();
    }

    private void getMessageDetail()
    {
        if (checkInternet)
        {
            Log.d("MSGDETILURL", AppUrls.BASE_URL + AppUrls.NOTIFICATION_CONVERSATION + "?entity_id=" + m_fromid + "&entity_type=" + m_fromtype);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.NOTIFICATION_CONVERSATION + "?entity_id=" + m_fromid + "&entity_type=" + m_fromtype,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {

                                Log.d("DETAILMESSAGRESP", response);
                                msgDetailModalList=new ArrayList<>();
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("response_code");
                                if (responceCode.equals("10100"))
                                {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                     for (int i = 0; i < jsonArray.length(); i++)
                                     {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                         MessagesDetailModel msgDetail = new MessagesDetailModel(jsonObject1);
                                         msgDetailModalList.add(msgDetail);
                                     }

                                    messageDetailAdapter = new MessagesDetailAdapter(msgDetailModalList, MessageDetailActivity.this, R.layout.row_message_detail,FROM_TYPE);
                                    layoutManager = new LinearLayoutManager(getApplicationContext());
                                    recycler_list_chat.setNestedScrollingEnabled(false);
                                    recycler_list_chat.setLayoutManager(layoutManager);
                                    recycler_list_chat.setAdapter(messageDetailAdapter);
                                }

                                if (responceCode.equals("10200")) {

                                    Toast.makeText(MessageDetailActivity.this, "Invlid Input..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10300")) {
                                    Toast.makeText(MessageDetailActivity.this, "No Data Found..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {

                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                  error.getMessage();
                }
            }) {

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MessageDetailActivity.this);
            requestQueue.add(stringRequest);

        } else {

            Toast.makeText(MessageDetailActivity.this,"No Internet Connection...!",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view)
    {
        if (view == back_img) {
            finish();
        }

        if(view==send_mesage)
        {
            checkInternet = NetworkChecking.isConnected(this);
            if (validate()) {
                if (checkInternet) {
                    Log.d("SENDURL:", AppUrls.BASE_URL + AppUrls.GET_NOTIFICATION_SEND_MSG);
                    final String msg_text = mesagetext_edt.getText().toString().trim();
                    StringRequest forgetReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_NOTIFICATION_SEND_MSG, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("MESRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                    getMessageDetail();
                                    mesagetext_edt.setText("");
                                }
                                if (successResponceCode.equals("10200")) {

                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                          error.getMessage();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("from_entity_id", user_id);
                            params.put("from_entity_type", user_type);
                            params.put("to_entity_id", m_fromid);
                            params.put("to_entity_type", m_fromtype);
                            params.put("msg", msg_text);
                            Log.d("SEMDMSGARAM:", params.toString());
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("x-access-token", token);
                            headers.put("x-device-id", device_id);
                            headers.put("x-device-platform", "ANDROID");
                            Log.d("MSGHEAED", headers.toString());
                            return headers;
                        }
                    };

                    forgetReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(MessageDetailActivity.this);
                    requestQueue.add(forgetReq);
                } else {
                    Toast.makeText(MessageDetailActivity.this, "No Inmtenet Connection..!", Toast.LENGTH_LONG).show();
                }
            }
        }

    }
    private boolean validate() {


        boolean result = true;
        int flag = 0;
        String name = mesagetext_edt.getText().toString().trim();
        if ((name == null || name.equals("") || name.length() < 1)) {
            mesagetext_edt.setError("Minimum 3 characters required");
            result = false;
        }
        return result;
    }
}
