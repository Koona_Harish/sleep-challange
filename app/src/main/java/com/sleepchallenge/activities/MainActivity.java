package com.sleepchallenge.activities;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.WebBannerAdapter;
import com.sleepchallenge.models.AllPromotionsModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.BackgroundStatus;
import com.sleepchallenge.utils.BannerLayout;
import com.sleepchallenge.utils.Config;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.ProfileDB;
import com.sleepchallenge.utils.UserSessionManager;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.hoang8f.android.segmented.SegmentedGroup;
import me.tankery.lib.circularseekbar.CircularSeekBar;

public class MainActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener,
        View.OnClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener, RadioGroup.OnCheckedChangeListener, DatePickerDialog.OnDateSetListener {

    EditText selectdateEt, noofdays_ET, time_et;
    TextView selectedtime_txt, startdate_text, noof_days_txt, hours_txt, time_txt, sleepremainder_text, hours_progress, time_progress, start_time_txt, end_time_txt, detox_txt,
            red_text, green_txt, more, buffertimeEt;
    ImageView banner_image,notification;
    CircularSeekBar hourseekbar;//, timeseekbar;
    CircularImageView profile_pic_iv;
    SeekBar timeseekbar;
    Button sendrequest_btn;
    private Calendar calendar;
    LinearLayout progress_layout;
    TextView twle_text, three_text, nine_text, six_text, twenty_text, time_six_text, time_eighteen_text, time_twel_text;
    private Boolean exit = false;
    Dialog dialog, requestdialog;
    AppCompatSpinner remainder_spi;
    SegmentedGroup challengegroup;
    ProfileDB profileDB;
    String selectedStr;
    RelativeLayout timings_layout, indicator_layout;
    private com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd;
    String[] remainders = {"Select", "15 min", "30 min", "45 min", "60 min"};
    String[] days = {"3 days", "4 days", "5 days", "6 days", "7 days"};
    int challengecount, alreadyapplied;
    private boolean checkInternet;
    DrawerLayout drawer;
    String timestr = "", promotion_status;
    RelativeLayout rl_promotion_layout;
    String selectedDatestr = "", daysstr = "", hourstr = "", remainderstr = "", device_id, user_id, user_type, token, refUserIdJ;
    Typeface regularFont, boldFont, thinFont, specialFont;
    LinearLayout linear_home, linear_profile, linear_concentration, linear_recent_payment, linear_win_lost, linear_mychallenges,linear_total_statistics, linear_mysponsorings, linear_all_sponsor_list,
            linear_my_group, linear_allmembers, linear_following, linear_refer_friend, linear_report, linear_help, linear_deactivate, linear_promotions, linear_myscratchcard;
    TextView player_user_name, home_text, profile_text, concentration_text, recent_text, win_lost_text, my_chllenges_text,my_statistics_text, my_sponsoting_text, all_sponsor_text, my_group_text, all_members_text,
            my_following_text, refer_friend_text, report_text, help_text, deactivate_text, ranking_text, count_sponsor_text, count_challenge_text, count_mesages_text, promotions_text, myscratchcard_text;
    NavigationView vNavigation;
    LinearLayout linear_sponsor, linear_chllenge, linear_messges;
    View home_view, profile_view, cc_view, payment_view, winlost_view, challenges_view,statistics_view, mysponsorings_view, allsponsors_view, promotions_view, mygroup_view, allmembers_view,
            following_view, referfriend_view, report_view, help_view, deactive_view, myscratchcard_view;
    UserSessionManager userSessionManager;
    private AdView mAdView;
    AdRequest adRequest;
    BannerLayout bannerVertical;
    WebBannerAdapter webBannerAdapter;
    ArrayList<AllPromotionsModel> promoList;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));
        TextView title = findViewById(R.id.title_text);
        title.setTypeface(boldFont);
        vNavigation = findViewById(R.id.nav_view);
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        selectedStr = "DAILY";
        bannerVertical = findViewById(R.id.recycler_ver);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        notification = findViewById(R.id.notification);
        notification.setOnClickListener(this);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        profileDB = new ProfileDB(this);

        setupMenu();
        getUserDetails();
        getCount();
        getRefCode();
        getFCMToken();

        more = findViewById(R.id.more);
        more.setOnClickListener(this);
        banner_image = findViewById(R.id.banner_image);
        selectdateEt = findViewById(R.id.start_date_et);
        selectdateEt.setTypeface(boldFont);
        selectdateEt.setOnClickListener(this);
        startdate_text = findViewById(R.id.start_date_txt);
        startdate_text.setTypeface(boldFont);
        rl_promotion_layout = findViewById(R.id.rl_promotion_layout);
        count_sponsor_text = findViewById(R.id.count_sponsor_text);
        count_challenge_text = findViewById(R.id.count_challenge_text);
        count_mesages_text = findViewById(R.id.count_mesages_text);
        linear_sponsor = findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge = findViewById(R.id.linear_chllenge);
        linear_chllenge.setOnClickListener(this);
        linear_messges = findViewById(R.id.linear_messges);
        View view = findViewById(R.id.custom_tab);
        linear_messges.setOnClickListener(this);
        noof_days_txt = findViewById(R.id.noofdays_txt);
        noof_days_txt.setTypeface(boldFont);
        noofdays_ET = findViewById(R.id.noofdays_et);
        noofdays_ET.setOnClickListener(this);
        noof_days_txt.setEnabled(false);
        noofdays_ET.setEnabled(false);
        noof_days_txt.setTextColor(getResources().getColor(R.color.gray));
        noofdays_ET.setHintTextColor(getResources().getColor(R.color.gray));
        noofdays_ET.setTypeface(boldFont);
        hours_txt = findViewById(R.id.hours_txt);
        hours_txt.setTypeface(regularFont);
        time_txt = findViewById(R.id.time_txt);
        time_txt.setTypeface(regularFont);
        twle_text = findViewById(R.id.twle_text);
        twle_text.setTypeface(regularFont);
        three_text = findViewById(R.id.three_text);
        three_text.setTypeface(regularFont);
        nine_text = findViewById(R.id.nine_text);
        nine_text.setTypeface(regularFont);
        six_text = findViewById(R.id.six_text);
        six_text.setTypeface(regularFont);
        red_text = findViewById(R.id.red_text);
        red_text.setTypeface(regularFont);
        green_txt = findViewById(R.id.green_text);
        green_txt.setTypeface(regularFont);
        timings_layout = findViewById(R.id.timing_layout);
        indicator_layout = findViewById(R.id.indictor_layout);
        progress_layout = findViewById(R.id.progress_layout);
        start_time_txt = findViewById(R.id.start_time_txt);
        end_time_txt = findViewById(R.id.end_time_txt);
        detox_txt = findViewById(R.id.detox_time_txt);
        start_time_txt.setTypeface(regularFont);
        end_time_txt.setTypeface(regularFont);
        detox_txt.setTypeface(regularFont);
        buffertimeEt = findViewById(R.id.buffer_time_et);
        buffertimeEt.setTypeface(regularFont);
        challengegroup = findViewById(R.id.challenge_group);
        challengegroup.setOnCheckedChangeListener(this);
        sleepremainder_text = findViewById(R.id.remainer_txt);
        sleepremainder_text.setTypeface(regularFont);
        sendrequest_btn = findViewById(R.id.sendrequest_btn);
        sendrequest_btn.setTypeface(regularFont);
        sendrequest_btn.setOnClickListener(this);
        hours_progress = findViewById(R.id.hours_progress_txt);
        hours_progress.setTypeface(regularFont);
        hourseekbar = findViewById(R.id.hours_seekbar);
        hourseekbar.setMax(12);
        hourseekbar.setProgress(6);
        hours_progress.setText("6 Hrs");
        time_progress = findViewById(R.id.time_progress_txt);
        time_progress.setTypeface(regularFont);
        timeseekbar = findViewById(R.id.time_seekbar);
        timeseekbar.setMax(100);
        timeseekbar.setEnabled(false);
        time_et = findViewById(R.id.time_et);
        time_et.setTypeface(regularFont);
        time_et.setOnClickListener(this);
        remainder_spi = findViewById(R.id.remainder_spi);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.spinner_row, R.id.spinner_txt, remainders);
        remainder_spi.setAdapter(adapter);
        remainder_spi.setOnItemSelectedListener(this);
        //get Year and Month
        calendar = Calendar.getInstance();
        hoursSeekbar();
        SharedPreferences backgroundpreferences = getSharedPreferences("background", MODE_PRIVATE);
        boolean isbackground = backgroundpreferences.getBoolean("isbackground", false);
        if(isbackground)
            startService(new Intent(MainActivity.this, BackgroundStatus.class));
        ///BANNER ADS
        mAdView = findViewById(R.id.adView);
        MobileAds.initialize(MainActivity.this, getString(R.string.admob_app_id));
        adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("abc")
                .build();

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
                Toast.makeText(getApplicationContext(), "Ad is closed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });

        if (user_type.equals("SPONSOR")) {
            //only for Sponser login
            linear_mychallenges.setVisibility(View.GONE);
            linear_total_statistics.setVisibility(View.GONE);
            linear_win_lost.setVisibility(View.GONE);
            linear_myscratchcard.setVisibility(View.GONE);
            linear_promotions.setVisibility(View.VISIBLE);
            linear_concentration.setVisibility(View.GONE);
        }


    }

    public void setupMenu() {

        View header = vNavigation.getHeaderView(0);
        viewsInitilization(header);
        profile_pic_iv = header.findViewById(R.id.profile_pic_iv);
        player_user_name = header.findViewById(R.id.player_user_name);
        player_user_name.setTypeface(boldFont);
        ranking_text = header.findViewById(R.id.ranking_text);
        ranking_text.setTypeface(thinFont);
        home_text = header.findViewById(R.id.home_text);
        home_text.setTypeface(boldFont);
        profile_text = header.findViewById(R.id.profile_text);
        profile_text.setTypeface(boldFont);
        concentration_text = header.findViewById(R.id.concentration_text);
        concentration_text.setTypeface(boldFont);
        recent_text = header.findViewById(R.id.recent_text);
        recent_text.setTypeface(boldFont);
        win_lost_text = header.findViewById(R.id.win_lost_text);
        win_lost_text.setTypeface(boldFont);
        my_chllenges_text = header.findViewById(R.id.my_chllenges_text);
        my_chllenges_text.setTypeface(boldFont);
        my_statistics_text = header.findViewById(R.id.my_statistics_text);
        my_statistics_text.setTypeface(boldFont);
        myscratchcard_text = header.findViewById(R.id.myscratchcard_text);
        myscratchcard_text.setTypeface(boldFont);
        my_sponsoting_text = header.findViewById(R.id.my_sponsoting_text);
        my_sponsoting_text.setTypeface(boldFont);
        my_group_text = header.findViewById(R.id.my_group_text);
        my_group_text.setTypeface(boldFont);
        all_members_text = header.findViewById(R.id.all_members_text);
        all_members_text.setTypeface(boldFont);
        my_following_text = header.findViewById(R.id.my_following_text);
        my_following_text.setTypeface(boldFont);
        refer_friend_text = header.findViewById(R.id.refer_friend_text);
        refer_friend_text.setTypeface(boldFont);
        help_text = header.findViewById(R.id.help_text);
        help_text.setTypeface(boldFont);
        deactivate_text = header.findViewById(R.id.deactivate_text);
        deactivate_text.setTypeface(boldFont);
        report_text = header.findViewById(R.id.report_text);
        report_text.setTypeface(boldFont);
        all_sponsor_text = header.findViewById(R.id.all_sponsor_text);
        all_sponsor_text.setTypeface(boldFont);
        promotions_text = header.findViewById(R.id.promotions_text);
        promotions_text.setTypeface(boldFont);

        linear_home = header.findViewById(R.id.linear_home);
        linear_home.setOnClickListener(this);
        linear_profile = header.findViewById(R.id.linear_profile);
        linear_profile.setOnClickListener(this);
        linear_concentration = header.findViewById(R.id.linear_concentration);
        linear_concentration.setOnClickListener(this);
        linear_recent_payment = header.findViewById(R.id.linear_recent_payment);
        linear_recent_payment.setOnClickListener(this);
        linear_win_lost = header.findViewById(R.id.linear_win_lost);
        linear_win_lost.setOnClickListener(this);
        linear_mychallenges = header.findViewById(R.id.linear_mychallenges);
        linear_mychallenges.setOnClickListener(this);
        linear_total_statistics = header.findViewById(R.id.linear_total_statistics);
        linear_total_statistics.setOnClickListener(this);
        linear_promotions = header.findViewById(R.id.linear_promotions);
        linear_promotions.setOnClickListener(this);
        linear_mysponsorings = header.findViewById(R.id.linear_mysponsorings);
        linear_mysponsorings.setOnClickListener(this);
        linear_all_sponsor_list = header.findViewById(R.id.linear_all_sponsor_list);
        linear_all_sponsor_list.setOnClickListener(this);
        linear_my_group = header.findViewById(R.id.linear_my_group);
        linear_my_group.setOnClickListener(this);
        linear_allmembers = header.findViewById(R.id.linear_allmembers);
        linear_allmembers.setOnClickListener(this);
        linear_following = header.findViewById(R.id.linear_following);
        linear_following.setOnClickListener(this);
        linear_refer_friend = header.findViewById(R.id.linear_refer_friend);
        linear_refer_friend.setOnClickListener(this);
        linear_report = header.findViewById(R.id.linear_report);
        linear_report.setOnClickListener(this);
        linear_help = header.findViewById(R.id.linear_help);
        linear_help.setOnClickListener(this);
        linear_deactivate = header.findViewById(R.id.linear_deactivate);
        linear_deactivate.setOnClickListener(this);
        linear_myscratchcard = header.findViewById(R.id.linear_myscratchcard);
        linear_myscratchcard.setOnClickListener(this);
    }

    private void getUserDetails() {
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.USER_BASIC_DETAIL + user_id + "&user_type=" + user_type;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("RESPONSE:", response);
                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    JSONObject jobj = jsonArray.getJSONObject(0);
                                    String name = jobj.getString("first_name") + " " + jobj.getString("last_name");
                                    String profile_pic = AppUrls.BASE_IMAGE_URL + jobj.getString("profile_pic");
                                    String overall_rank = jobj.getString("overall_rank");
                                    String email = jobj.getString("email");
                                    Picasso.with(MainActivity.this)
                                            .load(profile_pic)
                                            .placeholder(R.drawable.profile_img)
                                            //  .resize(200, 200)
                                            .into(profile_pic_iv);
                                    player_user_name.setText(name);
                                    ranking_text.setText(overall_rank);
                                    Log.d("xfkldfbng", profile_pic);
                                }
                                if (status.equals("10200")) {
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        } else {
            getProfile();
            //  Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    public void getFCMToken()
    {
        mRegistrationBroadcastReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {

                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();
                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    String message = intent.getStringExtra("message");
                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };
        displayFirebaseRegId();
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        if (!TextUtils.isEmpty(regId)) {
         //  Toast.makeText(getApplicationContext(), "FirebaseRegId: " + regId, Toast.LENGTH_LONG).show();
          sendFCMTokes(user_id, user_type, regId);
        } else {
             // Toast.makeText(getApplicationContext(), "Firebase Reg Id is not received yet!", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));
        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        // clear the notification area when the app is opened
        //  NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
         super.onPause();
    }

    public void sendFCMTokes(final String user_id, final String user_type, final String token_fcm) {
        checkInternet = NetworkChecking.isConnected(MainActivity.this);
        if (checkInternet) {
            Log.d("FCMurl", AppUrls.BASE_URL + AppUrls.FCM);
            StringRequest stringRequest = new StringRequest(Request.Method.PUT, AppUrls.BASE_URL + AppUrls.FCM,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("FCMRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    //   Toast.makeText(MainActivity.this, "Token Sent", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10200")) {
                                    Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                           error.getMessage();
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("fcm_register_id", token_fcm);
                    params.put("device_platform", "ANDROID");
                    Log.d("FCMPARAM", "FCMPARAM" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        } else {
            // Toast.makeText(MainActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }




    @Override
    public void onBackPressed() {


        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // super.onBackPressed();
            if (exit) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                moveTaskToBack(true);
            } else {
                Toast.makeText(getApplicationContext(), "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 2 * 1000);
            }
        }
    }

    private void getProfile() {
        String firname = profileDB.getFirstName();
        if (firname.length() > 0) {
            String fname = profileDB.getFirstName();
            String lname = profileDB.getLirstName();
            String overallrank = profileDB.getOverallRank();
            player_user_name.setText(fname + " " + lname);
            ranking_text.setText(overallrank);
            String profilepic = profileDB.getProfilePic();
            Picasso.with(this)
                    .load(profilepic)
                    .placeholder(R.drawable.profile_img) // img_circle_placeholder
                    .into(profile_pic_iv);
        }
    }

    private void hoursSeekbar() {
        hourseekbar.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {
            @Override
            public void onProgressChanged(CircularSeekBar circularSeekBar, float progress, boolean fromUser) {
                float curvalue = progress;
                if (curvalue >= 6) {
                    if (curvalue > 1 && curvalue < 6) {
                        hours_progress.setText("6 Hrs");
                    } else if (curvalue >= 6 && curvalue < 9) {
                        hours_progress.setText("8 Hrs");

                    } else if (curvalue > 9) {
                        hours_progress.setText("10 Hrs");
                        hourseekbar.setProgress(10);
                    }
                    Log.v("Progress", "///" + curvalue);
                } else {
                    hourseekbar.setProgress(6);
                    hours_progress.setText("6 Hrs");
                }
                time_et.setText("");
                timestr = "";
                progress_layout.setVisibility(View.GONE);
                hourstr = hours_progress.getText().toString();

            }

            @Override
            public void onStopTrackingTouch(CircularSeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(CircularSeekBar seekBar) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View view)
    {
          if(view==notification)
          {
              Intent profile = new Intent(MainActivity.this, NotificationActivity.class);
              startActivity(profile);
          }

        if (view == linear_home)
        {
            drawer.closeDrawer(GravityCompat.START);
            showViewsVisibile(View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,
                    View.GONE);
            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            Intent profile = new Intent(MainActivity.this, MainActivity.class);
            startActivity(profile);
        }
        if (view == linear_profile) {
            drawer.closeDrawer(GravityCompat.START);
            showViewsVisibile(View.GONE, View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE,View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,
                    View.GONE);

            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            Intent profile = new Intent(MainActivity.this, MyProfileActivity.class);
            startActivity(profile);
        }
        if (view == linear_concentration) {
            drawer.closeDrawer(GravityCompat.START);
            showViewsVisibile(View.GONE, View.GONE, View.VISIBLE, View.GONE, View.GONE, View.GONE,View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,
                    View.GONE);
            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            // Toast.makeText(this,"Under development",Toast.LENGTH_SHORT).show();
            if (checkInternet) {
                Intent profile = new Intent(MainActivity.this, NewConcentrateActvity.class);
                startActivity(profile);
            } else {
                Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }

        }
        if (view == linear_recent_payment) {
            drawer.closeDrawer(GravityCompat.START);

            showViewsVisibile(View.GONE, View.GONE, View.GONE, View.VISIBLE, View.GONE, View.GONE,View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,
                    View.GONE);
            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            Intent profile = new Intent(MainActivity.this, PaymentHistoryActivtiy.class);
            startActivity(profile);
        }
        if (view == linear_win_lost) {
            drawer.closeDrawer(GravityCompat.START);

            showViewsVisibile(View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE, View.GONE,View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,
                    View.GONE);

            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

            Intent profile = new Intent(MainActivity.this, WinLossActivity.class);
            startActivity(profile);
        }

        if (view == linear_promotions) {
            drawer.closeDrawer(GravityCompat.START);
            showViewsVisibile(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,View.GONE, View.GONE, View.GONE, View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,
                    View.GONE);

            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

            Intent profile = new Intent(MainActivity.this, MyPromotionsActivity.class);
            startActivity(profile);
        }


        if (view == linear_mychallenges) {
            drawer.closeDrawer(GravityCompat.START);

            showViewsVisibile(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE, View.GONE,View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,
                    View.GONE);
            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            Intent profile = new Intent(MainActivity.this, MyChallengesActivity.class);
            startActivity(profile);
        }
        if (view == linear_total_statistics) {
            drawer.closeDrawer(GravityCompat.START);

            showViewsVisibile(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE,View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,
                    View.GONE);
            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            Toast.makeText(this,"Under Development",Toast.LENGTH_SHORT).show();
           /* Intent profile = new Intent(MainActivity.this, MyChallengesActivity.class);
            startActivity(profile);*/
        }
        if (view == linear_myscratchcard) {
            drawer.closeDrawer(GravityCompat.START);

            showViewsVisibile(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,
                    View.GONE);
            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);


            Intent profile = new Intent(MainActivity.this, WinScratchCardsActivity.class);
            startActivity(profile);
        }

        if (view == linear_mysponsorings) {
            drawer.closeDrawer(GravityCompat.START);
            showViewsVisibile(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,View.GONE, View.GONE, View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,
                    View.GONE);
            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            Intent profile = new Intent(MainActivity.this, MySponsorActivities.class);
            startActivity(profile);
        }


        if (view == linear_all_sponsor_list) {
            drawer.closeDrawer(GravityCompat.START);
            showViewsVisibile(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,View.GONE,View.GONE, View.GONE, View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,
                    View.GONE);

            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

            Intent profile = new Intent(MainActivity.this, AllSponsorsActivity.class);
            startActivity(profile);
        }

        if (view == linear_my_group) {
            drawer.closeDrawer(GravityCompat.START);

            showViewsVisibile(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,
                    View.GONE);

            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

            Intent profile = new Intent(MainActivity.this, GroupsActivity.class);
            startActivity(profile);
        }

        if (view == linear_allmembers) {
            drawer.closeDrawer(GravityCompat.START);

            showViewsVisibile(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE, View.GONE, View.GONE, View.GONE, View.GONE,
                    View.GONE);

            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            Intent allmember = new Intent(MainActivity.this, AllMembersActivity.class);
            startActivity(allmember);
        }

        if (view == linear_following) {
            drawer.closeDrawer(GravityCompat.START);
            showViewsVisibile(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE, View.GONE, View.GONE, View.GONE,
                    View.GONE);

            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            Intent profile = new Intent(MainActivity.this, MyFollowing.class);
            startActivity(profile);
        }

        if (view == linear_refer_friend) {
            drawer.closeDrawer(GravityCompat.START);
            showViewsVisibile(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,View.GONE,View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE, View.GONE, View.GONE,
                    View.GONE);

            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

            dialog = new Dialog(MainActivity.this);
            dialog.setContentView(R.layout.refer_friend_dialog);

            ImageView close = dialog.findViewById(R.id.close);
            ImageView friend_share = dialog.findViewById(R.id.friend_share);
            TextView share_text = dialog.findViewById(R.id.share_text);
            TextView refer_friend_text = dialog.findViewById(R.id.refer_friend_text);
            TextView headet_text_dialog = dialog.findViewById(R.id.headet_text_dialog);

            refer_friend_text.setText(refUserIdJ);
            refer_friend_text.setTypeface(regularFont);
            share_text.setTypeface(regularFont);
            headet_text_dialog.setTypeface(regularFont);

            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });
            friend_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Sleep Challenge");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, "I'm inviting you to join SleepChallenge App. Here's my Refer Code " + refUserIdJ + ". just enter it while Register" + "\n" + AppUrls.SHARE_APP_URL);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            });

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            //    dialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
            dialog.show();

        }

        if (view == linear_report) {
            drawer.closeDrawer(GravityCompat.START);

            showViewsVisibile(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE, View.GONE,
                    View.GONE);

            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

            Intent profile = new Intent(MainActivity.this, FeedbackContactUsActivity.class);
            startActivity(profile);
        }

        if (view == linear_help) {
            drawer.closeDrawer(GravityCompat.START);

            showViewsVisibile(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.VISIBLE,
                    View.GONE);

            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);

            Intent profile = new Intent(MainActivity.this, FaqTermsConditionActivity.class);
            Bundle b = new Bundle();
            b.putString("condition", "NORMAL");
            profile.putExtras(b);
            startActivity(profile);
            startActivity(profile);
        }


        if (view == linear_deactivate) {
            drawer.closeDrawer(GravityCompat.START);

            showViewsVisibile(View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,View.GONE,View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE, View.GONE,
                    View.VISIBLE);

            home_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            profile_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            concentration_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            recent_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            win_lost_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_chllenges_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_statistics_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            promotions_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_sponsoting_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_sponsor_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_group_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            myscratchcard_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            all_members_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            my_following_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            refer_friend_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            report_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            help_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            deactivate_text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            Intent profile = new Intent(MainActivity.this, DeactivateActivity.class);
            startActivity(profile);
        }

        if (view == linear_chllenge) {

            if (checkInternet) {
                Intent chal_req = new Intent(MainActivity.this, ChallengeRequestActivity.class);
                startActivity(chal_req);

            } else {
                // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }

        if (view == linear_messges) {

            if (checkInternet) {
                Intent msg = new Intent(MainActivity.this, ChatMessagesActivity.class);
                msg.putExtra("condition", "NORMAL");    //for switching tab order
                startActivity(msg);

            } else {
                // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
        if (view == linear_sponsor) {

            if (checkInternet) {
                Intent sponsor = new Intent(MainActivity.this, SponserRequestActivity.class);
                startActivity(sponsor);

            } else {
                // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }


        if (view == selectdateEt) {
            if (selectedStr.equalsIgnoreCase("Daily")) {
                Calendar now = Calendar.getInstance();
                android.app.DatePickerDialog dpDialog = new android.app.DatePickerDialog(
                        MainActivity.this,
                        new android.app.DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                String sufix = getDayNumberSuffix(dayOfMonth);
                                selectdateEt.setText(dayOfMonth + sufix);
                                String date = "";
                                String mm = "";
                                if (dayOfMonth < 10)
                                    date = "0" + dayOfMonth;
                                else
                                    date = String.valueOf(dayOfMonth);
                                int monthInt = month + 1;
                                if (monthInt < 10) {
                                    mm = "0" + monthInt;
                                } else {
                                    mm = String.valueOf(monthInt);
                                }

                                selectedDatestr = year + "-" + mm + "-" + date;
                                hourseekbar.setEnabled(true);

                            }
                        },
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                Calendar cal = Calendar.getInstance();
                now.add(Calendar.DATE, 1);
                cal.add(Calendar.MONTH, 2);
                dpDialog.getDatePicker().setMinDate(now.getTimeInMillis());
                long afterTwoMonthsinMilli = cal.getTimeInMillis();
                dpDialog.getDatePicker().setMaxDate(afterTwoMonthsinMilli);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    dpDialog.getDatePicker().setFirstDayOfWeek(2);
                }
                dpDialog.show();

            } else {
                Calendar now = Calendar.getInstance();
                if (dpd == null) {
                    dpd = DatePickerDialog.newInstance(
                            MainActivity.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)
                    );
                } else {
                    dpd.initialize(
                            MainActivity.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)
                    );
                }
                dpd.setAccentColor(Color.parseColor("#009688"));
                Calendar originalDate = Calendar.getInstance();
                originalDate.add(Calendar.DAY_OF_MONTH, 1);
                long currentdate = originalDate.getTimeInMillis();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String dateString1 = sdf.format(currentdate);
                Date today = ConvertToDate(dateString1);
                String startdate = dateToString(today);
                if (selectedStr.equalsIgnoreCase("Weekly")) {
                    Calendar nextMonthDate = (Calendar) originalDate.clone();
                    // Add the Date as next month ahead
                    nextMonthDate.add(Calendar.MONTH, 2);
                    long nextmonthdate = nextMonthDate.getTimeInMillis();

                    String dateString2 = sdf.format(nextmonthdate);

                    Date nextdate = ConvertToDate(dateString2);


                    String enddate = dateToString(nextdate);
                    Log.v("Dates", ">>" + startdate + "///" + enddate);
                    dpd.setMinDate(toCalendar(today));
                    ArrayList<Date> days = new ArrayList<>();
                    ArrayList<Calendar> daycal = new ArrayList<>();
                    List<Date> dates = getDates(startdate, enddate);
                    for (Date date : dates) {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(date);
                        boolean monday = cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY;
                        if (monday) {
                            days.add(date);
                        }
                    }
                    for (int i = 0; i < days.size(); i++) {
                        Calendar calendar = toCalendar(days.get(i));
                        daycal.add(calendar);
                    }

                    Calendar[] mStringArray = new Calendar[daycal.size()];
                    mStringArray = daycal.toArray(mStringArray);
                    dpd.setSelectableDays(mStringArray);
                }
                dpd.setFirstDayOfWeek(2);
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        }

        if (view == sendrequest_btn) {
            String datestr = selectdateEt.getText().toString();
            // timestr = time_progress.getText().toString();
            hourstr = hours_progress.getText().toString();

            if (datestr.length() == 0) {
                Toast.makeText(this, "select startdate", Toast.LENGTH_SHORT).show();
            } else if (daysstr.length() == 0 && selectedStr.equalsIgnoreCase("Weekly")) {
                Toast.makeText(this, "select days", Toast.LENGTH_SHORT).show();
            } else if (hourstr.length() == 0 || hourstr.equalsIgnoreCase("0")) {
                Toast.makeText(this, "select hours", Toast.LENGTH_SHORT).show();
            } else if (timestr.length() == 0 || timestr.equalsIgnoreCase("0")) {
                Toast.makeText(this, "select sleep time", Toast.LENGTH_SHORT).show();
            } else if (remainderstr.length() == 0 || remainderstr.equalsIgnoreCase("Select")) {
                Toast.makeText(this, "select remainder", Toast.LENGTH_SHORT).show();
            } else {
                if (user_type.equalsIgnoreCase("SPONSOR")) {
                    Toast.makeText(MainActivity.this, " Sponsor does not perform this operation", Toast.LENGTH_SHORT).show();
                } else {
                    /*if(alreadyapplied==0)
                        sendRequestDialog();
                    else
                        Toast.makeText(MainActivity.this,"You have already one challenge",Toast.LENGTH_SHORT).show();*/
                    getChallengesCount();
                }

            }
        }
        if (view == noofdays_ET) {
            showNoofDaysDialog();
        }
        if (view.getId() == R.id.friend_txt) {
            requestdialog.cancel();
            Intent intent = new Intent(MainActivity.this, ChallengeToFriendActivity.class);
            intent.putExtra("challengedate", selectedDatestr);
            intent.putExtra("hours", hourstr);
            intent.putExtra("time", timestr);
            intent.putExtra("challengetype", selectedStr);
            intent.putExtra("remainder", remainderstr);
            if (selectedStr.equalsIgnoreCase("Weekly"))
                intent.putExtra("noofdays", daysstr);
            startActivity(intent);
        }
        if (view.getId() == R.id.group_txt) {
            requestdialog.cancel();
            Intent intent = new Intent(MainActivity.this, ChallengeToGroupActivity.class);
            intent.putExtra("challengedate", selectedDatestr);
            intent.putExtra("hours", hourstr);
            intent.putExtra("time", timestr);
            intent.putExtra("FROMWHERE", "DIALOG");
            intent.putExtra("challengetype", selectedStr);
            intent.putExtra("remainder", remainderstr);
            if (selectedStr.equalsIgnoreCase("Weekly"))
                intent.putExtra("noofdays", daysstr);
            startActivity(intent);
        }
        if (view.getId() == R.id.admin_txt) {
            requestdialog.cancel();
            Intent intent = new Intent(MainActivity.this, ChallengeAmountActivity.class);
            intent.putExtra("challengedate", selectedDatestr);
            intent.putExtra("hours", hourstr);
            intent.putExtra("time", timestr);
            intent.putExtra("challengetype", selectedStr);
            intent.putExtra("challengername", "ADMIN");
            intent.putExtra("challengerid", "59def9dce25e4081ac0f1093");
            intent.putExtra("challengertype", "ADMIN");
            intent.putExtra("activty", "challengeto");
            intent.putExtra("remainder", remainderstr);
            if (selectedStr.equalsIgnoreCase("Weekly")) {
                intent.putExtra("noofdays", daysstr);
            }
            startActivity(intent);
        }
        if (view == time_et) {
            showTimePickerDialog(view);
        }
    }

    private void sendRequestDialog() {
        requestdialog = new Dialog(this);
        requestdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        requestdialog.setContentView(R.layout.challengeto_dialog);
        ImageView close = requestdialog.findViewById(R.id.dialog_close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestdialog.dismiss();
            }
        });
        TextView admin_txt = requestdialog.findViewById(R.id.admin_txt);
        admin_txt.setOnClickListener(this);
        admin_txt.setTypeface(regularFont);
        TextView friend_txt = requestdialog.findViewById(R.id.friend_txt);
        friend_txt.setOnClickListener(this);
        friend_txt.setTypeface(regularFont);
        TextView group_txt = requestdialog.findViewById(R.id.group_txt);
        group_txt.setOnClickListener(this);
        group_txt.setTypeface(regularFont);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        Display display = this.getWindowManager().getDefaultDisplay();
        Point screenSize = new Point();
        display.getSize(screenSize);
        int width = screenSize.x;
        //alertDialog.getWindow().setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
        requestdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        requestdialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

        //TextView  challe = dialog.findViewById(R.id.daysLV);
        requestdialog.show();

    }


    private void showNoofDaysDialog() {
        dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.noof_days_row);
        ListView lv = dialog.findViewById(R.id.daysLV);
        lv.setOnItemClickListener(this);
        ArrayAdapter adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, days);
        lv.setAdapter(adapter);
        dialog.show();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        daysstr = days[position];
        String[] split = daysstr.split(" ");
        noofdays_ET.setText(split[0]);
        hourseekbar.setEnabled(true);

        dialog.cancel();
    }

    private String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String[] str = remainders[position].split(" ");
        remainderstr = str[0];


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.daily_radio) {
            selectedStr = "DAILY";
            noof_days_txt.setEnabled(false);
            noofdays_ET.setEnabled(false);
            noof_days_txt.setTextColor(getResources().getColor(R.color.gray));
            noofdays_ET.setHintTextColor(getResources().getColor(R.color.gray));
            noofdays_ET.setTextColor(getResources().getColor(R.color.gray));
            noofdays_ET.setText("");
            selectdateEt.setText("");
            hourseekbar.setProgress(6);
            timeseekbar.setProgress(0);
            hours_progress.setText("6 Hrs");
            timeseekbar.setProgress(0);
            timeseekbar.setSecondaryProgress(0);
            progress_layout.setVisibility(View.GONE);
            selectedDatestr = "";
            timestr = "";
            time_et.setText("");
            hourseekbar.setEnabled(true);


        } else {
            selectedStr = "WEEKLY";
            noof_days_txt.setEnabled(true);
            noofdays_ET.setEnabled(true);
            noof_days_txt.setTextColor(getResources().getColor(R.color.title_txt));
            noofdays_ET.setHintTextColor(getResources().getColor(R.color.enter_txt));
            noofdays_ET.setTextColor(getResources().getColor(R.color.enter_txt));
            noofdays_ET.setText("");
            selectdateEt.setText("");
            hourseekbar.setProgress(6);
            timeseekbar.setProgress(0);
            hours_progress.setText("6 Hrs");
            selectedDatestr = "";
            hourseekbar.setEnabled(true);
            timeseekbar.setProgress(0);
            timeseekbar.setSecondaryProgress(0);
            timestr = "";
            time_et.setText("");
            progress_layout.setVisibility(View.GONE);
        }
    }

    public static Calendar toCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    private Date ConvertToDate(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return convertedDate;
    }

    private String dateToString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");

        //to convert Date to String, use format method of SimpleDateFormat class.
        String strDate = dateFormat.format(date);
        return strDate;
    }


    private static List<Date> getDates(String dateString1, String dateString2) {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String sufix = getDayNumberSuffix(dayOfMonth);
        selectdateEt.setText(dayOfMonth + sufix);
        String date = "";
        String mm = "";
        if (dayOfMonth < 10)
            date = "0" + dayOfMonth;
        else
            date = String.valueOf(dayOfMonth);
        int monthInt = monthOfYear + 1;
        if (monthInt < 10) {
            mm = "0" + monthInt;
        } else {
            mm = String.valueOf(monthInt);
        }

        selectedDatestr = year + "-" + mm + "-" + date;

        hourseekbar.setEnabled(true);
    }

    private void getCount() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("CountUrl", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("COUNTRESP", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    int call_weekly_loop_ws = jsonArray.getInt("call_weekly_loop_ws");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    promotion_status = jsonArray.getString("has_promotion");
                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);
                                    if (promotion_status.equals("1")) {
                                        rl_promotion_layout.setVisibility(View.VISIBLE);
                                        getPromotions();
                                        //call promotion banner url
                                    } else {
                                        mAdView.loadAd(adRequest);    //Load Ad mob
                                    }


                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            error.getMessage();

                        }
                    }) {
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        } else {
            // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void getChallengesCount() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            String datetime = selectedDatestr + " " + timestr + ":00";
            String url = AppUrls.BASE_URL + AppUrls.CHALLANGE_COUNT + user_id + AppUrls.CHALLENGE_TYPE + selectedStr + AppUrls.CHALLENGE_DATE + datetime;
            Log.d("COUNTURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("Count:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {

                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    challengecount = jsonArray.getInt("count");
                                    alreadyapplied = jsonArray.getInt("activity_applied");
                                    if (alreadyapplied == 0)
                                        sendRequestDialog();
                                    else
                                        Toast.makeText(MainActivity.this, "You have already one challenge", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10200")) {
                                    com.sleepchallenge.utils.Dialog.hideProgressBar();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            error.getMessage();
                        }
                    }) {
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(MainActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    private void getRefCode() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Log.d("GetReferalURL", AppUrls.BASE_URL + AppUrls.REFERAL_code + user_id);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.REFERAL_code + user_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("GetReferalRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {

                                    JSONObject jObj = jsonObject.getJSONObject("data");
                                    //  refUserIdJ = jObj.getString("user_id");
                                    refUserIdJ = jObj.getString("refer_code");
                                    //refer_friend_text.setText(refCodeJ);

                                }
                                if (successResponceCode.equals("10200")) {

                                    Toast.makeText(MainActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            error.getMessage();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GetReferal_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {

            Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    //  ,winlost_view,challenges_view,mysponsorings_view,allsponsors_view,promotions_view,mygroup_view,allmembers_view,
    //  following_view,referfriend_view,report_view,help_view,deactive_view;
    private void viewsInitilization(View header) {
        home_view = header.findViewById(R.id.home_view);
        profile_view = header.findViewById(R.id.profile_view);
        cc_view = header.findViewById(R.id.cc_view);
        payment_view = header.findViewById(R.id.payment_view);
        winlost_view = header.findViewById(R.id.winlost_view);
        challenges_view = header.findViewById(R.id.challenges_view);
        statistics_view = header.findViewById(R.id.statistics_view);
        mysponsorings_view = header.findViewById(R.id.mysponsorings_view);
        allsponsors_view = header.findViewById(R.id.allsponsors_view);
        promotions_view = header.findViewById(R.id.promotions_view);
        mygroup_view = header.findViewById(R.id.mygroup_view);
        allmembers_view = header.findViewById(R.id.allmembers_view);
        following_view = header.findViewById(R.id.following_view);
        referfriend_view = header.findViewById(R.id.referfriend_view);
        report_view = header.findViewById(R.id.report_view);
        help_view = header.findViewById(R.id.help_view);
        deactive_view = header.findViewById(R.id.deactive_view);
        myscratchcard_view = header.findViewById(R.id.myscratchcard_view);

    }

    private void showViewsVisibile(int home, int profile, int cc, int payment, int winlost, int challenges,int analystics, int stachcard, int mysponsorings, int allsponsors, int promotions, int mygroup, int allmembers,
                                   int following, int referfriend, int report, int help, int deactive) {
        home_view.setVisibility(home);
        profile_view.setVisibility(profile);
        cc_view.setVisibility(cc);
        payment_view.setVisibility(payment);
        winlost_view.setVisibility(winlost);
        challenges_view.setVisibility(challenges);
        statistics_view.setVisibility(analystics);
        myscratchcard_view.setVisibility(stachcard);
        mysponsorings_view.setVisibility(mysponsorings);
        allsponsors_view.setVisibility(allsponsors);
        promotions_view.setVisibility(promotions);
        mygroup_view.setVisibility(mygroup);
        allmembers_view.setVisibility(allmembers);
        following_view.setVisibility(following);
        referfriend_view.setVisibility(referfriend);
        report_view.setVisibility(report);
        help_view.setVisibility(help);
        deactive_view.setVisibility(deactive);
    }

    public void showTimePickerDialog(View v) {
        progress_layout.setVisibility(View.GONE);
        timeseekbar.setProgress(0);
        timeseekbar.setSecondaryProgress(0);
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        TimePickerDialog dpDialog = new TimePickerDialog(this, this, hour, minute,
                android.text.format.DateFormat.is24HourFormat(this));
        dpDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                progress_layout.setVisibility(View.VISIBLE);
              /*  timings_layout.setVisibility(View.VISIBLE);
                indicator_layout.setVisibility(View.VISIBLE);*/
                timeseekbar.setProgress(100);
                timeseekbar.setSecondaryProgress(20);
            }
        });
        dpDialog.show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        progress_layout.setVisibility(View.VISIBLE);
        String format, endformat = "";
        String hour = "";
        String min = "";
        int hourformat = hourOfDay;
        int miformat = minute;
        if (hourformat < 10)
            hour = "0" + hourOfDay;
        else
            hour = String.valueOf(hourOfDay);
        if (miformat < 10) {
            min = "0" + minute;
        } else {
            min = String.valueOf(minute);
        }
        timestr = hour + ":" + min;
        Log.v("Time", "%%%" + timestr);
        if (hourOfDay == 0) {

            hourOfDay += 12;

            format = "AM";
        } else if (hourOfDay == 12) {

            format = "PM";

        } else if (hourOfDay > 12) {

            hourOfDay -= 12;

            format = "PM";

        } else {

            format = "AM";
        }

        if (hourOfDay < 10)
            hour = "0" + hourOfDay;
        else
            hour = String.valueOf(hourOfDay);
        if (minute < 10) {
            min = "0" + minute;
        } else {
            min = String.valueOf(minute);
        }
        String houstr = hours_progress.getText().toString();
        String split[] = houstr.split(" ");
        String hourspl = split[0];
        int h = Integer.parseInt(hourspl);
        int sleep = hourOfDay;
        time_et.setText(hour + ":" + min + format);
        timeseekbar.setSecondaryProgress(20);
        timeseekbar.setProgress(100);


        start_time_txt.setText(hour + ":" + min + format);
        showsdetoxformat(hourformat, minute);
        showTimeformat(hourOfDay, h, minute, format);

    }

    private void showsdetoxformat(int hour, int min) {
        String format = "";
        int start = hour - 2;
        if (start == 0) {

            start += 12;

            format = "AM";
        } else if (start == 12) {

            format = "PM";

        } else if (start > 12) {

            start -= 12;

            format = "PM";

        } else if (start < 0) {

            start += 12;

            format = "PM";

        } else {

            format = "AM";
        }
        detox_txt.setText("" + start + ":" + min + format);


    }

    private void showTimeformat(int hour, int h, int min, String format) {
        String format1 = "";
        int time = hour + h;
        if (format.equals("PM")) {
            if (time > 12) {
                time -= 12;

                format1 = "AM";
            } else if (time == 12) {
                format1 = "AM";
            } else {
                format1 = "PM";
            }

        } else {
            if (time < 12) {


                format1 = "AM";
            } else {
                time -= 12;
                format1 = "PM";
            }
        }
        if (hour == 12) {
            if (format.equals("PM"))
                format1 = "PM";
            else
                format1 = "AM";
        }
       end_time_txt.setText("" + time + ":" + min + format1);
    }

    private void getPromotions() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.PROMOTION;
            Log.d("PromotionList", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("PromotionListRESPONSE", response);
                                int count = jsonObject.getInt("count");
                                if (count > 1) {
                                    more.setVisibility(View.VISIBLE);
                                } else {
                                    more.setVisibility(View.GONE);
                                }
                                String status = jsonObject.getString("response_code");

                                if (status.equals("10100")) {

                                    promoList = new ArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jdataobj = jsonArray.getJSONObject(i);
                                        AllPromotionsModel allpromo = new AllPromotionsModel(jdataobj);
                                        promoList.add(allpromo);
                                        Log.d("LLLL", promoList.toString());

                                    }

                                    webBannerAdapter = new WebBannerAdapter(MainActivity.this, promoList);
                                    bannerVertical.setAdapter(webBannerAdapter);
                                    webBannerAdapter.setOnBannerItemClickListener(new BannerLayout.OnBannerItemClickListener() {
                                        @Override
                                        public void onItemClick(int position) {
                                            if (promoList.get(position).entity_id.equals(user_id)) {

                                            } else {
                                                if (promoList.get(position).entity_type.equals("USER") || promoList.get(position).entity_type.equals("SPONSOR")) {
                                                    Intent intent = new Intent(MainActivity.this, MemberDetailActivity.class);
                                                    intent.putExtra("MEMBER_ID", promoList.get(position).entity_id);
                                                    intent.putExtra("MEMBER_NAME", promoList.get(position).entity_name);
                                                    intent.putExtra("member_user_type", promoList.get(position).entity_type);
                                                    startActivity(intent);
                                                }
                   /* else if (promoList.get(position).entity_type.equals("GROUP")) {
                        // Toast.makeText(context, "Group", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this, GroupDetailActivity.class);
                        intent.putExtra("grp_id", promoList.get(position).entity_id);
                        intent.putExtra("grp_name", promoList.get(position).entity_name);
                        intent.putExtra("GROUP_CONVERSATION_TYPE", promoList.get(position).entity_type);
                        intent.putExtra("grp_admin_id", promoList.get(position).admin_id);

                        startActivity(intent);
                    } */
                                                else {

                                                }
                                            }
                                            //  Toast.makeText(AllPromotionsActivity.this, " " + position, Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                    JSONArray jsonArray1 = jsonObject.getJSONArray("data");
                                    JSONObject jobj = jsonArray1.getJSONObject(0);
                                    String image = AppUrls.BASE_IMAGE_URL + jobj.getString("banner_path");
                                    final String banner_status = jobj.getString("status");
                                    final String banner_user_id = jobj.getString("entity_id");
                                    final String banner_user_type = jobj.getString("entity_type");
                                    final String banner_user_name = jobj.getString("entity_name");
                                    final String admin_id = jobj.getString("admin_id");
                                    Log.d("banner_user_type", banner_user_type + "//" + banner_status);
                                    if (banner_status.equals("PAID")) {

                                        if (banner_user_id.equals(user_id)) {
                                            Log.d("UUUUUUUU", "UUUUUUUUUU");
                                        } else {
                                            banner_image.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    Log.d("11111111", banner_user_type);
                                                    if (banner_user_type.equals("USER")) {
                                                        Intent intent = new Intent(MainActivity.this, MemberDetailActivity.class);
                                                        intent.putExtra("MEMBER_ID", banner_user_id);
                                                        intent.putExtra("MEMBER_NAME", banner_user_name);
                                                        intent.putExtra("member_user_type", banner_user_type);
                                                        startActivity(intent);
                                                    } else if (banner_user_type.equals("SPONSOR")) {

                                                        Intent intent = new Intent(MainActivity.this, MemberDetailActivity.class);
                                                        //   Log.d("IDDD:",allsponsorMoldelList.get(position).getId());
                                                        intent.putExtra("MEMBER_ID", banner_user_id);
                                                        intent.putExtra("MEMBER_NAME", banner_user_name);
                                                        intent.putExtra("member_user_type", banner_user_type);
                                                        startActivity(intent);
                                                    } else {

                                                    }
                                                }
                                            });
                                        }


                                    } else {
                                        // more.setVisibility(View.VISIBLE);
                                        //nothing
                                    }


                                    Picasso.with(MainActivity.this)
                                            .load(image)
                                            .placeholder(R.drawable.img_circle_placeholder)
                                            .into(banner_image);
                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);

        } else {
            // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

}


