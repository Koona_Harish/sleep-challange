package com.sleepchallenge.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.MyFollowingAdapter;
import com.sleepchallenge.models.AllMembersModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.DividerItemDecorator;
import com.sleepchallenge.utils.MovableFloatingActionButton;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyFollowing extends AppCompatActivity implements View.OnClickListener {

    ImageView close,default_img;
    TextView title_text, my_followers_txt;
    Typeface regularFont, boldFont;
    UserSessionManager userSessionManager;
    private boolean checkInternet;

    String token, user_id, user_type, device_id;
    RecyclerView myfollowing_recyclerview;
    MyFollowingAdapter myFollowingAdapter;
    ArrayList<AllMembersModel> myFollowingMoldelList = new ArrayList<AllMembersModel>();
    RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_following);

        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        title_text = findViewById(R.id.title_text);
        title_text.setTypeface(boldFont);
        close = findViewById(R.id.back_img);
        close.setOnClickListener(this);
        default_img = findViewById(R.id.default_img);
       my_followers_txt =  findViewById(R.id.my_followers_txt);
        my_followers_txt.setTypeface(regularFont);
        MovableFloatingActionButton fab = findViewById(R.id.fab);
        CoordinatorLayout.LayoutParams lp  = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
        fab.setCoordinatorLayout(lp);
        fab.setOnClickListener(this);
        myfollowing_recyclerview = findViewById(R.id.myfollowing_recyclerview);
        myFollowingAdapter = new MyFollowingAdapter(myFollowingMoldelList, MyFollowing.this, R.layout.row_my_following);
        layoutManager = new LinearLayoutManager(this);
        myfollowing_recyclerview.setNestedScrollingEnabled(false);
        myfollowing_recyclerview.setLayoutManager(layoutManager);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(MyFollowing.this, R.drawable.recycler_view_divider));
        myfollowing_recyclerview.addItemDecoration(dividerItemDecoration);

        getMyFollowing();
    }

    private void getMyFollowing() {
        checkInternet = NetworkChecking.isConnected(MyFollowing.this);
        if (checkInternet) {
            String following_url = AppUrls.BASE_URL + AppUrls.MY_FOLLOWING + "?user_id=" + user_id;
                Log.d("FOLLOWURL", following_url);
            StringRequest strFollowing = new StringRequest(Request.Method.GET, following_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("MEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");
                        if (response_code.equals("10100")) {
                            JSONArray jsonArray = jobcode.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                AllMembersModel allmember = new AllMembersModel();
                                allmember.setId(jdataobj.getString("id"));
                                allmember.setUser_type(jdataobj.getString("user_type"));
                                allmember.setName(jdataobj.getString("name"));
                                allmember.setProfile_pic(AppUrls.BASE_IMAGE_URL + jdataobj.getString("profile_pic"));
                                allmember.setEmail(jdataobj.getString("email"));
                                allmember.setCountry_code(jdataobj.getString("country_code"));
                                allmember.setMobile(jdataobj.getString("mobile"));
                                allmember.setTotal_win(jdataobj.getString("total_win"));
                                allmember.setTotal_loss(jdataobj.getString("total_loss"));
                                allmember.setOverall_rank(jdataobj.getString("overall_rank"));
                                allmember.setWinning_per(jdataobj.getString("winning_per"));
                                String type=jdataobj.getString("user_type");
                                if(type.equalsIgnoreCase("Group")){
                                    allmember.setAdmin_id(jdataobj.getString("admin_id"));
                                }
                                myFollowingMoldelList.add(allmember);
                            }
                            myfollowing_recyclerview.setAdapter(myFollowingAdapter);
                        }
                        if (response_code.equals("10200")) {

                            myfollowing_recyclerview.setVisibility(View.GONE);
                            default_img.setVisibility(View.VISIBLE);
                            Toast.makeText(MyFollowing.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10300")) {

                            myfollowing_recyclerview.setVisibility(View.GONE);
                            default_img.setVisibility(View.VISIBLE);
                            Toast.makeText(MyFollowing.this, "Users does not exist..!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10400")) {

                            myfollowing_recyclerview.setVisibility(View.GONE);
                            default_img.setVisibility(View.VISIBLE);
                            Toast.makeText(MyFollowing.this, "No followings found.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.getMessage();
                }
            }) {
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MyFollowing.this);
            requestQueue.add(strFollowing);
        } else {
            Toast.makeText(MyFollowing.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view.getId() == R.id.fab) {

            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
                Intent followers = new Intent(MyFollowing.this, MyFollowersActivity.class);
                startActivity(followers);

            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
    }
}
