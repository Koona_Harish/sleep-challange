package com.sleepchallenge.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.filter.CustomFilterForAllSponsorsList;
import com.sleepchallenge.holders.AllMembersHolder;
import com.sleepchallenge.itemclicklisteners.AllMembersItemClickListener;
import com.sleepchallenge.models.AllMemberModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.DividerItemDecorator;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AllSponsorsActivity extends AppCompatActivity  implements View.OnClickListener
{

    ImageView back_img;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String token, user_id, user_type, device_id;
    SearchView all_sponsor_search;
    RecyclerView all_sponsor_recyclerView;
    AllSponsorsAdapter all_sponsorsAdapter;
    ArrayList<AllMemberModel> allsponsorMoldelList ;
    RecyclerView.LayoutManager layoutManager;
    TextView title_sponsor;
    Typeface regularFont;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_sponsors);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        checkInternet = NetworkChecking.isConnected(this);
        back_img = findViewById(R.id.back_img);
        back_img.setOnClickListener(this);

        title_sponsor= findViewById(R.id.title_sponsor);
        title_sponsor.setTypeface(regularFont);

        all_sponsor_search= findViewById(R.id.all_sponsor_search);
        all_sponsor_recyclerView =  findViewById(R.id.all_sponsor_recyclerView);

        all_sponsor_search.setOnClickListener(this);
        //View view = findViewById(R.id.custom_tab);
        all_sponsor_search.setIconified(false);
        all_sponsor_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                all_sponsor_search.setIconified(false);
            }
        });
        all_sponsor_search.clearFocus();
        all_sponsor_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if(all_sponsorsAdapter!=null)
                    all_sponsorsAdapter.getFilter().filter(query);
                return false;
            }
        });

        getAllSponsors();

    }



    private void getAllSponsors()
    {
        if (checkInternet)
        {
            Dialog.showProgressBar(AllSponsorsActivity.this, "Loading");

            String member_url = AppUrls.BASE_URL + AppUrls.ALL_SPONSOR_LIST + "?user_id=" + user_id + "&user_type=" + user_type;
            Log.d("ALLSPONSORURL", member_url);

            StringRequest strAllMember = new StringRequest(Request.Method.GET, member_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Dialog.hideProgressBar();
                    Log.d("ALLSPONSORRESP", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100"))
                        {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            allsponsorMoldelList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                AllMemberModel allsponsor = new AllMemberModel(jdataobj);
                                allsponsorMoldelList.add(allsponsor);
                            }
                            all_sponsorsAdapter = new AllSponsorsAdapter(allsponsorMoldelList, AllSponsorsActivity.this, R.layout.row_allmember);
                            layoutManager = new LinearLayoutManager(getApplicationContext());
                            all_sponsor_recyclerView.setNestedScrollingEnabled(false);
                            all_sponsor_recyclerView.setLayoutManager(layoutManager);
                            RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(AllSponsorsActivity.this, R.drawable.recycler_view_divider));
                            all_sponsor_recyclerView.addItemDecoration(dividerItemDecoration);

                            all_sponsor_recyclerView.setAdapter(all_sponsorsAdapter);
                        }
                        if (successResponceCode.equals("10200"))
                        {
                            Toast.makeText(AllSponsorsActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                        }
                        if (successResponceCode.equals("10300"))
                        {
                            Toast.makeText(AllSponsorsActivity.this, "No Data Found..!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Dialog.hideProgressBar();
                    Log.e("Error", error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strAllMember.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strAllMember);
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view)
    {
        if(view==back_img)
        {finish();}

    }

    public class AllSponsorsAdapter extends RecyclerView.Adapter<AllMembersHolder> implements Filterable
    {

        public ArrayList<AllMemberModel> allsponsorMoldelList, allmembersfilterList;
        AllSponsorsActivity context;
        LayoutInflater li;
        int resource;
        Typeface regularFont, boldFont,thinFont;

        CustomFilterForAllSponsorsList filter;


        public AllSponsorsAdapter(ArrayList<AllMemberModel> allmemberlistModels, AllSponsorsActivity context, int resource) {
            this.allsponsorMoldelList = allmemberlistModels;
            this.context = context;
            this.resource = resource;
            this.allmembersfilterList = allmemberlistModels;
            li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regularFont));
            boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.boldFont));
            thinFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.thinFont));

        }

        @Override
        public AllMembersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = li.inflate(resource, parent, false);
            AllMembersHolder slh = new AllMembersHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(final AllMembersHolder holder, final int position) {

            String str = allsponsorMoldelList.get(position).name;
            String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
            holder.all_member_user_name.setText(Html.fromHtml(converted_string));
            holder.all_member_user_name.setTypeface(boldFont);

            holder.all_member_ranking_text.setText(Html.fromHtml(allsponsorMoldelList.get(position).overall_rank));
            holder.all_member_totlapercentage.setText(Html.fromHtml(allsponsorMoldelList.get(position).winning_per + " %"));
            holder.all_member_win_per.setText(Html.fromHtml(allsponsorMoldelList.get(position).total_win));
            holder.all_member_los_per.setText(Html.fromHtml(allsponsorMoldelList.get(position).total_loss));


            String img_profile = allsponsorMoldelList.get(position).profile_pic;

            Log.d("PPPROFILE", img_profile);
            if (img_profile.equals("null") && img_profile.equals("")) {
                Picasso.with(context)
                        .load(R.drawable.members_dummy)
                        .placeholder(R.drawable.members_dummy)
                        .resize(60, 60)
                        .into(holder.all_member_image);
            } else {
                Picasso.with(context)
                        .load(allsponsorMoldelList.get(position).profile_pic)
                        .placeholder(R.drawable.members_dummy)
                        .into(holder.all_member_image);
            }




            holder.setItemClickListener(new AllMembersItemClickListener() {
                @Override
                public void onItemClick(View v, int pos) {

                    Intent intent=new Intent(context, MemberDetailActivity.class);
                    Log.d("IDDD:",allsponsorMoldelList.get(position).id);
                    intent.putExtra("MEMBER_ID",allsponsorMoldelList.get(position).id);
                    intent.putExtra("MEMBER_NAME",allsponsorMoldelList.get(position).name);
                    intent.putExtra("member_user_type",allsponsorMoldelList.get(position).user_type);
                    context.startActivity(intent);
                }
            });

        }

        @Override
        public int getItemCount() {
            return this.allsponsorMoldelList.size();
        }


        @Override
        public Filter getFilter()
        {
            if (filter == null) {
                filter = new CustomFilterForAllSponsorsList(allmembersfilterList, this);
            }

            return filter;
        }
    }
}
