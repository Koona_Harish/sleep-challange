package com.sleepchallenge.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.PayPalConfig;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONException;

import java.math.BigDecimal;
import java.util.HashMap;

public class PayPalActivity extends AppCompatActivity implements View.OnClickListener {

    TextView amount_txt;
    Button pay_btn;
    String paymentAmount, activity;
    public static final int PAYPAL_REQUEST_CODE = 123;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String user_id = "", challenge_goal = "",  span = "", date = "",deviceId="",challengetype="",member_id = "", member_user_type = "",
            opponent_id = "", opponent_type = "",sendtime="",sendremainder="",walletAmount="", user_type = "",challengeid = "",group_id = "", group_user_type = "",promotion_id="";

    //Paypal Configuration Object
    // private static PayPalConfiguration config = new PayPalConfiguration().environment(PayPalConfiguration.ENVIRONMENT_SANDBOX).clientId(PayPalConfig.PAYPAL_CLIENT_ID);

    //Live Credentials   // if we want credit card pay then remove acceptCreditCards(false) ;
    private static PayPalConfiguration config = new PayPalConfiguration().environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION).clientId(PayPalConfig.PAYPAL_CLIENT_ID).acceptCreditCards(true);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_pal);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        user_id = userDetails.get(UserSessionManager.USER_ID);

        amount_txt = findViewById(R.id.amount_txt);
        Bundle bundle = getIntent().getExtras();
        activity = bundle.getString("activity");

        if (activity.equals("AccountVerification"))
        {
            paymentAmount = bundle.getString("paymentAmount");
            Log.d("AccountVerification", "" + paymentAmount);
        }
        else if (activity.equals("Login"))
        {
            paymentAmount = bundle.getString("paymentAmount");
        }
        else if(activity.equalsIgnoreCase("challengeto"))
        {
            paymentAmount = bundle.getString("paymentAmount");
            user_id = bundle.getString("user_id");
            user_type = bundle.getString("user_type");
            challenge_goal = bundle.getString("challenge_goal");
            challengetype = bundle.getString("challenge_type");
            span = bundle.getString("span");
            date = bundle.getString("challenge_date");
            sendtime = bundle.getString("sleeptime");
            sendremainder = bundle.getString("remainder");
            opponent_id = bundle.getString("opponent_id");
            opponent_type = bundle.getString("opponent_type");

        }
        else if(activity.equalsIgnoreCase("MYPROFILE_ADDWALLET"))
        {
            paymentAmount = bundle.getString("myprofile_addWallet");
            user_id = bundle.getString("user_id");
            user_type = bundle.getString("user_type");
        }
        else if(activity.equalsIgnoreCase("challengeto") ||activity.equalsIgnoreCase("AcceptChallengePayment"))   {
            paymentAmount = bundle.getString("paymentAmount");
            challenge_goal = bundle.getString("challenge_goal");
            challengetype = bundle.getString("challenge_type");
            span = bundle.getString("span");
            date = bundle.getString("challenge_date");
            sendtime = bundle.getString("sleeptime");
            sendremainder = bundle.getString("remainder");
            challengeid = bundle.getString("challengeid");

        }else if(activity.equalsIgnoreCase("MemberDetail")){
            member_id=getIntent().getStringExtra("member_id");
            member_user_type=getIntent().getStringExtra("member_user_type");
            paymentAmount = bundle.getString("paymentAmount");
            walletAmount = bundle.getString("walletAmount");
        }else if(activity.equalsIgnoreCase("GroupDetail")){
            paymentAmount = bundle.getString("paymentAmount");
            walletAmount = bundle.getString("walletAmount");
            group_id=getIntent().getStringExtra("group_id");
            group_user_type=getIntent().getStringExtra("group_user_type");
        }else if(activity.equalsIgnoreCase("MeAsaSponsorAPP")){
            paymentAmount = bundle.getString("paymentAmount");
            walletAmount = bundle.getString("walletAmount");
            group_id = bundle.getString("member_id");
            group_user_type = bundle.getString("member_user_type");
        }else if(activity.equalsIgnoreCase("MyPromotionsActivity")){
            paymentAmount = bundle.getString("paymentAmount");
            walletAmount = bundle.getString("walletAmount");
            promotion_id = bundle.getString("promotionid");
       }

        amount_txt.setText("$ " + paymentAmount + " USD");
        pay_btn = findViewById(R.id.pay_btn);
        pay_btn.setOnClickListener(this);

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PAYPAL_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                if (confirm != null) {
                    try {
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);
                        Intent intent = new Intent(this, ConfirmationActivity.class);
                        intent.putExtra("PaymentDetails", paymentDetails);
                        intent.putExtra("activity", activity);
                        if (activity.equals("AccountVerification"))
                        {
                            intent.putExtra("PaymentAmount", paymentAmount);
                            Log.d("CHECKAMOUNT", paymentAmount);
                        }
                        else if (activity.equals("Login"))
                        {
                            intent.putExtra("PaymentAmount", paymentAmount);
                        }
                        else if(activity.equalsIgnoreCase("challengeto"))
                        {
                            intent.putExtra("PaymentAmount", paymentAmount);
                            intent.putExtra("user_id", user_id);
                            intent.putExtra("user_type",user_type);
                            intent.putExtra("challenge_goal", challenge_goal);
                            intent.putExtra("challenge_type", challengetype);
                            intent.putExtra("challenge_date", date);
                            intent.putExtra("amount", paymentAmount);
                            intent.putExtra("opponent_id", opponent_id);
                            intent.putExtra("opponent_type", opponent_type);
                            intent.putExtra("sleeptime", sendtime);
                            intent.putExtra("span", span);
                            intent.putExtra("remainder", sendremainder);
                            intent.putExtra("activity", activity);
                        }
                        else if(activity.equalsIgnoreCase("MYPROFILE_ADDWALLET"))
                        {
                            intent.putExtra("PaymentAmount", paymentAmount);
                            intent.putExtra("user_id", user_id);
                            intent.putExtra("user_type",user_type);
                            intent.putExtra("activity", activity);
                        }
                        else if(activity.equalsIgnoreCase("AcceptChallengePayment"))
                         {
                            intent.putExtra("PaymentAmount", paymentAmount);
                            intent.putExtra("user_id", user_id);
                            intent.putExtra("user_type",user_type);
                            intent.putExtra("challenge_goal", challenge_goal);
                            intent.putExtra("challenge_type", challengetype);
                            intent.putExtra("challenge_date", date);
                            intent.putExtra("amount", paymentAmount);
                            intent.putExtra("opponent_id", opponent_id);
                            intent.putExtra("opponent_type", opponent_type);
                            intent.putExtra("sleeptime", sendtime);
                            intent.putExtra("span", span);
                            intent.putExtra("remainder", sendremainder);
                            intent.putExtra("activity", activity);
                            intent.putExtra("challengeid", challengeid);
                        }else if(activity.equalsIgnoreCase("MemberDetail")){
                            intent.putExtra("member_id", member_id);
                            intent.putExtra("member_user_type", member_user_type);
                            intent.putExtra("PaymentAmount", paymentAmount);
                            intent.putExtra("walletAmount", "0");
                        }else if(activity.equalsIgnoreCase("GroupDetail")){
                            intent.putExtra("group_id", group_id);
                            intent.putExtra("group_user_type", group_user_type);
                            intent.putExtra("PaymentAmount", paymentAmount);
                            intent.putExtra("walletAmount", "0");
                        }else if(activity.equalsIgnoreCase("MeAsaSponsorAPP")){
                            intent.putExtra("member_id", member_id);
                            intent.putExtra("member_user_type", "ACTIVITY");
                            intent.putExtra("PaymentAmount", paymentAmount);
                            intent.putExtra("walletAmount", "0");
                        }else if(activity.equalsIgnoreCase("MyPromotionsActivity")){
                            intent.putExtra("promotionid", promotion_id);
                            intent.putExtra("PaymentAmount", paymentAmount);
                            intent.putExtra("walletAmount", "0");
                        }
                        startActivity(intent);
                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view == pay_btn) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                getPayment();
            } else {
                Toast.makeText(PayPalActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void getPayment() {
        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(paymentAmount)), "USD", "Sleep Challenge Fee", PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);

    }

    @Override
    public void onBackPressed() {
        onPressingBack();
    }

    private void onPressingBack() {
        if (activity.equals("Registration") || activity.equals("Login") || activity.equals("Welcome"))
        {
            final Intent intent;
            intent = new Intent(PayPalActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(PayPalActivity.this,R.style.MyDialogTheme);
            alertDialog.setTitle("Warning");
            alertDialog.setMessage("Do you cancel this transaction?");
            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    startActivity(intent);
                }
            });
            alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
        } else {
            final Intent intent;
            intent = new Intent(PayPalActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(PayPalActivity.this,R.style.MyDialogTheme);
            alertDialog.setTitle("Warning");
            alertDialog.setMessage("Do you cancel this transaction?");
            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    startActivity(intent);
                }
            });
            alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
        }
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }
}
