package com.sleepchallenge.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chaos.view.PinView;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AccountVerificationActivity extends AppCompatActivity implements View.OnClickListener {

    TextView otp_txt,txtresend;
    PinView pinView;
    Button verify_btn;
    Typeface regularFont, boldFont, thinFont;
    private boolean checkInternet;
    String send_mobile,countryCodeAndroid,deviceId,user_Id;
    UserSessionManager userSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_verification);

        Bundle bundle = getIntent().getExtras();
        send_mobile = bundle.getString("mobile");
        countryCodeAndroid = bundle.getString("countryCode");
        user_Id = bundle.getString("user_id");


        userSessionManager=new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);

        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));

        otp_txt = findViewById(R.id.otp_txt);
        otp_txt.setTypeface(boldFont);
        txtresend = findViewById(R.id.txtresend);
        txtresend.setTypeface(thinFont);
        txtresend.setOnClickListener(this);

        pinView = findViewById(R.id.pinView);
        pinView.setTypeface(thinFont);


        verify_btn = findViewById(R.id.verify_btn);
        verify_btn.setTypeface(regularFont);
        verify_btn.setOnClickListener(this);

        getOTP();
    }

    private void getOTP()
    {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Dialog.showProgressBar(this,"Loading...");
            Log.d("GETOTPURL:", AppUrls.BASE_URL + AppUrls.SENDOTP);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SENDOTP,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Dialog.hideProgressBar();
                            Log.d("GETOTPRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    Toast.makeText(AccountVerificationActivity.this, "OTP has been Sent to your registered Mobile Number", Toast.LENGTH_SHORT).show();
                                    Dialog.hideProgressBar();
                                }
                                if (successResponceCode.equals("10200")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(getApplicationContext(), "User Not Exist..", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10400")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(getApplicationContext(), "Unable to Send Otp..", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Dialog.hideProgressBar();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Dialog.hideProgressBar();

                            error.getMessage();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams(){
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("country_code", countryCodeAndroid);
                    params.put("mobile", send_mobile);
                    params.put("device_id", deviceId);
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(AccountVerificationActivity.this);
            requestQueue.add(stringRequest);
        } else {

        }
    }

    @Override
    public void onClick(View v)
    {
        if (v == verify_btn)
        {
          verifyOtp();
         }

        if (v == txtresend)
        {
             getOTP();
        }

    }

    private boolean validate() {
        boolean result = true;
        String otp = pinView.getText().toString().trim();
        if ((otp == null || otp.equals("")) || otp.length() != 4)
        {
            pinView.setError("Invalid OTP");
            result = false;
        }
        return result;
    }

    public void verifyOtp()
    {
        checkInternet = NetworkChecking.isConnected(this);
        if (validate())
        {
            Dialog.showProgressBar(AccountVerificationActivity.this,"Loading");
            if (checkInternet) {
                StringRequest strveriFyOtp = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.VERIFYOTP, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Dialog.hideProgressBar();

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String successResponceCode = jsonObject.getString("response_code");
                            if (successResponceCode.equals("10100")) {

                                Toast.makeText(AccountVerificationActivity.this, "Mobile has been Verified", Toast.LENGTH_LONG).show();
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                String is_email_verified = jsonObject1.getString("is_email_verified");
                                String initial_payment = jsonObject1.getString("initial_payment");

                                if(initial_payment.equals("0"))
                                {
                                    Intent intent = new Intent(AccountVerificationActivity.this, PayPalActivity.class);
                                    intent.putExtra("activity", "AccountVerification");
                                    intent.putExtra("paymentAmount", "1");
                                    startActivity(intent);
                                    finish();
                                }else {
                                    Intent intent = new Intent(AccountVerificationActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                            if (successResponceCode.equals("10200")) {
                                Dialog.hideProgressBar();
                                Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                            }

                            if (successResponceCode.equals("10300")) {
                                Dialog.hideProgressBar();
                                Toast.makeText(getApplicationContext(), "User Not Exist..!", Toast.LENGTH_SHORT).show();
                            }
                            if (successResponceCode.equals("10400")) {
                                Dialog.hideProgressBar();
                                Toast.makeText(getApplicationContext(), "Mobile is Already Verified..!", Toast.LENGTH_SHORT).show();
                            }
                            if (successResponceCode.equals("10500")) {
                                Dialog.hideProgressBar();
                                Toast.makeText(getApplicationContext(), "Unable to verify OTP..!", Toast.LENGTH_SHORT).show();
                            }
                            if (successResponceCode.equals("10600")) {
                                Dialog.hideProgressBar();
                                Toast.makeText(getApplicationContext(), "Invlid OTP..!", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Dialog.hideProgressBar();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialog.hideProgressBar();
                        error.getMessage();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams()  {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("otp", pinView.getText().toString());
                        params.put("user_id", user_Id);
                        return params;
                    }
                };
                strveriFyOtp.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(AccountVerificationActivity.this);
                requestQueue.add(strveriFyOtp);
            } else {
                Toast.makeText(AccountVerificationActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
    }
}
