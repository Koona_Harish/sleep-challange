package com.sleepchallenge.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.models.ConcenChallModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class NewConcentrateActvity extends AppCompatActivity implements View.OnClickListener {
    private boolean checkInternet;
    UserSessionManager session;
    TextView timer_text;
    Typeface boldFont;
    String user_id, user_type, token, device_ID;
    android.app.Dialog addMoneydialog;
    long seconds;
    int status;//0-TimeFinish,1-Before Time Finish,2-Already Played,3-No Challenge
    RecyclerView concen_challenge_recyclerView;
    ArrayList<ConcenChallModel> concenChallegneList, copyList, joinedArrList;
    int count=0;
    LinearLayout first_row_layout, second_row_layout, third_row_layout, fourth_row_layout;
    ImageView challegne_first_image, challegne_second_image, challegne_third_image, challegne_fourth_image, sec_challegne_first_image, sec_challegne_second_image, sec_challegne_third_image,
            sec_challegne_fourth_image, third_challegne_first_image, third_challegne_second_image, third_challegne_third_image, third_challegne_fourth_image, fourth_challegne_first_image,
            fourth_challegne_second_image, fourth_challegne_third_image, fourth_challegne_fourth_image;
    TextView challegne_first_text, challegne_second_text, challegne_third_text, challegne_fourth_text, sec_challegne_first_text, sec_challegne_second_text, sec_challegne_third_text,
            sec_challegne_fourth_text, third_challegne_first_text, third_challegne_second_text, third_challegne_third_text, third_challegne_fourth_text;

    String ChallengeId = "";
    String selectedId="",selectedSeId="";
    ArrayList<Object> imagesArr=new ArrayList<>();
    ArrayList<Object> numbers=new ArrayList<>();

    CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_concentrate_actvity);
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        session = new UserSessionManager(this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_ID = userDetails.get(UserSessionManager.DEVICE_ID);
        checkInternet = NetworkChecking.isConnected(this);
        concen_challenge_recyclerView = findViewById(R.id.concen_challenge_recyclerView);
        timer_text = findViewById(R.id.timer_text);
        timer_text.setTypeface(boldFont);
        initizeUI();
        getStatusData();

    }

    private void initizeUI() {
        first_row_layout = findViewById(R.id.first_row_layout);
        second_row_layout = findViewById(R.id.second_row_layout);
        third_row_layout = findViewById(R.id.third_row_layout);
        fourth_row_layout = findViewById(R.id.fourth_row_layout);

        challegne_first_image = findViewById(R.id.challegne_first_image);
        challegne_second_image = findViewById(R.id.challegne_second_image);
        challegne_third_image = findViewById(R.id.challegne_third_image);
        challegne_fourth_image = findViewById(R.id.challegne_fourth_image);
        challegne_first_image.setOnClickListener(this);
        challegne_second_image.setOnClickListener(this);
        challegne_third_image.setOnClickListener(this);
        challegne_fourth_image.setOnClickListener(this);

        sec_challegne_first_image = findViewById(R.id.sec_challegne_first_image);
        sec_challegne_second_image = findViewById(R.id.sec_challegne_second_image);
        sec_challegne_third_image = findViewById(R.id.sec_challegne_third_image);
        sec_challegne_fourth_image = findViewById(R.id.sec_challegne_fourth_image);
        sec_challegne_first_image.setOnClickListener(this);
        sec_challegne_second_image.setOnClickListener(this);
        sec_challegne_third_image.setOnClickListener(this);
        sec_challegne_fourth_image.setOnClickListener(this);

        third_challegne_first_image = findViewById(R.id.third_challegne_first_image);
        third_challegne_second_image = findViewById(R.id.third_challegne_second_image);
        third_challegne_third_image = findViewById(R.id.third_challegne_third_image);
        third_challegne_fourth_image = findViewById(R.id.third_challegne_fourth_image);
        third_challegne_first_image.setOnClickListener(this);
        third_challegne_second_image.setOnClickListener(this);
        third_challegne_third_image.setOnClickListener(this);
        third_challegne_fourth_image.setOnClickListener(this);
        //Numbers
        challegne_first_text = findViewById(R.id.challegne_first_text);
        challegne_second_text = findViewById(R.id.challegne_second_text);
        challegne_third_text = findViewById(R.id.challegne_third_text);
        challegne_fourth_text = findViewById(R.id.challegne_fourth_text);
        challegne_first_text.setOnClickListener(this);
        challegne_second_text.setOnClickListener(this);
        challegne_third_text.setOnClickListener(this);
        challegne_fourth_text.setOnClickListener(this);
        challegne_first_text.setTypeface(boldFont);
        challegne_second_text.setTypeface(boldFont);
        challegne_third_text.setTypeface(boldFont);
        challegne_fourth_text.setTypeface(boldFont);

        sec_challegne_first_text = findViewById(R.id.sec_challegne_first_text);
        sec_challegne_second_text = findViewById(R.id.sec_challegne_second_text);
        sec_challegne_third_text = findViewById(R.id.sec_challegne_third_text);
        sec_challegne_fourth_text = findViewById(R.id.sec_challegne_fourth_text);
        sec_challegne_first_text.setOnClickListener(this);
        sec_challegne_second_text.setOnClickListener(this);
        sec_challegne_third_text.setOnClickListener(this);
        sec_challegne_fourth_text.setOnClickListener(this);
        sec_challegne_first_text.setTypeface(boldFont);
        sec_challegne_second_text.setTypeface(boldFont);
        sec_challegne_third_text.setTypeface(boldFont);
        sec_challegne_fourth_text.setTypeface(boldFont);


        third_challegne_first_text = findViewById(R.id.third_challegne_first_text);
        third_challegne_second_text = findViewById(R.id.third_challegne_second_text);
        third_challegne_third_text = findViewById(R.id.third_challegne_third_text);
        third_challegne_fourth_text = findViewById(R.id.third_challegne_fourth_text);
        third_challegne_first_text.setOnClickListener(this);
        third_challegne_second_text.setOnClickListener(this);
        third_challegne_third_text.setOnClickListener(this);
        third_challegne_fourth_text.setOnClickListener(this);
        third_challegne_first_text.setTypeface(boldFont);
        third_challegne_second_text.setTypeface(boldFont);
        third_challegne_third_text.setTypeface(boldFont);
        third_challegne_fourth_text.setTypeface(boldFont);

      /*  fourth_challegne_first_image = findViewById(R.id.fourth_challegne_first_image);
        fourth_challegne_second_image = findViewById(R.id.fourth_challegne_second_image);
        fourth_challegne_third_image = findViewById(R.id.fourth_challegne_third_image);
        fourth_challegne_fourth_image = findViewById(R.id.fourth_challegne_fourth_image);
        fourth_challegne_first_image.setOnClickListener(this);
        fourth_challegne_second_image.setOnClickListener(this);
        fourth_challegne_third_image.setOnClickListener(this);
        fourth_challegne_fourth_image.setOnClickListener(this);*/


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it=new Intent(NewConcentrateActvity.this,MainActivity.class);
        startActivity(it);
    }

    private void getStatusData() {
        if (checkInternet) {//5b3394b7d1525d6cba393a2a------5b33943bd1525d6cba393a29
            String concen_status_url = AppUrls.BASE_URL + AppUrls.GET_CONCEN_CHALLNGE_STATUS + "?user_id=5b3394b7d1525d6cba393a2a";//+user_id;
            StringRequest strConcenReq = new StringRequest(Request.Method.GET, concen_status_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");

                        if (successResponceCode.equals("10100")) {
                            newChallange();
                            JSONObject getData = jsonObject.getJSONObject("data");
                            JSONArray getMemData = getData.getJSONArray("images");
                            ChallengeId = getData.getString("challenge_id");
                            concenChallegneList = new ArrayList<>();
                            copyList = new ArrayList<>();
                            joinedArrList = new ArrayList<>();
                            for (int i = 0; i < getMemData.length(); i++) {
                                JSONObject jdataobj = getMemData.getJSONObject(i);
                                ConcenChallModel concenChalng = new ConcenChallModel(jdataobj);
                                concenChallegneList.add(concenChalng);
                                copyList = (ArrayList<ConcenChallModel>) concenChallegneList.clone();   //clonning arrayList
                            }
                            joinedArrList.addAll(concenChallegneList);    //merging both afrter clonning
                            joinedArrList.addAll(copyList);
                            Collections.shuffle(joinedArrList);
                            showRandomChallenge("Image");

                        }
                        if (successResponceCode.equals("10200")) {
                            Toast.makeText(NewConcentrateActvity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                        }
                        if (successResponceCode.equals("10300")) {
                            status=3;
                            showCommonDialig();
                        }
                        if (successResponceCode.equals("10400")) {
                            status=2;
                           showCommonDialig();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.getMessage();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strConcenReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strConcenReq);
        } else {
            Toast.makeText(NewConcentrateActvity.this, "No Internet Connection...!", Toast.LENGTH_LONG);
        }

    }

    private void showRandomChallenge(String random){
        if(random.equalsIgnoreCase("Image")){
            challegne_first_image.setVisibility(View.VISIBLE);
            challegne_second_image.setVisibility(View.VISIBLE);
            challegne_third_image.setVisibility(View.VISIBLE);
            challegne_fourth_image.setVisibility(View.VISIBLE);
            sec_challegne_first_image.setVisibility(View.VISIBLE);
            sec_challegne_second_image.setVisibility(View.VISIBLE);
            sec_challegne_third_image.setVisibility(View.VISIBLE);
            sec_challegne_fourth_image.setVisibility(View.VISIBLE);
            third_challegne_first_image.setVisibility(View.VISIBLE);
            third_challegne_second_image.setVisibility(View.VISIBLE);
            third_challegne_third_image.setVisibility(View.VISIBLE);
            third_challegne_fourth_image.setVisibility(View.VISIBLE);


        }else {

            challegne_first_text.setVisibility(View.VISIBLE);
            challegne_second_text.setVisibility(View.VISIBLE);
            challegne_third_text.setVisibility(View.VISIBLE);
            challegne_fourth_text.setVisibility(View.VISIBLE);
            sec_challegne_first_text.setVisibility(View.VISIBLE);
            sec_challegne_second_text.setVisibility(View.VISIBLE);
            sec_challegne_third_text.setVisibility(View.VISIBLE);
            sec_challegne_fourth_text.setVisibility(View.VISIBLE);
            third_challegne_first_text.setVisibility(View.VISIBLE);
            third_challegne_second_text.setVisibility(View.VISIBLE);
            third_challegne_third_text.setVisibility(View.VISIBLE);
            third_challegne_fourth_text.setVisibility(View.VISIBLE);
        }
    }
    private void newChallange() {
        addMoneydialog = new android.app.Dialog(NewConcentrateActvity.this);
        addMoneydialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addMoneydialog.setContentView(R.layout.custom_dialog_play_button);
        CardView card_view = addMoneydialog.findViewById(R.id.card_view);
        card_view.setCardBackgroundColor(Color.parseColor("#003350"));
        TextView title_play = addMoneydialog.findViewById(R.id.title_play);
        title_play.setText("PLAY");
        title_play.setTypeface(boldFont);
        ImageView start_game_button = addMoneydialog.findViewById(R.id.start_game_button);
        start_game_button.setVisibility(View.VISIBLE);
        start_game_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addMoneydialog.dismiss();

                   first_row_layout.setVisibility(View.VISIBLE);
                   second_row_layout.setVisibility(View.VISIBLE);
                   third_row_layout.setVisibility(View.VISIBLE);

                 countDownTimer = new CountDownTimer(90000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        seconds=millisUntilFinished/1000;
                        timer_text.setText("Seconds Remaining : " + millisUntilFinished / 1000);
                    }

                    public void onFinish() {
                      timer_text.setText("done!");
                      status=0;
                       if(count==6){
                            challengeResult("WON");
                        }else{
                            challengeResult("LOST");

                        }
                  }

                }.start();

            }
        });
        addMoneydialog.show();
        addMoneydialog.setCanceledOnTouchOutside(false);
        addMoneydialog.setCancelable(false);

    }



    @Override
    public void onClick(View v) {
        String random="Image";
       if(random.equalsIgnoreCase("Image")){
           if (v == challegne_first_image) {
               imagesArr.add(challegne_first_image);
               startAnimation(challegne_first_image,0);
           }
           if (v == challegne_second_image) {
               imagesArr.add(challegne_second_image);
               startAnimation(challegne_second_image,1);
           }
           if (v == challegne_third_image) {
               imagesArr.add(challegne_third_image);
               startAnimation(challegne_third_image,2);
           }
           if (v == challegne_fourth_image) {
               imagesArr.add(challegne_fourth_image);
               startAnimation(challegne_fourth_image,3);
           }
           if (v == sec_challegne_first_image) {
               imagesArr.add(sec_challegne_first_image);
               startAnimation(sec_challegne_first_image,4);
           }
           if (v == sec_challegne_second_image) {
               imagesArr.add(sec_challegne_second_image);
               startAnimation(sec_challegne_second_image,5);
           }
           if (v == sec_challegne_third_image) {
               imagesArr.add(sec_challegne_third_image);
               startAnimation(sec_challegne_third_image,6);
           }
           if (v == sec_challegne_fourth_image) {
               imagesArr.add(sec_challegne_fourth_image);

               startAnimation(sec_challegne_fourth_image,7);
           }
           if (v == third_challegne_first_image) {
               imagesArr.add(third_challegne_first_image);
               startAnimation(third_challegne_first_image,8);
           }
           if (v == third_challegne_second_image) {
               imagesArr.add(third_challegne_second_image);
               startAnimation(third_challegne_second_image,9);
           }
           if (v == third_challegne_third_image) {
               imagesArr.add(third_challegne_third_image);
               startAnimation(third_challegne_third_image,10);
           }
           if (v == third_challegne_fourth_image) {
               imagesArr.add(third_challegne_fourth_image);
               startAnimation(third_challegne_fourth_image,11);
           }
       }else{
           if (v == challegne_first_text) {
               numbers.add(challegne_first_text);
               startAnimation(challegne_first_text,0);
           }
           if (v == challegne_second_text) {
               numbers.add(challegne_second_text);
               startAnimation(challegne_second_text,1);
           }
           if (v == challegne_third_text) {
               numbers.add(challegne_third_text);
               startAnimation(challegne_third_text,2);
           }
           if (v == challegne_fourth_text) {
               numbers.add(challegne_fourth_text);
               startAnimation(challegne_fourth_text,3);
           }
           if (v == sec_challegne_first_text) {
               numbers.add(sec_challegne_first_text);
               startAnimation(sec_challegne_first_text,4);
           }
           if (v == sec_challegne_second_text) {
               numbers.add(sec_challegne_second_text);
               startAnimation(sec_challegne_second_text,5);
           }
           if (v == sec_challegne_third_text) {
               numbers.add(sec_challegne_third_text);
               startAnimation(sec_challegne_third_text,6);
           }
           if (v == sec_challegne_fourth_text) {
               numbers.add(sec_challegne_fourth_text);

               startAnimation(sec_challegne_fourth_text,7);
           }
           if (v == third_challegne_first_text) {
               numbers.add(third_challegne_first_text);
               startAnimation(third_challegne_first_text,8);
           }
           if (v == third_challegne_second_text) {
               numbers.add(third_challegne_second_text);
               startAnimation(third_challegne_second_text,9);
           }
           if (v == third_challegne_third_text) {
               numbers.add(third_challegne_third_text);
               startAnimation(third_challegne_third_text,10);
           }
           if (v == third_challegne_fourth_text) {
               numbers.add(third_challegne_fourth_text);
               startAnimation(third_challegne_fourth_text,11);
           }
       }

        /*if (v == fourth_challegne_first_image) {
            startAnimation(challegne_first_image,12);
        }
        if (v == fourth_challegne_second_image) {
            startAnimation(challegne_first_image,0);
        }
        if (v == fourth_challegne_third_image) {
            startAnimation(challegne_first_image,0);
        }
        if (v == fourth_challegne_fourth_image) {
            startAnimation(challegne_first_image,0);
        }*/
    }

    private void startAnimation(final ImageView img, final int pos){
        final ObjectAnimator oa1 = ObjectAnimator.ofFloat(img, "scaleX", 1f, 0f);
        final ObjectAnimator oa2 = ObjectAnimator.ofFloat(img, "scaleX", 0f, 1f);
        oa1.setInterpolator(new DecelerateInterpolator());
        oa2.setInterpolator(new AccelerateDecelerateInterpolator());
        oa1.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                String imgsr=joinedArrList.get(pos).url_image;
                Picasso.with(NewConcentrateActvity.this)
                        .load(imgsr)
                        .into(img);
                oa2.start();
                String idstr=joinedArrList.get(pos).id;
                if(selectedId.length()==0){
                     Log.v("PICSId",idstr);
                    selectedId=idstr;
                }else if(selectedSeId.length()==0){
                    Log.v("PICSId2",idstr);
                    selectedSeId=idstr;
                }
                if(selectedId.length()>0 && selectedSeId.length()>0){
                    if(selectedId.equalsIgnoreCase(selectedSeId)){
                        count++;
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                selectedId="";selectedSeId="";

                                Log.v("PICS","MATCHINGG...");
                                for(int i=0;i<imagesArr.size();i++){
                                    ImageView imges= (ImageView) imagesArr.get(i);
                                    imges.setImageResource(0);
                                    imges.setOnClickListener(null);
                                }
                                imagesArr.clear();
                            }
                        },500);


                    }else {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                selectedId="";selectedSeId="";
                                // selectedpos=0;selectedsepos=0;
                                for(int i=0;i<imagesArr.size();i++){
                                    ImageView imges= (ImageView) imagesArr.get(i);
                                    imges.setImageResource(R.drawable.tile_back_star);
                                }
                                imagesArr.clear();
                                Log.v("PICS","NoT MATCHINGG...");

                            }
                        }, 500);



                    }
                }
                if(count==6){
                    countDownTimer.cancel();
                    status=1;
                    challengeResult("WON");

                }else{

                }
            }


        });
        oa1.start();
        oa1.setDuration(200);
        oa2.setDuration(200);

    }

    private void startAnimation(final TextView text, final int pos){
        final ObjectAnimator oa1 = ObjectAnimator.ofFloat(text, "scaleX", 1f, 0f);
        final ObjectAnimator oa2 = ObjectAnimator.ofFloat(text, "scaleX", 0f, 1f);
        oa1.setInterpolator(new DecelerateInterpolator());
        oa2.setInterpolator(new AccelerateDecelerateInterpolator());
        oa1.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                String imgsr=joinedArrList.get(pos).id;
                  text.setText(imgsr);
                  text.setBackgroundResource(0);
                oa2.start();
                String idstr=joinedArrList.get(pos).id;
                if(selectedId.length()==0){
                    Log.v("PICSId",idstr);
                    selectedId=idstr;
                }else if(selectedSeId.length()==0){
                    Log.v("PICSId2",idstr);
                    selectedSeId=idstr;
                }
                if(selectedId.length()>0 && selectedSeId.length()>0){
                    if(selectedId.equalsIgnoreCase(selectedSeId)){
                        count++;
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                selectedId="";selectedSeId="";

                                Log.v("PICS","MATCHINGG...");
                                for(int i=0;i<numbers.size();i++){
                                    TextView textView= (TextView) numbers.get(i);
                                     textView.setText("");
                                     textView.setBackgroundResource(0);
                                    textView.setOnClickListener(null);
                                }
                                numbers.clear();
                            }
                        },500);


                    }else {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                selectedId="";selectedSeId="";
                                // selectedpos=0;selectedsepos=0;
                                for(int i=0;i<numbers.size();i++){
                                    TextView textView= (TextView) numbers.get(i);
                                    textView.setText("");
                                    textView.setBackgroundResource(R.drawable.tile_back_star);

                                }
                                numbers.clear();
                                Log.v("PICS","NoT MATCHINGG...");

                            }
                        }, 500);



                    }
                }
                if(count==6){
                    challengeResult("WON");
                }else{

                }
            }


        });
        oa1.start();
        oa1.setDuration(200);
        oa2.setDuration(200);

    }

    private void challengeResult(final String result){
        if (checkInternet) {
          Log.d("CHALLENGERESULT", AppUrls.BASE_URL + AppUrls.GET_CONCEN_CHALLNGE_RESULT);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_CONCEN_CHALLNGE_RESULT,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {

                            Log.d("RESULTRES", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                String message = jsonObject.getString("message");
                                if (successResponceCode.equals("10100"))
                                {
                                    showCommonDialig();
                                }

                                if (successResponceCode.equals("10200")) {
                                  Toast.makeText(NewConcentrateActvity.this,message,Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("10300")) {
                                    Toast.makeText(NewConcentrateActvity.this,message,Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("10400")) {
                                   status=2;
                                   showCommonDialig();
                                }



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            error.getMessage();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("challenge_id", ChallengeId);
                    params.put("status", result);
                    Log.d("RESULTPARAMS:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }

    }


    private void showCommonDialig(){
        addMoneydialog = new android.app.Dialog(NewConcentrateActvity.this);
        addMoneydialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addMoneydialog.setContentView(R.layout.custom_dialog_play_button);
        CardView card_view = addMoneydialog.findViewById(R.id.card_view);
        card_view.setCardBackgroundColor(Color.parseColor("#003350"));
        TextView title_play = addMoneydialog.findViewById(R.id.title_play);
        if(status==0)
        title_play.setText("Time Finished!");
        else if(status==1)
        title_play.setText("   Congratulations \n You Won the Scratch Card");
        else if(status==2)
        title_play.setText("Challenge Already Played !");
        else
         title_play.setText("No Challeange Available for This Week !");
        title_play.setTypeface(boldFont);
        ImageView ok_button = addMoneydialog.findViewById(R.id.ok_button);
        ok_button.setVisibility(View.VISIBLE);
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addMoneydialog.dismiss();

                finish();
            }
        });
        addMoneydialog.show();
        addMoneydialog.setCanceledOnTouchOutside(false);
        addMoneydialog.setCancelable(false);
    }


}
