package com.sleepchallenge.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ChallengeAmountActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView back_img, notification,minus_img, plus_img;
    TextView title_text, choose_txt, price_txt,currency_text;
    Button pay_btn;
    private boolean checkInternet;
    ArrayList<String> priceArr=new ArrayList<>();
    UserSessionManager userSessionManager;
    String deviceId, user_type, user_id, token;
    int count=0;
    RelativeLayout minius_layout,plus_layout;
    TextView plus_txt;
    Typeface regularFont, boldFont, thinFont, specialFont;
    String challengedate="",noofdays="",hours="",timestr="",challengetype="",challengerName="",opponentid="",opponenttype="",remainderstr="",activty="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_amount);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));
        challengedate=getIntent().getStringExtra("challengedate");
        hours=getIntent().getStringExtra("hours");
        timestr=getIntent().getStringExtra("time");
        challengetype=getIntent().getStringExtra("challengetype");
        if(challengetype.equalsIgnoreCase("Weekly"))
            noofdays=getIntent().getStringExtra("noofdays");
        Log.v("Noof days","/////"+challengetype+"daysstr"+noofdays);
        challengerName=getIntent().getStringExtra("challengername");
        opponentid=getIntent().getStringExtra("challengerid");
        opponenttype=getIntent().getStringExtra("challengertype");
        remainderstr=getIntent().getStringExtra("remainder");
        activty=getIntent().getStringExtra("activty");

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        back_img = findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        notification = findViewById(R.id.notification);
        currency_text = findViewById(R.id.currency_text);
        currency_text.setTypeface(boldFont);
        title_text = findViewById(R.id.title_text);
        title_text.setTypeface(boldFont);
        choose_txt = findViewById(R.id.choose_txt);
        choose_txt.setTypeface(regularFont);
        price_txt = findViewById(R.id.price_txt);
       /* Shader shader = new LinearGradient(0, 0, 0, price_txt.getTextSize(), Color.parseColor("#01416E"), Color.parseColor("#4193D1"),
                Shader.TileMode.MIRROR);
        price_txt.getPaint().setShader(shader);*/
        price_txt.setTypeface(boldFont);
        minius_layout = findViewById(R.id.minius_layout);
        plus_layout = findViewById(R.id.plus_layout);
        plus_txt=findViewById(R.id.plus_txt);
        plus_txt.setText("+");
        minius_layout.setOnClickListener(this);

        plus_layout.setOnClickListener(this);
        pay_btn = findViewById(R.id.pay_btn);
        pay_btn = findViewById(R.id.pay_btn);
        pay_btn.setTypeface(regularFont);
        pay_btn.setOnClickListener(this);

        getPrices();
    }

    private void getPrices() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Dialog.showProgressBar(ChallengeAmountActivity.this, "Loading");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.CHALLENGE_PRICES,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            Dialog.hideProgressBar();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                 priceArr.clear();
                                if (successResponceCode.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                         for(int i=0;i<jsonArray.length();i++){
                                             String amount= (String) jsonArray.get(i);
                                          priceArr.add(amount);
                                         }
                                    if(challengetype.equalsIgnoreCase("Weekly")){
                                        String[] days = noofdays.split(" ");
                                        int price=Integer.parseInt(priceArr.get(0)) * Integer.parseInt(days[0]);
                                        price_txt.setText(""+price);
                                    }else {
                                        price_txt.setText(""+priceArr.get(0));
                                    }



                                }
                                if (successResponceCode.equals("10200")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(ChallengeAmountActivity.this, "Invalid input", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(ChallengeAmountActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Dialog.hideProgressBar();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Dialog.hideProgressBar();

                            error.getMessage();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ChallengeAmountActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == back_img) {
            finish();
        }

        if (v == minius_layout) {
               count--;
            if(challengetype.equalsIgnoreCase("Weekly")){

                if(count>=0) {
                    String[] days = noofdays.split(" ");
                    int price=Integer.parseInt(priceArr.get(count)) * Integer.parseInt(days[0]);
                    price_txt.setText(""+price);
                }else{
                    count=0;
                }
            }else {
                if(count>=0) {
                    price_txt.setText(priceArr.get(count));
                }else{
                    count=0;
                }
            }

        }

        if (v == plus_layout) {
                 count++;
            if(challengetype.equalsIgnoreCase("Weekly")){

                if(count<priceArr.size()) {
                    String[] days = noofdays.split(" ");
                    int price=Integer.parseInt(priceArr.get(count)) * Integer.parseInt(days[0]);
                    price_txt.setText(""+price);
                }else {
                    count=priceArr.size()-1;
                }
            }else{
                if(count<priceArr.size()) {
                    price_txt.setText(priceArr.get(count));
                }else {
                    count=priceArr.size()-1;
                }
            }
    }

        if (v == pay_btn) {
            String payamount=price_txt.getText().toString();
            Intent intent=new Intent(ChallengeAmountActivity.this,PaymentDetailsActivity.class);
             intent.putExtra("challengedate",challengedate);
            intent.putExtra("hours",hours);
            intent.putExtra("time",timestr);
            intent.putExtra("challengetype",challengetype);
            intent.putExtra("challengername",challengerName);
            intent.putExtra("challengerid",opponentid);
            intent.putExtra("challengertype",opponenttype);
            intent.putExtra("activty",activty);
            intent.putExtra("remainder",remainderstr);
            intent.putExtra("payamout",payamount);
            if(challengetype.equalsIgnoreCase("Weekly"))
                intent.putExtra("noofdays",noofdays);
            startActivity(intent);
        }

    }
}
