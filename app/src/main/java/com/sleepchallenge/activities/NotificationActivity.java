package com.sleepchallenge.activities;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class NotificationActivity extends AppCompatActivity implements View.OnClickListener {

    TextView title;
    ImageView back_img;
    UserSessionManager session;
    Typeface regularFont;
    String device_id, access_token, user_id;
    private boolean checkInternet;
    ArrayList<UserNotification> notificationArrayList;
    UserNotificationsAdapter notificationsAdapter;
    RecyclerView notification_recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        session = new UserSessionManager(this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        back_img=findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        notification_recyclerView=findViewById(R.id.notification_recyclerView);
        getNotificationData();

    }

    private void getNotificationData() {
        checkInternet = NetworkChecking.isConnected(NotificationActivity.this);
        if (checkInternet) {
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_NOTIFICATION + user_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //  pprogressDialog.dismiss();
                            Log.d("Notification", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    notificationArrayList = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        UserNotification notification = new UserNotification(jsonArray.getJSONObject(i));
                                        notificationArrayList.add(notification);
                                    }
                                    notificationsAdapter = new UserNotificationsAdapter(notificationArrayList, NotificationActivity.this, R.layout.row_notification);
                                    LinearLayoutManager layoutManager = new LinearLayoutManager(NotificationActivity.this);
                                    notification_recyclerView.setNestedScrollingEnabled(false);
                                    notification_recyclerView.setLayoutManager(layoutManager);
                                    notification_recyclerView.setAdapter(notificationsAdapter);

                                }
                                if (successResponceCode.equals("10200")) {


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                              error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(NotificationActivity.this);
            requestQueue.add(strRe);
        } else {

            Toast.makeText(NotificationActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(View view)
    {
       if(view==back_img)
       {
           finish();
       }
    }



    //////////////////ADAPTER CLASS/////////////////////////////////////

    public class UserNotificationsAdapter extends RecyclerView.Adapter<UserNotificationHolder>
    {
        public ArrayList<UserNotification> userNotificationsList;
        NotificationActivity mContext;
        LayoutInflater li;
        int resource;
        String msgBody;
        Dialog dialog;

        public UserNotificationsAdapter(ArrayList<UserNotification> notificationArrayList, NotificationActivity context, int resource) {
            this.userNotificationsList = notificationArrayList;
            this.mContext = context;
            this.resource = resource;
            li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public UserNotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = li.inflate(resource, parent, false);
            UserNotificationHolder specialHolder = new UserNotificationHolder(layout);
            return specialHolder;
        }

        @Override
        public void onBindViewHolder(UserNotificationHolder holder, final int position) {
            Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), mContext.getResources().getString(R.string.regularFont));
            holder.card.setCardBackgroundColor(Color.parseColor("#E0E0E0"));
            holder.notification_txt.setText(userNotificationsList.get(position).title);

            holder.notification_txt.setTypeface(typeface);
            String createddate = userNotificationsList.get(position).sentOn;
            String createtime = userNotificationsList.get(position).sentOnText;
            // msgBody=userNotificationsList.get(position).text;

            String[] dateStr = createddate.split(" ");

            holder.created_date_txt.setText(createtime);

            holder.card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog = new Dialog(mContext);
                    //  dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.custom_dialog_message_detail);
                    Window window = dialog.getWindow();
                    window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
                    window.setGravity(Gravity.CENTER);


                    Button ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
                    TextView message_detail_text = (TextView) dialog.findViewById(R.id.message_detail_text);
                    message_detail_text.setText(Html.fromHtml(userNotificationsList.get(position).text));

                    //  dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);
                    ok_btn.setOnClickListener(new View.OnClickListener() {

                        @Override

                        public void onClick(View v) {
                            dialog.dismiss();
                        }

                    });
                    dialog.show();

                }
            });
        }

        @Override
        public int getItemCount() {
            return userNotificationsList.size();
        }

        public String parseDateToddMMyyyy(String datestr) {
            String inputPattern = "yyyy-MM-dd";
            String outputPattern = "dd MMM yyyy";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            Date date = null;
            String str = null;

            try {
                date = inputFormat.parse(datestr);
                str = outputFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return str;
        }
    }


    //////////////////HOLDER CLASS/////////////////////////////////////

    public class UserNotificationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView notification_txt;
        public TextView created_date_txt;
        public CardView card;
        AllItemClickListeners allItemClickListener;

        public UserNotificationHolder(View itemView) {
            super(itemView);
            notification_txt =  itemView.findViewById(R.id.user_notification_txt);
            created_date_txt = itemView.findViewById(R.id.date);
            card =  itemView.findViewById(R.id.card);
        }

        @Override
        public void onClick(View view) {

            this.allItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(AllItemClickListeners ic) {
            this.allItemClickListener = ic;
        }
    }


    //////////////////MODEL CLASS/////////////////////////////////////
    public class UserNotification implements Serializable
    {

        public String id, title, text, sentOnText, sentOn;

        public UserNotification(JSONObject jsonObject)
        {
            try {
                this.id = jsonObject.getString("id");
                this.title = jsonObject.getString("title");
                this.text = jsonObject.getString("text");
                this.sentOnText = jsonObject.getString("sent_on_txt");
                this.sentOn = jsonObject.getString("sent_on");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
