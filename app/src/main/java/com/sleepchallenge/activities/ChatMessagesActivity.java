package com.sleepchallenge.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sleepchallenge.R;
import com.sleepchallenge.adapters.SleepChllengesViewPagerAdapter;
import com.sleepchallenge.fragments.GroupChatFragment;
import com.sleepchallenge.fragments.SponsorChatFragment;
import com.sleepchallenge.fragments.UserChatFragment;
import com.sleepchallenge.utils.UserSessionManager;

import java.util.HashMap;

public class ChatMessagesActivity extends AppCompatActivity implements  View.OnClickListener{

     ImageView back_img;
     TextView title_text_chat;
     Typeface regularfont;
     TabLayout tab;
     String type,user_type;
     ViewPager view_pager_chatmsg;
    SleepChllengesViewPagerAdapter chat_pager_adapter;
    UserSessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_messages);
        regularfont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_type = userDetails.get(UserSessionManager.USER_TYPE);

        Bundle b = getIntent().getExtras();
        type = b.getString("condition");

        title_text_chat=findViewById(R.id.title_text_chat);
        title_text_chat.setTypeface(regularfont);
            back_img=findViewById(R.id.back_img);
            back_img.setOnClickListener(this);

        tab =  findViewById(R.id.tabLayout_chatmsg);
        view_pager_chatmsg =  findViewById(R.id.view_pager_chatmsg);
        setupViewPager(view_pager_chatmsg);
        tab.setupWithViewPager(view_pager_chatmsg);
  }


    private void setupViewPager(ViewPager view_pager_suggest)
    {
        chat_pager_adapter = new SleepChllengesViewPagerAdapter(getSupportFragmentManager());


        if (type.equals("GROUP_NOT_USER"))
        {
            view_pager_suggest.setCurrentItem(2);
            chat_pager_adapter.addFrag(new GroupChatFragment(), "Group Chat");
            chat_pager_adapter.addFrag(new UserChatFragment(), "User Chat");
            chat_pager_adapter.addFrag(new SponsorChatFragment(), "Sponsor Chat");
        }
        else if(user_type.equals("SPONSOR"))
        {
            chat_pager_adapter.addFrag(new UserChatFragment(), "User Chat");
            chat_pager_adapter.addFrag(new GroupChatFragment(), "Group Chat");
            chat_pager_adapter.addFrag(new SponsorChatFragment(), "Sponsor Chat");
        }
        else
        {
            view_pager_suggest.setCurrentItem(1);
            chat_pager_adapter.addFrag(new UserChatFragment(), "User Chat");
            chat_pager_adapter.addFrag(new GroupChatFragment(), "Group Chat");
            chat_pager_adapter.addFrag(new SponsorChatFragment(), "Sponsor Chat");
        }
        view_pager_suggest.setAdapter(chat_pager_adapter);

        tab.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(view_pager_chatmsg) {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab)
                    {
                        super.onTabSelected(tab);

                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }

                }
        );
    }
    @Override
    public void onClick(View view)
    {
        if(view==back_img)
            finish();
    }
}
