package com.sleepchallenge.activities;

import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import info.hoang8f.android.segmented.SegmentedGroup;

public class ChallengeStatusActivity extends AppCompatActivity implements View.OnClickListener,RadioGroup.OnCheckedChangeListener{
    private boolean checkInternet;
    String deviceId, user_type, user_id, token;
    Typeface regularFont, boldFont, thinFont;
    UserSessionManager userSessionManager;
    ImageView close_btn;
    PieChart challengeChart;
    String challengegoal;
    LineChart challengelinechart,accchallengelinechart,touchchallengelinechart;
    View saperator_view,accsaperator_view,touchsaperator_view;
    String challengeId,challengetype,dayOfTheWeek,opponentId ;
    String noofdays="",datefor="";
    boolean isStartTime=true;
    boolean isEndTime=true;
    boolean isaccStartTime=true;
    boolean isaccEndTime=true;
    boolean istouchStartTime=true;
    boolean istouchEndTime=true;
    String starttimestr="",endtimestr="";
    ArrayList<Float> noisevalues=new ArrayList<>();
    ArrayList<Float> acclometervalues=new ArrayList<>();
    ArrayList<Float> touchvalues=new ArrayList<>();
    ArrayList<String> datetime=new ArrayList<>();
    ArrayList<String> datewithtime=new ArrayList<>();
    ArrayList<Integer> datewithtimeMills=new ArrayList<>();
    ArrayList<String> accdatetime=new ArrayList<>();
    ArrayList<String> accdatewithtime=new ArrayList<>();
    ArrayList<Integer> accdatewithtimeMills=new ArrayList<>();
    ArrayList<String> touchdatetime=new ArrayList<>();
    ArrayList<String> touchdatewithtime=new ArrayList<>();
    ArrayList<Integer> touchdatewithtimeMills=new ArrayList<>();
    float totalgoal,buffertime,remaingtime;
    TextView monday_txt,tue_txt,wed_txt,thr_txt,fri_txt,sat_txt,sun_txt,sleeptime_txt,wakeup_time_txt,current_challenge_txt,
              current_goal_txt,current_noof_days_txt;
    TextView monday_date_txt,tu_date_txt,wend_date_txt,thr_date_txt,friday_date_txt,sat_date_txt,sunday_date_txt;
    LinearLayout mon_layout,tue_layout,wed_layout,thr_layout,friday_layout,sat_layout,sunday_layout;
     RelativeLayout indicator_layout,moving_indictor_layout,touch_indictor_layout;
     TextView  green_text,noise_values_txt,moving_green_text,movment_values_txt,touch_values_txt,touch_green_text;
     SegmentedGroup challenge_status_group;
    ArrayList<PieEntry> yEntrys = new ArrayList<>();
    ArrayList<String> xEntrys = new ArrayList<>();
    ArrayList<Float> yData = new ArrayList<>();
    ArrayList<String> xData = new ArrayList<>();
    ArrayList<Object> challengerdateslist=new ArrayList<>();
    Map<String,String> currentdayObj=new HashMap<>();
    Map<String,String> monObj=new HashMap<>();
    Map<String,String> tueObj=new HashMap<>();
    Map<String,String> wedObj=new HashMap<>();
    Map<String,String> ThrObj=new HashMap<>();
    Map<String,String> FriObj=new HashMap<>();
    Map<String,String> satObj=new HashMap<>();
    Map<String,String> sunObj=new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_status);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));
        initilizeUI();
        challengeId=getIntent().getStringExtra("challengeid");
        challengetype=getIntent().getStringExtra("challengetype");
        opponentId=getIntent().getStringExtra("opponentid");
        Log.v("Values==","////"+opponentId);
        TextView title = findViewById(R.id.title_text);
        title.setTypeface(boldFont);
        challengeStatus(user_id);

    }
    private void addPieChart() {
        challengeChart.setRotationEnabled(true);
        challengeChart.setHoleRadius(25f);
        challengeChart.setTransparentCircleAlpha(0);
        challengeChart.setCenterText("Sleep Challenge");
        challengeChart.setCenterTextSize(10);

        for (int i = 0; i < yData.size(); i++) {
            yEntrys.add(new PieEntry(yData.get(i), xData.get(i)));
        }

       for (int i = 0; i < xData.size(); i++) {
            xEntrys.add(xData.get(i));
        }

        //create the data set
        PieDataSet pieDataSet = new PieDataSet(yEntrys, "");

        pieDataSet.setValueTextSize(12);
        pieDataSet.setValueTextColor(Color.WHITE);

        //add colors to dataset
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.parseColor("#239843"));
        colors.add(Color.parseColor("#0987be"));
        colors.add(Color.parseColor("#ff3232"));
        pieDataSet.setColors(colors);
        PieData data = new PieData(pieDataSet);
         data.setValueFormatter(new PercentFormatter());
        challengeChart.setDrawSliceText(false);
        challengeChart.setData(data);
        challengeChart.getDescription().setText("Statistics");
        challengeChart.setDrawHoleEnabled(true);
        challengeChart.setTransparentCircleRadius(58f);
        challengeChart.setHoleRadius(0);
        pieDataSet.setColors(colors);
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.WHITE);
        Legend l = challengeChart.getLegend();
        l.setForm(Legend.LegendForm.SQUARE);
        l.setPosition(Legend.LegendPosition.BELOW_CHART_RIGHT);
        challengeChart.animateXY(1400, 1400);


    }

    private void showLineChart(){
        green_text.setText("Sleep time between start and end points");
        int statpos = 0,endpos=0;
        int startt=convertToMills(starttimestr);
        int endt=convertToMills(endtimestr);

        for(int i=0;i<datewithtimeMills.size();i++){
                if(datewithtimeMills.get(i)>=startt){
                    if(isStartTime){
                       statpos=i;
                        isStartTime=false;
                    }
                }
       }
        for(int j=0;j<datewithtimeMills.size();j++){
                if(datewithtimeMills.get(j)>=endt){
                    if(isEndTime){
                        endpos=j;
                        isEndTime=false;
                    }
                }
        }

      // x-axis limit line
        LimitLine sleepstart = new LimitLine((float) statpos, "Start");
        sleepstart.setLineWidth(2f);
        sleepstart.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        sleepstart.setTextSize(10f);
        sleepstart.setTypeface(regularFont);
        sleepstart.setLineColor(getResources().getColor(R.color.wincolor));

        LimitLine sleepend = new LimitLine((float) endpos, "End");
        sleepend.setLineWidth(2f);
        sleepend.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        sleepend.setTextSize(10f);
        sleepend.setTypeface(regularFont);
        sleepend.setLineColor(getResources().getColor(R.color.wincolor));


        LimitLine detoxline = new LimitLine((float) 0, " detoxbuffer");
        detoxline.setLineWidth(2f);
        detoxline.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        detoxline.setTextSize(10f);
        detoxline.setTypeface(regularFont);
        detoxline.setLineColor(getResources().getColor(R.color.losscolor));

        LimitLine aftersleepline = new LimitLine((float) datetime.size()-1, "After Sleep Time");
        aftersleepline.setLineWidth(2f);
        aftersleepline.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        aftersleepline.setTextSize(10f);
        aftersleepline.setTypeface(regularFont);
        aftersleepline.setLineColor(getResources().getColor(R.color.losscolor));

        XAxis xAxis = challengelinechart.getXAxis();
        xAxis.enableGridDashedLine(5f, 5f, 0f);
        xAxis.setLabelCount(6, true);
        xAxis.setDrawLabels(true);
        xAxis.setValueFormatter(new MyXAxisValueFormatter(datetime));
        xAxis.setLabelRotationAngle(90);
        xAxis.removeAllLimitLines();
        xAxis.addLimitLine(sleepstart);
        xAxis.addLimitLine(sleepend);
        xAxis.addLimitLine(detoxline);
        xAxis.addLimitLine(aftersleepline);
        LimitLine ll2 = new LimitLine(10f, "Limit");
        ll2.setLineWidth(2f);
        ll2.enableDashedLine(5f, 5f, 0f);
        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        ll2.setTextSize(5f);

        YAxis leftAxis = challengelinechart.getAxisLeft();
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        challengelinechart.getAxisLeft().setAxisMaximum(0f);
        challengelinechart.getAxisRight().setAxisMaximum(0f);
        leftAxis.setGranularity(2.0f);
        leftAxis.addLimitLine(ll2);
        challengelinechart.getAxisLeft().setAxisMinValue(Collections.min(noisevalues));
        challengelinechart.getAxisLeft().setAxisMaximum(Collections.max(noisevalues));
        leftAxis.setLabelCount(6, true);
        challengelinechart.getAxisRight().setEnabled(false);
        challengelinechart.getAxisRight().setEnabled(false);
        challengelinechart.getDescription().setEnabled(false);

       /* if(noisevalues.size()>15)
        challengelinechart.setScaleMinima(8f, 1f);*/
        setdata(noisevalues.size());
        challengelinechart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        challengelinechart.animateX(2500);
        challengelinechart.getLegend().setEnabled(false);


    }

    private void showAcclometerLineChart(){
        moving_green_text.setText("Sleep time between start and end points");
        int statpos = 0,endpos=0;
        int startt=convertToMills(starttimestr);
        int endt=convertToMills(endtimestr);
       for(int i=0;i<accdatewithtimeMills.size();i++){
           if(accdatewithtimeMills.get(i)>=startt){
                if(isaccStartTime){
                   statpos=i;
                    isaccStartTime=false;
                }


            }

        }
        for(int j=0;j<accdatewithtimeMills.size();j++){
            if(accdatewithtimeMills.get(j)>=endt){
                if(isaccEndTime){
                   endpos=j;
                    isaccEndTime=false;
                }
            }
        }
        // x-axis limit line
        LimitLine sleepstart = new LimitLine((float) statpos, "Start");
        sleepstart.setLineWidth(2f);
        sleepstart.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        sleepstart.setTextSize(10f);
        sleepstart.setTypeface(regularFont);
        sleepstart.setLineColor(getResources().getColor(R.color.wincolor));

        LimitLine sleepend = new LimitLine((float) endpos, "End");
        sleepend.setLineWidth(2f);
        sleepend.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        sleepend.setTextSize(10f);
        sleepend.setTypeface(regularFont);
        sleepend.setLineColor(getResources().getColor(R.color.wincolor));


        LimitLine detoxline = new LimitLine((float) 0, " detoxbuffer");
        detoxline.setLineWidth(2f);
        detoxline.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        detoxline.setTextSize(10f);
        detoxline.setTypeface(regularFont);
        detoxline.setLineColor(getResources().getColor(R.color.losscolor));

        LimitLine aftersleepline = new LimitLine((float) accdatetime.size()-1, "After Sleep Time");
        aftersleepline.setLineWidth(2f);
        aftersleepline.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        aftersleepline.setTextSize(10f);
        aftersleepline.setTypeface(regularFont);
        aftersleepline.setLineColor(getResources().getColor(R.color.losscolor));

        XAxis xAxis = accchallengelinechart.getXAxis();
        xAxis.enableGridDashedLine(5f, 5f, 0f);
        xAxis.setLabelCount(6, true);
        xAxis.setDrawLabels(true);
        xAxis.setValueFormatter(new MyXAxisValueFormatter(accdatetime));
        xAxis.setLabelRotationAngle(90);
        xAxis.removeAllLimitLines();
        xAxis.addLimitLine(sleepstart);
        xAxis.addLimitLine(sleepend);
        xAxis.addLimitLine(detoxline);
        xAxis.addLimitLine(aftersleepline);
        LimitLine ll2 = new LimitLine(1f, "Limit");
        ll2.setLineWidth(2f);
        ll2.enableDashedLine(5f, 5f, 0f);
        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        ll2.setTextSize(5f);

        YAxis leftAxis = accchallengelinechart.getAxisLeft();
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        accchallengelinechart.getAxisLeft().setAxisMaximum(0f);
        accchallengelinechart.getAxisRight().setAxisMaximum(0f);
        leftAxis.setGranularity(2.0f);
        leftAxis.addLimitLine(ll2);
        accchallengelinechart.getAxisLeft().setAxisMinValue(Collections.min(acclometervalues));
        accchallengelinechart.getAxisLeft().setAxisMaximum(Collections.max(acclometervalues));
        leftAxis.setLabelCount(6, true);
        accchallengelinechart.getAxisRight().setEnabled(false);
        accchallengelinechart.getAxisRight().setEnabled(false);
        accchallengelinechart.getDescription().setEnabled(false);
        setAcclometerdata(acclometervalues.size());
        accchallengelinechart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        accchallengelinechart.animateX(2500);
        accchallengelinechart.getLegend().setEnabled(false);
    /*    Legend l = challengelinechart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);*/

    }

    private void showTouchLineChart(){
        touch_green_text.setText("Sleep time between start and end points");
        int statpos = 0,endpos=0;
        int startt=convertToMills(starttimestr);
        int endt=convertToMills(endtimestr);
        for(int i=0;i<touchdatewithtimeMills.size();i++){
            if(touchdatewithtimeMills.get(i)>=startt){
                if(istouchStartTime){
                   statpos=i;
                    istouchStartTime=false;
                }


            }

        }
        for(int j=0;j<touchdatewithtimeMills.size();j++){

            if(touchdatewithtimeMills.get(j)>=endt){
                if(istouchEndTime){
                   endpos=j;
                    istouchEndTime=false;
                }
            }

        }
        // x-axis limit line
        LimitLine sleepstart = new LimitLine((float) statpos, "Start");
        sleepstart.setLineWidth(2f);
        sleepstart.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        sleepstart.setTextSize(10f);
        sleepstart.setTypeface(regularFont);
        sleepstart.setLineColor(getResources().getColor(R.color.wincolor));

        LimitLine sleepend = new LimitLine((float) endpos, "End");
        sleepend.setLineWidth(2f);
        sleepend.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        sleepend.setTextSize(10f);
        sleepend.setTypeface(regularFont);
        sleepend.setLineColor(getResources().getColor(R.color.wincolor));


        LimitLine detoxline = new LimitLine((float) 0, " detoxbuffer");
        detoxline.setLineWidth(2f);
        detoxline.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        detoxline.setTextSize(10f);
        detoxline.setTypeface(regularFont);
        detoxline.setLineColor(getResources().getColor(R.color.losscolor));

        LimitLine aftersleepline = new LimitLine((float) touchdatetime.size()-1, "After Sleep Time");
        aftersleepline.setLineWidth(2f);
        aftersleepline.setLabelPosition(LimitLine.LimitLabelPosition.LEFT_TOP);
        aftersleepline.setTextSize(10f);
        aftersleepline.setTypeface(regularFont);
        aftersleepline.setLineColor(getResources().getColor(R.color.losscolor));

        XAxis xAxis = touchchallengelinechart.getXAxis();
        xAxis.enableGridDashedLine(5f, 5f, 0f);
        xAxis.setLabelCount(6, true);
        xAxis.setDrawLabels(true);
        xAxis.setValueFormatter(new MyXAxisValueFormatter(touchdatetime));
        xAxis.setLabelRotationAngle(90);
        xAxis.removeAllLimitLines();
        xAxis.addLimitLine(sleepstart);
        xAxis.addLimitLine(sleepend);
        xAxis.addLimitLine(detoxline);
        xAxis.addLimitLine(aftersleepline);
        LimitLine ll2 = new LimitLine(1f, "Limit");
        ll2.setLineWidth(2f);
        ll2.enableDashedLine(5f, 5f, 0f);
        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        ll2.setTextSize(5f);

        YAxis leftAxis = touchchallengelinechart.getAxisLeft();
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        touchchallengelinechart.getAxisLeft().setAxisMaximum(0f);
        touchchallengelinechart.getAxisRight().setAxisMaximum(0f);
        leftAxis.setGranularity(2.0f);
        leftAxis.addLimitLine(ll2);
        touchchallengelinechart.getAxisLeft().setAxisMinValue(0);
        touchchallengelinechart.getAxisLeft().setAxisMaximum(2);
        leftAxis.setLabelCount(6, true);
        touchchallengelinechart.getAxisRight().setEnabled(false);
        touchchallengelinechart.getAxisRight().setEnabled(false);
        touchchallengelinechart.getDescription().setEnabled(false);
        setTouchdata(touchvalues.size());
        touchchallengelinechart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        touchchallengelinechart.animateX(2500);
        touchchallengelinechart.getLegend().setEnabled(false);
    /*    Legend l = challengelinechart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);*/

    }
    @Override
    public void onClick(View v) {
        if(v==close_btn){
            finish();
        }else if(challengetype.equalsIgnoreCase("DAILY")){
            showLineGraph(currentdayObj);
        }else if(v==mon_layout || v==monday_txt){
          showLineGraph(monObj);
        }else if(v==tue_layout|| v==tue_txt){
            showLineGraph(tueObj);
        }else if(v==wed_layout|| v==wed_txt){
            showLineGraph(wedObj);
        }else if(v==thr_layout|| v==thr_txt){
            showLineGraph(ThrObj);
        }else if(v==friday_layout|| v==fri_txt){
            showLineGraph(FriObj);
        }else if(v==sat_layout|| v==sat_txt){
            showLineGraph(satObj);
        }else if(v==sunday_layout|| v==sun_txt){
            showLineGraph(sunObj);
        }

    }

   //show line chart
    private void setdata(int size){

        ArrayList<Entry> values = new ArrayList<Entry>();

        for (int i = 0; i < size; i++) {

            float val = noisevalues.get(i);
            values.add(new Entry(i, val));
        }
        LineDataSet set1;
        if (challengelinechart.getData() != null &&
                challengelinechart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) challengelinechart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            challengelinechart.getData().notifyDataChanged();
            challengelinechart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "Sleep Challenge");

            set1.setDrawIcons(false);

            // set the line to be drawn like this "- - - - - -"
            set1.enableDashedLine(5f, 5f, 0f);
            set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setColor(Color.BLACK);
            set1.setCircleColor(Color.BLACK);
            set1.setLineWidth(1f);

            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            // set data
            challengelinechart.setData(data);
        }
    }

    //show acclometer values
    private void setAcclometerdata(int size){

        ArrayList<Entry> values = new ArrayList<Entry>();

        for (int i = 0; i < size; i++) {

            float val = acclometervalues.get(i);
            values.add(new Entry(i, val));
        }
        LineDataSet set1;
        if (accchallengelinechart.getData() != null &&
                accchallengelinechart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) accchallengelinechart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            accchallengelinechart.getData().notifyDataChanged();
            accchallengelinechart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "Sleep Challenge");

            set1.setDrawIcons(false);

            // set the line to be drawn like this "- - - - - -"
            set1.enableDashedLine(5f, 5f, 0f);
            set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setColor(Color.BLACK);
            set1.setCircleColor(Color.BLACK);
            set1.setLineWidth(1f);

            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            // set data
            accchallengelinechart.setData(data);
        }
    }


    //show acclometer values
    private void setTouchdata(int size){

        ArrayList<Entry> values = new ArrayList<Entry>();

        for (int i = 0; i < size; i++) {

            float val = touchvalues.get(i);
            values.add(new Entry(i, val));
        }
        LineDataSet set1;
        if (touchchallengelinechart.getData() != null &&
                touchchallengelinechart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) touchchallengelinechart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            touchchallengelinechart.getData().notifyDataChanged();
            touchchallengelinechart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "Sleep Challenge");

            set1.setDrawIcons(false);

            // set the line to be drawn like this "- - - - - -"
            set1.enableDashedLine(5f, 5f, 0f);
            set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setColor(Color.BLACK);
            set1.setCircleColor(Color.BLACK);
            set1.setLineWidth(1f);

            set1.setCircleRadius(3f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(9f);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            // set data
            touchchallengelinechart.setData(data);
        }
    }

    private void initilizeUI(){
        close_btn=findViewById(R.id.back_img);
        close_btn.setOnClickListener(this);
        monday_txt=findViewById(R.id.monday_txt);
        monday_txt.setTypeface(boldFont);
        monday_txt.setOnClickListener(this);
        tue_txt=findViewById(R.id.tue_txt);
        tue_txt.setOnClickListener(this);
        tue_txt.setTypeface(boldFont);
        wed_txt=findViewById(R.id.wend_txt);
        wed_txt.setOnClickListener(this);
        wed_txt.setTypeface(boldFont);
        thr_txt=findViewById(R.id.thr_txt);
        thr_txt.setOnClickListener(this);
        thr_txt.setTypeface(boldFont);
        fri_txt=findViewById(R.id.friday_txt);
        fri_txt.setOnClickListener(this);
        fri_txt.setTypeface(boldFont);
        sat_txt=findViewById(R.id.sat_txt);
        sat_txt.setTypeface(boldFont);
        sat_txt.setOnClickListener(this);
        sun_txt=findViewById(R.id.sunday_txt);
        sun_txt.setTypeface(boldFont);
        sun_txt.setOnClickListener(this);

        monday_date_txt=findViewById(R.id.monday_date_txt);
        monday_date_txt.setTypeface(regularFont);
        tu_date_txt=findViewById(R.id.tu_date_txt);
        tu_date_txt.setTypeface(regularFont);
        wend_date_txt=findViewById(R.id.wend_date_txt);
        wend_date_txt.setTypeface(regularFont);
        thr_date_txt=findViewById(R.id.thr_date_txt);
        thr_date_txt.setTypeface(regularFont);
        friday_date_txt=findViewById(R.id.friday_date_txt);
        friday_date_txt.setTypeface(regularFont);
        sat_date_txt=findViewById(R.id.sat_date_txt);
        sat_date_txt.setTypeface(regularFont);
        sunday_date_txt=findViewById(R.id.sunday_date_txt);
        sunday_date_txt.setTypeface(regularFont);

        mon_layout=findViewById(R.id.mon_layout);
        tue_layout=findViewById(R.id.tue_layout);
        wed_layout=findViewById(R.id.wed_layout);
        thr_layout=findViewById(R.id.thr_layout);
        friday_layout=findViewById(R.id.friday_layout);
        sat_layout=findViewById(R.id.sat_layout);
        sunday_layout=findViewById(R.id.sunday_layout);
        mon_layout.setOnClickListener(this);
        tue_layout.setOnClickListener(this);
        wed_layout.setOnClickListener(this);
        thr_layout.setOnClickListener(this);
        friday_layout.setOnClickListener(this);
        sat_layout.setOnClickListener(this);
        sunday_layout.setOnClickListener(this);
        sleeptime_txt=findViewById(R.id.current_sleep_time_txt);
        sleeptime_txt.setTypeface(regularFont);
        wakeup_time_txt=findViewById(R.id.wake_up_time_txt);
        wakeup_time_txt.setTypeface(regularFont);
        challengeChart = findViewById(R.id.challenge_piechart);
        challengelinechart = findViewById(R.id.challenge_linechart);
        accchallengelinechart = findViewById(R.id.accchallenge_linechart);
       touchchallengelinechart = findViewById(R.id.touchchallenge_linechart);
        saperator_view=findViewById(R.id.saperator_view);
        accsaperator_view=findViewById(R.id.accsaperator_view);
        touchsaperator_view=findViewById(R.id.touchsaperator_view);
       // current_challenge_txt=findViewById(R.id.current_challenge_txt);
        current_goal_txt=findViewById(R.id.current_goal_txt);
        current_noof_days_txt=findViewById(R.id.current_noof_days_txt);
       // current_challenge_txt.setTypeface(regularFont);
        challenge_status_group=findViewById(R.id.challenge_status_group);
        challenge_status_group.setOnCheckedChangeListener(this);
        current_goal_txt.setTypeface(regularFont);
        current_noof_days_txt.setTypeface(regularFont);

        indicator_layout=findViewById(R.id.indictor_layout);
        green_text=findViewById(R.id.green_text);
        green_text.setTypeface(regularFont);
        noise_values_txt=findViewById(R.id.noise_values_txt);
        noise_values_txt.setTypeface(regularFont);
        moving_indictor_layout=findViewById(R.id.moving_indictor_layout);
        moving_green_text=findViewById(R.id.moving_green_text);
        moving_green_text.setTypeface(regularFont);
        movment_values_txt=findViewById(R.id.movment_values_txt);
        movment_values_txt.setTypeface(regularFont);

        touch_indictor_layout=findViewById(R.id.touch_indictor_layout);
        touch_green_text=findViewById(R.id.touch_green_text);
        touch_green_text.setTypeface(regularFont);
        touch_values_txt=findViewById(R.id.touch_values_txt);
        touch_values_txt.setTypeface(regularFont);

    }

    private void showVisible(int mon,int tue,int wed,int thr,int fri,int sat,int sun){
        mon_layout.setVisibility(mon);
        tue_layout.setVisibility(tue);
        wed_layout.setVisibility(wed);
        thr_layout.setVisibility(thr);
        friday_layout.setVisibility(fri);
        sat_layout.setVisibility(sat);
        sunday_layout.setVisibility(sun);
    }

    private void challengeStatus(String id){
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Dialog.showProgressBar(ChallengeStatusActivity.this, "Loading");
            String url = AppUrls.BASE_URL + AppUrls.CHALLENGE_STATUS +challengeId+ "&user_id=" + id;
            Log.d("STATUSURL", url);
            StringRequest statusstr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Dialog.hideProgressBar();
                    Log.d("STATUSRES", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100"))
                        {
                            JSONObject responseObj = jsonObject.optJSONObject("data");

                            JSONArray opponent_daily_status=responseObj.optJSONArray("opponent_daily_status");
                            JSONObject challengeobj=responseObj.optJSONObject("challenge");

                            starttimestr=challengeobj.optString("start_on");
                            endtimestr=challengeobj.optString("completed_on");
                            String completed_time=parseTime(challengeobj.optString("completed_on"));
                            String start_time=parseTime(challengeobj.optString("start_on"));
                            sleeptime_txt.setText("StartTime : "+start_time);
                            wakeup_time_txt.setText("Completed Time :"+completed_time);
                            challengegoal=challengeobj.optString("challenge_goal");
                            noofdays=challengeobj.optString("day_span");
                            current_goal_txt.setText("Challenge Goal : "+challengegoal+" Hours");
                            Type stringStringMap = new TypeToken<ArrayList<Object>>() {
                            }.getType();
                            challengerdateslist.clear();
                            ArrayList<Object> tmpObj= new Gson().fromJson(responseObj.optString("challenger_daily_status"), stringStringMap);
                            challengerdateslist.addAll(tmpObj);
                          if(challengerdateslist.size()>0){
                              if(challengetype.equalsIgnoreCase("DAILY")){
                                  currentdayObj= (Map<String, String>) challengerdateslist.get(0);

                              }else{

                                  monObj = (Map<String, String>) challengerdateslist.get(0);
                                  tueObj = (Map<String, String>) challengerdateslist.get(1);
                                  wedObj = (Map<String, String>) challengerdateslist.get(2);
                                  ThrObj = (Map<String, String>) challengerdateslist.get(3);
                                  FriObj = (Map<String, String>) challengerdateslist.get(4);
                                  satObj = (Map<String, String>) challengerdateslist.get(5);
                                  sunObj = (Map<String, String>) challengerdateslist.get(6);
                              }
                          }

                                current_noof_days_txt.setText("Noofdays :"+noofdays);

                            int goal=Integer.parseInt(challengegoal);
                            float total=(float)(goal);
                           totalgoal=(total/24)*100;
                           buffertime=((float) 2/24)*100;
                           remaingtime=((float) ((int)24-(total+2))/24)*100;
                            xData.add("SleepingTime");
                            xData.add("BufferTime");
                            xData.add("RemaingTime");
                            yData.add(totalgoal);
                            yData.add(buffertime);
                            yData.add(remaingtime);
                            addPieChart();
                            showStatus();
                        }
                        if (successResponceCode.equals("10200"))
                        {
                            Toast.makeText(ChallengeStatusActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Dialog.hideProgressBar();
                    Log.e("Error", error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            statusstr.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(statusstr);
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void challengeGraph(){
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Dialog.showProgressBar(ChallengeStatusActivity.this, "Fetching data...");
            String url = AppUrls.BASE_URL + AppUrls.GRAPH_DATA +user_id+ "&challenge_id="+challengeId+ "&date_for="+datefor;
            Log.d("STATUSURL", url);
            StringRequest statusstr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Dialog.hideProgressBar();
                    Log.d("STATUSRES", response);

                    try {
                        noisevalues.clear();
                        datetime.clear();
                        datewithtime.clear();
                        datewithtimeMills.clear();
                        acclometervalues.clear();
                        accdatetime.clear();
                        accdatewithtimeMills.clear();
                        accdatewithtime.clear();
                        touchvalues.clear();
                        touchdatetime.clear();
                        touchdatewithtimeMills.clear();
                        touchdatewithtime.clear();
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100"))
                        {
                           graphVisibilty(View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE);

                            JSONObject resObj = jsonObject.optJSONObject("data");
                            JSONArray noisearr=resObj.optJSONArray("noise_data");
                            JSONArray accarr=resObj.optJSONArray("accelerometer_data");
                            JSONArray toucharr=resObj.optJSONArray("touch_data");
                            if(noisearr.length()>0){
                                for(int i=0;i<noisearr.length();i++){
                                    JSONObject jsonObject1=  noisearr.getJSONObject(i);
                                    String value= (String) jsonObject1.get("val");
                                    String time= (String) jsonObject1.get("t");
                                    String date= (String) jsonObject1.get("d");
                                    noisevalues.add(Float.parseFloat(value));
                                    datetime.add(time);
                                    datewithtime.add(date+" "+time);
                                    datewithtimeMills.add(convertToMills(date+" "+time));


                                }
                                graphVisibilty(View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE);
                                showLineChart();
                            }

                            if(accarr.length()>0){
                                for(int i=0;i<accarr.length();i++){
                                    JSONObject jsonObject1=  accarr.getJSONObject(i);
                                    String value= (String) jsonObject1.get("val");
                                    String time= (String) jsonObject1.get("t");
                                    String date= (String) jsonObject1.get("d");
                                    acclometervalues.add(Float.parseFloat(value));
                                    accdatetime.add(time);
                                    accdatewithtime.add(date+" "+time);
                                    accdatewithtimeMills.add(convertToMills(date+" "+time));


                                }
                                graphVisibilty(View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.GONE,View.GONE,View.GONE,View.GONE);
                                showAcclometerLineChart();

                            }
                            if(toucharr.length()>0){
                                for(int i=0;i<toucharr.length();i++){
                                    JSONObject jsonObject1=  toucharr.getJSONObject(i);
                                    String value= (String) jsonObject1.get("val");
                                    String time= (String) jsonObject1.get("t");
                                    String date= (String) jsonObject1.get("d");
                                    touchvalues.add(Float.parseFloat(value));
                                    touchdatetime.add(time);
                                    touchdatewithtime.add(date+" "+time);
                                    touchdatewithtimeMills.add(convertToMills(date+" "+time));


                                }
                                graphVisibilty(View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE);
                                showTouchLineChart();

                            }


                        }
                        if (successResponceCode.equals("10200"))
                        {
                            Toast.makeText(ChallengeStatusActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                        }
                        if (successResponceCode.equals("10300"))
                        {

                            graphVisibilty(View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE,View.GONE);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Dialog.hideProgressBar();
                    Log.e("Error", error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            statusstr.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(statusstr);
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void showStatus(){
       /* SimpleDateFormat sdf1 = new SimpleDateFormat("E");
        Date d = new Date();
        dayOfTheWeek = sdf1.format(d);*/
       if(challengerdateslist.size()>0){
           if(challengetype.equalsIgnoreCase("DAILY")){
               current_noof_days_txt.setVisibility(View.GONE);
               current_goal_txt.setTypeface(boldFont);
               RelativeLayout.LayoutParams layoutParams =
                       (RelativeLayout.LayoutParams)current_goal_txt.getLayoutParams();
               layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
               current_goal_txt.setLayoutParams(layoutParams);
               SimpleDateFormat sdf = new SimpleDateFormat("dd/MM");
               Calendar c = Calendar.getInstance();

               String status="";
               if(currentdayObj.containsKey("winning_status")){
                   status=currentdayObj.get("winning_status");
               }
               datefor=currentdayObj.get("challenge_date");
               String completed_on=currentdayObj.get("completed_on");
               String[] split=completed_on.split(" ");
               String date = datefor;
               dayOfTheWeek=getDayOfWeek(date);
               date=parseDate(date);
               if(dayOfTheWeek.equalsIgnoreCase("Mon")){
                   mon_layout.setVisibility(View.VISIBLE);
                   monday_date_txt.setText(date);
                   if(status.equalsIgnoreCase("WON")){
                       monday_txt.setBackgroundResource(R.drawable.wonchallenge_bg);
                   }else if(status.equalsIgnoreCase("LOST")){
                       monday_txt.setBackgroundResource(R.drawable.losschallenge_bg);
                   }else {
                       monday_txt.setBackgroundResource(R.drawable.upcoming_bg);
                   }
               }else if(dayOfTheWeek.equalsIgnoreCase("Tue")){
                   tue_layout.setVisibility(View.VISIBLE);
                   tu_date_txt.setText(date);
                   if(status.equalsIgnoreCase("WON")){
                       tue_txt.setBackgroundResource(R.drawable.wonchallenge_bg);
                   }else if(status.equalsIgnoreCase("LOST")){
                       tue_txt.setBackgroundResource(R.drawable.losschallenge_bg);
                   }else {
                       tue_txt.setBackgroundResource(R.drawable.upcoming_bg);
                   }
               }else if(dayOfTheWeek.equalsIgnoreCase("Wed")){
                   wed_layout.setVisibility(View.VISIBLE);
                   wend_date_txt.setText(date);
                   if(status.equalsIgnoreCase("WON")){
                       wed_txt.setBackgroundResource(R.drawable.wonchallenge_bg);
                   }else if(status.equalsIgnoreCase("LOST")){
                       wed_txt.setBackgroundResource(R.drawable.losschallenge_bg);
                   }else {
                       wed_txt.setBackgroundResource(R.drawable.upcoming_bg);
                   }
               }else if(dayOfTheWeek.equalsIgnoreCase("Thu")){
                   thr_layout.setVisibility(View.VISIBLE);
                   thr_date_txt.setText(date);
                   if(status.equalsIgnoreCase("WON")){
                       thr_txt.setBackgroundResource(R.drawable.wonchallenge_bg);
                   }else if(status.equalsIgnoreCase("LOST")){
                       thr_txt.setBackgroundResource(R.drawable.losschallenge_bg);
                   }else {
                       thr_txt.setBackgroundResource(R.drawable.upcoming_bg);
                   }
               }else if(dayOfTheWeek.equalsIgnoreCase("Fri")){
                   friday_layout.setVisibility(View.VISIBLE);
                   friday_date_txt.setText(date);
                   if(status.equalsIgnoreCase("WON")){
                       fri_txt.setBackgroundResource(R.drawable.wonchallenge_bg);
                   }else if(status.equalsIgnoreCase("LOST")){
                       fri_txt.setBackgroundResource(R.drawable.losschallenge_bg);
                   }else {
                       fri_txt.setBackgroundResource(R.drawable.upcoming_bg);
                   }

               }else if(dayOfTheWeek.equalsIgnoreCase("Sat")){
                   sat_layout.setVisibility(View.VISIBLE);
                   sat_date_txt.setText(date);
                   if(status.equalsIgnoreCase("WON")){
                       sat_txt.setBackgroundResource(R.drawable.wonchallenge_bg);
                   }else if(status.equalsIgnoreCase("LOST")){
                       sat_txt.setBackgroundResource(R.drawable.losschallenge_bg);
                   }else {
                       sat_txt.setBackgroundResource(R.drawable.upcoming_bg);
                   }
               }else if(dayOfTheWeek.equalsIgnoreCase("Sun")){
                   sunday_layout.setVisibility(View.VISIBLE);
                   sunday_date_txt.setText(date);
                   if(status.equalsIgnoreCase("WON")){
                       sun_txt.setBackgroundResource(R.drawable.wonchallenge_bg);
                   }else if(status.equalsIgnoreCase("LOST")){
                       sun_txt.setBackgroundResource(R.drawable.losschallenge_bg);
                   }else {
                       sun_txt.setBackgroundResource(R.drawable.upcoming_bg);
                   }

               }

           }else{
               showVisible(View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE,View.VISIBLE);

               current_goal_txt.setTypeface(regularFont);

                   datefor=monObj.get("challenge_date");
                   monday_date_txt.setText(parseDate(monObj.get("challenge_date")));
                   tu_date_txt.setText(parseDate(tueObj.get("challenge_date")));
                   wend_date_txt.setText(parseDate(wedObj.get("challenge_date")));
                   thr_date_txt.setText(parseDate(ThrObj.get("challenge_date")));
                   friday_date_txt.setText(parseDate(FriObj.get("challenge_date")));
                   sat_date_txt.setText(parseDate(satObj.get("challenge_date")));
                   sunday_date_txt.setText(parseDate(sunObj.get("challenge_date")));
                   String monstatus=monObj.get("winning_status");
                   String tuestatus=tueObj.get("winning_status");
                   String wedstatus=wedObj.get("winning_status");
                   String thrstatus=ThrObj.get("winning_status");
                   String fristatus=FriObj.get("winning_status");
                   String satstatus=satObj.get("winning_status");
                   String sunstatus=sunObj.get("winning_status");
                   if(monstatus.equalsIgnoreCase("WON")){
                       monday_txt.setBackgroundResource(R.drawable.wonchallenge_bg);
                   }else if(monstatus.equalsIgnoreCase("LOST")){
                       monday_txt.setBackgroundResource(R.drawable.losschallenge_bg);
                   }else {
                       monday_txt.setBackgroundResource(R.drawable.upcoming_bg);
                   }
                   if(tuestatus.equalsIgnoreCase("WON")){
                       tue_txt.setBackgroundResource(R.drawable.wonchallenge_bg);
                   }else if(tuestatus.equalsIgnoreCase("LOST")){
                       tue_txt.setBackgroundResource(R.drawable.losschallenge_bg);
                   }else {
                       tue_txt.setBackgroundResource(R.drawable.upcoming_bg);
                   }
                   if(wedstatus.equalsIgnoreCase("WON")){
                       wed_txt.setBackgroundResource(R.drawable.wonchallenge_bg);
                   }else if(wedstatus.equalsIgnoreCase("LOST")){
                       wed_txt.setBackgroundResource(R.drawable.losschallenge_bg);
                   }else {
                       wed_txt.setBackgroundResource(R.drawable.upcoming_bg);
                   }
                   if(thrstatus.equalsIgnoreCase("WON")){
                       thr_txt.setBackgroundResource(R.drawable.wonchallenge_bg);
                   }else if(thrstatus.equalsIgnoreCase("LOST")){
                       thr_txt.setBackgroundResource(R.drawable.losschallenge_bg);
                   }else {
                       thr_txt.setBackgroundResource(R.drawable.upcoming_bg);
                   }
                   if(fristatus.equalsIgnoreCase("WON")){
                       fri_txt.setBackgroundResource(R.drawable.wonchallenge_bg);
                   }else if(fristatus.equalsIgnoreCase("LOST")){
                       fri_txt.setBackgroundResource(R.drawable.losschallenge_bg);
                   }else {
                       fri_txt.setBackgroundResource(R.drawable.upcoming_bg);
                   }
                   if(satstatus.equalsIgnoreCase("WON")){
                       sat_txt.setBackgroundResource(R.drawable.wonchallenge_bg);
                   }else if(satstatus.equalsIgnoreCase("LOST")){
                       sat_txt.setBackgroundResource(R.drawable.losschallenge_bg);
                   }else {
                       sat_txt.setBackgroundResource(R.drawable.upcoming_bg);
                   }
                   if(sunstatus.equalsIgnoreCase("WON")){
                       sun_txt.setBackgroundResource(R.drawable.wonchallenge_bg);
                   }else if(sunstatus.equalsIgnoreCase("LOST")){
                       sun_txt.setBackgroundResource(R.drawable.losschallenge_bg);
                   }else {
                       sun_txt.setBackgroundResource(R.drawable.upcoming_bg);
                   }



           }
       }

        challengeGraph();

    }

    private int convertToMills(String date){
        int mills=0;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date mDate = sdf.parse(date);
             long millss = mDate.getTime();
             mills = (int) TimeUnit.MILLISECONDS.toSeconds(millss);
            Log.v("Time","//"+mills);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mills;
    }

    public String parseTime(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "HH:mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
    private String parseDate(String date){
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd/MM";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date1 = null;
        String str = null;

        try {
            date1 = inputFormat.parse(date);
            str = outputFormat.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return str;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if(checkedId==R.id.user_status_radio){
            challengeStatus(user_id);
        }else {
            challengeStatus(opponentId);
        }
    }

    public class MyXAxisValueFormatter implements IAxisValueFormatter {

        private ArrayList<String> mValues;

        public MyXAxisValueFormatter(ArrayList<String> values) {
            this.mValues = values;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            // "value" represents the position of the label on the axis (x or y)
            //if(mValues.length > (int) value) {
            if (value >= 0) {
                if (mValues.size()> (int) value) {
                    return mValues.get((int) value);
                } else return "";
            } else {
                return "";
            }
            //} else return null;
        }

        /** this is only needed if numbers are returned, else return 0 */

    }
   private void showLineGraph(Map<String,String> tmp){
        isStartTime=true;isEndTime=true;
        isaccStartTime=true;isaccEndTime=true;
        datefor=tmp.get("challenge_date");
        starttimestr=tmp.get("start_on");
        endtimestr=tmp.get("completed_on");
        challengeGraph();
   }

    public static String getDayOfWeek(String stringDate) {
        String dayOfWeek;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        Calendar calender = new GregorianCalendar();

        try {
            date = format.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
            dayOfWeek = null;
        }

        calender.setTime(date);

        switch (calender.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:
                dayOfWeek = "Mon";
                break;
            case Calendar.TUESDAY:
                dayOfWeek = "Tue";
                break;
            case Calendar.WEDNESDAY:
                dayOfWeek = "Wed";
                break;
            case Calendar.THURSDAY:
                dayOfWeek = "Thu";
                break;
            case Calendar.FRIDAY:
                dayOfWeek = "Fri";
                break;
            case Calendar.SATURDAY:
                dayOfWeek = "Sat";
                break;
            case Calendar.SUNDAY:
                dayOfWeek = "Sun";
                break;

            default:
                dayOfWeek = null;
                break;
        }

        return dayOfWeek;
    }
    private Date ConvertToDate(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return convertedDate;
    }

    private String dateToString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

        //to convert Date to String, use format method of SimpleDateFormat class.
        String strDate = dateFormat.format(date);
        return strDate;
    }
  private void graphVisibilty(int noisechart,int noisesaperator_view,int noiseindicator_layout,int noise_text,int accclinechart,int moving_view,
                              int moving_indictor,int movment_text,int touchlinechart,int touch_view,
                              int touch_indictor,int touch_text){
      challengelinechart.setVisibility(noisechart);
      saperator_view.setVisibility(noisesaperator_view);
      indicator_layout.setVisibility(noiseindicator_layout);
      noise_values_txt.setVisibility(noise_text);
      accchallengelinechart.setVisibility(accclinechart);
      accsaperator_view.setVisibility(moving_view);
      moving_indictor_layout.setVisibility(moving_indictor);
      movment_values_txt.setVisibility(movment_text);
      touchchallengelinechart.setVisibility(touchlinechart);
      touchsaperator_view.setVisibility(touch_view);
      touch_indictor_layout.setVisibility(touch_indictor);
      touch_values_txt.setVisibility(touch_text);

  }


}
