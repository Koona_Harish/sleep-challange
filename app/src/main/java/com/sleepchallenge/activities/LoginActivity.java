package com.sleepchallenge.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    private TextView forgot_txt;
    EditText username_edt, password_edt;
    Button login_btn;

    String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
    String deviceId, user_type, country_code, send_emailORmobile, send_pasword;
    Typeface regularFont,  thinFont, specialFont;
    UserSessionManager userSessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        country_code = userDetails.get(UserSessionManager.COUNTRY_CODE);


        Bundle bundle = getIntent().getExtras();
        user_type = bundle.getString("USERTYPE");
        Log.d("LOGDETAIL", deviceId + "///" + user_type + "///" + country_code);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));

        checkInternet = NetworkChecking.isConnected(this);

        login_btn = findViewById(R.id.login_btn);
        login_btn.setTypeface(regularFont);

        username_edt = findViewById(R.id.username_edt);
        username_edt.setTypeface(specialFont);
        password_edt = findViewById(R.id.password_edt);
        password_edt.setTypeface(specialFont);
        forgot_txt = findViewById(R.id.forgot_txt);
        forgot_txt.setTypeface(thinFont);

        forgot_txt.setOnClickListener(this);
        login_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (v == login_btn) {
            checkInternet = NetworkChecking.isConnected(LoginActivity.this);
            if (validate()) {
                if (checkInternet) {
                    Dialog.showProgressBar(this, "Please Wait...");
                    generalLogin();
                } else {

                    Toast.makeText(getApplicationContext(), "No Internet Connection ..!!", Toast.LENGTH_SHORT).show();
                }
            }
        }

        if (v == forgot_txt) {
            Intent it = new Intent(LoginActivity.this, ForgotPassword.class);
            startActivity(it);
        }
    }


    private void generalLogin() {
        send_emailORmobile = username_edt.getText().toString();
        send_pasword = password_edt.getText().toString();
        Log.d("LOGINURL", AppUrls.BASE_URL + AppUrls.LOGIN);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d("LGOINRESP", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String successResponceCode = jsonObject.getString("response_code");
                    if (successResponceCode.equals("10100")) {
                        // getToken();
                        Dialog.hideProgressBar();
                        JSONObject jobj = jsonObject.getJSONObject("data");
                        Log.d("JOBJ", jobj.toString());
                        String token = jobj.getString("token");
                        String user_id = jobj.getString("user_id");
                        String user_type = jobj.getString("user_type");
                        String is_mobile_verified = jobj.getString("is_mobile_verified");
                        String is_email_verified = jobj.getString("is_email_verified");
                        String initial_payment = jobj.getString("initial_payment");
                        Log.d("JWT", token + "//" + initial_payment);
                        String[] parts = token.split("\\.");
                        byte[] dataDec = Base64.decode(parts[1], Base64.DEFAULT);
                        String decodedString = "";
                        try {
                            decodedString = new String(dataDec, "UTF-8");
                            Log.d("TOKENSFSF", decodedString);
                            JSONObject jsonObject2 = new JSONObject(decodedString);
                            Log.d("JSONDATAsfa", jsonObject2.toString());
                            String username = jsonObject2.getString("name");
                            String email = jsonObject2.getString("email");
                            String mobile = jsonObject2.getString("mobile");
                            String status = jsonObject2.getString("status");
                            String country_code = jsonObject2.getString("country_code");

                            userSessionManager.createUserLoginSession(user_id, user_type, token, username, email, mobile, country_code);
                            if (is_mobile_verified.equals("0")) {
                                Intent itootp = new Intent(LoginActivity.this, AccountVerificationActivity.class);
                                itootp.putExtra("mobile", mobile);
                                itootp.putExtra("countryCode", country_code);
                                itootp.putExtra("user_id", user_id);
                                startActivity(itootp);
                            } else if (initial_payment.equals("0")) {
                                Toast.makeText(LoginActivity.this, "In order to validate your payment method, we will deduct $ 1 and this will be deposited into your application wallet account", Toast.LENGTH_SHORT).show();
                                Intent iacc_page = new Intent(LoginActivity.this, PayPalActivity.class);
                                iacc_page.putExtra("activity", "Login");
                                iacc_page.putExtra("paymentAmount", "1");
                                startActivity(iacc_page);
                            } else {
                                Intent imain = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(imain);
                            }


                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    if (successResponceCode.equals("10200")) {
                        Dialog.hideProgressBar();
                        Toast.makeText(getApplicationContext(), "Invalid Input..!", Toast.LENGTH_SHORT).show();
                    }
                    if (successResponceCode.equals("10300")) {
                        Dialog.hideProgressBar();
                        Toast.makeText(getApplicationContext(), "User does not Exist..!", Toast.LENGTH_SHORT).show();
                    }
                    if (successResponceCode.equals("10400")) {
                        Dialog.hideProgressBar();
                        Toast.makeText(getApplicationContext(), "Account is Deactivated please contact to Admin..!", Toast.LENGTH_SHORT).show();
                    }
                    if (successResponceCode.equals("10500")) {
                        Dialog.hideProgressBar();
                        Toast.makeText(getApplicationContext(), "Account is Blocked please contact to Admin..!", Toast.LENGTH_SHORT).show();
                    }

                    if (successResponceCode.equals("10600")) {
                        Dialog.hideProgressBar();
                        Toast.makeText(getApplicationContext(), "Invalid Credentials, Pleae Try Again..!", Toast.LENGTH_SHORT).show();
                    }

                    if (successResponceCode.equals("10700")) {
                        Dialog.hideProgressBar();
                        Toast.makeText(getApplicationContext(), "It Seen you are login from Differnet Device!", Toast.LENGTH_SHORT).show();
                         forceLogin();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Dialog.hideProgressBar();

               error.getMessage();
            }
        }) {
            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_type", user_type);
                params.put("username", send_emailORmobile);      // here if we login with mobile no. ,same field and variable use
                params.put("password", send_pasword);
                params.put("device_id", deviceId);
                params.put("force_login", "0");
                Log.d("LOGINPARAM", params.toString());
                return params;// {force_login =0, email=9370744745, password=123456, country_code=+91, device_id=451e23be6ccc4b18, user_type=USER}
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(stringRequest);

    }

   /////// FORCE LOGIN CODE
       public void forceLogin()
       {
           AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this,R.style.MyDialogTheme);
           builder.setTitle("You already logged in from a different device (Multiple device login is not possible)" + "\n" + "Do you want to continue to login click Yes..?");
           builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
               @Override
               public void onClick(DialogInterface dialog, int i)
               {
                   send_emailORmobile = username_edt.getText().toString();
                   send_pasword = password_edt.getText().toString();
                   Log.d("LOGINURL", AppUrls.BASE_URL + AppUrls.LOGIN);
                   StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOGIN, new Response.Listener<String>() {

                       @Override
                       public void onResponse(String response) {
                           Log.d("LGOINRESP", response);
                           try {
                               JSONObject jsonObject = new JSONObject(response);
                               String successResponceCode = jsonObject.getString("response_code");
                               if (successResponceCode.equals("10100")) {
                                   // getToken();
                                   Dialog.hideProgressBar();
                                   JSONObject jobj = jsonObject.getJSONObject("data");
                                   Log.d("JOBJ", jobj.toString());
                                   String token = jobj.getString("token");
                                   String user_id = jobj.getString("user_id");
                                   String user_type = jobj.getString("user_type");
                                   String is_mobile_verified = jobj.getString("is_mobile_verified");
                                   String is_email_verified = jobj.getString("is_email_verified");
                                   String initial_payment = jobj.getString("initial_payment");
                                   Log.d("JWT", token + "//" + initial_payment);
                                   String[] parts = token.split("\\.");
                                   byte[] dataDec = Base64.decode(parts[1], Base64.DEFAULT);
                                   String decodedString = "";
                                   try {
                                       decodedString = new String(dataDec, "UTF-8");
                                       Log.d("TOKENSFSF", decodedString);
                                       JSONObject jsonObject2 = new JSONObject(decodedString);
                                       Log.d("JSONDATAsfa", jsonObject2.toString());
                                       String username = jsonObject2.getString("name");
                                       String email = jsonObject2.getString("email");
                                       String mobile = jsonObject2.getString("mobile");
                                       String status = jsonObject2.getString("status");
                                       String country_code = jsonObject2.getString("country_code");

                                       userSessionManager.createUserLoginSession(user_id, user_type, token, username, email, mobile, country_code);
                                       if (is_mobile_verified.equals("0")) {
                                           Intent itootp = new Intent(LoginActivity.this, AccountVerificationActivity.class);
                                           itootp.putExtra("mobile", mobile);
                                           itootp.putExtra("countryCode", country_code);
                                           itootp.putExtra("user_id", user_id);
                                           startActivity(itootp);
                                       } else if (initial_payment.equals("0")) {
                                           Toast.makeText(LoginActivity.this, "In order to validate your payment method, we will deduct $ 1 and this will be deposited into your application wallet account", Toast.LENGTH_SHORT).show();
                                           Intent iacc_page = new Intent(LoginActivity.this, PayPalActivity.class);
                                           iacc_page.putExtra("activity", "Login");
                                           iacc_page.putExtra("paymentAmount", "1");
                                           startActivity(iacc_page);
                                       } else {
                                           Intent imain = new Intent(LoginActivity.this, MainActivity.class);
                                           startActivity(imain);
                                       }


                                   } catch (UnsupportedEncodingException e) {
                                       e.printStackTrace();
                                   }
                               }
                               if (successResponceCode.equals("10200")) {
                                   Dialog.hideProgressBar();
                                   Toast.makeText(getApplicationContext(), "Invalid Input..!", Toast.LENGTH_SHORT).show();
                               }
                               if (successResponceCode.equals("10300")) {
                                   Dialog.hideProgressBar();
                                   Toast.makeText(getApplicationContext(), "User does not Exist..!", Toast.LENGTH_SHORT).show();
                               }
                               if (successResponceCode.equals("10400")) {
                                   Dialog.hideProgressBar();
                                   Toast.makeText(getApplicationContext(), "Account is Deactivated please contact to Admin..!", Toast.LENGTH_SHORT).show();
                               }
                               if (successResponceCode.equals("10500")) {
                                   Dialog.hideProgressBar();
                                   Toast.makeText(getApplicationContext(), "Account is Blocked please contact to Admin..!", Toast.LENGTH_SHORT).show();
                               }

                               if (successResponceCode.equals("10600")) {
                                   Dialog.hideProgressBar();
                                   Toast.makeText(getApplicationContext(), "Invalid Credentials, Pleae Try Again..!", Toast.LENGTH_SHORT).show();
                               }

                               if (successResponceCode.equals("10700")) {
                                   Dialog.hideProgressBar();
                                   Toast.makeText(getApplicationContext(), "It Seen you are login from Differnet Device!", Toast.LENGTH_SHORT).show();
                                   forceLogin();
                               }

                           } catch (JSONException e) {
                               e.printStackTrace();
                           }
                       }
                   }, new Response.ErrorListener() {
                       @Override
                       public void onErrorResponse(VolleyError error) {
                           Dialog.hideProgressBar();
                           error.getMessage();
                       }
                   }) {
                       @Override
                       protected Map<String, String> getParams()  {
                           Map<String, String> params = new HashMap<String, String>();
                           params.put("user_type", user_type);
                           params.put("username", send_emailORmobile);      // here if we login with mobile no. ,same field and variable use
                           params.put("password", send_pasword);
                           params.put("device_id", deviceId);
                           params.put("force_login", "1");
                           Log.d("LOGINPARAM", params.toString());
                           return params;// {force_login =0, email=9370744745, password=123456, country_code=+91, device_id=451e23be6ccc4b18, user_type=USER}
                       }
                   };
                   stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                   RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                   requestQueue.add(stringRequest);
               }
           });
           builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
               @Override
               public void onClick(DialogInterface dialog, int i)
               {
                   dialog.cancel();
               }
           });
           AlertDialog dialog = builder.create();
           dialog.show();
       }




    //Validation fields
    private boolean validate() {
        boolean result = true;
        char first = 0;
        String email = username_edt.getText().toString().trim();
        String password = password_edt.getText().toString().trim();
        if (email.length() == 0 && password.length() == 0) {
            Toast.makeText(this, "Enter Username and Password", Toast.LENGTH_SHORT).show();
            result = false;
        } else {
            String firstLetter = String.valueOf(email.charAt(0)); //line had 0 to 5 string index
            first = firstLetter.charAt(0);
            if (Character.isDigit(first)) {
                if (email.length() < 10) {
                    username_edt.setError("Please enter valid MobileNumber");
                    //Toast.makeText(this,"Please enter valid mobile no",Toast.LENGTH_SHORT).show();
                    result = false;
                } else if (password.isEmpty() || password.length() < 6) {
                    password_edt.setError("Minimum 6 characters required");
                    //Toast.makeText(this,"Minimum 6 characters required",Toast.LENGTH_SHORT).show();
                    result = false;
                }
            } else if (Character.isLetter(first)) {
                if (!email.matches(EMAIL_REGEX)) {
                    username_edt.setError("Please enter valid Email");
                    //Toast.makeText(this,"Please enter valid email",Toast.LENGTH_SHORT).show();
                    result = false;
                } else if (password.isEmpty() || password.length() < 6) {
                    password_edt.setError("Minimum 6 characters required");
                    //Toast.makeText(this,"Minimum 6 characters required",Toast.LENGTH_SHORT).show();
                    result = false;
                }
            }
        }

        return result;
    }
}
