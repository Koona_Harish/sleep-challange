package com.sleepchallenge.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.AddMemberGroupAdapter;
import com.sleepchallenge.adapters.GetMemGroupAdapter;
import com.sleepchallenge.models.AddMembersGroupModel;
import com.sleepchallenge.models.GetMemInGroupModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.ImagePermissions;
import com.sleepchallenge.utils.MyMultipartEntity;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CreateGroupActivity extends AppCompatActivity implements View.OnClickListener {

    UserSessionManager userSessionManager;
    Bitmap bitmap;
    String device_name, device_model, user_id, user_type, device_id, token, groupName;
    ImageView addMemButt;
    EditText groupNamET;
    SearchView searchV;
    TextView add_mem_but,txtv,addMemButt_text;
    Button createGrouButt, updateGrouButt;
    ImageView camera_click, close, group_user_image;
    Uri outputFileUri;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";
    String selectedImagePath = "";
    private boolean checkInternet;

    UserSessionManager session;
    RecyclerView memListRecycler;
    LinearLayoutManager lLayMan;
    AddMemberGroupAdapter addMemAdap;
    ArrayList<AddMembersGroupModel> addMemList = new ArrayList<AddMembersGroupModel>();
    RecyclerView getaddMemRecycler;
    LinearLayoutManager lLMan;
    GetMemGroupAdapter getMemAdap;
    ArrayList<GetMemInGroupModel> getMemList = new ArrayList<GetMemInGroupModel>();
    String selected_gmNames, selected_gmIds, update_key, update_group_id;
    HttpEntity resEntity;
    GridLayoutManager gridLayoutManagerVertical;
    Typeface regularFont,boldFont,thinFont,specialFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);

        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));

        device_name = android.os.Build.MANUFACTURER;
        device_model = android.os.Build.MODEL;
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        update_key = getIntent().getStringExtra("GROUPEDIT");
        update_group_id = getIntent().getStringExtra("GroupId");
        Log.d("DDDDDDDD", update_key + "///" + update_group_id);
        camera_click =  findViewById(R.id.camera_click);

        createGrouButt =  findViewById(R.id.createGrouButt);
        createGrouButt.setTypeface(specialFont);
        createGrouButt.setOnClickListener(this);
        updateGrouButt =  findViewById(R.id.updateGrouButt);
        updateGrouButt.setOnClickListener(this);
        groupNamET =  findViewById(R.id.groupNamET);

        addMemButt_text =  findViewById(R.id.addMemButt_text);
        addMemButt_text.setTypeface(boldFont);


        addMemButt =  findViewById(R.id.addMemButt);
        addMemButt.setOnClickListener(this);
        camera_click = findViewById(R.id.camera_click);
        camera_click.setOnClickListener(this);
        group_user_image = findViewById(R.id.group_user_image);
        close =  findViewById(R.id.close);
        close.setOnClickListener(this);


        getaddMemRecycler =  findViewById(R.id.getaddMemRecycler);
        getMemAdap = new GetMemGroupAdapter(getMemList, CreateGroupActivity.this, R.layout.row_get_mem_in_group);
        lLMan = new LinearLayoutManager(this);
        getaddMemRecycler.setNestedScrollingEnabled(false);
        getaddMemRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        StaggeredGridLayoutManager gridLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        gridLayoutManagerVertical = new GridLayoutManager(this, 3, LinearLayoutManager.VERTICAL, false);
        gridLayoutManager.setReverseLayout(true);
        getaddMemRecycler.setLayoutManager(gridLayoutManagerVertical);
    }


    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == camera_click) {
            selectOption();
        }
        if (view == addMemButt) {
            memListDialog();
        }
        if (view == createGrouButt) {
            if (getCreateGroupValidation())
            {
                if(selectedImagePath == null || selectedImagePath.equals("")||getMemList.isEmpty())
                {
                    Toast.makeText(CreateGroupActivity.this, "Please Fill All Details", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    getCreateGroup();
                }

            }
        }
    }

    private void getCreateGroup() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet)
        {


            String urllllll = AppUrls.BASE_URL + AppUrls.CREATE_GROUP;
            Log.d("GROUPCREATIONURL:", urllllll);
            Log.d("PATHHHHH", selectedImagePath);
            CreateGroupActivity.this.runOnUiThread(new Runnable() {
                public void run() {

                }
            });
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            new HttpUpload(this, selectedImagePath).execute();



        }
        else {

            Toast.makeText(CreateGroupActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void getMembersGroup() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.MEMBERS_ADD_GROUP + user_id + "&user_type=" + user_type,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("MEMADDGRO_URL", AppUrls.BASE_URL + AppUrls.MEMBERS_ADD_GROUP + user_id + "&user_type=" + user_type);
                            Log.d("RESP_MEMGRO", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {

                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jArr.length(); i++) {
                                        AddMembersGroupModel rhm = new AddMembersGroupModel();
                                        JSONObject itemArray = jArr.getJSONObject(i);
                                        rhm.setId(itemArray.getString("id"));
                                        rhm.setName(itemArray.getString("name"));
                                        addMemList.add(rhm);
                                    }
                                    memListRecycler.setAdapter(addMemAdap);
                                }
                                if (successResponceCode.equals("10200")) {

                                    Toast.makeText(CreateGroupActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {

                                    Toast.makeText(CreateGroupActivity.this, "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("MemGroupAdd_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(CreateGroupActivity.this);
            requestQueue.add(strRe);
        } else {

            Toast.makeText(CreateGroupActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void memListDialog() {
        addMemList.clear();
        final Dialog dialog = new Dialog(CreateGroupActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_member_list);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        add_mem_but =  dialog.findViewById(R.id.add_mem_but);
        txtv=dialog.findViewById(R.id.txtv);
        txtv.setTypeface(regularFont);
        searchV =  dialog.findViewById(R.id.searchV);
        EditText searchEditText = (EditText) searchV.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.white));
        searchEditText.setHintTextColor(getResources().getColor(R.color.white));
        memListRecycler =  dialog.findViewById(R.id.memListRecycler);
        addMemAdap = new AddMemberGroupAdapter(addMemList, CreateGroupActivity.this, R.layout.add_memebers_dialog_row);
        lLayMan = new LinearLayoutManager(this);
        memListRecycler.setNestedScrollingEnabled(false);
        memListRecycler.setLayoutManager(lLayMan);
        add_mem_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                //   Toast.makeText(CreateGroupActivity.this, "Members Added", Toast.LENGTH_SHORT).show();

            }

        });
        searchV.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                addMemAdap.getFilter().filter(query);
                return false;
            }
        });

        getMembersGroup();
        dialog.show();
    }


    public void setGmName(ArrayList<String> gm_ids, ArrayList<String> gm_names) {
        selected_gmIds = gm_ids.toString();
        getMemList.clear();
        for (int i = 0; i < gm_names.size(); i++) {
            GetMemInGroupModel getMemL = new GetMemInGroupModel();
            selected_gmNames = gm_names.get(i);
            getMemL.setName(selected_gmNames);
            getMemList.add(getMemL);
            Log.d("GetMemName", selected_gmNames);
            Log.d("GetMemId", gm_ids.get(i));
        }
        getaddMemRecycler.setAdapter(getMemAdap);
    }

    private boolean getCreateGroupValidation() {
        Boolean result = true;

        String name = groupNamET.getText().toString();
        if (name.equals("") || name.length() < 3) {
            groupNamET.setError("Enter Valid Name");
            result = false;
        } else if (!name.matches("^[\\p{L} .'-]+$")) {
            groupNamET.setError("Special characters not allowed");
            result = false;
        } else if (getMemList.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please add minimum group members", Toast.LENGTH_LONG).show();
            // groupNamET.setError("Please add minimum group members");
        } else {
            groupNamET.setError(null);
        }
        return result;
    }


    private void selectOption() {

        final Dialog dialog = new Dialog(CreateGroupActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.camera_dialog);

        TextView take_camera_pic =  dialog.findViewById(R.id.take_camera_pic);
        TextView take_gallery_pic =  dialog.findViewById(R.id.take_gallery_pic);
        TextView cancel =  dialog.findViewById(R.id.cancel);
        final boolean result = ImagePermissions.checkPermission(CreateGroupActivity.this);
        dialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        take_camera_pic.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

                if (result)

                    if (ContextCompat.checkSelfPermission(CreateGroupActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        if (getFromPref(CreateGroupActivity.this, ALLOW_KEY)) {
                            showSettingsAlert();
                        } else if (ContextCompat.checkSelfPermission(CreateGroupActivity.this,
                                android.Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {

                            // Should we show an explanation?
                            if (ActivityCompat.shouldShowRequestPermissionRationale(CreateGroupActivity.this,
                                    android.Manifest.permission.CAMERA)) {
                                showAlert();
                            } else {
                                // No explanation needed, we can request the permission.
                                ActivityCompat.requestPermissions(CreateGroupActivity.this,
                                        new String[]{android.Manifest.permission.CAMERA},
                                        0);
                            }
                        }
                    } else {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        ContentValues values = new ContentValues(1);
                        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
                        outputFileUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        startActivityForResult(intent, 0);
                    }
                dialog.cancel();

            }

        });
        take_gallery_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
                dialog.cancel();

            }

        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel(); // dismissing the popup
            }

        });
        dialog.show();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    Log.d("PPP", outputFileUri.toString());
                    if (outputFileUri != null) {
                        bitmap = decodeSampledBitmapFromUri(outputFileUri, group_user_image.getWidth(), group_user_image.getHeight());
                        if (bitmap == null) {
                            Toast.makeText(getApplicationContext(), "the image data could not be decoded" + outputFileUri.getPath(), Toast.LENGTH_LONG).show();
                        } else {
                            selectedImagePath = getRealPathFromURI(CreateGroupActivity.this, outputFileUri);// outputFileUri.getPath().
                            Log.d("CAMERASELECTPATH", selectedImagePath);
                            group_user_image.setImageBitmap(bitmap);
                        }
                    }
                }
                break;

            case 1:

                if (resultCode == RESULT_OK) {
                    Uri targetUri = data.getData();
                    Log.d("TGGGGG", targetUri.toString());
                    Bitmap bitmap;
                    bitmap = decodeSampledBitmapFromUri(targetUri, group_user_image.getWidth(), group_user_image.getHeight());
                    if (bitmap == null) {
                        Toast.makeText(getApplicationContext(), "the image data could not be decoded" + targetUri.getPath(), Toast.LENGTH_LONG).show();
                    } else {
                        selectedImagePath = getPath(targetUri);// targetUri.getPath();
                        Log.d("GALLRYSSSSSSSSS", selectedImagePath);
                        group_user_image.setImageBitmap(bitmap);
                    }
                }
                break;

            default:

                break;

        }

    }

    public String getPath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null,
                null);
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            return cursor.getString(columnIndex);
        }

        return uri.getPath();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            if ("content".equals(contentUri.getScheme()))
            {
                String[] proj = {MediaStore.Images.Media.DATA};
                cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } else {
                return contentUri.getPath();
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }

        }

    }

    public Bitmap decodeSampledBitmapFromUri(Uri uri, int reqWidth, int reqHeight)
    {
        Bitmap bm = null;
        String filePath =getPath(uri);
       BitmapFactory.Options options = new BitmapFactory.Options();
       options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
       float maxHeight = 516.0f;    //816 and 612
        float maxWidth = 412.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            bm = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(bm);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            bm = Bitmap.createBitmap(bm, 0, 0,
                    bm.getWidth(), bm.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = filePath;
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            bm.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return bm;

    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    private void showAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(CreateGroupActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(CreateGroupActivity.this,
                                new String[]{android.Manifest.permission.CAMERA},
                                0);
                    }
                });
        alertDialog.show();
    }

    public static void saveToPreferences(Context context, String key, Boolean allowed) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putBoolean(key, allowed);
        prefsEditor.commit();
    }

    public static Boolean getFromPref(Context context, String key) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        return (myPrefs.getBoolean(key, false));
    }

    private void showSettingsAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(CreateGroupActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startInstalledAppDetailsActivity(CreateGroupActivity.this);
                    }
                });

        alertDialog.show();
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    private String removeCharacter(String word) {
        String[] specialCharacters = {"-", " ", "(", ")", "[", "]", "}", "."};
        StringBuilder sb = new StringBuilder(word);
        for (int i = 0; i < sb.toString().length() - 1; i++) {
            for (String specialChar : specialCharacters) {
                if (sb.toString().contains(specialChar)) {
                    int index = sb.indexOf(specialChar);
                    sb.deleteCharAt(index);
                }
            }
        }
        return sb.toString();
    }

    private String convertToBase64(String imagePath) {
        Bitmap bm = BitmapFactory.decodeFile(imagePath);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] byteArrayImage = baos.toByteArray();
        String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
        return encodedImage;

    }


    public class HttpUpload extends AsyncTask<Void, Integer, Void> {

        private Context context;
        private String imgPath;
        private HttpClient client;
        private ProgressDialog pd;
        private long totalSize;
        public String url = AppUrls.BASE_URL + AppUrls.CREATE_GROUP;


        public HttpUpload(Context context, String imgPath) {
            super();
            this.context = context;
            this.imgPath = imgPath;
        }

        @Override
        protected void onPreExecute() {
            //Set timeout parameters
            int timeout = 10000;
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeout);
            HttpConnectionParams.setSoTimeout(httpParameters, timeout);
            //We'll use the DefaultHttpClient
            client = new DefaultHttpClient(httpParameters);


        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                //  File file = new File(imgPath);
                Bitmap bmp = BitmapFactory.decodeFile(imgPath);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.JPEG, 70, bos);
                InputStream in = new ByteArrayInputStream(bos.toByteArray());
                String val1 = selected_gmIds;
                String grp_name = groupNamET.getText().toString();
                Log.d("USERDETAIL", user_id + "//" + grp_name + "//" + removeCharacter(val1.toString()));
                //Create the POST object
                HttpPost post = new HttpPost(url);
                //Create the multipart entity object and add a progress listener
                //this is a our extended class so we can know the bytes that have been transfered
                MultipartEntity entity = new MyMultipartEntity(new MyMultipartEntity.ProgressListener() {
                    @Override
                    public void transferred(long num) {
                        //Call the onProgressUpdate method with the percent completed
                        publishProgress((int) ((num / (float) totalSize) * 100));
                        Log.d("DEBUG", num + " - " + totalSize);
                    }
                });
                //Add the file to the content's body
                String va = val1.toString();
                String strWithoutSpace1 = va.replaceAll(" ", "");
                int mem = strWithoutSpace1.lastIndexOf(",");
                if (mem > 0)
                    strWithoutSpace1 = new StringBuilder(strWithoutSpace1).replace(mem, mem + 1, "").toString();
                ContentBody cbFile = new InputStreamBody(in, "image/jpeg", "jpeg");
                //  ContentBody cbFile = new FileBody(file, "image/*");
                entity.addPart("file", cbFile);
                entity.addPart("group_name", new StringBody(grp_name));
                entity.addPart("user_id", new StringBody(user_id));
                entity.addPart("members", new StringBody(removeCharacter(val1.toString())));
                //After adding everything we get the content's lenght
                totalSize = entity.getContentLength();
                //We add the entity to the post request
                post.setEntity(entity);
                //Execute post request
                HttpResponse response = client.execute(post);
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == HttpStatus.SC_OK) {
                    //If everything goes ok, we can get the response
                    String fullRes = EntityUtils.toString(response.getEntity());
                    Log.d("DEBUG", fullRes);
                    finish();
                } else {
                    Log.d("DEBUG", "HTTP Fail, Response Code: " + statusCode);
                }

            } catch (ClientProtocolException e) {
                // Any error related to the Http Protocol (e.g. malformed url)
                e.printStackTrace();
            } catch (IOException e) {
                // Any IO error (e.g. File not found)
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            //Set the pertange done in the progress dialog
        }

        @Override
        protected void onPostExecute(Void result) {
            //Dismiss progress dialog
            Toast.makeText(context, "Group Created..", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(CreateGroupActivity.this,GroupsActivity.class);
            startActivity(intent);
            Log.d("resultsssssssssss", "" + result);
        }
    }

}

