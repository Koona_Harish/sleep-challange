package com.sleepchallenge.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.AllMembersAdapter;
import com.sleepchallenge.models.AllMemberModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.DividerItemDecorator;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AllMembersActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView back_img,notification;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    TextView title,count_sponsor_text, count_challenge_text, count_mesages_text;
    String user_id, user_type, token, device_ID;
    RecyclerView all_members_recyclerView;
    ArrayList<AllMemberModel> allMemberList;
    AllMembersAdapter allMemberAdapter;
    RecyclerView.LayoutManager layoutManager;
    Typeface thinFont,regularFont,boldFont;
    SearchView member_search;
    LinearLayout linear_sponsor, linear_chllenge, linear_messges;
    String moregroup="",groupId="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_members);

        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_ID = userDetails.get(UserSessionManager.DEVICE_ID);
        checkInternet = NetworkChecking.isConnected(this);
        back_img = findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
        notification = findViewById(R.id.notification);
        notification.setOnClickListener(this);
        title= findViewById(R.id.title);
        TextView title_text=findViewById(R.id.title_text);
        title_text.setTypeface(boldFont);
        title.setTypeface(regularFont);

        count_sponsor_text =  findViewById(R.id.count_sponsor_text);
        count_challenge_text =  findViewById(R.id.count_challenge_text);
        count_mesages_text =  findViewById(R.id.count_mesages_text);
        linear_sponsor =  findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge =  findViewById(R.id.linear_chllenge);
        linear_chllenge.setOnClickListener(this);
        linear_messges =  findViewById(R.id.linear_messges);
        linear_messges.setOnClickListener(this);
        moregroup=getIntent().getStringExtra("more");
        if(moregroup!=null && moregroup.length()>0){
            groupId=getIntent().getStringExtra("group_id");
        }
        member_search= findViewById(R.id.member_search);
        all_members_recyclerView =  findViewById(R.id.all_members_recyclerView);

        member_search.setOnClickListener(this);
        //View view = findViewById(R.id.custom_tab);
        member_search.setIconified(false);
        member_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                member_search.setIconified(false);
            }
        });
        member_search.clearFocus();
        member_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if(allMemberAdapter!=null)
                    allMemberAdapter.getFilter().filter(query);
                return false;
            }
        });


        getAllMembers();
        getCount();
    }

    private void getAllMembers() {
        if (checkInternet) {
            Dialog.showProgressBar(AllMembersActivity.this, "Loading");
            String url="";
            if(groupId!=null && groupId.length()>0){
                url = AppUrls.BASE_URL + AppUrls.GROUP_DETIL_MEMBER_MORE + "?user_id=" + user_id + "&group_id=" + groupId;
            }else{
                url = AppUrls.BASE_URL + AppUrls.ALL_MEMBERS_LIST + "?user_id=" + user_id + "&user_type=" + user_type;
            }


            Log.d("ALLMEMBERURL", url);

            StringRequest strAllMember = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Dialog.hideProgressBar();
                    Log.d("ALLMEMBERESP", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100"))
                        {
                            allMemberList = new ArrayList<>();
                            if(groupId!=null && groupId.length()>0){
                                JSONObject getData = jsonObject.getJSONObject("data");
                                JSONArray getMemData = getData.getJSONArray("members");
                                for (int i = 0; i < getMemData.length(); i++) {
                                    JSONObject jdataobj = getMemData.getJSONObject(i);
                                    AllMemberModel allmember = new AllMemberModel(jdataobj);
                                    allMemberList.add(allmember);
                                }
                            }else {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jdataobj = jsonArray.getJSONObject(i);
                                    AllMemberModel allmember = new AllMemberModel(jdataobj);
                                    allMemberList.add(allmember);
                                }
                            }

                            allMemberAdapter = new AllMembersAdapter(allMemberList, AllMembersActivity.this, R.layout.row_allmember,groupId);
                            layoutManager = new LinearLayoutManager(getApplicationContext());
                            all_members_recyclerView.setNestedScrollingEnabled(false);
                            all_members_recyclerView.setLayoutManager(layoutManager);
                            RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(AllMembersActivity.this, R.drawable.recycler_view_divider));
                            all_members_recyclerView.addItemDecoration(dividerItemDecoration);

                               all_members_recyclerView.setAdapter(allMemberAdapter);
                       }
                        if (successResponceCode.equals("10200"))
                        {
                            Toast.makeText(AllMembersActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                        }
                        if (successResponceCode.equals("10300"))
                        {
                            Toast.makeText(AllMembersActivity.this, "User Not Exist..!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Dialog.hideProgressBar();
                    Log.e("Error", error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strAllMember.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strAllMember);
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
   }

    @Override
    public void onClick(View view) {
        if (view == back_img) {
            finish();
        }if (view == notification) {
            Intent notif = new Intent(AllMembersActivity.this, NotificationActivity.class);
            startActivity(notif);
        }

        if (view == linear_sponsor) {
            Intent sponsor = new Intent(AllMembersActivity.this, SponserRequestActivity.class);
            startActivity(sponsor);
        }
        if (view == linear_chllenge) {
            Intent chellenge = new Intent(AllMembersActivity.this, ChallengeRequestActivity.class);
            startActivity(chellenge);
        }
        if (view == linear_messges) {
            Intent msg = new Intent(AllMembersActivity.this, ChatMessagesActivity.class);
            msg.putExtra("condition", "NORMAL");    //for switching tab order
            startActivity(msg);
        }
    }


    private void getCount() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("CountUrl", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("COUNTRESP:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);

                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(AllMembersActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }
}
