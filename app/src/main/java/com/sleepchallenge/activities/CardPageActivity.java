package com.sleepchallenge.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.clock.scratch.ScratchView;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CardPageActivity extends AppCompatActivity implements View.OnClickListener
{
    TextView prize_won_text,tell_to_button;
    Typeface specialFont,boldFont;
    ImageView close;
    private boolean checkInternet;
    String user_id,user_type,token,device_ID,scratch_amount,scratch_ID,ref_txn_id = "";
     UserSessionManager session;
    ScratchView scratch_view;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congratulations);
        boldFont=Typeface.createFromAsset(this.getAssets(),getResources().getString(R.string.boldFont));
        specialFont=Typeface.createFromAsset(this.getAssets(),getResources().getString(R.string.specialFont));
        session=new UserSessionManager(CardPageActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_ID = userDetails.get(UserSessionManager.DEVICE_ID);
        close=findViewById(R.id.close);
        close.setOnClickListener(this);
        Bundle bundle = getIntent().getExtras();
        scratch_amount = bundle.getString("AMOUNT");
        scratch_ID = bundle.getString("SCRATCH_ID");
        Log.d("DATAAA",scratch_amount+"///"+scratch_ID);
        checkInternet= NetworkChecking.isConnected(CardPageActivity.this);
        prize_won_text=findViewById(R.id.prize_won_text);
        prize_won_text.setTypeface(specialFont);
        tell_to_button=findViewById(R.id.tell_to_button);
        tell_to_button.setTypeface(boldFont);
        tell_to_button.setOnClickListener(this);
        scratch_view =  findViewById(R.id.scratch_view);

        scratch_view.setEraseStatusListener(new ScratchView.EraseStatusListener() {
            @Override
            public void onProgress(int percent) {
                if(scratch_amount!=null){
                    if (scratch_amount.equals("") || scratch_amount.equals("0")) {
                        prize_won_text.setText("Better luck  \n next time..!");
                    } else {
                        prize_won_text.setText("You've won" + "\n" + Html.fromHtml("&#36;" + "<b>" + scratch_amount + "</b>"));
                    }
                }else {
                    prize_won_text.setText("Testing");
                }

            }

            @Override
            public void onCompleted(View view) {
                scratch_view.clear();
                sendAfterScratch();
            }
        });

    }

    private void sendAfterScratch()
    {
        checkInternet = NetworkChecking.isConnected(CardPageActivity.this);
        if (checkInternet) {

            String afterScratcUrl=AppUrls.BASE_URL + AppUrls.SAFTER_CRATCH_CARD_DONE;
            Log.d("GETOTPURL:", afterScratcUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, afterScratcUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            Log.d("GETOTPRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    //  Toast.makeText(CardPageActivity.this, "Scratch card updated successfully..!", Toast.LENGTH_LONG).show();
                                    JSONObject data = jsonObject.getJSONObject("data");
                                    ref_txn_id = data.getString("ref_txn_id");
                                }
                                if (successResponceCode.equals("10200"))
                                {
                                    //   Toast.makeText(CardPageActivity.this, "Invalid input..!", Toast.LENGTH_LONG).show();
                                }
                                if (successResponceCode.equals("10300"))
                                {
                                    //   Toast.makeText(CardPageActivity.this, "Scratch Card not found..!", Toast.LENGTH_LONG).show();
                                }
                                if (successResponceCode.equals("10400"))
                                {
                                    //    Toast.makeText(CardPageActivity.this, "Scratch card is already scratched..!", Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams()  {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("user_type", user_type);
                    params.put("scratch_card_id", scratch_ID);
                    Log.d("GETOTPPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GETOTPEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(CardPageActivity.this);
            requestQueue.add(stringRequest);

        } else {

            Toast.makeText(CardPageActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View view) {

        if(view==close)
        {
            if (ref_txn_id.equals(""))
            {
               scratch_view.clear();
            }
            else
                {
                CardPageActivity.this.finish();
            }
        }

        if(view==tell_to_button)
        {

        }
    }


    @Override
    public void onBackPressed()
    {

        if (ref_txn_id.equals(""))
        {
            scratch_view.clear();
        } else {
            CardPageActivity.this.finish();
        }
        //  super.onBackPressed();
    }


}
