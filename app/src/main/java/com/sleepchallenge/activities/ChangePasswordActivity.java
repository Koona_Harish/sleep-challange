package com.sleepchallenge.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.NetworkChecking;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    EditText newpsw_edt, conifrmpassword_edt;
    Button submit_btn;
   Typeface  boldFont , specialFont;
    private boolean checkInternet;
    String user_Id,nonce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);


        Bundle bundle = getIntent().getExtras();
        user_Id = bundle.getString("USERID");
        nonce = bundle.getString("NONCE");
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));

        newpsw_edt = findViewById(R.id.newpsw_edt);
        conifrmpassword_edt = findViewById(R.id.conifrmpassword_edt);
        newpsw_edt.setTypeface(specialFont);
        conifrmpassword_edt.setTypeface(specialFont);
        submit_btn = findViewById(R.id.submit_btn);
        submit_btn.setOnClickListener(this);
        submit_btn.setTypeface(boldFont);
    }

    @Override
    public void onClick(View v) {
        if (v == submit_btn) {
            if (validate()) {
                checkInternet = NetworkChecking.isConnected(ChangePasswordActivity.this);
                if (checkInternet) {
                    final String newPassword = newpsw_edt.getText().toString().trim();
                    Log.d("CHAGNEURL", AppUrls.BASE_URL + AppUrls.CHANGEPASSWORD);

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.CHANGEPASSWORD,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response)
                                {
                                    Dialog.hideProgressBar();
                                    Log.d("CHANGEPASSRESP", response);
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String successResponceCode = jsonObject.getString("response_code");
                                        String message = jsonObject.getString("message");
                                        if (successResponceCode.equals("10100"))
                                        {
                                            Toast.makeText(getApplicationContext(), "Your password has been Changed...!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(ChangePasswordActivity.this, WelcomeActivity.class);
                                            startActivity(intent);


                                        }

                                        if (successResponceCode.equals("10200")) {
                                            Toast.makeText(getApplicationContext(), "Sorry, Try again...!", Toast.LENGTH_SHORT).show();
                                        }

                                        if (successResponceCode.equals("10300")) {
                                            Toast.makeText(getApplicationContext(), "Your new password and old password should not match..!", Toast.LENGTH_SHORT).show();
                                        }

                                        if (successResponceCode.equals("10400")) {

                                            Toast.makeText(getApplicationContext(), "Your old password is incorrect..!", Toast.LENGTH_SHORT).show();
                                        }

                                        if (successResponceCode.equals("11786")) {

                                            Toast.makeText(getApplicationContext(), "All fields are required.!", Toast.LENGTH_SHORT).show();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    error.getMessage();
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("nonce", nonce);
                            params.put("user_id", user_Id);
                            params.put("password", newPassword);
                            Log.d("CHAGNEPASS:", params.toString());
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders()  {
                            Map<String, String> headers = new HashMap<>();
                            //headers.put("Authorization-Basic", jwt_token);
                            return headers;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(this);
                    requestQueue.add(stringRequest);
                }
            }
        }
    }

    private boolean validate() {
        int flag = 0;
        boolean result = true;
        String newPassword = newpsw_edt.getText().toString().trim();
        if (newPassword.isEmpty() || newPassword.length() < 6) {
            Toast.makeText(this, "Minimum 6 characters required", Toast.LENGTH_SHORT).show();
            result = false;
        }

        String confrm_password = conifrmpassword_edt.getText().toString().trim();
        if (confrm_password.isEmpty() || confrm_password.length() < 6) {
            Toast.makeText(this, "Minimum 6 characters required", Toast.LENGTH_SHORT).show();
            result = false;
        }

        if (newPassword != "" && confrm_password != "" && !confrm_password.equals(newPassword) && flag == 0) {
            //password_edt.setError("Password and Confirm not Match");
           // Toast.makeText(this, "Password and Confirm Password not Match", Toast.LENGTH_SHORT).show();
            conifrmpassword_edt.setError("Password and Confirm Password not Match");
            result = false;
        } else {
            conifrmpassword_edt.setError(null);
        }
        return result;

    }
}
