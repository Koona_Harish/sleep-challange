package com.sleepchallenge.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.MyFollowersAdapter;
import com.sleepchallenge.models.AllMembersModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.DividerItemDecorator;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyFollowersActivity extends AppCompatActivity implements View.OnClickListener {

    Typeface regularFont, boldFont;
    ImageView close,default_img;
    UserSessionManager userSessionManager;
    TextView title_text;
    private boolean checkInternet;

    String token, user_id, user_type, device_id;
    RecyclerView myfollowers_recyclerview;
    MyFollowersAdapter myFollowersAdapter;
    ArrayList<AllMembersModel> myFollowersMoldelList = new ArrayList<AllMembersModel>();
    RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_followers);

        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));


        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        default_img = findViewById(R.id.default_img);
        title_text =  findViewById(R.id.title_text);
        title_text.setTypeface(boldFont);
        close =  findViewById(R.id.back_img);
        close.setOnClickListener(this);
        myfollowers_recyclerview =  findViewById(R.id.myfollowers_recyclerview);
        myFollowersAdapter = new MyFollowersAdapter(myFollowersMoldelList, MyFollowersActivity.this, R.layout.row_my_following);
        layoutManager = new LinearLayoutManager(this);
        myfollowers_recyclerview.setNestedScrollingEnabled(false);
        myfollowers_recyclerview.setLayoutManager(layoutManager);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(this, R.drawable.recycler_view_divider));
        myfollowers_recyclerview.addItemDecoration(dividerItemDecoration);
        getMyFollosers();
    }

    private void getMyFollosers() {
        checkInternet = NetworkChecking.isConnected(MyFollowersActivity.this);
        if (checkInternet) {
            String following_url = AppUrls.BASE_URL + AppUrls.MY_FOLLOWERS + "?user_id=" + user_id;
            Log.d("FOLLOWERSURL", following_url);
            StringRequest strFollowers = new StringRequest(Request.Method.GET, following_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("FOLLOWersMEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");
                        if (response_code.equals("10100")) {
                            JSONArray jsonArray = jobcode.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                AllMembersModel allmember = new AllMembersModel();
                                allmember.setId(jdataobj.getString("id"));
                                allmember.setUser_type(jdataobj.getString("user_type"));
                                allmember.setName(jdataobj.getString("name"));
                                allmember.setProfile_pic(AppUrls.BASE_IMAGE_URL + jdataobj.getString("profile_pic"));
                                allmember.setEmail(jdataobj.getString("email"));
                                allmember.setCountry_code(jdataobj.getString("country_code"));
                                allmember.setMobile(jdataobj.getString("mobile"));
                                allmember.setTotal_win(jdataobj.getString("total_win"));
                                allmember.setTotal_loss(jdataobj.getString("total_loss"));
                                allmember.setOverall_rank(jdataobj.getString("overall_rank"));
                                allmember.setWinning_per(jdataobj.getString("winning_per"));
                                String type=jdataobj.getString("user_type");
                                if(type.equalsIgnoreCase("Group")){
                                    allmember.setAdmin_id(jdataobj.getString("admin_id"));
                                }
                                myFollowersMoldelList.add(allmember);
                            }
                            myfollowers_recyclerview.setAdapter(myFollowersAdapter);
                        }
                        if (response_code.equals("10200")) {

                            myfollowers_recyclerview.setVisibility(View.GONE);
                            default_img.setVisibility(View.VISIBLE);
                            Toast.makeText(MyFollowersActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10300")) {

                            myfollowers_recyclerview.setVisibility(View.GONE);
                            default_img.setVisibility(View.VISIBLE);
                            Toast.makeText(MyFollowersActivity.this, "Users does not exist..!", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10400")) {

                            myfollowers_recyclerview.setVisibility(View.GONE);
                            default_img.setVisibility(View.VISIBLE);
                            Toast.makeText(MyFollowersActivity.this, "No Followers found.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.getMessage();
                }
            }) {
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(MyFollowersActivity.this);
            requestQueue.add(strFollowers);
        } else {
            Toast.makeText(MyFollowersActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
    }
}
