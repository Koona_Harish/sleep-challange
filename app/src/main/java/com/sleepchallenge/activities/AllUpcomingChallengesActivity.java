package com.sleepchallenge.activities;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.AllChallangUpcomingAdapter;
import com.sleepchallenge.models.AllChallengesStatusModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AllUpcomingChallengesActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    ProgressDialog progressDialog;
    String device_id, token, user_id;
    UserSessionManager session;
    RecyclerView recycler_allmore_upcoming;
    LinearLayoutManager layoutManager;
    ImageView close, refresh;
    private boolean userScrolled = true;

    RelativeLayout bottomLayout;
    String from_chalng_fragment;
    AllChallangUpcomingAdapter allChallangRunningAdapter;
    ArrayList<AllChallengesStatusModel> allchlngUpcoming = new ArrayList<AllChallengesStatusModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_upcoming_challenges);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        progressDialog = new ProgressDialog(AllUpcomingChallengesActivity.this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        from_chalng_fragment = getIntent().getExtras().getString("condition");

        bottomLayout =  findViewById(R.id.loadItemsLayout_recyclerView);
        recycler_allmore_upcoming =  findViewById(R.id.recycler_allmore_upcoming);
        refresh = findViewById(R.id.refresh);
        refresh.setOnClickListener(this);
        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        recycler_allmore_upcoming.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(AllUpcomingChallengesActivity.this);
        recycler_allmore_upcoming.setLayoutManager(layoutManager);
        allchlngUpcoming.clear();
        allChallangRunningAdapter = new AllChallangUpcomingAdapter(allchlngUpcoming, AllUpcomingChallengesActivity.this, R.layout.row_all_challenges_more);

        getAllUpcomChallangesMore(0);
    }

    public void getAllUpcomChallangesMore(int defaultPageNo) {

        checkInternet = NetworkChecking.isConnected(AllUpcomingChallengesActivity.this);
        if (checkInternet) {
            allchlngUpcoming.clear();
            String url = AppUrls.BASE_URL + AppUrls.MORECHALLENGES + "?user_id=" + user_id + "&status=UPCOMMING&page=" + defaultPageNo + "&type=" + from_chalng_fragment;
            Log.d("UPPPLISTURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("UPPPLISTRESP", response);
                                String responceCode = jsonObject.getString("response_code");
                                if (responceCode.equals("10100")) {

                                    progressDialog.dismiss();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    ContentValues values = new ContentValues();
                                    JSONArray jsonArray = jsonObject1.getJSONArray("challenges");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        AllChallengesStatusModel itemRunnAll = new AllChallengesStatusModel();
                                        itemRunnAll.setChallenge_id(jsonObject2.getString("challenge_id"));
                                        itemRunnAll.setUser_id(jsonObject2.getString("user_id"));
                                        itemRunnAll.setUser_type(jsonObject2.getString("user_type"));
                                        itemRunnAll.setOpponent_id(jsonObject2.getString("opponent_id"));
                                        itemRunnAll.setOpponent_type(jsonObject2.getString("opponent_type"));
                                        itemRunnAll.setRemainder(jsonObject2.getString("remainder"));
                                        itemRunnAll.setStart_time(jsonObject2.getString("start_time"));
                                        itemRunnAll.setChallenge_goal(jsonObject2.getString("challenge_goal"));
                                        itemRunnAll.setChallenger_group_id(jsonObject2.getString("challenger_group_id"));
                                        itemRunnAll.setPause_access(jsonObject2.getString("pause_access"));
                                        itemRunnAll.setIs_group_admin(jsonObject2.getString("is_group_admin"));
                                        itemRunnAll.setUser_name(jsonObject2.getString("user_name"));
                                        itemRunnAll.setOpponent_name(jsonObject2.getString("opponent_name"));
                                        itemRunnAll.setWinning_status(jsonObject2.getString("winning_status"));
                                        itemRunnAll.setWinning_amount(jsonObject2.getString("winning_amount"));
                                        itemRunnAll.setWinner(jsonObject2.getString("winner"));
                                        itemRunnAll.setStatus(jsonObject2.getString("status"));
                                        itemRunnAll.setAmount(jsonObject2.getString("amount"));
                                        itemRunnAll.setChallenge_type(jsonObject2.getString("challenge_type"));
                                        itemRunnAll.setStart_on(jsonObject2.getString("start_on"));
                                        itemRunnAll.setPaused_on(jsonObject2.getString("paused_on"));
                                        itemRunnAll.setPaused_by(jsonObject2.getString("paused_by"));
                                        itemRunnAll.setResume_on(jsonObject2.getString("resume_on"));
                                        itemRunnAll.setCompleted_on_txt(jsonObject2.getString("completed_on_txt"));
                                        itemRunnAll.setCompleted_on(jsonObject2.getString("completed_on"));
                                        itemRunnAll.setWinning_reward_type(jsonObject2.getString("winning_reward_type"));
                                        itemRunnAll.setWinning_reward_value(jsonObject2.getString("winning_reward_value"));
                                        itemRunnAll.setIs_scratched(jsonObject2.getString("is_scratched"));

                                        allchlngUpcoming.add(itemRunnAll);
                                    }
                                    recycler_allmore_upcoming.setAdapter(allChallangRunningAdapter);
                                    bottomLayout.setVisibility(View.GONE);
                                }

                                if (responceCode.equals("10200")) {
                                    recycler_allmore_upcoming.setVisibility(View.GONE);
                                    progressDialog.dismiss();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();
                    error.getMessage();
                }
            })
            {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(AllUpcomingChallengesActivity.this);
            requestQueue.add(stringRequest);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == refresh) {

        }
    }
}
