package com.sleepchallenge.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chaos.view.PinView;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.NetworkChecking;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgetPassOtpActivity extends AppCompatActivity implements View.OnClickListener
{
    TextView otp_txt;
    PinView pinView;
    private boolean checkInternet;
    String send_mobile,countryCodeAndroid,deviceId,user_Id;
    Typeface regularFont, boldFont, thinFont;
    Button verify_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass_otp);

        Bundle bundle = getIntent().getExtras();
        user_Id = bundle.getString("USERID");

        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));

        otp_txt = findViewById(R.id.otp_txt);
        otp_txt.setTypeface(boldFont);

        pinView = findViewById(R.id.pinView);
        pinView.setTypeface(thinFont);

        verify_btn = findViewById(R.id.verify_btn);
        verify_btn.setTypeface(regularFont);
        verify_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v)
    {
        if (v == verify_btn)
        {
             verifyOtp();
        }
    }

    public void verifyOtp()
    {
        checkInternet = NetworkChecking.isConnected(this);
        if (validate())
        {
            if (checkInternet) {
                Log.d("FORGETOTPURL", AppUrls.BASE_URL + AppUrls.VERIFY_FORGETPASSWORD_OTP);
                StringRequest strveriFyOtp = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.VERIFY_FORGETPASSWORD_OTP, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Dialog.hideProgressBar();
                        Log.d("FORGETOTPRESP", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String successResponceCode = jsonObject.getString("response_code");
                            if (successResponceCode.equals("10100")) {
                                Dialog.hideProgressBar();
                                Toast.makeText(ForgetPassOtpActivity.this, "OTP has been Verified", Toast.LENGTH_LONG).show();
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                String  nonce = jsonObject1.getString("nonce");

                                Intent i = new Intent(ForgetPassOtpActivity.this, ChangePasswordActivity.class);
                                    i.putExtra("NONCE",nonce);
                                    i.putExtra("USERID",user_Id);
                                    startActivity(i);

                            }
                            if (successResponceCode.equals("10200")) {
                                Dialog.hideProgressBar();
                                Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                            }

                            if (successResponceCode.equals("10300")) {
                                Dialog.hideProgressBar();
                                Toast.makeText(getApplicationContext(), "User Not Exist..!", Toast.LENGTH_SHORT).show();
                            }
                            if (successResponceCode.equals("10400")) {
                                Dialog.hideProgressBar();
                                Toast.makeText(getApplicationContext(), "Unable to verify OTP.!", Toast.LENGTH_SHORT).show();
                            }
                            if (successResponceCode.equals("10500")) {
                                Dialog.hideProgressBar();
                                Toast.makeText(getApplicationContext(), "Invlid OTP..!", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Dialog.hideProgressBar();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Dialog.hideProgressBar();
                        error.getMessage();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("otp", pinView.getText().toString());
                        params.put("user_id", user_Id);
                        Log.d("FORGETOTPPPARAM:", params.toString());
                        return params;
                    }
                };
                strveriFyOtp.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(ForgetPassOtpActivity.this);
                requestQueue.add(strveriFyOtp);
            } else {
                Toast.makeText(ForgetPassOtpActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
    }



    private boolean validate() {
        boolean result = true;
        String otp = pinView.getText().toString().trim();
        if ((otp == null || otp.equals("")) || otp.length() != 4)
        {
            pinView.setError("Invalid OTP");
            result = false;
        }
        return result;
    }

}
