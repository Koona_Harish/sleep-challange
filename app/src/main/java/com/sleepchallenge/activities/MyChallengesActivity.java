package com.sleepchallenge.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.MyChallengesViewPagerAdapter;
import com.sleepchallenge.fragments.MyChallengesGroupFragment;
import com.sleepchallenge.fragments.MyChallengesIndividualFragment;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.ChallengeIsAvailable;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MyChallengesActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close,notification;
    ViewPager view_pager_my_chalng;
    TabLayout tab;
    private boolean checkInternet;
    MyChallengesViewPagerAdapter myChallengeadapter;
    UserSessionManager userSessionManager;
    String token, user_id, user_type, device_id;
    Typeface regularFont, boldFont, thinFont;
    TextView sponsor_text, challenge_text, mesages_text,count_sponsor_text, count_challenge_text, count_mesages_text;
    LinearLayout linear_sponsor, linear_chllenge, linear_messges;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_challenges);

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        notification = findViewById(R.id.notification);
        notification.setOnClickListener(this);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));
        TextView title = findViewById(R.id.title_text);
        title.setTypeface(boldFont);
        tab =findViewById(R.id.tabLayout_mychalleng);
        view_pager_my_chalng = findViewById(R.id.view_pager_my_chalng);
        setupViewPager(view_pager_my_chalng);
        tab.setupWithViewPager(view_pager_my_chalng);
        sponsor_text = findViewById(R.id.sponsor_text);
        challenge_text = findViewById(R.id.challenge_text);
        mesages_text =findViewById(R.id.mesages_text);

        count_sponsor_text = findViewById(R.id.count_sponsor_text);
        count_challenge_text =findViewById(R.id.count_challenge_text);
        count_mesages_text = findViewById(R.id.count_mesages_text);

        linear_sponsor =findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge =findViewById(R.id.linear_chllenge);
        linear_chllenge.setOnClickListener(this);
        linear_messges = findViewById(R.id.linear_messges);
        linear_messges.setOnClickListener(this);

        getCount();

    }

    private void setupViewPager(ViewPager view_pager_suggest) {
        myChallengeadapter = new MyChallengesViewPagerAdapter(getSupportFragmentManager());
        myChallengeadapter.addFrag(new MyChallengesIndividualFragment(), "Individual");
        myChallengeadapter.addFrag(new MyChallengesGroupFragment(), "Group");
        view_pager_suggest.setAdapter(myChallengeadapter);
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == notification)
        {
            Intent notif = new Intent(MyChallengesActivity.this, NotificationActivity.class);
            startActivity(notif);
        }
        if (view == linear_sponsor) {
            Intent sponsor = new Intent(this, SponserRequestActivity.class);
            startActivity(sponsor);

        }
        if (view == linear_chllenge) {
            Intent chellenge = new Intent(this, ChallengeRequestActivity.class);
            startActivity(chellenge);

        }
        if (view == linear_messges) {
            Intent msg = new Intent(MyChallengesActivity.this, ChatMessagesActivity.class);
            msg.putExtra("condition", "NORMAL");    //for switching tab order
            startActivity(msg);

        }
    }

    private void getCount() {
        checkInternet = NetworkChecking.isConnected(MyChallengesActivity.this);
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("COUNT_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {

                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);

                                }
                                if (status.equals("10200")) {


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            error.getMessage();
                        }
                    }) {
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {

                Toast.makeText(MyChallengesActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

}