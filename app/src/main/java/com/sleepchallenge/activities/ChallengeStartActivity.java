package com.sleepchallenge.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.BackgroundStatus;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;
import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ChallengeStartActivity extends AppCompatActivity {
    private boolean checkInternet;
   Typeface regularFont, boldFont, thinFont,specialFont;
    UserSessionManager userSessionManager;
    String deviceId, user_type, user_id, token;
    ImageView back_img;
    RotateLoading rotateLoading;
    CircularImageView userprofile_pic,opponentprofile_pic;
    TextView username_text,opponent_text,start_time_lbl,start_time_txt,endtime_lbl,endtime_text,buffertime_lbl,buffertime_text,
              goal_lbl,goal_text,amount_lbl,amount_text,processing_text,note_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_start);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));
        TextView title = findViewById(R.id.title_text);
        title.setTypeface(boldFont);
        initilizeUI();
        SharedPreferences preferences = getSharedPreferences("Timings", MODE_PRIVATE);
        String challengeid = preferences.getString("challengeid", "");
        challengeDetails(challengeid);

    }
    private void initilizeUI(){
        username_text=findViewById(R.id.username_text);
        opponent_text=findViewById(R.id.opponent_text);
        start_time_lbl=findViewById(R.id.start_time_lbl);
        start_time_txt=findViewById(R.id.start_time_txt);
        endtime_lbl=findViewById(R.id.endtime_lbl);
        endtime_text=findViewById(R.id.endtime_text);
        buffertime_lbl=findViewById(R.id.buffertime_lbl);
        buffertime_text=findViewById(R.id.buffertime_text);
        goal_lbl=findViewById(R.id.goal_lbl);
        goal_text=findViewById(R.id.goal_text);
        amount_lbl=findViewById(R.id.amount_lbl);
        amount_text=findViewById(R.id.amount_text);
        processing_text=findViewById(R.id.processing_text);
        note_text=findViewById(R.id.note_text);
        userprofile_pic=findViewById(R.id.userprofile_pic);
        opponentprofile_pic=findViewById(R.id.opponentprofile_pic);
        rotateLoading = findViewById(R.id.rotateloading);
        username_text.setTypeface(regularFont);
        opponent_text.setTypeface(regularFont);
        start_time_lbl.setTypeface(regularFont);
        start_time_txt.setTypeface(regularFont);
        endtime_lbl.setTypeface(regularFont);
        endtime_text.setTypeface(regularFont);
        buffertime_lbl.setTypeface(regularFont);
        buffertime_text.setTypeface(regularFont);
        goal_text.setTypeface(regularFont);
        amount_lbl.setTypeface(regularFont);
        amount_text.setTypeface(regularFont);
        processing_text.setTypeface(specialFont);
        note_text.setTypeface(boldFont);


    }

    private void challengeDetails(String id){
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Dialog.showProgressBar(ChallengeStartActivity.this, "Loading...");
            String url = AppUrls.BASE_URL + AppUrls.CHALLENGE_STATUS +id+ "&user_id=" + user_id;
            Log.d("STATUSURL", url);
            StringRequest statusstr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Dialog.hideProgressBar();
                    Log.d("STATUSRES", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100"))
                        {
                            JSONObject responseObj = jsonObject.optJSONObject("data");

                            JSONObject challengeobj=responseObj.optJSONObject("challenge");
                              rotateLoading.start();
                             String completed_time=parseTime(challengeobj.optString("completed_on"));
                            String start_time=parseTime(challengeobj.optString("start_on"));
                            start_time_txt.setText(start_time);
                            endtime_text.setText(completed_time);
                            String challengegoal=challengeobj.optString("challenge_goal");
                            goal_text.setText(challengegoal+" Hours");
                            String user_name= (String) challengeobj.get("user_name");
                            username_text.setText(user_name);
                            amount_text.setText("$"+challengeobj.get("amount"));
                            Type stringStringMap = new TypeToken<Map<String,Object>>() {
                            }.getType();
                            Map<String,Object> tmpObj= new Gson().fromJson(responseObj.optString("challenge"), stringStringMap);
                            String opponent="";
                            if(tmpObj.containsKey("opponent_name")){
                                opponent= (String) tmpObj.get("opponent_name");
                            }else {
                                opponent="ADMIN";
                            }
                            if(tmpObj.containsKey("challenger_profile_pic")){
                                String userpic = AppUrls.BASE_IMAGE_URL + (String) tmpObj.get("challenger_profile_pic");
                               Picasso.with(ChallengeStartActivity.this).load(userpic).into(userprofile_pic);
                            }
                            if(tmpObj.containsKey("opponent_profile_pic")){
                                String opponentpic = (String) tmpObj.get("opponent_profile_pic");
                                if(opponentpic.length()>0){
                                    String pic = AppUrls.BASE_IMAGE_URL + (String) tmpObj.get("opponent_profile_pic");
                                    Picasso.with(ChallengeStartActivity.this).load(pic).into(opponentprofile_pic);
                                }else {
                                    opponentprofile_pic.setImageResource(R.drawable.profile_img);
                                }

                            }
                            opponent_text.setText(opponent);
                            String timesplit[]=start_time.split(":");
                            String split[]  =timesplit[1].split(" ");
                            showsdetoxformat(Integer.parseInt(timesplit[0]),Integer.parseInt(split[0]));
                            startService(new Intent(ChallengeStartActivity.this, BackgroundStatus.class));
                        }
                        if (successResponceCode.equals("10200"))
                        {
                            Toast.makeText(ChallengeStartActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Dialog.hideProgressBar();
                    Log.e("Error", error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            statusstr.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(statusstr);
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public String parseTime(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void showsdetoxformat(int hour, int min) {

        String format = "";
        int start = hour - 2;
        if (start == 0) {

            start += 12;

            format = "AM";
        } else if (start == 12) {

            format = "PM";

        } else if (start > 12) {

            start -= 12;

            format = "PM";

        } else if (start < 0) {

            start += 12;

            format = "AM";

        } else {

            format = "PM";
        }
        String mins;
        if (min < 10) {
            mins = "0" + min;
        } else {
            mins = String.valueOf(min);
        }
        buffertime_text.setText(start + ":" + mins + format);


    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it=new Intent(ChallengeStartActivity.this,MainActivity.class);
        startActivity(it);
    }
}
