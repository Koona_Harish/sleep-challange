package com.sleepchallenge.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.SleepChllengesViewPagerAdapter;
import com.sleepchallenge.fragments.AllGroupsFragment;
import com.sleepchallenge.fragments.MyGroupsFragment;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.MovableFloatingActionButton;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class GroupsActivity extends AppCompatActivity implements View.OnClickListener
{
    ViewPager view_pager_group;
    TabLayout tab;
    ImageView back_img;
    TextView count_sponsor_text, count_challenge_text, count_mesages_text,add_group_text;
    SleepChllengesViewPagerAdapter group_adapter;
    LinearLayout linear_sponsor, linear_chllenge, linear_messges;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String user_id, user_type, token, device_ID;
    Typeface specialFont,boldFont;
    MovableFloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_ID = userDetails.get(UserSessionManager.DEVICE_ID);

        back_img =  findViewById(R.id.back_img);
        back_img.setOnClickListener(this);

        TextView title_text=findViewById(R.id.title_text);
        title_text.setTypeface(boldFont);
        fab = findViewById(R.id.fab);
        Typeface regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        add_group_text=findViewById(R.id.addgroup_txt);
        add_group_text.setTypeface(regularFont);
        CoordinatorLayout.LayoutParams lp  = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
        fab.setCoordinatorLayout(lp);
        fab.setOnClickListener(this);
        tab =  findViewById(R.id.tabLayout_group);
        view_pager_group =  findViewById(R.id.view_pager_group);
        setupViewPager(view_pager_group);
        tab.setupWithViewPager(view_pager_group);

        count_sponsor_text =  findViewById(R.id.count_sponsor_text);
        count_challenge_text = findViewById(R.id.count_challenge_text);
        count_mesages_text =  findViewById(R.id.count_mesages_text);

        linear_sponsor = findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge = findViewById(R.id.linear_chllenge);
        linear_chllenge.setOnClickListener(this);
        linear_messges =  findViewById(R.id.linear_messges);
        linear_messges.setOnClickListener(this);
        getCount();


    }

    private void setupViewPager(ViewPager view_pager_suggest)
    {
        group_adapter = new SleepChllengesViewPagerAdapter(getSupportFragmentManager());

        group_adapter.addFrag(new MyGroupsFragment(), "My Groups");
        group_adapter.addFrag(new AllGroupsFragment(), "All Groups");


        view_pager_suggest.setAdapter(group_adapter);

        tab.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(view_pager_group) {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab)
                    {
                        super.onTabSelected(tab);
                        if (tab.getPosition() == 1) {
                            fab.setVisibility(View.GONE);
                            add_group_text.setVisibility(View.GONE);
                          
                        }
                        else if(tab.getPosition() == 0)
                        {
                            fab.setVisibility(View.VISIBLE);
                            add_group_text.setVisibility(View.VISIBLE);
                        }


                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }

                }
        );
    }
    @Override
    public void onClick(View view)
    {

        if(view==back_img)
        {
            finish();
        }
        if(view==fab) {
            Intent createGroup=new Intent(GroupsActivity.this,CreateGroupActivity.class);
            startActivity(createGroup);
        }
        if (view == linear_sponsor) {
            Intent sponsor = new Intent(GroupsActivity.this, SponserRequestActivity.class);
            startActivity(sponsor);
        }
        if (view == linear_chllenge) {
            Intent chellenge = new Intent(GroupsActivity.this, ChallengeRequestActivity.class);
            startActivity(chellenge);
        }
        if (view == linear_messges) {
            Intent msg = new Intent(GroupsActivity.this, ChatMessagesActivity.class);
            msg.putExtra("condition", "NORMAL");    //for switching tab order
            startActivity(msg);
        }
    }

    private void getCount() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("CountUrl", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("COUNTRESP:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    count_mesages_text.setText(messages);
                                    Log.d("COUN",challenges_request+"//"+sponsor_request+"//"+messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);

                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(GroupsActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }
}
