package com.sleepchallenge.activities;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.ChallengeToFriendAdapter;
import com.sleepchallenge.models.ChallengeToFriendModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ChallengeToFriendActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close, no_data_image, voice_search;
    TextView count_sponsor_text, count_challenge_text, count_mesages_text;
    Typeface regularFont, boldFont, thinFont, specialFont;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token, user_id, user_type, device_id;
    SearchView members_search;
    RecyclerView recycler_all_members;
    ChallengeToFriendAdapter allMembersAdapter;
    ArrayList<ChallengeToFriendModel> allmemMoldelList = new ArrayList<ChallengeToFriendModel>();
    RecyclerView.LayoutManager layoutManager;
    SearchView activity_search;
    EditText searchEditText;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    String challengedate="",noofdays="",hours="",timestr="",challengetype="",remainderstr="";
    LinearLayout linear_sponsor, linear_chllenge, linear_messges;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_to_friend);

        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        challengedate=getIntent().getStringExtra("challengedate");
        hours=getIntent().getStringExtra("hours");
        timestr=getIntent().getStringExtra("time");
        challengetype=getIntent().getStringExtra("challengetype");
        remainderstr=getIntent().getStringExtra("remainder");
           if(challengetype.equalsIgnoreCase("Weekly"))
            {
                noofdays=getIntent().getStringExtra("noofdays");
            }

            Log.v("Noof days","/////"+challengetype+"daysstr"+noofdays);

        activity_search = findViewById(R.id.members_search);
        activity_search.setFocusable(false);
        activity_search.setIconified(false);
        activity_search.setIconifiedByDefault(false);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        searchEditText = activity_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setHintTextColor(getResources().getColor(R.color.gray));
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        ImageView voiceIcon =  activity_search.findViewById(android.support.v7.appcompat.R.id.search_voice_btn);
        voiceIcon.setImageResource(R.drawable.voice_icon);
        pprogressDialog = new ProgressDialog(ChallengeToFriendActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        voice_search = findViewById(R.id.voice_search);
        no_data_image =  findViewById(R.id.no_data_image);
        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        linear_sponsor = findViewById(R.id.linear_sponsor);
        linear_sponsor.setOnClickListener(this);
        linear_chllenge = findViewById(R.id.linear_chllenge);
        linear_chllenge.setOnClickListener(this);
        linear_messges = findViewById(R.id.linear_messges);
        View view = findViewById(R.id.custom_tab);
        linear_messges.setOnClickListener(this);
        count_sponsor_text =  findViewById(R.id.count_sponsor_text);
        count_challenge_text =  findViewById(R.id.count_challenge_text);
        count_mesages_text =  findViewById(R.id.count_mesages_text);

        members_search =  findViewById(R.id.members_search);
        members_search.setOnClickListener(this);
        members_search.setIconified(false);
        members_search.clearFocus();
        members_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                members_search.setIconified(false);
            }
        });

        members_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                allMembersAdapter.getFilter().filter(query);
                return false;
            }
        });
        voice_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                        "Search Activities");
                try {
                    startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
                } catch (ActivityNotFoundException a) {
                    Toast.makeText(getApplicationContext(),
                            "Search not Supported",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
        recycler_all_members = (RecyclerView) findViewById(R.id.recycler_all_members);
        allMembersAdapter = new ChallengeToFriendAdapter(allmemMoldelList, ChallengeToFriendActivity.this, R.layout.row_allmember,challengedate,challengetype,hours,timestr,noofdays,remainderstr);

        layoutManager = new LinearLayoutManager(this);
        recycler_all_members.setNestedScrollingEnabled(false);
        recycler_all_members.setLayoutManager(layoutManager);
        getAllmembers();
        getCount();
    }

    private void getAllmembers() {
        allmemMoldelList.clear();
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url_all_members = AppUrls.BASE_URL + AppUrls.CHALLENGE_TO_USER + user_id + AppUrls.USER_TYPE + user_type;
            Log.d("MEMBERSURL", url_all_members);
            StringRequest req_members = new StringRequest(Request.Method.GET, url_all_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.dismiss();
                    Log.d("MEMRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {
                            JSONArray jsonArray = jobcode.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                ChallengeToFriendModel allmember = new ChallengeToFriendModel();
                                allmember.setId(jdataobj.getString("id"));
                                allmember.setUser_type(jdataobj.getString("user_type"));
                                allmember.setName(jdataobj.getString("name"));
                                allmember.setProfile_pic(AppUrls.BASE_IMAGE_URL + jdataobj.getString("profile_pic"));
                                allmember.setEmail(jdataobj.getString("email"));
                                allmember.setCountry_code(jdataobj.getString("country_code"));
                                allmember.setMobile(jdataobj.getString("mobile"));
                                allmember.setTotal_win(jdataobj.getString("total_win"));
                                allmember.setTotal_loss(jdataobj.getString("total_loss"));
                                allmember.setOverall_rank(jdataobj.getString("overall_rank"));
                                allmember.setWinning_per(jdataobj.getString("winning_per"));
                                allmember.setAlready_applied(jdataobj.getString("already_applied"));

                                allmemMoldelList.add(allmember);
                            }
                            recycler_all_members.setAdapter(allMembersAdapter);

                        }
                        if (response_code.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(ChallengeToFriendActivity.this, "Invalid input.!", Toast.LENGTH_LONG).show();

                        }
                        if (response_code.equals("10300")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(ChallengeToFriendActivity.this, "Users does not exist..!", Toast.LENGTH_LONG).show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();
                    error.getMessage();
                }
            }) {

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(ChallengeToFriendActivity.this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(ChallengeToFriendActivity.this, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            finish();
        }
        if (view == linear_chllenge) {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
                Intent sponsor = new Intent(ChallengeToFriendActivity.this, ChallengeRequestActivity.class);
                startActivity(sponsor);

            } else {
                // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }

        if (view == linear_messges) {

        }
        if (view == linear_sponsor) {

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    searchEditText.setText("" + result.get(0));
                    allMembersAdapter.getFilter().filter("" + result.get(0));
                }
                break;
            }

        }
    }

    private void getCount() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            String url = AppUrls.BASE_URL + AppUrls.COUNTS + user_id;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("ACTIVITY_RESPONSE:", response);

                                String status = jsonObject.getString("response_code");
                                if (status.equals("10100")) {
                                    JSONObject jsonArray = jsonObject.getJSONObject("data");
                                    String messages = jsonArray.getString("messages");
                                    String challenges_request = jsonArray.getString("challenges_request");
                                    String sponsor_request = jsonArray.getString("sponsor_request");
                                    count_mesages_text.setText(messages);
                                    count_challenge_text.setText(challenges_request);
                                    count_sponsor_text.setText(sponsor_request);

                                }
                                if (status.equals("10200")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(ChallengeToFriendActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }
}
