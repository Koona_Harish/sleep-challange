package com.sleepchallenge.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sleepchallenge.R;
import com.sleepchallenge.adapters.SleepChllengesViewPagerAdapter;
import com.sleepchallenge.fragments.ContactusFragment;
import com.sleepchallenge.fragments.FeedBackFragment;
import com.sleepchallenge.utils.MovableFloatingActionButton;
import com.sleepchallenge.utils.NetworkChecking;

public class FeedbackContactUsActivity extends AppCompatActivity implements  View.OnClickListener{

    private boolean checkInternet;
    ViewPager view_pager_feedback_contact;
    TabLayout tab;
    ImageView back_img;
    TextView report_history_text;
    Typeface regularFont,boldFont;
    SleepChllengesViewPagerAdapter feedback_contact_adapter;
    MovableFloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_contact_us);
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        report_history_text =  findViewById(R.id.report_history_text);
        report_history_text.setTypeface(regularFont);

        tab =  findViewById(R.id.tabLayout_feedback_contact);
        fab = findViewById(R.id.fab);
        CoordinatorLayout.LayoutParams lp  = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
        fab.setCoordinatorLayout(lp);
        fab.setOnClickListener(this);
        TextView title_txt=findViewById(R.id.title_text);
        title_txt.setTypeface(boldFont);
        view_pager_feedback_contact =  findViewById(R.id.view_pager_feedback_contact);
        setupViewPager(view_pager_feedback_contact);
        tab.setupWithViewPager(view_pager_feedback_contact);
        back_img =  findViewById(R.id.back_img);
        back_img.setOnClickListener(this);
    }

    private void setupViewPager(ViewPager view_pager_suggest)
    {
        feedback_contact_adapter = new SleepChllengesViewPagerAdapter(getSupportFragmentManager());

        feedback_contact_adapter.addFrag(new FeedBackFragment(), "FeedBack");
        feedback_contact_adapter.addFrag(new ContactusFragment(), "Contact Us");


        view_pager_suggest.setAdapter(feedback_contact_adapter);

        tab.setOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(view_pager_feedback_contact) {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab)
                    {
                        super.onTabSelected(tab);
                        if (tab.getPosition() == 1) {
                            fab.setVisibility(View.GONE);
                            report_history_text.setVisibility(View.GONE);
                        }
                        else if(tab.getPosition() == 0)
                        {
                            fab.setVisibility(View.VISIBLE);
                            report_history_text.setVisibility(View.VISIBLE);
                        }

                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        super.onTabUnselected(tab);
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {
                        super.onTabReselected(tab);
                    }

                }
        );
    }

    @Override
    public void onClick(View view)
    {
        if(view==back_img)
        {finish();}

        if(view==fab)
        {
            checkInternet = NetworkChecking.isConnected(getApplicationContext());
            if (checkInternet) {
                Intent i = new Intent(FeedbackContactUsActivity.this, ReportHistory.class);
                startActivity(i);

            } else {
                Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
    }
}
