package com.sleepchallenge.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hbb20.CountryCodePicker;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.GPSTracker;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    ImageView cal_img;
    EditText fname_edt, lname_edt, mobile_edt, email_edt, password_edt;
    Button register_btn;
    CheckBox t_c_chk, eula_chk;
    TextView male_txt, dob_txt, female_txt, other_txt, t_c_textview, eula_textview;
    Typeface regularFont, boldFont, thinFont, specialFont;
    String countryCodeAndroid = "91", deviceId, sendUserType, login_type, send_fb_user_data, send_fb_name, send_fb_lastname, send_fb_email, send_fb_provider, send_google_name, send_google_lastname, send_google_email, send_google_id, send_google_provider;
    String send_name, send_lastname, send_email, send_pasword, send_mobile, send_dob, send_gender = "Male";
    CountryCodePicker ccp;
    String EMAIL_REG = "\"^[\\\\w_\\\\.+]*[\\\\w_\\\\.]\\\\@([\\\\w]+\\\\.)+[\\\\w]+[\\\\w]$\";";
    public int mYear, mMonth, mDay;
    UserSessionManager userSessionManager;
    GPSTracker gps;
    Geocoder geocoder;
    List<Address> addresses;
    String errorResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);

        Bundle bundle = getIntent().getExtras();
        sendUserType = bundle.getString("USERTYPE");
        login_type = bundle.getString("LOGIN");
        Log.d("LOGINTYPE", login_type + "///" + sendUserType + "///" + deviceId);
        checkInternet = NetworkChecking.isConnected(RegistrationActivity.this);
        gps=new GPSTracker(this);
        geocoder = new Geocoder(this, Locale.getDefault());
        regularFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.specialFont));

        ccp = findViewById(R.id.ccp);
        ccp.setTypeFace(thinFont);
        // ccp.setContentColor(Color.parseColor("#000000"));
        cal_img = findViewById(R.id.cal_img);
        cal_img.setOnClickListener(this);

        fname_edt = findViewById(R.id.fname_edt);
        fname_edt.setTypeface(specialFont);
        lname_edt = findViewById(R.id.lname_edt);
        lname_edt.setTypeface(specialFont);
        dob_txt = findViewById(R.id.dob_txt);
        dob_txt.setTypeface(thinFont);
        mobile_edt = findViewById(R.id.mobile_edt);
        mobile_edt.setTypeface(thinFont);
        email_edt = findViewById(R.id.email_edt);
        email_edt.setTypeface(specialFont);
        password_edt = findViewById(R.id.password_edt);
        password_edt.setTypeface(specialFont);

        t_c_chk = findViewById(R.id.t_c_chk);
        t_c_chk.setTypeface(thinFont);
        eula_chk = findViewById(R.id.eula_chk);
        eula_chk.setTypeface(thinFont);

        male_txt = findViewById(R.id.male_txt);
        male_txt.setTypeface(boldFont);
        male_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                other_txt.setTextColor(Color.parseColor("#B0B0B0"));
                other_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
                female_txt.setTextColor(Color.parseColor("#B0B0B0"));
                female_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
                male_txt.setTextColor(Color.parseColor("#00ABF7"));
                male_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
                send_gender = "Male";
            }
        });
        female_txt = findViewById(R.id.female_txt);
        female_txt.setTypeface(boldFont);
        female_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                male_txt.setTextColor(Color.parseColor("#B0B0B0"));
                male_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
                other_txt.setTextColor(Color.parseColor("#B0B0B0"));
                other_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
                female_txt.setTextColor(Color.parseColor("#FF1493"));
                female_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
                send_gender = "Female";
            }
        });

        other_txt = findViewById(R.id.other_txt);
        other_txt.setTypeface(boldFont);
        other_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                male_txt.setTextColor(Color.parseColor("#B0B0B0"));
                male_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
                female_txt.setTextColor(Color.parseColor("#B0B0B0"));
                female_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
                other_txt.setTextColor(Color.parseColor("#FFFF00"));
                other_txt.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
                send_gender = "Other";
            }
        });

        t_c_textview = findViewById(R.id.t_c_textview);
        t_c_textview.setTypeface(thinFont);
        eula_textview = findViewById(R.id.eula_textview);
        eula_textview.setTypeface(thinFont);

        register_btn = findViewById(R.id.register_btn);
        register_btn.setTypeface(regularFont);
        register_btn.setOnClickListener(this);
        ccp.clearFocus();

        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                countryCodeAndroid = ccp.getSelectedCountryCode();

                Log.d("CountryCode", countryCodeAndroid);
            }
        });

        if (login_type.equals("Facebook")) {
            Bundle bundle_fb = getIntent().getExtras();

            send_fb_name = bundle_fb.getString("name");
            send_fb_lastname = bundle_fb.getString("lastname");
            send_fb_email = bundle_fb.getString("email");
            send_fb_user_data = bundle_fb.getString("user_fb_gmail_id");
            send_fb_provider = bundle_fb.getString("provider");
            if (!send_fb_email.equals(EMAIL_REG)) {
                email_edt.setFocusable(true);
            }
            fname_edt.setText(send_fb_name);
            fname_edt.setFocusable(false);
            lname_edt.setText(send_fb_lastname);
            lname_edt.setFocusable(false);
            email_edt.setText(send_fb_email);
            email_edt.setFocusable(false);

        }

        if (login_type.equals("Google")) {
            Bundle bundle_google = getIntent().getExtras();
            send_google_name = bundle_google.getString("name");
            send_google_lastname = bundle_google.getString("lastname");
            send_google_email = bundle_google.getString("email");
            send_google_id = bundle_google.getString("user_fb_gmail_id");
            send_google_provider = bundle_google.getString("provider");

            fname_edt.setText(send_google_name);
            fname_edt.setFocusable(false);
            lname_edt.setText(send_google_lastname);
            lname_edt.setFocusable(false);
            email_edt.setText(send_google_email);
            email_edt.setFocusable(false);

        }

        SpannableString ss = new SpannableString("I agree to Terms and Conditions");

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent go = new Intent(RegistrationActivity.this, FaqTermsConditionActivity.class);
                Bundle b = new Bundle();
                b.putString("condition", "TermsandConditions");
                go.putExtras(b);
                startActivity(go);
                // startActivity(new Intent(RegistrationActivity.this, FaqTermsConditionActivity.class));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 11, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new UnderlineSpan(), 11, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        t_c_textview.setTypeface(thinFont);
        t_c_textview.setText(ss);
        t_c_textview.setMovementMethod(LinkMovementMethod.getInstance());
        t_c_textview.setHighlightColor(Color.TRANSPARENT);


        SpannableString ss_eula_rules = new SpannableString("I agree to Eula Rules");

        ClickableSpan clickableSpanEula = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent go = new Intent(RegistrationActivity.this, FaqTermsConditionActivity.class);
                Bundle b = new Bundle();
                b.putString("condition", "EULA");
                go.putExtras(b);
                startActivity(go);
                // startActivity(new Intent(RegistrationActivity.this, FaqTermsConditionActivity.class));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss_eula_rules.setSpan(clickableSpanEula, 11, 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss_eula_rules.setSpan(new UnderlineSpan(), 11, 21, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        eula_textview.setTypeface(thinFont);
        eula_textview.setText(ss_eula_rules);
        eula_textview.setMovementMethod(LinkMovementMethod.getInstance());
        eula_textview.setHighlightColor(Color.TRANSPARENT);

    }

    @Override
    public void onClick(View v) {
        if (v == register_btn) {
            if (validate()) {
                if (checkInternet) {
                    Dialog.showProgressBar(this, "Registering...");
                    send_name = fname_edt.getText().toString();
                    send_lastname = lname_edt.getText().toString();
                    send_email = email_edt.getText().toString();
                    send_pasword = password_edt.getText().toString();
                    send_mobile = mobile_edt.getText().toString();
                    send_dob = dob_txt.getText().toString();
                    Log.d("VALUES", sendUserType + "///" + send_name + "///" + send_name + "///" + send_lastname + "///" + send_gender + "///" + send_email + "///" + send_dob + "///" + send_pasword + "///" + send_mobile + "///" + countryCodeAndroid + "///" + deviceId);


                    Log.d("REGMANURL", AppUrls.BASE_URL + AppUrls.REGISTRATION);

                    StringRequest strRegister = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.REGISTRATION, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("REGMANRESP", response);
                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                    Dialog.hideProgressBar();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String token = jsonObject1.getString("token");
                                    String user_id = jsonObject1.getString("user_id");
                                    String user_type = jsonObject1.getString("user_type");
                                    String is_email_verified = jsonObject1.getString("is_email_verified");
                                    String is_mobile_verified = jsonObject1.getString("is_mobile_verified");
                                    String initial_payment = jsonObject1.getString("initial_payment");
                                    Log.d("JWTMANN", token + "//" + initial_payment);
                                    String[] parts = token.split("\\.");
                                    byte[] dataDec = Base64.decode(parts[1], Base64.DEFAULT);
                                    String decodedString = "";
                                    try {
                                        decodedString = new String(dataDec, "UTF-8");
                                        Log.d("TOKENSMANN", decodedString);
                                        JSONObject jsonObject2 = new JSONObject(decodedString);
                                        Log.d("JSONDATMANN", jsonObject2.toString());
                                        String username = jsonObject2.getString("name");
                                        String email = jsonObject2.getString("email");
                                        String mobile = jsonObject2.getString("mobile");
                                        String country_code = jsonObject2.getString("country_code");
                                        String status = jsonObject2.getString("status");

                                        //set value in session
                                        sendUserLocation(user_id,token);
                                        userSessionManager.createUserLoginSession(user_id, user_type, token, username, email, mobile, country_code);

                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                    if (is_mobile_verified.equals("0")) {
                                        Intent itootp = new Intent(RegistrationActivity.this, AccountVerificationActivity.class);
                                        itootp.putExtra("mobile", send_mobile);
                                        itootp.putExtra("countryCode", "+" + countryCodeAndroid);
                                        itootp.putExtra("user_id", user_id);

                                        startActivity(itootp);
                                    } else if (initial_payment.equals("0")) {
                                        Intent iacc_page = new Intent(RegistrationActivity.this, PayPalActivity.class);
                                        iacc_page.putExtra("activity", "Registration");
                                        iacc_page.putExtra("paymentAmount", "1");
                                        startActivity(iacc_page);
                                        finish();
                                    } else {
                                        Intent imain = new Intent(RegistrationActivity.this, MainActivity.class);
                                        imain.putExtra("USERTYPE", user_type);
                                        startActivity(imain);
                                    }
                                } else if (successResponceCode.equals("10200")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(getApplicationContext(), "Invalid input..!", Toast.LENGTH_SHORT).show();
                                } else if (successResponceCode.equals("10300")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(getApplicationContext(), "User already exist..!", Toast.LENGTH_SHORT).show();
                                } else if (successResponceCode.equals("10400")) {
                                    Dialog.hideProgressBar();
                                    Toast.makeText(getApplicationContext(), " Invalid refercode..!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Dialog.hideProgressBar();
                            error.getMessage();
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams(){
                            Map<String, String> params = new HashMap<>();
                            Log.d("LOGNNNNNNNN", login_type);

                            if (login_type.equals("Manual")) {
                                params.put("user_type", sendUserType);
                                params.put("first_name", send_name);
                                params.put("last_name", send_lastname);
                                params.put("gender", send_gender);
                                params.put("email", send_email);
                                params.put("dob", send_dob);
                                params.put("password", send_pasword);
                                params.put("mobile", send_mobile);
                                params.put("country_code", "+" + countryCodeAndroid);
                                params.put("device_id", deviceId);
                                params.put("refercode", "");
                                params.put("provider", "Manual");
                                params.put("identifier", "xxxx");
                                params.put("device_platform", "ANDROID");
                                //    params.put("force_login", "1");
                                Log.d("REGPARAMManual", params.toString());
                            }
                            if (login_type.equals("Google")) {
                                params.put("user_type", sendUserType);
                                params.put("first_name", send_google_name);
                                params.put("last_name", send_google_lastname);
                                params.put("gender", send_gender);
                                params.put("email", send_google_email);
                                params.put("dob", send_dob);
                                params.put("password", send_pasword);
                                params.put("mobile", send_mobile);
                                params.put("country_code", "+" + countryCodeAndroid);
                                params.put("device_id", deviceId);
                                params.put("provider", "Google");
                                params.put("identifier", send_google_id);
                                params.put("refercode", "");
                                params.put("device_platform", "ANDROID");
                                //  params.put("force_login", "1");
                                Log.d("REGPARAMGoogle", params.toString());
                            }
                            if (login_type.equals("Facebook")) {
                                params.put("user_type", sendUserType);
                                params.put("first_name", send_fb_name);
                                params.put("last_name", send_fb_lastname);
                                params.put("gender", send_gender);
                                params.put("email", send_fb_email);
                                params.put("dob", send_dob);
                                params.put("password", send_pasword);
                                params.put("mobile", send_mobile);
                                params.put("country_code", "+" + countryCodeAndroid);
                                params.put("device_id", deviceId);
                                params.put("provider", "Facebook");
                                params.put("identifier", send_fb_user_data);
                                params.put("refercode", "");
                                params.put("device_platform", "ANDROID");
                                //   params.put("force_login", "1");
                                Log.d("REGPARAMFacebook", params.toString());
                            }

                            return params;
                        }


                    };


                    strRegister.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(RegistrationActivity.this);
                    requestQueue.add(strRegister);

                } else {
                    Dialog.hideProgressBar();
                    Toast.makeText(getApplicationContext(), "No Internet Connection Please Check Your Internet Connection..!!", Toast.LENGTH_SHORT).show();
                }
            }
        }

        if (v == cal_img) {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String strMonth = null, strDay = null;
                    int month = monthOfYear + 1;
                    if (month < 10) {
                        strMonth = "0" + month;
                    } else {
                        strMonth = month + "";
                    }
                    if (dayOfMonth < 10) {
                        strDay = "0" + dayOfMonth;
                    } else {                              //strDay + "-" + strMonth + "-" + year
                        strDay = dayOfMonth + "";
                    }
                    dob_txt.setText(String.valueOf(year + "-" + strMonth + "-" + strDay));

                    dob_txt.setError(null);
                }
            }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.show();
        }
    }

    private boolean validate() {

        boolean result = true;
        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";

        String name = fname_edt.getText().toString().trim();
        String lastname = lname_edt.getText().toString().trim();
        String email = email_edt.getText().toString().trim();
        String mobile = mobile_edt.getText().toString().trim();
        String password = password_edt.getText().toString().trim();
        String dob = dob_txt.getText().toString().trim();

        if ((name == null || name.equals("") || name.length() < 3)) {
            fname_edt.setError("Minimum 3 characters required");
            result = false;
        } else {
            if (!name.matches("^[\\p{L} .'-]+$")) {
                fname_edt.setError("Special characters not allowed");
                result = false;
            } else {
                fname_edt.setError(null);
            }
        }

        if ((lastname == null || lastname.equals("") || lastname.length() < 3)) {
            lname_edt.setError("Minimum 3 characters required");
            result = false;
        } else {
            if (!name.matches("^[\\p{L} .'-]+$")) {
                lname_edt.setError("Special characters not allowed");
                result = false;
            } else {
                lname_edt.setError(null);
            }
        }
        if ((mobile == null || mobile.equals("") || mobile.length() < 10)) {
            mobile_edt.setError("Invalid Mobile Number");
            result = false;
        } else {
            mobile_edt.setError(null);
        }

        if (!email.matches(EMAIL_REGEX)) {
            email_edt.setError("Invalid Email");
            result = false;
        } else {
            email_edt.setError(null);
        }
        if (dob == null || dob.equals("") || dob.length() < 0) {
            dob_txt.setError("Invalid DOB");
            result = false;
        } else {
            dob_txt.setError(null);
        }


        if (password.isEmpty() || password.length() < 6) {
            password_edt.setError("Minimum 6 characters required");
            result = false;
        } else {
            password_edt.setError(null);
        }

        if (!t_c_chk.isChecked()) {
            Toast.makeText(getApplicationContext(), "Please Check Terms & Conditions", Toast.LENGTH_SHORT).show();
            result = false;
        }
        if (!eula_chk.isChecked()) {
            Toast.makeText(getApplicationContext(), "Please Check Eula Rules", Toast.LENGTH_SHORT).show();
            result = false;
        }

        return result;
    }

    private void sendUserLocation(String userid,String token){
        if(gps.canGetLocation())
        {
            double latitude = gps.getLatitude();
            String lat = String.valueOf(latitude);
            double longitude = gps.getLongitude();
            String lng = String.valueOf(longitude);
            Log.d("sdjgbs", "Your Location is - \nLat: " + latitude + "\nLong: " + longitude);

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String place = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();// Here 1 represent max location result to returned, by documents it recommended 1 to 5
                sendUserData(userid, lat, lng, city, country, state, place,token);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void sendUserData(final String user_id, final String lat, final String lng, final String city, final String country, final String state, final String place,final String token) {
        checkInternet = NetworkChecking.isConnected(RegistrationActivity.this);
        if (checkInternet) {
            Log.d("URL", AppUrls.BASE_URL + AppUrls.SET_USER_LOCATION);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SET_USER_LOCATION,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("LOCATIONRES", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {
                                  /*  Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                                    startActivity(intent);*/
                                }
                                if (successResponceCode.equals("10200")) {
                                    Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("lat", lat);
                    params.put("long", lng);
                    params.put("city", city);
                    params.put("country", country);
                    params.put("state", state);
                    params.put("place", place);
                    Log.d("REPORT_LOC", "REPORT_LOC" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(RegistrationActivity.this);
            requestQueue.add(stringRequest);
        } else {
            // Toast.makeText(MainActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

}
