package com.sleepchallenge.itemclicklisteners;

import android.view.View;

public interface ChallengeIndividRunItemClickListener
{
    void onItemClick(View v, int pos);
}
