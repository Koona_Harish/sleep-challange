package com.sleepchallenge.itemclicklisteners;

import android.view.View;

public interface AllItemClickListeners
{
    void onItemClick(View v, int pos);
}
