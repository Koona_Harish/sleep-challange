package com.sleepchallenge.itemclicklisteners;

import android.view.View;

public interface AllMembersItemClickListener
{
    void onItemClick(View v, int pos);
}
