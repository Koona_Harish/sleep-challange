package com.sleepchallenge.itemclicklisteners;

import android.view.View;

public interface GetMemberUpdtStatusItemClickListener
{
    void onItemClick(View v, int pos);
}
