package com.sleepchallenge.itemclicklisteners;

import android.view.View;

public interface AllChalngMoreItemClickListener
{
    void onItemClick(View v, int pos);
}
