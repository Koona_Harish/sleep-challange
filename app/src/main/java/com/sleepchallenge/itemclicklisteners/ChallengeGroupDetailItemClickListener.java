package com.sleepchallenge.itemclicklisteners;

import android.view.View;

public interface ChallengeGroupDetailItemClickListener
{
    void onItemClick(View v, int pos);
}
