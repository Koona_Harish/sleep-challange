package com.sleepchallenge.itemclicklisteners;

import android.view.View;

/**
 * Created by admin on 12/18/2017.
 */

public interface GetMemInGroupClickListner {
    void onItemClick(View view, int layoutPosition);
}
