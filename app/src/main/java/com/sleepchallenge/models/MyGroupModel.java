package com.sleepchallenge.models;

import com.sleepchallenge.utils.AppUrls;

import org.json.JSONObject;

public class MyGroupModel {

    public String id, admin_id, name, group_pic,total_win, group_members, total_loss, overall_rank, winning_per;


    public MyGroupModel(JSONObject jsonObject) {

        try {
            if (jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id = jsonObject.getString("id");

            if (jsonObject.has("admin_id") && !jsonObject.isNull("admin_id"))
                this.admin_id = jsonObject.getString("admin_id");

            if (jsonObject.has("name") && !jsonObject.isNull("name"))
                this.name = jsonObject.getString("name");

            if (jsonObject.has("group_pic") && !jsonObject.isNull("group_pic"))
                this.group_pic = AppUrls.BASE_IMAGE_URL + jsonObject.getString("group_pic");

            if (jsonObject.has("total_win") && !jsonObject.isNull("total_win"))
                this.total_win = jsonObject.getString("total_win");

            if (jsonObject.has("total_loss") && !jsonObject.isNull("total_loss"))
                this.total_loss = jsonObject.getString("total_loss");

            if (jsonObject.has("overall_rank") && !jsonObject.isNull("overall_rank"))
                this.overall_rank = jsonObject.getString("overall_rank");

            if (jsonObject.has("winning_per") && !jsonObject.isNull("winning_per"))
                this.winning_per = jsonObject.getString("winning_per");

            if (jsonObject.has("group_members") && !jsonObject.isNull("group_members"))  //count
                this.group_members = jsonObject.getString("group_members") ;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}