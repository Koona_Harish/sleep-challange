package com.sleepchallenge.models;

import com.sleepchallenge.utils.AppUrls;

import org.json.JSONObject;

public class GroupMessageModel
{
    public String  id,from_name,profile_pic,msg,conversation_type,is_read,sent_on_txt,sent_on;

    public GroupMessageModel(JSONObject jsonObject) {

        try {
            if (jsonObject.has("group_id") && !jsonObject.isNull("group_id"))
                this.id = jsonObject.getString("group_id");

            if (jsonObject.has("from_name") && !jsonObject.isNull("from_name"))
                this.from_name = jsonObject.getString("from_name");

            if (jsonObject.has("conversation_type") && !jsonObject.isNull("conversation_type"))
                this.conversation_type = jsonObject.getString("conversation_type");

            if (jsonObject.has("profile_pic") && !jsonObject.isNull("profile_pic"))
                this.profile_pic = AppUrls.BASE_IMAGE_URL + jsonObject.getString("profile_pic");

            if (jsonObject.has("msg") && !jsonObject.isNull("msg"))
                this.msg = jsonObject.getString("msg");

            if (jsonObject.has("is_read") && !jsonObject.isNull("is_read"))
                this.is_read = jsonObject.getString("is_read");

            if (jsonObject.has("sent_on_txt") && !jsonObject.isNull("sent_on_txt"))
                this.sent_on_txt = jsonObject.getString("sent_on_txt");

            if (jsonObject.has("sent_on") && !jsonObject.isNull("sent_on"))
                this.sent_on = jsonObject.getString("sent_on");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
