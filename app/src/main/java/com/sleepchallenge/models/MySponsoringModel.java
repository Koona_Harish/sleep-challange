package com.sleepchallenge.models;

import com.sleepchallenge.utils.AppUrls;

import org.json.JSONObject;

public class MySponsoringModel
{


    public String name,id,profile_pic,status,date,amount,to_sponsor_type;

    public MySponsoringModel(JSONObject jsonObject)
    {
        try
        {
            if(jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id = jsonObject.getString("id");

            if(jsonObject.has("name") && !jsonObject.isNull("name"))
                this.name = jsonObject.getString("name");

            if(jsonObject.has("status") && !jsonObject.isNull("status"))
                this.status = jsonObject.getString("status");

            if(jsonObject.has("date") && !jsonObject.isNull("date"))
                this.date = jsonObject.getString("date");

            if(jsonObject.has("amount") && !jsonObject.isNull("amount"))
                this.amount = jsonObject.getString("amount");

            if(jsonObject.has("to_sponsor_type") && !jsonObject.isNull("to_sponsor_type"))
                this.to_sponsor_type = jsonObject.getString("to_sponsor_type");

            if(jsonObject.has("profile_pic") && !jsonObject.isNull("profile_pic"))
                this.profile_pic = AppUrls.BASE_IMAGE_URL+jsonObject.getString("profile_pic");

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
