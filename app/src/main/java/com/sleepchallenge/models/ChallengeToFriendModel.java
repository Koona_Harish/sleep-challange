package com.sleepchallenge.models;

/**
 * Created by Devolper on 21-Jul-17.
 */

public class ChallengeToFriendModel
{
    public String id;
    public String user_type;
    public String name;
    public String profile_pic;
    public String email;
    public String country_code;
    public String mobile;
    public String total_win;
    public String total_loss;
    public String overall_rank;
    public String winning_per;
    public String image1;
    public String image2;
    public String image3;
    public String image4;
    public String image5;
    public String already_applied;

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getImage5() {
        return image5;
    }

    public void setImage5(String image5) {
        this.image5 = image5;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTotal_win() {
        return total_win;
    }

    public void setTotal_win(String total_win) {
        this.total_win = total_win;
    }

    public String getTotal_loss() {
        return total_loss;
    }

    public void setTotal_loss(String total_loss) {
        this.total_loss = total_loss;
    }

    public String getOverall_rank() {
        return overall_rank;
    }

    public void setOverall_rank(String overall_rank) {
        this.overall_rank = overall_rank;
    }

    public String getWinning_per() {
        return winning_per;
    }

    public void setWinning_per(String winning_per) {
        this.winning_per = winning_per;
    }

    public String getAlready_applied() {
        return already_applied;
    }

    public void setAlready_applied(String already_applied) {
        this.already_applied = already_applied;
    }
}
