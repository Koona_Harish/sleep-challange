package com.sleepchallenge.models;

import com.sleepchallenge.utils.AppUrls;

import org.json.JSONObject;

public class AllMemberModel {

    public String id, user_type, name, profile_pic, email, country_code, mobile, total_win, rank_apply_challenges,
            group_user_type,total_loss, overall_rank, winning_per,user_name,user_rank,win_challenges,lost_challenges,winning_percentage;


    public AllMemberModel(JSONObject jsonObject) {

        try {
            if (jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id = jsonObject.getString("id");
            if (jsonObject.has("user_type") && !jsonObject.isNull("user_type"))
                this.user_type = jsonObject.getString("user_type");
            if (jsonObject.has("name") && !jsonObject.isNull("name"))
                this.name = jsonObject.getString("name");
            if (jsonObject.has("profile_pic") && !jsonObject.isNull("profile_pic"))
                this.profile_pic = AppUrls.BASE_IMAGE_URL + jsonObject.getString("profile_pic");
            if (jsonObject.has("email") && !jsonObject.isNull("email"))
                this.email = jsonObject.getString("email");
            if (jsonObject.has("country_code") && !jsonObject.isNull("country_code"))
                this.country_code = jsonObject.getString("country_code");
            if (jsonObject.has("mobile") && !jsonObject.isNull("mobile"))
                this.mobile = jsonObject.getString("mobile");
            if (jsonObject.has("total_win") && !jsonObject.isNull("total_win"))
                this.total_win = jsonObject.getString("total_win");
            if (jsonObject.has("rank_apply_challenges") && !jsonObject.isNull("rank_apply_challenges"))
                this.rank_apply_challenges = jsonObject.getString("rank_apply_challenges");
            if (jsonObject.has("total_loss") && !jsonObject.isNull("total_loss"))
                this.total_loss = jsonObject.getString("total_loss");
            if (jsonObject.has("overall_rank") && !jsonObject.isNull("overall_rank"))
                this.overall_rank = jsonObject.getString("overall_rank");
            if (jsonObject.has("winning_per") && !jsonObject.isNull("winning_per"))
                this.winning_per = jsonObject.getString("winning_per");
            if (jsonObject.has("group_user_type") && !jsonObject.isNull("group_user_type"))
                this.group_user_type = jsonObject.getString("group_user_type");
            if (jsonObject.has("user_name") && !jsonObject.isNull("user_name"))
                this.user_name = jsonObject.getString("user_name");
            if (jsonObject.has("user_rank") && !jsonObject.isNull("user_rank"))
                this.user_rank = jsonObject.getString("user_rank");
            if (jsonObject.has("win_challenges") && !jsonObject.isNull("win_challenges"))
                this.win_challenges = jsonObject.getString("win_challenges");
            if (jsonObject.has("lost_challenges") && !jsonObject.isNull("lost_challenges"))
                this.lost_challenges = jsonObject.getString("lost_challenges");
            if (jsonObject.has("winning_percentage") && !jsonObject.isNull("winning_percentage"))
                this.winning_percentage = jsonObject.getString("winning_percentage");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}