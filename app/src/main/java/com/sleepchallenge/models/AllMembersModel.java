package com.sleepchallenge.models;

public class AllMembersModel {

    public String id;
    public String user_type;
    public String name;
    public String profile_pic;

    public String getAdmin_id() {
        return admin_id;
    }

    public void setAdmin_id(String admin_id) {
        this.admin_id = admin_id;
    }

    public String email;
    public String country_code;
    public String mobile;
    public String total_win;
    public String total_loss;
    public String overall_rank;
    public String winning_per;
    public String admin_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTotal_win() {
        return total_win;
    }

    public void setTotal_win(String total_win) {
        this.total_win = total_win;
    }

    public String getTotal_loss() {
        return total_loss;
    }

    public void setTotal_loss(String total_loss) {
        this.total_loss = total_loss;
    }

    public String getOverall_rank() {
        return overall_rank;
    }

    public void setOverall_rank(String overall_rank) {
        this.overall_rank = overall_rank;
    }

    public String getWinning_per() {
        return winning_per;
    }

    public void setWinning_per(String winning_per) {
        this.winning_per = winning_per;
    }
}
