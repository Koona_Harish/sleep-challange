package com.sleepchallenge.models;

import com.sleepchallenge.utils.AppUrls;

import org.json.JSONObject;

public class AllPromotionsModel {

    public String id,banner_path,status,entity_id,entity_type,admin_id,entity_name;


    public AllPromotionsModel(JSONObject jsonObject) {

        try {
            if (jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id = jsonObject.getString("id");
            if (jsonObject.has("status") && !jsonObject.isNull("status"))
                this.status = jsonObject.getString("status");
            if (jsonObject.has("entity_id") && !jsonObject.isNull("entity_id"))
                this.entity_id = jsonObject.getString("entity_id");
            if (jsonObject.has("banner_path") && !jsonObject.isNull("banner_path"))
                this.banner_path = AppUrls.BASE_IMAGE_URL + jsonObject.getString("banner_path");
            if (jsonObject.has("entity_type") && !jsonObject.isNull("entity_type"))
                this.entity_type = jsonObject.getString("entity_type");
            if (jsonObject.has("admin_id") && !jsonObject.isNull("admin_id"))
                this.admin_id = jsonObject.getString("admin_id");
            if (jsonObject.has("entity_name") && !jsonObject.isNull("entity_name"))
                this.entity_name = jsonObject.getString("entity_name");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}