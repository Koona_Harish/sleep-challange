package com.sleepchallenge.models;

import com.sleepchallenge.utils.AppUrls;

import org.json.JSONObject;

public class SponsorReqModel
{


    public String id, amount_paid,status, sponsor_type, sponsor_id, sponsor_name, sponsor_image;

    public SponsorReqModel(JSONObject jsonObject)
    {
        try
        {
            if(jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id = jsonObject.getString("id");

            if(jsonObject.has("amount_paid") && !jsonObject.isNull("amount_paid"))
                this.amount_paid = jsonObject.getString("amount_paid");

            if(jsonObject.has("status") && !jsonObject.isNull("status"))
                this.status = jsonObject.getString("status");

            if(jsonObject.has("sponsor_type") && !jsonObject.isNull("sponsor_type"))
                this.sponsor_type = jsonObject.getString("sponsor_type");

            if(jsonObject.has("sponsor_id") && !jsonObject.isNull("sponsor_id"))
                this.sponsor_id = jsonObject.getString("sponsor_id");

            if(jsonObject.has("sponsor_name") && !jsonObject.isNull("sponsor_name"))
                this.sponsor_name = jsonObject.getString("sponsor_name");

            if(jsonObject.has("sponsor_image") && !jsonObject.isNull("sponsor_image"))
                this.sponsor_image = AppUrls.BASE_IMAGE_URL+jsonObject.getString("sponsor_image");

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
