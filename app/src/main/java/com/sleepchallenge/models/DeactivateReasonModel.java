package com.sleepchallenge.models;

import org.json.JSONObject;

public class DeactivateReasonModel
{
    public String id;
    public String reason;

    public DeactivateReasonModel(JSONObject jsonObject) {

        try {
            if (jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id = jsonObject.getString("id");
            if (jsonObject.has("reason") && !jsonObject.isNull("reason"))
                this.reason = jsonObject.getString("reason");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
