package com.sleepchallenge.models;

/**
 * Created by admin on 12/4/2017.
 */

public class ReportHistoryModel {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDevice_name() {
        return device_name;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }

    public String getDevice_platform() {
        return device_platform;
    }

    public void setDevice_platform(String device_platform) {
        this.device_platform = device_platform;
    }

    public String getCreated_on_txt() {
        return created_on_txt;
    }

    public void setCreated_on_txt(String created_on_txt) {
        this.created_on_txt = created_on_txt;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    String id;
    String title;
    String type;
    String details;
    String device_name;
    String device_platform;
    String created_on_txt;
    String created_on;

}
