package com.sleepchallenge.models;

import com.sleepchallenge.utils.AppUrls;

import org.json.JSONObject;

public class ConcenChallModel {

    public String id, url_image,dummy_image;


    public ConcenChallModel(JSONObject jsonObject) {

        try {
            if (jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id = jsonObject.getString("id");

            if (jsonObject.has("url") && !jsonObject.isNull("url"))
                this.url_image = AppUrls.BASE_IMAGE_URL + jsonObject.getString("url");
            if (jsonObject.has("dummy_image") && !jsonObject.isNull("dummy_image"))
                this.dummy_image = AppUrls.BASE_IMAGE_URL + jsonObject.getString("dummy_image");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}