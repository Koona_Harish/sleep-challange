package com.sleepchallenge.models;

import org.json.JSONObject;

public class MessagesDetailModel
{
   public String id,from_id,from_name,recipient,msg,is_read,sent_on_txt,sent_on;

    public MessagesDetailModel(JSONObject jsonObject) {

        try {

            if (jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id =  jsonObject.getString("id");

            if (jsonObject.has("from_id") && !jsonObject.isNull("from_id"))
                this.from_id = jsonObject.getString("from_id");

            if (jsonObject.has("from_name") && !jsonObject.isNull("from_name"))
                this.from_name = jsonObject.getString("from_name");

            if (jsonObject.has("recipient") && !jsonObject.isNull("recipient"))
                this.recipient = jsonObject.getString("recipient");

            if (jsonObject.has("msg") && !jsonObject.isNull("msg"))
                this.msg = jsonObject.getString("msg");

            if (jsonObject.has("is_read") && !jsonObject.isNull("is_read"))
                this.is_read = jsonObject.getString("is_read");

            if (jsonObject.has("sent_on_txt") && !jsonObject.isNull("sent_on_txt"))
                this.sent_on_txt = jsonObject.getString("sent_on_txt");

            if (jsonObject.has("sent_on") && !jsonObject.isNull("sent_on"))
                this.sent_on = jsonObject.getString("sent_on");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }




}
