package com.sleepchallenge.models;

import com.sleepchallenge.utils.AppUrls;

import org.json.JSONObject;

/**
 * Created by Devolper on 21-Jul-17.
 */

public class GrpInMemberDetailModel
{

    public String id,group_name, admin_id,conversation_type,group_pic,group_rank,group_members,winning_percentage,win_challenges,lost_challenges;

    public GrpInMemberDetailModel(JSONObject jsonObject) {

        try {
            if (jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id = jsonObject.getString("id");

            if (jsonObject.has("group_name") && !jsonObject.isNull("group_name"))
                this.group_name = jsonObject.getString("group_name");

            if (jsonObject.has("admin_id") && !jsonObject.isNull("admin_id"))
                this.admin_id = jsonObject.getString("admin_id");

            if (jsonObject.has("group_pic") && !jsonObject.isNull("group_pic"))
                this.group_pic = AppUrls.BASE_IMAGE_URL + jsonObject.getString("group_pic");

            if (jsonObject.has("conversation_type") && !jsonObject.isNull("conversation_type"))
                this.conversation_type = jsonObject.getString("conversation_type");

            if (jsonObject.has("group_rank") && !jsonObject.isNull("group_rank"))
                this.group_rank = jsonObject.getString("group_rank");

            if (jsonObject.has("winning_percentage") && !jsonObject.isNull("winning_percentage"))
                this.winning_percentage = jsonObject.getString("winning_percentage");

            if (jsonObject.has("win_challenges") && !jsonObject.isNull("win_challenges"))
                this.win_challenges = jsonObject.getString("win_challenges");

            if (jsonObject.has("lost_challenges") && !jsonObject.isNull("lost_challenges"))
                this.lost_challenges = jsonObject.getString("lost_challenges");

            if (jsonObject.has("group_members") && !jsonObject.isNull("group_members"))
                this.group_members = jsonObject.getString("group_members");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
