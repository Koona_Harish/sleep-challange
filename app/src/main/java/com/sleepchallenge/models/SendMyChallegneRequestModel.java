package com.sleepchallenge.models;

import com.sleepchallenge.utils.AppUrls;

import org.json.JSONObject;

public class SendMyChallegneRequestModel {

    public String id,status,amount,challenge_goal,start_time,remainder,days,type,start_on,completed_on,user_wallet,group_wallet,user_pic,group_pic,user_name,group_name,activity_name,user_rank
      ,group_rank;


    public SendMyChallegneRequestModel(JSONObject jsonObject) {

        try {
            if (jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id = jsonObject.getString("id");

            if (jsonObject.has("status") && !jsonObject.isNull("status"))
                this.status = jsonObject.getString("status");

            if (jsonObject.has("challenge_goal") && !jsonObject.isNull("challenge_goal"))
                this.challenge_goal = jsonObject.getString("challenge_goal");

            if (jsonObject.has("user_pic") && !jsonObject.isNull("user_pic"))
                this.user_pic = AppUrls.BASE_IMAGE_URL + jsonObject.getString("user_pic");

            if (jsonObject.has("group_pic") && !jsonObject.isNull("group_pic"))
                this.group_pic = AppUrls.BASE_IMAGE_URL + jsonObject.getString("group_pic");

            if (jsonObject.has("start_on") && !jsonObject.isNull("start_on"))
                this.start_on = jsonObject.getString("start_on");

            if (jsonObject.has("completed_on") && !jsonObject.isNull("completed_on"))
                this.completed_on = jsonObject.getString("completed_on");

            if (jsonObject.has("amount") && !jsonObject.isNull("amount"))
                this.amount = jsonObject.getString("amount");

            if (jsonObject.has("user_wallet") && !jsonObject.isNull("user_wallet"))
                this.user_wallet = jsonObject.getString("user_wallet");

            if (jsonObject.has("group_wallet") && !jsonObject.isNull("group_wallet"))
                this.group_wallet = jsonObject.getString("group_wallet");

            if (jsonObject.has("activity_name") && !jsonObject.isNull("activity_name"))
                this.activity_name = jsonObject.getString("activity_name");

            if (jsonObject.has("user_rank") && !jsonObject.isNull("user_rank"))
                this.user_rank = jsonObject.getString("user_rank");

            if (jsonObject.has("group_rank") && !jsonObject.isNull("group_rank"))
                this.group_rank = jsonObject.getString("group_rank");


            if (jsonObject.has("group_name") && !jsonObject.isNull("group_name"))
                this.group_name = jsonObject.getString("group_name");

            if (jsonObject.has("user_name") && !jsonObject.isNull("user_name"))
                this.user_name = jsonObject.getString("user_name");

            if (jsonObject.has("remainder") && !jsonObject.isNull("remainder"))
                this.remainder = jsonObject.getString("remainder");

            if (jsonObject.has("start_time") && !jsonObject.isNull("start_time"))
                this.start_time = jsonObject.getString("start_time");

            if (jsonObject.has("days") && !jsonObject.isNull("days"))
                this.days = jsonObject.getString("days");

            if (jsonObject.has("type") && !jsonObject.isNull("type"))  //type means USER or GROUP  for my send requested challenge
                this.type = jsonObject.getString("type");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}