package com.sleepchallenge.models;

import com.sleepchallenge.utils.AppUrls;

import org.json.JSONObject;

public class MessagesModel
{
   public String from_id,from_name,from_user_type,profile_pic,msg,is_read,sent_on_txt,sent_on;

    public MessagesModel(JSONObject jsonObject) {

        try {
            if (jsonObject.has("from_id") && !jsonObject.isNull("from_id"))
                this.from_id = jsonObject.getString("from_id");

            if (jsonObject.has("from_name") && !jsonObject.isNull("from_name"))
                this.from_name = jsonObject.getString("from_name");

            if (jsonObject.has("from_user_type") && !jsonObject.isNull("from_user_type"))
                this.from_user_type = jsonObject.getString("from_user_type");

            if (jsonObject.has("profile_pic") && !jsonObject.isNull("profile_pic"))
                this.profile_pic = AppUrls.BASE_IMAGE_URL + jsonObject.getString("profile_pic");

            if (jsonObject.has("msg") && !jsonObject.isNull("msg"))
                this.msg = jsonObject.getString("msg");

            if (jsonObject.has("is_read") && !jsonObject.isNull("is_read"))
                this.is_read = jsonObject.getString("is_read");

            if (jsonObject.has("sent_on_txt") && !jsonObject.isNull("sent_on_txt"))
                this.sent_on_txt = jsonObject.getString("sent_on_txt");

            if (jsonObject.has("sent_on") && !jsonObject.isNull("sent_on"))
                this.sent_on = jsonObject.getString("sent_on");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }




}
