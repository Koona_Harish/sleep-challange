package com.sleepchallenge.models;

import com.sleepchallenge.utils.AppUrls;

import org.json.JSONObject;

public class GetMemberinGroupDetailModel {

    public String id, group_user_type, user_name, profile_pic, user_rank, win_challenges, lost_challenges, winning_percentage;


    public GetMemberinGroupDetailModel(JSONObject jsonObject) {

        try {
            if (jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id = jsonObject.getString("id");
            if (jsonObject.has("group_user_type") && !jsonObject.isNull("group_user_type"))
                this.group_user_type = jsonObject.getString("group_user_type");
            if (jsonObject.has("user_rank") && !jsonObject.isNull("user_rank"))
                this.user_rank = jsonObject.getString("user_rank");
            if (jsonObject.has("profile_pic") && !jsonObject.isNull("profile_pic"))
                this.profile_pic = AppUrls.BASE_IMAGE_URL + jsonObject.getString("profile_pic");
            if (jsonObject.has("user_name") && !jsonObject.isNull("user_name"))
                this.user_name = jsonObject.getString("user_name");
            if (jsonObject.has("win_challenges") && !jsonObject.isNull("win_challenges"))
                this.win_challenges = jsonObject.getString("win_challenges");
            if (jsonObject.has("lost_challenges") && !jsonObject.isNull("lost_challenges"))
                this.lost_challenges = jsonObject.getString("lost_challenges");
            if (jsonObject.has("winning_percentage") && !jsonObject.isNull("winning_percentage"))
                this.winning_percentage = jsonObject.getString("winning_percentage");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}