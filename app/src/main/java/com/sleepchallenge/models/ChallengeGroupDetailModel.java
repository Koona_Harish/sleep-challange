package com.sleepchallenge.models;

public class ChallengeGroupDetailModel {

    public String challenge_id;
    public String user_id;
    public String user_type;
    public String opponent_id;
    public String remainder;
    public String start_time;
    public String challenge_goal;
    public String pause_access;
    public String is_group_admin;
    public String user_name;
    public String opponent_name;
    public String challenger_group_id;
    public String winning_status;
    public String winning_amount;
    public String winner;
    public String status;
    public String amount;
    public String challenge_type;
    public String start_on;
    public String paused_on;
    public String paused_by;
    public String resume_on;
    public String completed_on_txt;
    public String completed_on;
    public String winning_reward_type;
    public String winning_reward_value;
    public String is_scratched;

    public String getChallenge_id() {
        return challenge_id;
    }

    public void setChallenge_id(String challenge_id) {
        this.challenge_id = challenge_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getOpponent_id() {
        return opponent_id;
    }

    public void setOpponent_id(String opponent_id) {
        this.opponent_id = opponent_id;
    }

    public String getRemainder() {
        return remainder;
    }

    public void setRemainder(String remainder) {
        this.remainder = remainder;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getChallenge_goal() {
        return challenge_goal;
    }

    public void setChallenge_goal(String challenge_goal) {
        this.challenge_goal = challenge_goal;
    }

    public String getPause_access() {
        return pause_access;
    }

    public void setPause_access(String pause_access) {
        this.pause_access = pause_access;
    }

    public String getIs_group_admin() {
        return is_group_admin;
    }

    public void setIs_group_admin(String is_group_admin) {
        this.is_group_admin = is_group_admin;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getOpponent_name() {
        return opponent_name;
    }

    public void setOpponent_name(String opponent_name) {
        this.opponent_name = opponent_name;
    }

    public String getChallenger_group_id() {
        return challenger_group_id;
    }

    public void setChallenger_group_id(String challenger_group_id) {
        this.challenger_group_id = challenger_group_id;
    }

    public String getWinning_status() {
        return winning_status;
    }

    public void setWinning_status(String winning_status) {
        this.winning_status = winning_status;
    }

    public String getWinning_amount() {
        return winning_amount;
    }

    public void setWinning_amount(String winning_amount) {
        this.winning_amount = winning_amount;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getChallenge_type() {
        return challenge_type;
    }

    public void setChallenge_type(String challenge_type) {
        this.challenge_type = challenge_type;
    }

    public String getStart_on() {
        return start_on;
    }

    public void setStart_on(String start_on) {
        this.start_on = start_on;
    }

    public String getPaused_on() {
        return paused_on;
    }

    public void setPaused_on(String paused_on) {
        this.paused_on = paused_on;
    }

    public String getPaused_by() {
        return paused_by;
    }

    public void setPaused_by(String paused_by) {
        this.paused_by = paused_by;
    }

    public String getResume_on() {
        return resume_on;
    }

    public void setResume_on(String resume_on) {
        this.resume_on = resume_on;
    }

    public String getCompleted_on_txt() {
        return completed_on_txt;
    }

    public void setCompleted_on_txt(String completed_on_txt) {
        this.completed_on_txt = completed_on_txt;
    }

    public String getCompleted_on() {
        return completed_on;
    }

    public void setCompleted_on(String completed_on) {
        this.completed_on = completed_on;
    }

    public String getWinning_reward_type() {
        return winning_reward_type;
    }

    public void setWinning_reward_type(String winning_reward_type) {
        this.winning_reward_type = winning_reward_type;
    }

    public String getWinning_reward_value() {
        return winning_reward_value;
    }

    public void setWinning_reward_value(String winning_reward_value) {
        this.winning_reward_value = winning_reward_value;
    }

    public String getIs_scratched() {
        return is_scratched;
    }

    public void setIs_scratched(String is_scratched) {
        this.is_scratched = is_scratched;
    }
}
