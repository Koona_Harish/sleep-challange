package com.sleepchallenge.models;

import org.json.JSONObject;

public class PaymentHistoryModel
{

    public String  id,user_id,user_type,title,txn_type,txn_flag,txn_status,amount,created_on_txt,created_on,updated_on_txt,updated_on,for_span;

    public PaymentHistoryModel(JSONObject jsonObject) {

        try {
            if (jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id = jsonObject.getString("id");
            if (jsonObject.has("user_type") && !jsonObject.isNull("user_type"))
                this.user_type = jsonObject.getString("user_type");
            if (jsonObject.has("user_id") && !jsonObject.isNull("user_id"))
                this.user_id = jsonObject.getString("user_id");
            if (jsonObject.has("title") && !jsonObject.isNull("title"))
                this.title = jsonObject.getString("title");
            if (jsonObject.has("txn_type") && !jsonObject.isNull("txn_type"))
                this.txn_type = jsonObject.getString("txn_type");
            if (jsonObject.has("txn_flag") && !jsonObject.isNull("txn_flag"))
                this.txn_flag = jsonObject.getString("txn_flag");
            if (jsonObject.has("txn_status") && !jsonObject.isNull("txn_status"))
                this.txn_status = jsonObject.getString("txn_status");
            if (jsonObject.has("amount") && !jsonObject.isNull("amount"))
                this.amount = jsonObject.getString("amount");
            if (jsonObject.has("created_on_txt") && !jsonObject.isNull("created_on_txt"))
                this.created_on_txt = jsonObject.getString("created_on_txt");
            if (jsonObject.has("created_on") && !jsonObject.isNull("created_on"))
                this.created_on = jsonObject.getString("created_on");
            if (jsonObject.has("updated_on_txt") && !jsonObject.isNull("updated_on_txt"))
                this.updated_on_txt = jsonObject.getString("updated_on_txt");
            if (jsonObject.has("updated_on") && !jsonObject.isNull("updated_on"))
                this.updated_on = jsonObject.getString("updated_on");

            if (jsonObject.has("for_span") && !jsonObject.isNull("for_span"))
                this.for_span = jsonObject.getString("for_span");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}