package com.sleepchallenge.models;

public class GetGroupMembersModel
{



    public String group_user_type_M;
    public String id_M;
    public String user_name_M;

    public String getGroup_user_type_M() {
        return group_user_type_M;
    }

    public void setGroup_user_type_M(String group_user_type_M) {
        this.group_user_type_M = group_user_type_M;
    }

    public String getId_M() {
        return id_M;
    }

    public void setId_M(String id_M) {
        this.id_M = id_M;
    }

    public String getUser_name_M() {
        return user_name_M;
    }

    public void setUser_name_M(String user_name_M) {
        this.user_name_M = user_name_M;
    }
}
