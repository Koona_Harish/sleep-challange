package com.sleepchallenge.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.sleepchallenge.R;
import com.sleepchallenge.activities.ChallengeStartActivity;
import com.sleepchallenge.activities.ChallengeStatusActivity;
import com.sleepchallenge.activities.MainActivity;

public class LocalNotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
      showNotification(context);
    }
    public void showNotification(Context context) {
        Intent intent = new Intent(context, ChallengeStartActivity.class);
        PendingIntent pi = PendingIntent.getActivity(context, 2, intent, 0);
        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText("Your Challenge will be start after 15 mins Please App keep in Background.otherwise you loss the challenge.");
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle("Sleep Challenge")
               .setStyle(bigText);
        mBuilder.setContentIntent(pi);
        mBuilder.setDefaults(Notification.DEFAULT_SOUND);

        mBuilder.setAutoCancel(true);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(2, mBuilder.build());
    }
}
