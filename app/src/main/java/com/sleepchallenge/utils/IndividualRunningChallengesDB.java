package com.sleepchallenge.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class IndividualRunningChallengesDB extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "runningchallenges.db";
    private static final String TABLE_DETAILS = "runningchallengesdetails";

    public static final String CHALLENGE_ID = "challenge_id";
    public static final String USER_ID = "user_id";
    public static final String OPPONENT_ID = "opponent_id";
    public static final String USER_TYPE = "user_type";
    public static final String REMAINDER = "remainder";
    public static final String START_TIME = "start_time";
    public static final String CHALLENGE_GOAL = "challenge_goal";
    public static final String PAUSE_ACCESS = "pause_access";
    public static final String IS_GROUP_ADMIN = "is_group_admin";
    public static final String USER_NAME = "user_name";
    public static final String OPPONENT_NAME = "opponent_name";
    public static final String WINNING_AMOUNT = "winning_amount";
    public static final String WINNING_STATUS = "winning_status";
    public static final String WINNER = "winner";
    public static final String STATUS = "status";
    public static final String AMOUNT = "amount";
    public static final String CHALLENGE_TYPE = "challenge_type";
    public static final String START_ON = "start_on";
    public static final String PAUSED_ON = "paused_on";
    public static final String PAUSED_BY = "paused_by";
    public static final String RESUME_ON = "resume_on";
    public static final String COMPLETED_ON_TXT = "completed_on_txt";
    public static final String COMPLETED_ON = "completed_on";
    public static final String WINNING_REWARD_TYPE = "winning_reward_type";
    public static final String WINNING_REWARD_VALUE = "winning_reward_value";
    public static final String RUNNING_COUNT = "running_count";
    //public static final String ACTIVITY_NAME = "activity_name";
    public static final String IS_SCRATCHED = "is_scratched";


    public IndividualRunningChallengesDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public IndividualRunningChallengesDB(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ACTIVITY_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_DETAILS + "("
                + CHALLENGE_ID + " TEXT ,"
                + USER_ID + " TEXT ,"
                + OPPONENT_ID + " TEXT ,"
                + USER_TYPE + " TEXT ,"
                + REMAINDER + " TEXT ,"
                + START_TIME + " TEXT ,"
                + CHALLENGE_GOAL + " TEXT ,"
                + PAUSE_ACCESS + " TEXT ,"
                + IS_GROUP_ADMIN + " TEXT ,"
                + USER_NAME + " TEXT ,"
                + OPPONENT_NAME + " TEXT ,"
                + WINNING_AMOUNT + " TEXT ,"
                + WINNING_STATUS + " TEXT ,"
                + WINNER + " TEXT ,"
                + STATUS + " TEXT ,"
                + AMOUNT + " TEXT ,"
                + CHALLENGE_TYPE + " TEXT ,"
                + START_ON + " TEXT ,"
                + PAUSED_ON + " TEXT ,"
                + PAUSED_BY + " TEXT ,"
                + RESUME_ON + " TEXT ,"
                + COMPLETED_ON_TXT + " TEXT ,"
                + COMPLETED_ON + " TEXT ,"
                + WINNING_REWARD_TYPE + " TEXT ,"
                + WINNING_REWARD_VALUE + " TEXT ,"
                + RUNNING_COUNT + " VARCHAR ,"
                + IS_SCRATCHED + " TEXT "+
                 ")";
        db.execSQL(CREATE_ACTIVITY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DETAILS);
        onCreate(db);
    }

    public void addRunningChallengesList(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DETAILS, null, contentValues);
        db.close();
    }

    public List<String> getChallengeID() {
        List<String> challenge_id = new ArrayList<String>();
          try {
              String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
              SQLiteDatabase db = this.getWritableDatabase();
              Cursor cursor = db.rawQuery(selectQuery, null);
              if (cursor.moveToFirst()){
                  do {
                      challenge_id.add(cursor.getString(0));

                  } while (cursor.moveToNext());
              }
          }catch (Exception e){

          }

        return challenge_id;
    }

    public List<String> getUserID() {
        List<String> user_id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_id.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return user_id;
    }

    public List<String> getOpponentId() {
        List<String> opponent_id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                opponent_id.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return opponent_id;
    }



    public List<String> getUserType() {
        List<String> user_type = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_type.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return user_type;
    }

    public List<String> getRemainder() {
        List<String> remainder = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                remainder.add(cursor.getString(4));

            } while (cursor.moveToNext());
        }
        return remainder;

    }public List<String> getStartTime() {
        List<String> start_time = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                start_time.add(cursor.getString(5));

            } while (cursor.moveToNext());
        }
        return start_time;

    }public List<String> getChallengeGoal() {
        List<String> challenge_goal = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                challenge_goal.add(cursor.getString(6));

            } while (cursor.moveToNext());
        }
        return challenge_goal;

    }public List<String> getPauseAccess() {
        List<String> pause_access = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                pause_access.add(cursor.getString(7));

            } while (cursor.moveToNext());
        }
        return pause_access;

    }public List<String> getIsGroupAdmin() {
        List<String> is_group_admin = new ArrayList<String>();
          try {
              String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
              SQLiteDatabase db = this.getWritableDatabase();
              Cursor cursor = db.rawQuery(selectQuery, null);
              if (cursor.moveToFirst()){
                  do {
                      is_group_admin.add(cursor.getString(8));

                  } while (cursor.moveToNext());
              }
          }catch (Exception e){

          }

        return is_group_admin;
    }
     public List<String> getUserName() {
        List<String> user_name = new ArrayList<String>();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()){
                do {
                    user_name.add(cursor.getString(9));

                } while (cursor.moveToNext());
            }
        }catch (Exception e){

        }

        return user_name;
    }
    public List<String> getOpponentName() {
        List<String> opponent_name = new ArrayList<String>();
         try {
             String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
             SQLiteDatabase db = this.getWritableDatabase();
             Cursor cursor = db.rawQuery(selectQuery, null);
             if (cursor.moveToFirst()){
                 do {
                     opponent_name.add(cursor.getString(10));

                 } while (cursor.moveToNext());
             }
         }catch (Exception e){

         }

        return opponent_name;
    }
    public List<String> getWinningAmount() {
        List<String> winning_amount = new ArrayList<String>();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()){
                do {
                    winning_amount.add(cursor.getString(11));

                } while (cursor.moveToNext());
            }
        }catch (Exception e){

        }
       return winning_amount;
    }
public List<String> getWinningStatus() {
        List<String> winning_status = new ArrayList<String>();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()){
                do {
                    winning_status.add(cursor.getString(12));

                } while (cursor.moveToNext());
            }
        }catch (Exception e){

        }

        return winning_status;
    }
public List<String> getWinner() {
        List<String> winner = new ArrayList<String>();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()){
                do {
                    winner.add(cursor.getString(13));

                } while (cursor.moveToNext());
            }
        }catch (Exception e){

        }

        return winner;
    }

public List<String> getStatus() {
        List<String> status = new ArrayList<String>();
          try {
              String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
              SQLiteDatabase db = this.getWritableDatabase();
              Cursor cursor = db.rawQuery(selectQuery, null);
              if (cursor.moveToFirst()){
                  do {
                      status.add(cursor.getString(14));

                  } while (cursor.moveToNext());
              }
          }catch (Exception e){

          }

        return status;
    }

public List<String> getAmount() {
        List<String> amount = new ArrayList<String>();
         try {
             String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
             SQLiteDatabase db = this.getWritableDatabase();
             Cursor cursor = db.rawQuery(selectQuery, null);
             if (cursor.moveToFirst()){
                 do {
                     amount.add(cursor.getString(15));

                 } while (cursor.moveToNext());
             }
         }catch (Exception e){

         }

        return amount;
    }
    public List<String> getChallengeType() {
        List<String> challenge_type = new ArrayList<String>();
           try {
               String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
               SQLiteDatabase db = this.getWritableDatabase();
               Cursor cursor = db.rawQuery(selectQuery, null);
               if (cursor.moveToFirst()){
                   do {
                       challenge_type.add(cursor.getString(16));

                   } while (cursor.moveToNext());
               }
           }catch (Exception e){

           }

        return challenge_type;
    }

public List<String> getStartOn() {
        List<String> start_on = new ArrayList<String>();
         try {
             String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
             SQLiteDatabase db = this.getWritableDatabase();
             Cursor cursor = db.rawQuery(selectQuery, null);
             if (cursor.moveToFirst()){
                 do {
                     start_on.add(cursor.getString(17));

                 } while (cursor.moveToNext());
             }
         }catch (Exception e){

         }

        return start_on;
    }

    public List<String> getPausedOn() {
        List<String> paused_on = new ArrayList<String>();
         try {
             String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
             SQLiteDatabase db = this.getWritableDatabase();
             Cursor cursor = db.rawQuery(selectQuery, null);
             if (cursor.moveToFirst()){
                 do {
                     paused_on.add(cursor.getString(18));

                 } while (cursor.moveToNext());
             }
         }catch (Exception e){

         }

        return paused_on;
    }

    public List<String> getPaused_by() {
        List<String> paused_by = new ArrayList<String>();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()){
                do {
                    paused_by.add(cursor.getString(19));

                } while (cursor.moveToNext());
            }
        }catch (Exception e){

        }

        return paused_by;
    }

    public List<String> getResumeOn() {
        List<String> resume_on = new ArrayList<String>();
         try {
             String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
             SQLiteDatabase db = this.getWritableDatabase();
             Cursor cursor = db.rawQuery(selectQuery, null);
             if (cursor.moveToFirst()){
                 do {
                     resume_on.add(cursor.getString(20));

                 } while (cursor.moveToNext());
             }
         }catch (Exception e){

         }

        return resume_on;
    }


    public List<String> getCompletedOnTxt() {
        List<String> completed_on_txt = new ArrayList<String>();
         try {
             String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
             SQLiteDatabase db = this.getWritableDatabase();
             Cursor cursor = db.rawQuery(selectQuery, null);
             if (cursor.moveToFirst()){
                 do {
                     completed_on_txt.add(cursor.getString(21));

                 } while (cursor.moveToNext());
             }
         }catch (Exception e){

         }

        return completed_on_txt;
    }


    public List<String> getCompletedOn() {
        List<String> completed_on = new ArrayList<String>();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()){
                do {
                    completed_on.add(cursor.getString(22));

                } while (cursor.moveToNext());
            }
        }catch (Exception e){

        }

        return completed_on;
    }


    public List<String> getWinningRewardType() {
        List<String> winning_reward_type = new ArrayList<String>();
         try {
             String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
             SQLiteDatabase db = this.getWritableDatabase();
             Cursor cursor = db.rawQuery(selectQuery, null);
             if (cursor.moveToFirst()){
                 do {
                     winning_reward_type.add(cursor.getString(23));

                 } while (cursor.moveToNext());
             }
         }catch (Exception e){

         }

        return winning_reward_type;
    }

    public List<String> getWinningRewardValue() {
        List<String> winning_reward_value = new ArrayList<String>();
          try {
              String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
              SQLiteDatabase db = this.getWritableDatabase();
              Cursor cursor = db.rawQuery(selectQuery, null);
              if (cursor.moveToFirst()){
                  do {
                      winning_reward_value.add(cursor.getString(24));

                  } while (cursor.moveToNext());
              }
          }catch (Exception e){

          }

        return winning_reward_value;
    }
    public int getRunningCount() {
        int running_count=0;

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                running_count=cursor.getInt(25);

            } while (cursor.moveToNext());
        }
        // Log.d("DBRUNNING",""running_count);
        return running_count;
    }

    public List<String> getIsScratched() {
        List<String> is_scratched = new ArrayList<String>();
          try {
              String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
              SQLiteDatabase db = this.getWritableDatabase();
              Cursor cursor = db.rawQuery(selectQuery, null);
              if (cursor.moveToFirst()){
                  do {
                      is_scratched.add(cursor.getString(26));

                  } while (cursor.moveToNext());
              }
          }catch (Exception e){

          }

        return is_scratched;
    }


    public  boolean CheckIsDataAlreadyInDBorNot(String id) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String Query =  "SELECT * FROM "+ TABLE_DETAILS+" WHERE CHALLENGE_ID ='"+id +"' ";
            Cursor cursor = db.rawQuery(Query, null);
            if(cursor.getCount() <= 0){
                cursor.close();
                return false;
            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        return true;
    }
    public void emptyDBBucket()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_DETAILS); //delete all rows in a table
        db.close();
    }
}
