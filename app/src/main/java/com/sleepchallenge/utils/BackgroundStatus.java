package com.sleepchallenge.utils;

import android.app.AlertDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.activities.NewConcentrateActvity;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

public class BackgroundStatus extends Service implements SensorEventListener {
    String starttime, completedtime, challengeid, startdate, enddate,challenge;
    String startdatestr, currentdatestr, enddatestr, sensorcapture_dat;
    public static final int notify = 600000;
    public static final int challengenotify = 60000;
    private Handler sensorHandler = new Handler();
    private Handler challengeHandler = new Handler();
    private Timer sensorTimer = null;
    BackgroundStatusDB challengestatusDB;
    TouchStatusDB touchStatusDB;
    IndividualUpcomingChallengesDB individualUpcomingChallengesDB;
    GroupUpcomingChallengesDB groupUpcomingChallengesDB;
    UserSessionManager userSessionManager;
    String token, user_id, user_type, device_id;
    public static final int notify1 = 900000;
    public static final int accnotify = 900000;
    private Handler serviceHandler = new Handler();
    private Handler accserviceHandler = new Handler();
    private Timer serviceTimer = null;
    private Timer accserviceTimer = null;
    private Timer challengeTimer = null;
    private Handler touchhandler = new Handler();
    private Timer secreenMonitorTimer = null;
    public static final int touchnotify = 900000;
    SensorManager sensorManager;
    Sensor accelometer;
    String noiseValue = "", accvalue = "";
    int timeinsecs;
    ArrayList<Float> noisevaluearr=new ArrayList<>();
    ArrayList<String> noisetimearr=new ArrayList<>();
    ChallengeDatesDB challengeDatesDB;
    private BroadcastReceiver mReceiver = null;
    android.app.Dialog addMoneydialog;
    private float[] mGravity;
    private float mAccel;
    private float mAccelCurrent;
    private float mAccelLast;
    /* constants */
    private static final int POLL_INTERVAL = 300;

    /**
     * running state
     **/
    private boolean mRunning = false;

    /**
     * config state
     **/
    private int mThreshold;

    private PowerManager.WakeLock mWakeLock;
    private Handler mHandler = new Handler();
    private DetectNoise mSensor;
    private Runnable mSleepTask = new Runnable() {
        public void run() {
            Log.v("Noise", "runnable mSleepTask");
            start();
        }
    };

    // Create runnable thread to Monitor Voice
    private Runnable mPollTask = new Runnable() {
        public void run() {
            double amp = mSensor.getAmplitude();
            if ((amp > 0)) {
                detectNoise(amp);
            }
            mHandler.postDelayed(mPollTask, POLL_INTERVAL);
        }
    };

    public BackgroundStatus() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        getDetails();
        sensorManager.registerListener(this, accelometer, SensorManager.SENSOR_DELAY_NORMAL);
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mThreshold = 1;
        mSensor = new DetectNoise();
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, accelometer, SensorManager.SENSOR_DELAY_NORMAL);
        mAccel = 0.00f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;



    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        stopSelf();
        if(mReceiver!=null)
        unregisterReceiver(mReceiver);
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {

        challengeTimer.cancel();
        if(serviceTimer!=null)
        serviceTimer.cancel();
        if(sensorTimer!=null)
        sensorTimer.cancel();
        if(accserviceTimer!=null)
        accserviceTimer.cancel();
        if(secreenMonitorTimer!=null)
        secreenMonitorTimer.cancel();
       /* if(mReceiver!=null)
            unregisterReceiver(mReceiver);*/
        Log.e("Service is Destroyed", "OK");
        super.onDestroy();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        double speed = getAccelerometer(event.values);
        /*if(speed > 0.9 && speed < 1.1) {

        } else {*/
        accvalue = String.valueOf(speed);
        // Toast.makeText(this,"Moving",Toast.LENGTH_SHORT).show();
        // }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    class sensorDisply extends TimerTask {

        @Override
        public void run() {
            sensorHandler.post(new Runnable() {
                @Override
                public void run() {
                    //if(startdatestr.equals(currentdatestr)){
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {

                        SharedPreferences preferences = getSharedPreferences("running", MODE_PRIVATE);
                        String endtime = preferences.getString("completedtime", "");
                        Date star = formatter.parse(endtime);
                        Date current = formatter.parse(sensorcapture_dat);
                        long completed_time = star.getTime();
                        long currenttime = current.getTime();
                        if (currenttime >= completed_time) {
                            Log.v("Challenge","Finished");
                            Log.v("Challenge","//"+challenge);
                            if (challenge.equalsIgnoreCase("GROUP"))
                                groupUpcomingChallengesDB.updatestatus("COMPLETED", challengeid);
                            else
                                individualUpcomingChallengesDB.updatestatus("COMPLETED", challengeid);
                            stopService(new Intent(BackgroundStatus.this, GpsService.class));
                            unregisterReceiver(mReceiver);
                            stop();
                            stopSelf();
                            /*challengeTimer = new Timer();   //recreate new
                            challengeTimer.scheduleAtFixedRate(new getChallenge(), 0, challengenotify);*/
                        } else {

                            if (noiseValue.length() > 0) {
                                sensorcapture_dat = parseDate(Calendar.getInstance().getTime());
                                ContentValues values = new ContentValues();
                                values.put(BackgroundStatusDB.CHALLENGE_ID, challengeid);
                                values.put(BackgroundStatusDB.START_TIME, starttime);
                                values.put(BackgroundStatusDB.END_TIME, completedtime);
                                values.put(BackgroundStatusDB.START_DATE, startdate);
                                values.put(BackgroundStatusDB.END_DATE, enddate);

                                values.put(BackgroundStatusDB.SENSOR_VALUE, noiseValue);
                                values.put(BackgroundStatusDB.SENSOR_CAPTURE_DATE, sensorcapture_dat);
                                // if(accvalue.length()>0){
                                values.put(BackgroundStatusDB.ACC_SENSOR, accvalue);
                                Log.v("Inserting%%%%%%%%", "////" + values.toString());
                                challengestatusDB.addCurrentChallenge(values);
                                /*for(int i=0;i<noisevaluearr.size();i++){
                                    float val=noisevaluearr.get(i);
                                    if(val>=10){
                                        values.put(BackgroundStatusDB.CHALLENGE_ID, challengeid);
                                        values.put(BackgroundStatusDB.START_TIME, starttime);
                                        values.put(BackgroundStatusDB.END_TIME, completedtime);
                                        values.put(BackgroundStatusDB.START_DATE, startdate);
                                        values.put(BackgroundStatusDB.END_DATE, enddate);
                                        values.put(BackgroundStatusDB.SENSOR_VALUE, String.valueOf(noisevaluearr.get(i)));
                                        values.put(BackgroundStatusDB.SENSOR_CAPTURE_DATE, noisetimearr.get(i));
                                        // if(accvalue.length()>0){
                                        values.put(BackgroundStatusDB.ACC_SENSOR, accvalue);
                                        //challengestatusDB.addCurrentChallenge(values);
                                        challengestatusDB.addCurrentChallenge(values);
                                        Log.v("Inserting", "////" + values.toString());
                                    }

                                }*/
                                //}

                                noiseValue = "";
                                accvalue = "";
                                //noisevaluearr.clear();
                              //  noisetimearr.clear();
                           }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    // }

                }
            });
        }
    }

    class serviceCall extends TimerTask {

        @Override
        public void run() {
            serviceHandler.post(new Runnable() {
                @Override
                public void run() {
                    List<String> chalenge_id = challengestatusDB.getChallengeId();
                    List<String> starttimes = challengestatusDB.getStartTime();
                    List<String> endtimes = challengestatusDB.getEndTime();
                    List<String> startdates = challengestatusDB.getStartDates();
                    List<String> enddates = challengestatusDB.getEnddates();
                    List<String> noises = challengestatusDB.getSensorValues();
                    List<String> sensordates = challengestatusDB.getSensorDates();

                    if (NetworkChecking.isConnected(getApplicationContext())) {

                       for (int i = 0; i < chalenge_id.size(); i++) {
                           statusApiCall(chalenge_id.get(i), starttimes.get(i), endtimes.get(i), startdates.get(i), enddates.get(i), noises.get(i), sensordates.get(i));
                        }

                    } else {
                        //Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }


    class accserviceCall extends TimerTask {

        @Override
        public void run() {
            accserviceHandler.post(new Runnable() {
                @Override
                public void run() {
                    List<String> chalenge_id = challengestatusDB.getChallengeId();
                    List<String> starttimes = challengestatusDB.getStartTime();
                    List<String> endtimes = challengestatusDB.getEndTime();
                    List<String> startdates = challengestatusDB.getStartDates();
                    List<String> enddates = challengestatusDB.getEnddates();
                    List<String> acc = challengestatusDB.getAcsensorValues();
                    List<String> sensordates = challengestatusDB.getSensorDates();

                    if (NetworkChecking.isConnected(getApplicationContext())) {
                        for (int i = 0; i < chalenge_id.size(); i++) {
                            accstatusApiCall(chalenge_id.get(i), starttimes.get(i), endtimes.get(i), startdates.get(i), enddates.get(i), acc.get(i), sensordates.get(i));
                        }
                    } else {
                        // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }


    class getChallenge extends TimerTask {

        @Override
        public void run() {
            challengeHandler.post(new Runnable() {
                @Override
                public void run() {
                    String dbchallengeid = challengeDatesDB.getRunningChallenge();
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SharedPreferences preferences = getSharedPreferences("running", MODE_PRIVATE);
                    sensorcapture_dat = parseDate(Calendar.getInstance().getTime());
                    String endti = preferences.getString("completedtime", "");
                    if(endti!=null && endti.length()>0){
                        Date star = null;
                        Date current =null;
                        long completed_time=0;
                        long currenttime=0;
                        try {
                            star = formatter.parse(endti);
                            current = formatter.parse(sensorcapture_dat);
                            completed_time = star.getTime();
                            currenttime = current.getTime();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if (currenttime >= completed_time) {
                            stopSelf();
                        }else{
                            if (dbchallengeid != null && dbchallengeid.length() > 0) {
                                Toast.makeText(BackgroundStatus.this, "ChallengeStarted", Toast.LENGTH_SHORT).show();
                                challengeid = preferences.getString("challengeid", "");
                                challenge = preferences.getString("challenge", "");
                                if (challenge.equalsIgnoreCase("GROUP"))
                                    groupUpcomingChallengesDB.updatestatus("RUNNING", challengeid);
                                else
                                    individualUpcomingChallengesDB.updatestatus("RUNNING", challengeid);
                                String start = preferences.getString("starttime", "");
                                String date[] = start.split(" ");
                                startdate = date[0];
                                starttime = parseTime(start);
                                String endtime = preferences.getString("completedtime", "");
                                String edate[] = start.split(" ");
                                enddate = edate[0];
                                completedtime = parseTime(endtime);
                                startdatestr = parseDate(start);
                                enddatestr = parseDate(endtime);
                                currentdatestr = parseCurrentDate(Calendar.getInstance().getTime());
                               /* if (sensorTimer != null)
                                    sensorTimer.cancel();
                                else*/
                                    sensorTimer = new Timer();   //recreate new
                                sensorTimer.scheduleAtFixedRate(new sensorDisply(), 0, notify);

//                                if (serviceTimer != null) // Cancel if already existed
//                                    serviceTimer.cancel();
//                                else
                                    serviceTimer = new Timer();   //recreate new
                                serviceTimer.scheduleAtFixedRate(new serviceCall(), 0, notify1);
                               /* if (accserviceTimer != null) // Cancel if already existed
                                    accserviceTimer.cancel();
                                else*/
                                    accserviceTimer = new Timer();   //recreate new
                                accserviceTimer.scheduleAtFixedRate(new accserviceCall(), 0, accnotify);

                               /* if (secreenMonitorTimer != null) // Cancel if already existed
                                    secreenMonitorTimer.cancel();
                                else*/
                                    secreenMonitorTimer = new Timer();   //recreate new
                                secreenMonitorTimer.scheduleAtFixedRate(new screenMonitor(), 0, touchnotify);
                                PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                                mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "NoiseAlert");
                                //get Time zone
                                TimeZone tz = TimeZone.getDefault();
                                Calendar cal = GregorianCalendar.getInstance(tz);
                                timeinsecs = (tz.getOffset(cal.getTimeInMillis())) / 1000;
                                start();
                                final IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
                                filter.addAction(Intent.ACTION_SCREEN_OFF);
                                filter.addAction(Intent.ACTION_USER_PRESENT);
                                mReceiver = new ScreenReceiver();
                                registerReceiver(mReceiver, filter);
                                Log.v("TimeC", "????" + completedtime + "//" + challengeid + "//" + starttime);
                                startService(new Intent(BackgroundStatus.this, GpsService.class));
                                challengeTimer.cancel();

                            } else {
                                new sensorDisply().cancel();
                                // new serviceCall().cancel();

                            }
                        }
                    }



                }
            });
        }
    }

    private void statusApiCall(final String challenid, final String starttime, final String endtime, final String startdate, final String enddate, final String senosrv, final String sensordate) {
        Log.v("Service Call", challenid + "///" + starttime + "///" + endtime + "///" + startdate + "///" + enddate + "///" + senosrv + "///" + sensordate);
        String url = AppUrls.BASE_URL + AppUrls.UPDATE_CHALLENGE_STATUS;
        Log.d("TERACKOFINURL", url);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("UPDATE_RESPONSE:", response);

                            String status = jsonObject.getString("response_code");
                            if (status.equals("10100")) {

                                //  Toast.makeText(getApplicationContext(), "Data save successfully...!", Toast.LENGTH_LONG).show();
                               /* JSONObject jobJ = jsonObject.getJSONObject("data");
                                String uniqu_id = jobJ.getString("unique_id");*/
                                challengestatusDB.deleteAllRows();

                            }
                            if (status.equals("10200")) {

                                Toast.makeText(getApplicationContext(), "Invalid Input...!", Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("challenge_id", challenid);
                params.put("user_id", user_id);
                params.put("sensor", "NOISE");
                params.put("sensor_capture_value", senosrv);
                params.put("sensor_capture_date", sensordate);
                params.put("for_date", startdate);
                params.put("device_id", device_id);
                params.put("device_model", android.os.Build.MODEL);
                params.put("device_name", Build.MANUFACTURER);
                params.put("timezone_diff_sec", String.valueOf(timeinsecs));
                Log.d("STATUSPARAM:", params.toString());
                return params;
            }

            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("x-access-token", token);
                headers.put("x-device-id", device_id);
                headers.put("x-device-platform", "ANDROID");
                Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                return headers;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(BackgroundStatus.this);
        requestQueue.add(stringRequest);
    }

    private void accstatusApiCall(final String challenid, final String starttime, final String endtime, final String startdate, final String enddate, final String senosrv, final String sensordate) {
        Log.v("Service Call", challenid + "///" + starttime + "///" + endtime + "///" + startdate + "///" + enddate + "///" + senosrv + "///" + sensordate);
        String url = AppUrls.BASE_URL + AppUrls.UPDATE_CHALLENGE_STATUS;
        Log.d("TERACKOFINURL", url);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("UPDATE_RESPONSE:", response);

                            String status = jsonObject.getString("response_code");
                            if (status.equals("10100")) {

                                //  Toast.makeText(getApplicationContext(), "Data save successfully...!", Toast.LENGTH_LONG).show();
                               /* JSONObject jobJ = jsonObject.getJSONObject("data");
                                String uniqu_id = jobJ.getString("unique_id");*/
                                challengestatusDB.deleteAllRows();

                            }
                            if (status.equals("10200")) {

                                Toast.makeText(getApplicationContext(), "Invalid Input...!", Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("challenge_id", challenid);
                params.put("user_id", user_id);
                params.put("sensor", "ACCELEROMETER");
                params.put("sensor_capture_value", senosrv);
                params.put("sensor_capture_date", sensordate);
                params.put("for_date", startdate);
                params.put("device_id", device_id);
                params.put("device_model", android.os.Build.MODEL);
                params.put("device_name", Build.MANUFACTURER);
                params.put("timezone_diff_sec", String.valueOf(timeinsecs));
                Log.d("STATUSPARAM:", params.toString());
                return params;
            }

            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("x-access-token", token);
                headers.put("x-device-id", device_id);
                headers.put("x-device-platform", "ANDROID");
                Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                return headers;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(BackgroundStatus.this);
        requestQueue.add(stringRequest);
    }


    class screenMonitor extends TimerTask {

        @Override
        public void run() {
            touchhandler.post(new Runnable() {
                @Override
                public void run() {
                    List<String> chalenge_id = touchStatusDB.getChallengeId();
                    List<String> starttimes = touchStatusDB.getStartTime();
                    List<String> endtimes = touchStatusDB.getEndTime();
                    List<String> startdates = touchStatusDB.getStartDates();
                    List<String> enddates = touchStatusDB.getEnddates();
                    List<String> sensordates = touchStatusDB.getSensorDates();
                    List<Integer> touchvalues = touchStatusDB.getScreentouchValues();

                    if (NetworkChecking.isConnected(getApplicationContext())) {
                        if (touchvalues != null && touchvalues.size() > 0) {
                            for (int i = 0; i < touchvalues.size(); i++) {
                                screenstatusApiCall(chalenge_id.get(i), starttimes.get(i), endtimes.get(i), startdates.get(i), enddates.get(i), sensordates.get(i), touchvalues.get(i));
                            }
                        } else {
                            Log.v("Touch", ">>>>>" + "No vvaaa");
                        }

                    } else {
                        // Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }


    private void screenstatusApiCall(final String challenid, final String starttime, final String endtime, final String startdate, final String enddate, final String sensordate, final int touch) {
        Log.v("Service Call", challenid + "///" + starttime + "///" + endtime + "///" + startdate + "///" + enddate + "///" + touch + "///" + sensordate);
        String url = AppUrls.BASE_URL + AppUrls.UPDATE_CHALLENGE_STATUS;
        Log.d("TOUCHURL", url);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("UPDATE_RESPONSE:", response);

                            String status = jsonObject.getString("response_code");
                            if (status.equals("10100")) {

                                //  Toast.makeText(getApplicationContext(), "Data save successfully...!", Toast.LENGTH_LONG).show();
                               /* JSONObject jobJ = jsonObject.getJSONObject("data");
                                String uniqu_id = jobJ.getString("unique_id");*/
                                touchStatusDB.deleteAllRows();

                            }
                            if (status.equals("10200")) {


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("challenge_id", challenid);
                params.put("user_id", user_id);
                params.put("sensor", "TOUCH");
                params.put("sensor_capture_value", String.valueOf(touch));
                params.put("sensor_capture_date", sensordate);
                params.put("for_date", startdate);
                params.put("device_id", device_id);
                params.put("device_model", android.os.Build.MODEL);
                params.put("device_name", Build.MANUFACTURER);
                params.put("timezone_diff_sec", String.valueOf(timeinsecs));
                Log.d("STATUSPARAM:", params.toString());
                return params;
            }

            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("x-access-token", token);
                headers.put("x-device-id", device_id);
                headers.put("x-device-platform", "ANDROID");
                Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                return headers;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(BackgroundStatus.this);
        requestQueue.add(stringRequest);
    }


    private void start() {
        mSensor.start();
        if (!mWakeLock.isHeld()) {
            mWakeLock.acquire();
        }
        mHandler.postDelayed(mPollTask, POLL_INTERVAL);
    }

    private void stop() {
        Log.d("Noise", "==== Stop Noise Monitoring===");
        if (mWakeLock.isHeld()) {
            mWakeLock.release();
        }
        mHandler.removeCallbacks(mSleepTask);
        mHandler.removeCallbacks(mPollTask);
        mSensor.stop();

        mRunning = false;

    }

    private void detectNoise(double signalEMA) {
        // double newKB = Math.round(signalEMA*100.0)/100.0;
        float val = Float.parseFloat(String.valueOf(signalEMA));
        String noise = String.valueOf(round(val, 3));
       // sensorcapture_dat = parseDate(Calendar.getInstance().getTime());
        noiseValue = noise;
     /*  noisevaluearr.add(round(val, 3));
       noisetimearr.add(sensorcapture_dat);*/
       Log.d("SONUND","//"+ noise);

    }

    public String parseTime(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "HH:mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private String parseDate(String date) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "yyyy-MM-dd HH:mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date1 = null;
        String str = null;

        try {
            date1 = inputFormat.parse(date);
            str = outputFormat.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return str;
    }

    private String parseCurrentDate(Date date) {
        String startdate = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date);
        return startdate;

    }

    private String parseDate(Date date) {
        String startdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
        return startdate;

    }

    public static float round(float d, int decimalPlace) {
        return BigDecimal.valueOf(d).setScale(decimalPlace, BigDecimal.ROUND_HALF_UP).floatValue();
    }

    private double getAccelerometer(float[] values) {
        // Movement
        float x = values[0];
        float y = values[1];
        float z = values[2];

        float accelerationSquareRoot =
                (float) ((x * x + y * y + z * z) / (9.80665 * 9.80665));

        return Math.sqrt(accelerationSquareRoot);
    }

    private void getDetails(){
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        challengestatusDB = new BackgroundStatusDB(this);
        challengeDatesDB = new ChallengeDatesDB(this);
        touchStatusDB = new TouchStatusDB(this);
        individualUpcomingChallengesDB = new IndividualUpcomingChallengesDB(this);
        groupUpcomingChallengesDB = new GroupUpcomingChallengesDB(this);
        SharedPreferences backgroundpreferences = getSharedPreferences("background", MODE_PRIVATE);
        SharedPreferences.Editor editor=backgroundpreferences.edit();
        editor.putBoolean("isbackground",true);
        editor.apply();
       /* if (challengeTimer != null) // Cancel if already existed
            challengeTimer.cancel();
        else*/
            challengeTimer = new Timer();   //recreate new
        challengeTimer.scheduleAtFixedRate(new getChallenge(), 0, challengenotify);
    }

}
