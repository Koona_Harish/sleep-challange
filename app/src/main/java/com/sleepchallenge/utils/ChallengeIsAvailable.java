package com.sleepchallenge.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;

public class ChallengeIsAvailable {
    private Context context;
    public ChallengeIsAvailable(Context context) {
            this.context = context;
    }
    public void challengeCheckRemainder() {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            //calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MINUTE, 00);
            calendar.set(Calendar.HOUR, 12);
            calendar.set(Calendar.AM_PM, Calendar.AM);
            Intent intent = new Intent(context, ChallengeReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    context.getApplicationContext(), 1, intent, 0);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY, pendingIntent);

        }

}
