package com.sleepchallenge.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class BackgroundStatusDB extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "runningchallenge.db";
    public static final String TABLE_DETAILS = "challengestatus";

    public static final String CHALLENGE_ID = "challenge_id";
    public static final String START_TIME = "start_time";
    public static final String END_TIME = "end_time";
    public static final String START_DATE = "start_date";
    public static final String END_DATE= "end_date";
    public static final String SENSOR_VALUE= "sensor_value";
    public static final String SENSOR_CAPTURE_DATE= "sensor_capture_date";
    public static final String ACC_SENSOR= "accelometer";
    public static final String SCREEN_TOUCH= "screentouch";

    public BackgroundStatusDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CURR_CHALLENGE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_DETAILS + "("
                + CHALLENGE_ID + " TEXT ,"
                + START_TIME + " TEXT ,"
                + END_TIME + " TEXT ,"
                + START_DATE + " TEXT ,"
                + END_DATE + " TEXT ,"
                + SENSOR_VALUE + " TEXT ,"
                + SENSOR_CAPTURE_DATE + " TEXT ,"
                + ACC_SENSOR + " TEXT ,"
                + SCREEN_TOUCH + " INTEGER " +
                ")";
        db.execSQL(CREATE_CURR_CHALLENGE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void addCurrentChallenge(ContentValues contentValues) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DETAILS, null, contentValues);
        db.close();
    }
    public List<String> getChallengeId() {
        List<String> challeng_id = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                challeng_id.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        return challeng_id;
    }
    public List<String> getStartTime() {
        List<String> starttime = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                starttime.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return starttime;
    }
    public List<String> getEndTime() {
        List<String> end_time = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                end_time.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return end_time;
    }
    public List<String> getStartDates() {
        List<String> startdates = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                startdates.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return startdates;
    }
    public List<String> getEnddates() {
        List<String> enddates = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                enddates.add(cursor.getString(4));

            } while (cursor.moveToNext());
        }
        return enddates;
    }
    public List<String> getSensorValues() {
        List<String> sensorvalues = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                sensorvalues.add(cursor.getString(5));

            } while (cursor.moveToNext());
        }
        return sensorvalues;
    }
    public List<String> getAcsensorValues() {
        List<String> sensorvalues = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                sensorvalues.add(cursor.getString(7));

            } while (cursor.moveToNext());
        }
        return sensorvalues;
    }
    public List<Integer> getScreentouchValues() {
        List<Integer> touchvalues = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                touchvalues.add(cursor.getInt(8));

            } while (cursor.moveToNext());
        }
        return touchvalues;
    }
    public List<String> getSensorDates() {
        List<String> sensordates = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                sensordates.add(cursor.getString(6));

            } while (cursor.moveToNext());
        }
        return sensordates;
    }
    public void deleteAllRows(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_DETAILS);
    }
}
