package com.sleepchallenge.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class WinLossDB extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 5;
    private static final String DATABASE_NAME = "winloss.db";
    public static final String TABLE_DETAILS = "winlossdetails";
    public static final String ID = "id";
    public static final String CHALLENGE_DATA = "challenge_data";

  ;
    public WinLossDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_WINLOSS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_DETAILS + "("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + CHALLENGE_DATA + " VARCHAR "+
               ")";
        db.execSQL(CREATE_WINLOSS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void addData(ContentValues contentValues) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DETAILS, null, contentValues);
        db.close();
    }

    public List<String> getChallengeData() {
        List<String> data=new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                data.add(cursor.getString(cursor.getColumnIndex(CHALLENGE_DATA)));

            } while (cursor.moveToNext());
        }
        return data;
    }
    public void clearDatabase(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_DETAILS);
    }
}
