package com.sleepchallenge.utils;

/**
 * Created by administator on 2/1/2018.
 */

import android.app.Application;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class MyApplication extends Application {

    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
       // Fabric.with(this,new Crashlytics());
        /*final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)           // Enables Crashlytics debugger
                .build();
        Fabric.with(fabric);*/
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }




}