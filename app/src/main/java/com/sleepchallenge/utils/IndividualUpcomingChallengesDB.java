package com.sleepchallenge.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class IndividualUpcomingChallengesDB extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "upcomingchallenges.db";
    private static final String TABLE_DETAILS = "upcomingchallengesdetails";
    public static final String CHALLENGE_ID = "challenge_id";
    public static final String USER_NAME = "user_name";
    public static final String OPPONENT_NAME = "opponent_name";
    public static final String WINNING_STATUS = "winning_status";
    public static final String IS_GROUP_ADMIN = "is_group_admin";
    public static final String STATUS = "status";
    public static final String AMOUNT = "amount";
    public static final String WINNING_AMOUNT = "winning_amount";
    public static final String START_ON = "start_on";
    public static final String PAUSED_ON = "paused_on";
    public static final String RESUME_ON = "resume_on";
    public static final String COMPLETED_ON_TXT = "completed_on_txt";
    public static final String COMPLETED_ON = "completed_on";
    public static final String WINNER = "winner";
    public static final String PAUSED_BY = "paused_by";
    public static final String CHALLENGE_TYPE = "challenge_type";
    public static final String WIN_REWARD_TYPE = "winning_reward_type";
    public static final String WIN_REWARD_VALUE = "winning_reward_value";
    public static final String IS_SCRATCHED = "is_scratched";
   public static final String UPCOMING_COUNT = "upcoming_count";
   public static final String OPPONENT_ID = "opponent_id";

    public IndividualUpcomingChallengesDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public IndividualUpcomingChallengesDB(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ACTIVITY_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_DETAILS + "("
                + CHALLENGE_ID + " TEXT ,"
                + USER_NAME + " TEXT ,"
                + OPPONENT_NAME + " TEXT ,"
                + WINNING_STATUS + " TEXT ,"
                + IS_GROUP_ADMIN + " TEXT ,"
                + STATUS + " TEXT ,"
                + AMOUNT + " TEXT ,"
                + WINNING_AMOUNT + " TEXT ,"
                + START_ON + " TEXT ,"
                + PAUSED_ON + " TEXT ,"
                + RESUME_ON + " TEXT ,"
                + COMPLETED_ON_TXT + " TEXT ,"
                + COMPLETED_ON + " TEXT ,"
                + WINNER + " TEXT ,"
                + PAUSED_BY + " TEXT ,"
                + CHALLENGE_TYPE + " TEXT ,"
                + WIN_REWARD_TYPE + " TEXT ,"
                + WIN_REWARD_VALUE + " TEXT ,"
                + IS_SCRATCHED + " TEXT ,"
                + UPCOMING_COUNT + " TEXT ,"
                + OPPONENT_ID + " TEXT " +
                 ")";
        db.execSQL(CREATE_ACTIVITY_TABLE);

     //db.execSQL("CREATE INDEX idxModel ON serfrd (NAME);");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DETAILS);
        onCreate(db);
    }

    public void addUpcomingChallengesList(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DETAILS, null, contentValues);
        db.close();
    }

    public List<String> getChallengeID() {
        List<String> challenge_id = new ArrayList<String>();
         try {
             String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
             SQLiteDatabase db = this.getWritableDatabase();
             Cursor cursor = db.rawQuery(selectQuery, null);
             if (cursor.moveToFirst()){
                 do {
                     challenge_id.add(cursor.getString(0));

                 } while (cursor.moveToNext());
             }
         }catch (Exception e){
             e.printStackTrace();
         }

        return challenge_id;
    }

    public List<String> getUserName() {
        List<String> user_name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                user_name.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return user_name;
    }



    public List<String> getOpponentName() {
        List<String> opponent_name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                opponent_name.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return opponent_name;
    }

    public List<String> getWinningStatus() {
        List<String> winning_status = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                winning_status.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return winning_status;
    }public List<String> getIsGroupAdmin() {
        List<String> is_group_admin = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                is_group_admin.add(cursor.getString(4));

            } while (cursor.moveToNext());
        }
        return is_group_admin;
    }
    public List<String> getStatus() {
        List<String> status = new ArrayList<String>();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()){
                do {
                    status.add(cursor.getString(5));

                } while (cursor.moveToNext());
            }
        }catch (Exception e){

        }

        return status;
    }public List<String> getAmount() {
        List<String> amount = new ArrayList<String>();
         try {
             String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
             SQLiteDatabase db = this.getWritableDatabase();
             Cursor cursor = db.rawQuery(selectQuery, null);
             if (cursor.moveToFirst()){
                 do {
                     amount.add(cursor.getString(6));

                 } while (cursor.moveToNext());
             }
         }catch (Exception e){

         }

        return amount;
    }
    public List<String> getWinningAmount() {
        List<String> winning_amount = new ArrayList<String>();
           try {
               String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
               SQLiteDatabase db = this.getWritableDatabase();
               Cursor cursor = db.rawQuery(selectQuery, null);
               if (cursor.moveToFirst()){
                   do {
                       winning_amount.add(cursor.getString(7));

                   } while (cursor.moveToNext());
               }
           }catch (Exception e){
               e.printStackTrace();
           }

        return winning_amount;
    }

public List<String> getStartOn() {
        List<String> start_on = new ArrayList<String>();
          try {
              String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
              SQLiteDatabase db = this.getWritableDatabase();
              Cursor cursor = db.rawQuery(selectQuery, null);
              if (cursor.moveToFirst()){
                  do {
                      start_on.add(cursor.getString(8));

                  } while (cursor.moveToNext());
              }
          }catch (Exception e){
              e.printStackTrace();
          }

        return start_on;
    }
public List<String> getPausedOn() {
        List<String> paused_on = new ArrayList<String>();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()){
                do {
                    paused_on.add(cursor.getString(9));

                } while (cursor.moveToNext());
            }
        }catch (Exception e){

        }

        return paused_on;
    }

public List<String> getResumeOn() {
        List<String> resume_on = new ArrayList<String>();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()){
                do {
                    resume_on.add(cursor.getString(10));

                } while (cursor.moveToNext());
            }
        }catch (Exception e){

        }

        return resume_on;
    }

public List<String> getCompletedOnTxt() {
        List<String> completed_on_txt = new ArrayList<String>();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()){
                do {
                    completed_on_txt.add(cursor.getString(11));

                } while (cursor.moveToNext());
            }
        }catch (Exception e){

        }

        return completed_on_txt;
    }
    public List<String> getCompletedOn() {
        List<String> completed_on = new ArrayList<String>();
         try {
             String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
             SQLiteDatabase db = this.getWritableDatabase();
             Cursor cursor = db.rawQuery(selectQuery, null);
             if (cursor.moveToFirst()){
                 do {
                     completed_on.add(cursor.getString(12));

                 } while (cursor.moveToNext());
             }
         }catch (Exception e){

         }

        return completed_on;
    }



    public List<String> getWinner() {
        List<String> winner = new ArrayList<String>();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()){
                do {
                    winner.add(cursor.getString(13));

                } while (cursor.moveToNext());
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return winner;
    }

    public List<String> getPaused_by() {
        List<String> paused_by = new ArrayList<String>();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()){
                do {
                    paused_by.add(cursor.getString(14));

                } while (cursor.moveToNext());
            }
        }catch (Exception e){

        }

        return paused_by;
    }

    public List<String> getChallengeType() {
        List<String> challenge_type = new ArrayList<String>();
          try {
              String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
              SQLiteDatabase db = this.getWritableDatabase();
              Cursor cursor = db.rawQuery(selectQuery, null);
              if (cursor.moveToFirst()){
                  do {
                      challenge_type.add(cursor.getString(15));

                  } while (cursor.moveToNext());
              }
          }catch (Exception e){
              e.printStackTrace();
          }

        return challenge_type;
    }

    public List<String> win_RewardType() {
        List<String> reward_type = new ArrayList<String>();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()){
                do {
                    reward_type.add(cursor.getString(16));

                } while (cursor.moveToNext());
            }
        }catch (Exception e){

        }

        return reward_type;
    }


    public List<String> win_RewardValue() {
        List<String> reward_value = new ArrayList<String>();
         try {
             String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
             SQLiteDatabase db = this.getWritableDatabase();
             Cursor cursor = db.rawQuery(selectQuery, null);
             if (cursor.moveToFirst()){
                 do {
                     reward_value.add(cursor.getString(17));

                 } while (cursor.moveToNext());
             }
         }catch (Exception e){

         }

        return reward_value;
    }


    public List<String> isScratched() {
        List<String> is_scratch = new ArrayList<String>();
         try {
             String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
             SQLiteDatabase db = this.getWritableDatabase();
             Cursor cursor = db.rawQuery(selectQuery, null);
             if (cursor.moveToFirst()){
                 do {
                     is_scratch.add(cursor.getString(18));

                 } while (cursor.moveToNext());
             }
         }catch (Exception e){

         }

        return is_scratch;
    }



    public int getUpcomingingCount() {
        int upcominging_count=0;
          try {
              String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
              SQLiteDatabase db = this.getWritableDatabase();
              Cursor cursor = db.rawQuery(selectQuery, null);
              if (cursor.moveToFirst()){
                  do {
                      upcominging_count=cursor.getInt(19);

                  } while (cursor.moveToNext());
              }
          }catch (Exception e){

          }

        // Log.d("DBRUNNING",""running_count);
        return upcominging_count;
    }

    public List<String> getOpponetIds() {
        List<String> oppids = new ArrayList<String>();
        try {
            String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()){
                do {
                    oppids.add(cursor.getString(20));

                } while (cursor.moveToNext());
            }
        }catch (Exception e){

        }

        return oppids;
    }

    public void emptyDBBucket()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_DETAILS); //delete all rows in a table
        db.close();
    }



    public void openDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    //CLOSE
    public void closeDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    public  boolean CheckIsDataAlreadyInDBorNot(String id) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String Query =  "SELECT * FROM "+ TABLE_DETAILS+" WHERE CHALLENGE_ID ='"+id +"' ";
            Cursor cursor = db.rawQuery(Query, null);
            if(cursor.getCount() <= 0){
                cursor.close();
                return false;
            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        return true;
    }
    public void updatestatus( String status,String id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(STATUS, status);

        db.update(TABLE_DETAILS, values, CHALLENGE_ID+ " = ?",new String[] { id });

        db.close();
    }
}
