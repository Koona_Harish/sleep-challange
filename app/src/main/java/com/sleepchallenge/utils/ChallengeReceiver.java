package com.sleepchallenge.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static android.content.Context.MODE_PRIVATE;

public class ChallengeReceiver extends BroadcastReceiver {
    ChallengeDatesDB challengeDatesDB;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        //Toast.makeText(context,"Check wait for notification",Toast.LENGTH_SHORT).show();
        challengeDatesDB=new ChallengeDatesDB(context);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date currentdate = new Date();
        String cdate=dateFormat.format(currentdate);

        String date=challengeDatesDB.checkChallengeDate(cdate);
        SharedPreferences preferences = context.getSharedPreferences("Timings", MODE_PRIVATE);
         int starttime = preferences.getInt("starttime", 0);
         String datestr=getDateStr(starttime);
         Date start=ConvertToDate(datestr);
        Calendar startcalendar = Calendar.getInstance();
        startcalendar.setTime(start);
        startcalendar.add(Calendar.MINUTE, -15);
        long start_time = startcalendar.getTimeInMillis();
        //Toast.makeText(context,date,Toast.LENGTH_SHORT).show();
        if(date.equalsIgnoreCase(cdate)){
            Intent nointent = new Intent(context, LocalNotificationReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    context.getApplicationContext(), 234324243, nointent, 0);
            AlarmManager alarmManager = (AlarmManager)context. getSystemService(Context.ALARM_SERVICE);
            alarmManager.set(AlarmManager.RTC_WAKEUP, start_time, pendingIntent);
        }


    }

    private String getDateStr(long seconds)
    {
        String datestr="";
        // Create a DateFormatter object for displaying date in specified format.
        long ms = TimeUnit.SECONDS.toMillis(seconds);
        Date date = new Date(ms);
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        datestr = dateformat.format(date);
        return datestr;
    }

    private Date ConvertToDate(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return convertedDate;
    }
}
