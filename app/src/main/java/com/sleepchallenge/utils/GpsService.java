package com.sleepchallenge.utils;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class GpsService extends Service {

    public static final int notify = 180000;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    SharedPreferences sharedPreferences;
    double latitude = 0, longitude = 0;
    CurrentLocationDB currentLocationDB;
    UserSessionManager userSessionManager;
    String token, user_id, user_type, device_id;
    public static final int notify1 = 180000;
    private Handler mHandler1 = new Handler();
    private Timer mTimer1 = null;
    String challengeId="";

    @Override
    public void onCreate() {

        // TODO Auto-generated method stub
        super.onCreate();
//        GPS_FILTER = "MyGPSLocation";
        SharedPreferences preferences=getSharedPreferences("running",MODE_PRIVATE);
        challengeId =   preferences.getString("challengeid","");
        if (mTimer != null) // Cancel if already existed
            mTimer.cancel();
        else
            mTimer = new Timer();   //recreate new
        mTimer.scheduleAtFixedRate(new TimeDisplay(), 0, notify);

        if (mTimer1 != null) // Cancel if already existed
            mTimer1.cancel();
        else
            mTimer1 = new Timer();   //recreate new
        mTimer1.scheduleAtFixedRate(new TimeApiDisplay(), 0, notify1);

        sharedPreferences = getSharedPreferences("Location", MODE_PRIVATE);


        currentLocationDB = new CurrentLocationDB(getApplicationContext());

        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);


    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        mTimer.cancel();    //For Cancel Timer
        mTimer1.cancel();
        Log.e("Service is Destroyed", "OK");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        stopSelf();
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }


    class TimeDisplay extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {

                    if (sharedPreferences.getString("Latitude", "").length() > 0) {
                        latitude = Double.parseDouble(sharedPreferences.getString("Latitude", ""));
                        longitude = Double.parseDouble(sharedPreferences.getString("Longitude", ""));
                    }

                    // display toast
                    GPSTracker gps = new GPSTracker(GpsService.this);

                    // check if GPS enabled
                    if (gps.canGetLocation()) {
                        double newLatitude = gps.getLatitude();
                        double newLongitude = gps.getLongitude();
//                        Log.v("old Lat: " + latitude, "" + "old Long: " + longitude);
//                        Log.v("Lat: " + newLatitude, "" + "Long: " + newLongitude);

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("Latitude", String.valueOf(newLatitude));
                        editor.putString("Longitude", String.valueOf(newLongitude));
                        editor.apply();

                        if (latitude > 0) {
                            Location crntLocation = new Location("crntlocation");
                            crntLocation.setLatitude(latitude);
                            crntLocation.setLongitude(longitude);

                            Location newLocation = new Location("newlocation");
                            newLocation.setLatitude(newLatitude);
                            newLocation.setLongitude(newLongitude);

                            float distance = crntLocation.distanceTo(newLocation);       //   in meters
                            Log.v("distance", " >>> " + distance);

                           if (distance >= 1)
                            {
                                ContentValues values = new ContentValues();
                                values.put(CurrentLocationDB.CHALLENGE_ID, challengeId);
                                values.put(CurrentLocationDB.LAT, newLatitude);
                                values.put(CurrentLocationDB.LONGI, newLongitude);
                                values.put(CurrentLocationDB.CURRENT_TIME, System.currentTimeMillis() / 1000L);
                                currentLocationDB.addCurrentLocation(values);
                            }
                        }

                    } else {
                        // can't get location
                        // GPS or Network is not enabled
                        // Ask user to enable GPS/network in settings
                       // gps.showSettingsAlert();
                    }
                }
            });
        }
    }

    class TimeApiDisplay extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler1.post(new Runnable() {
                @Override
                public void run() {
                    List<String> uniqueId = currentLocationDB.getID(currentLocationDB.TABLE_DETAILS);
                    List<String> chalenge_id = currentLocationDB.getChallengeId(currentLocationDB.TABLE_DETAILS);
                    List<Double> lat = currentLocationDB.getLat(currentLocationDB.TABLE_DETAILS);
                    List<Double> longi = currentLocationDB.getLongi(currentLocationDB.TABLE_DETAILS);
                    if (NetworkChecking.isConnected(getApplicationContext())) {
                        for (int i = 0; i < uniqueId.size(); i++) {
                            trackApiCall(uniqueId.get(i), chalenge_id.get(i), lat.get(i), longi.get(i));
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

    private void trackApiCall(final String id, final String cid, final Double lat, final Double longi) {
        String url = AppUrls.BASE_URL + AppUrls.GPS_TRACKING;
        Log.d("TERACKOFINURL", url);
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("TRACK_RESPONSE:", response);

                            String status = jsonObject.getString("response_code");
                            if (status.equals("10100")) {

                              //  Toast.makeText(getApplicationContext(), "Data save successfully...!", Toast.LENGTH_LONG).show();
                                JSONObject jobJ = jsonObject.getJSONObject("data");
                                String uniqu_id = jobJ.getString("unique_id");
                                currentLocationDB.deleteAllRows(currentLocationDB.TABLE_DETAILS);

                            }
                            if (status.equals("10200")) {

                                Toast.makeText(getApplicationContext(), "Invalid Input...!", Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("challenge_id", challengeId);
                params.put("latitude", String.valueOf(lat));
                params.put("longitude", String.valueOf(longi));
                params.put("unique_id", String.valueOf(id));
                params.put("user_id", user_id);
                Log.d("TRACKPARAM:", params.toString());
                return params;
            }

            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("x-access-token", token);
                headers.put("x-device-id", device_id);
                headers.put("x-device-platform", "ANDROID");
                Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                return headers;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }
}