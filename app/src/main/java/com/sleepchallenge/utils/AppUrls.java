package com.sleepchallenge.utils;

public class AppUrls {
    //http://activity-challenge.com/sleep_challenge/
    public static String BASE_URL = "http://activity-challenge.com:8091/api/v1/";
    public static String BASE_IMAGE_URL = "http://activity-challenge.com/sleep_challenge/";

    public static String SHARE_APP_URL= "http://www.sleep-challenge.com/share";

    public static String REGISTRATION = "register";
    public static String SOCIAL_LOGIN_STATUS = "social_login";
    public static String SENDOTP = "send_otp";
    public static String VERIFYOTP = "verify_otp";
    public static String LOGIN = "verify_login";
    public static String FORGOTPASSWORD = "forget_password";
    public static String VERIFY_FORGETPASSWORD_OTP = "verify_fp_otp";
    public static String CHANGEPASSWORD = "change_pwd";
    public static String INITIAL_PAYMENT = "payment/initial_payment";
    public static String GET_STATUS = "user/session_data?user_id=";
    public static String CONTACT_US_LINK = "web/contact-us";
    public static String HELP_LINK = "web/help";
    public static String FEEDBACK = "user/feedback";
    public static String FEEDBACK_HISTORY = "user/feedback?id=";
    public static String TERMS_CONDITION_LINK = "web/tnc";
    public static String USER_BASIC_DETAIL = "user/get_basic_details?user_id=";
    public static String ALL_MEMBERS_LIST = "user";
    public static String CHALLENGE_PRICES = "activity/challenges/prices";
    public static String CHALLENGE_TO_USER = "user/challenges/users?user_id=";
    public static String USER_TYPE = "&user_type=";
    public static String COUNTS = "user/counts?user_id=";
    public static String CREATE_GROUP = "group/create";
    public static String MEMBERS_ADD_GROUP = "group/members_mst?user_id=";
    public static String GET_GROUPS = "group";
    public static String GET_GROUPS_FOR_CHALLENGE = "user/challenges/groups";

    public static String WALLET_AMOUNT = "payment/wallet_amount?user_id=";
    public static String SEND_CHALLENGES = "user/challenges";
    public static String GET_CHALLENGE_REQUEST = "user/challenges/request";

    public static String ACEPT_CHALLENGE_REQUEST = "user/challenges/accept";
    public static String REJECT_CHALLENGE_REQUEST = "user/challenges/reject";

    public static String MY_SEND_CHALLENGE_REQUEST = "user/challenges/my_send_challenges";
    public static String MY_SEND_NCEL_CHALLENGE = "user/challenges/my_challenge_cancellation";

    public static String MY_CHALLENGES = "user/challenges/my_challenges?";
    public static String PAUSE_CHALLENGE = "user/challenges/pause";
    public static String CHALLANGE_COUNT = "user/challenges/applied/count?user_id=";
    public static String CHALLENGE_TYPE="&challenge_type=";
    public static String CHALLENGE_DATE="&challenge_date=";
    public static String MORECHALLENGES="user/challenges";


    public static String ALL_SPONSOR_LIST="sponsoring/all";
    public static String GET_USER_MEMBER_DETAIL="user/member_details";

    public static String MEMBER_FOLLOWING_STATUS = "user/following_status";
    public static String FOLLOW_USER = "user/follow_user";
    public static String UNFOLLOW_USER = "user/unfollow_user";

    public static String BLOCK_USER = "user/block_user";
    public static String UNBLOCK_USER = "user/unblock_user";

    public static String MY_FOLLOWING = "user/followings";
    public static String MY_FOLLOWERS = "user/followers";
    public static String GROUP_FOLLOWING_STATUS = "group/following_status";
    public static String GROUP_FOLLOW = "group/follow_group";
    public static String GROUP_UNFOLLOW = "group/unfollow_group";

    public static String GROUP_DELETE = "group/";
    public static String GET_GROUP_MEMEBER_STATUS = "group/get_members_status";
    public static String GROUP_UPDATE_PROFILE_PIC = "group/update_group_pic";
    public static String GROUP_UPDATE = "group/update";
    public static String GROUP_DEL = "group/";
    public static String GROUP_UPDATE_NAME = "group/update_name";
    public static String GROUP_DETIL_MEMBER_MORE = "group/members";

    public static String REFERAL_code = "user/refercode?user_id=";
    public static String RECENT_PAYMETN_HISTORY ="payment/get_recent_payment";
    public static String UPDATE_PROFILE_PIC = "user/update_profile_pic";
    public static String UPDATE_PROFILE = "user/update";

    public static String USER_WALLET_PAY = "payment/money/add";    //ADD MONEY TO WALLET
    public static String TRANSFER_AMOUNT_TO_PAYPAL_ACC = "payment/paypal/payout";   //SEND MONEY TO PAYPAL
    public static String CHALLENGE_STATUS = "user/challenges/status_details?challenge_id=";
    public static String UPDATE_CHALLENGE_STATUS = "user/challenges/details";

    public static String SPONSOR_REQUEST = "sponsoring/?id=";
    public static String SPONSOR_PAY = "sponsoring";

    public static String ACCEPT_SPONSOR = "sponsoring/accept_sponsor_request";
    public static String REJECT_SPONSOR = "sponsoring/reject_sponsor_request";
    public static String MY_SPONSORINGS = "sponsoring/my_sponsoring";
    public static String TYPE = "&type=";
    public static String WIN_LOSS_CHALLENGES = "user/challenges/win_loss";

    public static String SEND_VERIFICATION_EMAIL = "user/send-verification-email";
    public static String ADD_PROMOTE = "promotion/add_promote";
    public static String MY_PROMOTIONS = "promotion/get_my_promotions?id=";
    public static String PROMOTION_PAY = "promotion/pay_promotion";

    public static String USER_DEACTIVATE_REASON = "user/deactivate_reasons";
    public static String DEACTIVATE_USER_DETAIL = "user/deactivate_details/";
    public static String DEACTIVATE_USER = "user/deactivate_user/";
    public static String SET_USER_LOCATION = "user/set_location";
    public static String GPS_TRACKING = "user/challenges/latlong";

    public static String GET_NOTIFICATION_MESSAGE = "notification/chat/user";
    public static String GET_NOTIFICATION_SEND_MSG = "notification/messages/send";
    public static String NOTIFICATION_CONVERSATION = "notification/messages/conversation";

    public static String ABUSE_TYPE = "user/abuse_type";
    public static String REPORT_ABUSE= "user/report_abuse";

    public static String GROUP_LIST = "notification/chat/group";
    public static String GROUP_SEND_MESSAGES = "notification/chat/send";
    public static String GROUP_MESSAGES_CONVERSATION = "notification/chat/conversation";
    public static String GRAPH_DATA = "user/challenges/graph_data?user_id=";


    public static String GET_SCRATCHCARDS = "payment/scratch_card";
    public static String SAFTER_CRATCH_CARD_DONE ="payment/scratch_card/do_scratch";
    public static String GET_CONCEN_CHALLNGE_STATUS ="user/challenges/concentration";
    public static String GET_CONCEN_CHALLNGE_RESULT ="user/challenges/concentration/result";


    public static String PROMOTION = "promotion";

    public static String FCM = "activity/user/fcm";
    public static String GET_NOTIFICATION = "notification/system?user_id=";




}
