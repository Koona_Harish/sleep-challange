package com.sleepchallenge.utils;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.MODE_PRIVATE;

public class ScreenReceiver extends BroadcastReceiver {

    public static boolean wasScreenOn = true;
    ChallengeDatesDB challengeDatesDB;
    String starttime, completedtime, challengeid, startdate, enddate;
    String startdatestr, currentdatestr, enddatestr, sensorcapture_dat;
    int timeinsecs;
    Context ctx;
    String token, user_id, user_type, device_id;
    TouchStatusDB touchStatusDB;

    int touch;//0-OFF,1-ON,2-UNLOCK
    UserSessionManager userSessionManager;
    @Override
    public void onReceive(final Context context, final Intent intent) {
        ctx=context;
        challengeDatesDB = new ChallengeDatesDB(context);
        touchStatusDB = new TouchStatusDB(context);
        userSessionManager=new UserSessionManager(ctx);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            // do whatever you need to do here
            touch=0;
            Log.v("Screen","OFF");
            wasScreenOn = false;
            touchScreen();
        }else if(intent.getAction().equals(Intent.ACTION_USER_PRESENT))
        {
            touch=1;
            Log.v("Screen","UnLock");
            touchScreen();
//Handle resuming events
        }


    }

    private void  touchScreen(){
        String dbchallengeid = challengeDatesDB.getRunningChallenge();
        SharedPreferences preferences = ctx.getSharedPreferences("running", MODE_PRIVATE);
        Log.v("TOUCHChalllenge", "????" + dbchallengeid);
        //if (dbchallengeid != null && dbchallengeid.length() > 0) {
            challengeid = preferences.getString("challengeid", "");
            String start = preferences.getString("starttime", "");
            String date[] = start.split(" ");
            startdate = date[0];
            starttime = parseTime(start);
            String endtime = preferences.getString("completedtime", "");
            String edate[] = start.split(" ");
            enddate = edate[0];
            completedtime = parseTime(endtime);
            startdatestr = parseDate(start);
            enddatestr = parseDate(endtime);
            currentdatestr = parseCurrentDate(Calendar.getInstance().getTime());
            //get Time zone
            TimeZone tz = TimeZone.getDefault();
            Calendar cal = GregorianCalendar.getInstance(tz);
            timeinsecs = (tz.getOffset(cal.getTimeInMillis())) / 1000;
            savelocalDB();
            /*try {
                secreenMonitorTimer = new Timer();   //recreate new
                secreenMonitorTimer.scheduleAtFixedRate(new screenMonitor(), 0, touchnotify);
            }catch (Exception e){
                e.printStackTrace();
            }*/

        /*} else {


        }*/
    }

    private void savelocalDB(){
        sensorcapture_dat = parseDate(Calendar.getInstance().getTime());
        ContentValues values = new ContentValues();
        values.put(TouchStatusDB.CHALLENGE_ID, challengeid);
        values.put(TouchStatusDB.START_TIME, starttime);
        values.put(TouchStatusDB.END_TIME, completedtime);
        values.put(TouchStatusDB.START_DATE, startdate);
        values.put(TouchStatusDB.END_DATE, enddate);
        values.put(TouchStatusDB.SENSOR_CAPTURE_DATE, sensorcapture_dat);
        values.put(TouchStatusDB.SCREEN_TOUCH, touch);

        touchStatusDB.addChallenge(values);
       Log.v("Insertingtouch", "////" + values.toString());
         touch=0;
    }
    public String parseTime(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "HH:mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private String parseDate(String date) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "yyyy-MM-dd HH:mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date1 = null;
        String str = null;

        try {
            date1 = inputFormat.parse(date);
            str = outputFormat.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return str;
    }
    private String parseCurrentDate(Date date) {
        String startdate = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date);
        return startdate;

    }
    private String parseDate(Date date) {
        String startdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
        return startdate;

    }
}

