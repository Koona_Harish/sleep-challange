package com.sleepchallenge.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class ProfileDB extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = "userprofile.db";
    public static final String TABLE_DETAILS = "userprofile";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String Gender = "gender";
    public static final String EMAIL = "email";
    public static final String IS_EMAIL_VERIFIED = "is_email_verified";
    public static final String MOBILE = "mobile";
    public static final String REFER_CODE = "refer_code";
    public static final String OVERALL_RANK = "overall_rank";
    public static final String FOLLOWERS_COUNT = "followers_cnt";
    public static final String FOLLOWING_USERS_COUNT = "following_users_cnt";
    public static final String SPONSOR_BY_COUNT = "sponsor_by_cnt";
    public static final String WALLET_AMOUNT = "wallet_amt";
    public static final String PROFILE_PIC = "profile_pic";
    public static final String PAYPAL_REGISTERED_EMAIL = "paypal_registered_email";

    public ProfileDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PROFILE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_DETAILS + "("
                + FIRST_NAME + " TEXT ,"
                + LAST_NAME + " TEXT ,"
                + Gender + " TEXT ,"
                + EMAIL + " TEXT ,"
                + IS_EMAIL_VERIFIED + " INTEGER ,"
                + MOBILE + " TEXT ,"
                + REFER_CODE + " TEXT ,"
                + OVERALL_RANK + " TEXT ,"
                + FOLLOWERS_COUNT + " TEXT ,"
                + FOLLOWING_USERS_COUNT + " TEXT ,"
                + SPONSOR_BY_COUNT + " TEXT ,"
                + WALLET_AMOUNT + " TEXT ,"
                + PROFILE_PIC + " TEXT ,"
                + PAYPAL_REGISTERED_EMAIL + " TEXT " +

                ")";
        db.execSQL(CREATE_PROFILE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void addUserData(ContentValues contentValues) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DETAILS, null, contentValues);
        db.close();
    }
    public String getFirstName() {
        String firstname="";

        String selectQuery = "SELECT "+ FIRST_NAME + " FROM "+ TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                firstname =  cursor.getString(cursor.getColumnIndex(FIRST_NAME));

            } while (cursor.moveToNext());
        }
        return firstname;
    }
    public String getLirstName() {
        String lastname="";
        String selectQuery = "SELECT "+ LAST_NAME + " FROM "+ TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                lastname =  cursor.getString(cursor.getColumnIndex(LAST_NAME));

            } while (cursor.moveToNext());
        }
        return lastname;
    }

    public String getGender() {
        String gender="";

        String selectQuery = "SELECT "+ Gender + " FROM "+ TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                gender =  cursor.getString(cursor.getColumnIndex(Gender));

            } while (cursor.moveToNext());
        }
        return gender;
    }
    public String getEmail() {
        String email="";

        String selectQuery = "SELECT "+ EMAIL + " FROM "+ TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                email =  cursor.getString(cursor.getColumnIndex(EMAIL));

            } while (cursor.moveToNext());
        }
        return email;
    }
    public int getisEmailVerifired() {
        int emailverifired=0;

        String selectQuery = "SELECT "+ IS_EMAIL_VERIFIED + " FROM "+ TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                emailverifired =  cursor.getInt(cursor.getColumnIndex(IS_EMAIL_VERIFIED));

            } while (cursor.moveToNext());
        }
        return emailverifired;
    }
    public String getMobileNo() {
        String mobileno="";

        String selectQuery = "SELECT "+ MOBILE + " FROM "+ TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                mobileno =  cursor.getString(cursor.getColumnIndex(MOBILE));

            } while (cursor.moveToNext());
        }
        return mobileno;
    }
    public String getReferCode() {
        String refercode="";
        String selectQuery = "SELECT "+ REFER_CODE + " FROM "+ TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                refercode =  cursor.getString(cursor.getColumnIndex(REFER_CODE));

            } while (cursor.moveToNext());
        }
        return refercode;
    }
    public String getFollowingUsersCount() {
        String followingusercount="";

        String selectQuery = "SELECT "+ FOLLOWING_USERS_COUNT + " FROM "+ TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                followingusercount =  cursor.getString(cursor.getColumnIndex(FOLLOWING_USERS_COUNT));

            } while (cursor.moveToNext());
        }
        return followingusercount;
    }
    public String getFollowersCount() {
        String followerscount="";
        String selectQuery = "SELECT "+ FOLLOWERS_COUNT + " FROM "+ TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                followerscount =  cursor.getString(cursor.getColumnIndex(FOLLOWERS_COUNT));

            } while (cursor.moveToNext());
        }
        return followerscount;
    }
    public String getSponsorByCount() {
        String sponsorbycount="";

        String selectQuery = "SELECT "+ SPONSOR_BY_COUNT + " FROM "+ TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                sponsorbycount =  cursor.getString(cursor.getColumnIndex(SPONSOR_BY_COUNT));

            } while (cursor.moveToNext());
        }
        return sponsorbycount;
    }
    public String getWalletAmount() {
        String amount="";

        String selectQuery = "SELECT "+ WALLET_AMOUNT + " FROM "+ TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                amount =  cursor.getString(cursor.getColumnIndex(WALLET_AMOUNT));

            } while (cursor.moveToNext());
        }
        return amount;
    }
    public String getProfilePic() {
        String profilepic="";
        String selectQuery = "SELECT "+ PROFILE_PIC + " FROM "+ TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                profilepic =  cursor.getString(cursor.getColumnIndex(PROFILE_PIC));

            } while (cursor.moveToNext());
        }
        return profilepic;
    }
    public String getOverallRank() {
        String overalrank="";

        String selectQuery = "SELECT "+ OVERALL_RANK + " FROM "+ TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                overalrank =  cursor.getString(cursor.getColumnIndex(OVERALL_RANK));

            } while (cursor.moveToNext());
        }
        return overalrank;
    }
    public String getPaypalRegisteredEmail() {
        String registeredmail="";
        String selectQuery = "SELECT "+ PAYPAL_REGISTERED_EMAIL + " FROM "+ TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                registeredmail =  cursor.getString(cursor.getColumnIndex(PAYPAL_REGISTERED_EMAIL));

            } while (cursor.moveToNext());
        }
        return registeredmail;
    }


    public void deleteAllRows(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_DETAILS);
    }
}
