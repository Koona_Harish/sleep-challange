package com.sleepchallenge.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class CurrentLocationDB extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 6;
    private static final String DATABASE_NAME = "currentlocation.db";
    public static final String TABLE_DETAILS = "currentloc_table";
    public static final String TABLE_DETAILS_GROUP = "currentloc_table_group";

    public static final String ID = "ID";
    public static final String CHALLENGE_ID = "challenge_id";
    public static final String LAT = "lat";
    public static final String LONGI = "longi";
    public static final String CURRENT_TIME = "current_time_date";

    public CurrentLocationDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CURR_LOC_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_DETAILS + "("
                + CHALLENGE_ID + " TEXT ,"
                + LAT + " VARCHAR ,"
                + LONGI + " VARCHAR ,"
                + CURRENT_TIME + " VARCHAR " +
                ")";
        db.execSQL(CREATE_CURR_LOC_TABLE);
        db.execSQL("CREATE INDEX index_name ON " + TABLE_DETAILS + "(" + CURRENT_TIME + ")");

        String CREATE_CURR_LOC_TABLE_GROUP = "CREATE TABLE IF NOT EXISTS " + TABLE_DETAILS_GROUP + "("
                + CHALLENGE_ID + " TEXT ,"
                + LAT + " VARCHAR ,"
                + LONGI + " VARCHAR ,"
                + CURRENT_TIME + " VARCHAR " +
                ")";
        db.execSQL(CREATE_CURR_LOC_TABLE_GROUP);
        db.execSQL("CREATE INDEX index_name_group ON " + TABLE_DETAILS_GROUP + "(" + CURRENT_TIME + ")");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DETAILS_GROUP);
        onCreate(db);
    }

    public void addCurrentLocation(ContentValues contentValues) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DETAILS, null, contentValues);
        db.close();
    }

    public void addCurrentLocationGroup(ContentValues contentValues) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DETAILS_GROUP, null, contentValues);
        db.close();
    }

    public List<String> getChallengeId(String tableName) {
        List<String> challeng_id = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + tableName;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                challeng_id.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        return challeng_id;
    }

    public List<Double> getLat(String tableName) {
        List<Double> latitude = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + tableName;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                latitude.add(cursor.getDouble(1));

            } while (cursor.moveToNext());
        }
        return latitude;
    }

    public List<Double> getLongi(String tableName) {
        List<Double> longitutede = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + tableName;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                longitutede.add(cursor.getDouble(2));

            } while (cursor.moveToNext());
        }
        return longitutede;
    }

    public List<String> getID(String tableName) {
        List<String> id = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + tableName;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                id.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return id;
    }

    public void deleteAllRows(String tableName){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ tableName);
    }

}
