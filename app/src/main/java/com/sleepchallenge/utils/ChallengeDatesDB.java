package com.sleepchallenge.utils;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.ListView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ChallengeDatesDB extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "challengedates.db";
    public static final String TABLE_DETAILS = "challengedates";
    public static final String CHALLENGE_ID = "challenge_id";
    public static final String CHALLENGE_DATE = "challenge_date";
    public static final String START_TIME = "start_time";
    public static final String END_TIME = "end_time";
    public static final String CHALLENGE = "challenge";

    Context context;
    public ChallengeDatesDB(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CHALLENGE_DATETABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_DETAILS + "("
                + CHALLENGE_ID + " TEXT ,"
                + CHALLENGE_DATE + " TEXT ,"
                + START_TIME + " INTEGER ,"
                + END_TIME + " INTEGER ,"
                + CHALLENGE + " TEXT " +

               ")";

        String CREATE_CHALLENGE_INDEX= "CREATE INDEX time_index ON "+TABLE_DETAILS+"("+START_TIME+","+END_TIME+")";
        db.execSQL(CREATE_CHALLENGE_DATETABLE);
        db.execSQL(CREATE_CHALLENGE_INDEX);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void interstChallengeDates(ContentValues contentValues) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DETAILS, null, contentValues);
        db.close();
    }
    public String getRunningChallenge(){
        Calendar startcalendar = Calendar.getInstance();
        Calendar endcalendar = Calendar.getInstance();
        Date currenttime=Calendar.getInstance().getTime();
        long se=currenttime.getTime();
        int current = (int) TimeUnit.MILLISECONDS.toSeconds(se);
        String challengeid="",challenge="";
        String sqlQuery= "SELECT * FROM "+ TABLE_DETAILS+" WHERE START_TIME <= "+current+" AND END_TIME >= "+ current +" ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sqlQuery, null);
        if (cursor.moveToFirst()){
            do {

                challengeid =  cursor.getString(cursor.getColumnIndex(CHALLENGE_ID));
                challenge =  cursor.getString(cursor.getColumnIndex(CHALLENGE));
                int starttime =  cursor.getInt(cursor.getColumnIndex(START_TIME));
                int endtime =  cursor.getInt(cursor.getColumnIndex(END_TIME));
                startcalendar.setTimeInMillis(TimeUnit.SECONDS.toMillis(starttime));
                endcalendar.setTimeInMillis(TimeUnit.SECONDS.toMillis(endtime));
                SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String startdate=dateformat.format(startcalendar.getTime());
                String enddate=dateformat.format(endcalendar.getTime());
                Log.v("QueryInsideDate","...."+startdate+"///"+enddate+"///"+starttime+"///"+endtime);
                SharedPreferences preferences=context.getSharedPreferences("running", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=preferences.edit();
                editor.putString("challengeid",challengeid);
                editor.putString("starttime",startdate);
                editor.putString("completedtime",enddate);
                editor.putString("challenge",challenge);
                editor.apply();


            } while (cursor.moveToNext());
        }
        Log.v("ChallengeID",">>>>"+challengeid);
        return challengeid;
    }

    public String checkChallengeDate(String date){
        SQLiteDatabase db = this.getWritableDatabase();
        String challengedate="";
       String sqlQuery= "SELECT * FROM "+ TABLE_DETAILS+" WHERE CHALLENGE_DATE ='"+date +"' ";
        Cursor cursor = db.rawQuery(sqlQuery, null);
        if (cursor.moveToFirst()){
            do {
                challengedate =  cursor.getString(cursor.getColumnIndex(CHALLENGE_DATE));
                int starttime =  cursor.getInt(cursor.getColumnIndex(START_TIME));
                String challengeid =  cursor.getString(cursor.getColumnIndex(CHALLENGE_ID));
                SharedPreferences preferences=context.getSharedPreferences("Timings", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=preferences.edit();
                editor.putInt("starttime",starttime);
                editor.putString("challengeid",challengeid);
                editor.apply();
            } while (cursor.moveToNext());
        }
        return challengedate;
    }

}
