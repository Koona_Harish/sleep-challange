package com.sleepchallenge.adapters;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.activities.AllUpcomingChallengesActivity;
import com.sleepchallenge.holders.AllChallangesMoreHolder;
import com.sleepchallenge.itemclicklisteners.AllChalngMoreItemClickListener;
import com.sleepchallenge.models.AllChallengesStatusModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AllChallangUpcomingAdapter extends RecyclerView.Adapter<AllChallangesMoreHolder> {

    public ArrayList<AllChallengesStatusModel> allChalngMorelistModels;
    AllUpcomingChallengesActivity context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont, thinFont, specialFont;
    UserSessionManager session;
    String user_name, send_pauseVal, device_id, token, challange_id, Resume;
    Dialog dialog;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String Paused_by, challegne_type;

    public AllChallangUpcomingAdapter(ArrayList<AllChallengesStatusModel> allChalngMorelistModels, AllUpcomingChallengesActivity context, int resource) {

        this.allChalngMorelistModels = allChalngMorelistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        regularFont = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.specialFont));

        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        checkInternet = NetworkChecking.isConnected(context);
        pprogressDialog = new ProgressDialog(context);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
    }

    @Override
    public AllChallangesMoreHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        AllChallangesMoreHolder slh = new AllChallangesMoreHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final AllChallangesMoreHolder holder, final int position) {

        Resume = parseDateToddMMyyyy(allChalngMorelistModels.get(position).getResume_on());
        challegne_type = allChalngMorelistModels.get(position).getChallenge_type();
        String str_opponentname = allChalngMorelistModels.get(position).getOpponent_name();
        String converted_string = str_opponentname.substring(0, 1).toUpperCase() + str_opponentname.substring(1);

        String usernem = allChalngMorelistModels.get(position).getUser_name();
        String converted_user_naem = usernem.substring(0, 1).toUpperCase() + usernem.substring(1);

        String paused_by = allChalngMorelistModels.get(position).getPaused_by();
        if (!paused_by.equals("")) {
            Paused_by = paused_by.substring(0, 1).toUpperCase() + paused_by.substring(1);
        }

        String status = allChalngMorelistModels.get(position).getStatus();

        if (status.equals("PAUSED")) {
            holder.pause_challenge.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Paused by : " + Paused_by);
            holder.won_cash.setVisibility(View.GONE);
        }

        holder.all_challenger_names.setText(Html.fromHtml(converted_user_naem));
        holder.oponent_names.setText(Html.fromHtml(converted_string));
        holder.vsText.setTypeface(thinFont);

        holder.all_cash_prize_amount.setText(("$" + allChalngMorelistModels.get(position).getAmount()));

        String timedate = parseDateToddMMyyyy(allChalngMorelistModels.get(position).getStart_on());
        holder.all_dateandtime.setText(Html.fromHtml(timedate));

        challegne_type = allChalngMorelistModels.get(position).getChallenge_type();
        if (challegne_type.equals("DAILY")) {
            holder.challenge_type.setText(challegne_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.daily_bg));
        } else if (challegne_type.equals("LONGRUN")) {
            holder.challenge_type.setText(challegne_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.longrun_bg));
        } else {
            holder.challenge_type.setText(challegne_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.week_bg));
        }


        String PAUSE_STATUS = allChalngMorelistModels.get(position).getPause_access();
        if (PAUSE_STATUS.equals("0")) {
            holder.pause_challenge.setVisibility(View.GONE);
        } else {
            holder.pause_challenge.setVisibility(View.VISIBLE);
        }
        if (challegne_type.equalsIgnoreCase("SPECIAL_EVENT"))
            holder.pause_challenge.setVisibility(View.GONE);
        if (status.equals("YETTOSTART")) {
            holder.all_status.setText(Html.fromHtml("UPCOMING"));
        } else {
            holder.all_status.setText(Html.fromHtml(allChalngMorelistModels.get(position).getStatus()));
        }

        holder.all_status.setBackgroundColor(Color.parseColor("#DD6B55"));
        holder.all_status.setTextColor(Color.parseColor("#ffffff"));


        /*Picasso.with(context)
                .load(allChalngMorelistModels.get(position).getActivity_image())
                .placeholder(R.drawable.no_image_found)
                .into(holder.all_activity_image);*/

        holder.share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take a Challenge with -");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I'm inviting you to join ActivityChallenge App. To stay active and earn money Why don't you join?. " + AppUrls.SHARE_APP_URL);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });


        holder.setItemClickListener(new AllChalngMoreItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {


            }
        });

        holder.pause_challenge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.pause_challenge_custom_dialog);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                TextView ok_pause_btn = (TextView) dialog.findViewById(R.id.ok_pause_btn);

                final RadioGroup radio_pause_group = (RadioGroup) dialog.findViewById(R.id.radio_pause_group);
                dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);
                ok_pause_btn.setOnClickListener(new View.OnClickListener() {
                    @Override

                    public void onClick(View v) {
                        if (checkInternet) {
                            RadioButton radioPPButton;
                            int selectedId = radio_pause_group.getCheckedRadioButtonId();
                            radioPPButton = (RadioButton) dialog.findViewById(selectedId);
                            switch (selectedId) {
                                case R.id.radio_buttn_onewk:

                                    send_pauseVal = "1_WEEK";
                                    break;

                                case R.id.radio_buttn_twowk:

                                    send_pauseVal = "2_WEEK";
                                    break;

                                case R.id.radio_buttn_onemonth:
                                    send_pauseVal = "1_MONTH";
                                    break;

                                case R.id.radio_buttn_threemonth:
                                    send_pauseVal = "3_MONTH";
                                    break;

                                case R.id.radio_buttn_sixmonth:
                                    send_pauseVal = "6_MONTH";
                                    break;

                                case R.id.radio_buttn_oneyr:
                                    send_pauseVal = "1_YEAR";
                                    break;
                            }
                            challange_id = allChalngMorelistModels.get(position).getChallenge_id();
                            sendReqToPauseChallng(challange_id, send_pauseVal);
                        } else {
                            Toast.makeText(context, "No Internet Connection..!", Toast.LENGTH_LONG).show();
                        }

                    }

                });
                dialog.show();

            }
        });
    }

    private void sendReqToPauseChallng(final String challange_id, final String send_pauseVal) {
        if (checkInternet) {
            Log.d("PAUSEURL:", AppUrls.BASE_URL + AppUrls.PAUSE_CHALLENGE);
            StringRequest forgetReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.PAUSE_CHALLENGE, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("PAUSERESP", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100")) {
                            Intent intent = new Intent(context, AllUpcomingChallengesActivity.class);
                            context.startActivity(intent);
                            context.finish();

                            //  context.getAllUpcomChallangesMore(0);
                            pprogressDialog.dismiss();
                            Toast.makeText(context, "  Challenge Paused successfully,\n will be Resume on " + Resume, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();

                        }
                        if (successResponceCode.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(context, "Invalid Input..", Toast.LENGTH_SHORT).show();
                        }
                        if (successResponceCode.equals("10300")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(context, " Invalid Challenge..", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.cancel();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("challenge_id", challange_id);
                    params.put("time_span", send_pauseVal);

                    Log.d("PAUSEPARAM:", params.toString());
                    return params;

                }
            };

            forgetReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(forgetReq);
        } else {
            Toast.makeText(context, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public int getItemCount() {
        return this.allChalngMorelistModels.size();
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


}
