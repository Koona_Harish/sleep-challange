package com.sleepchallenge.adapters;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sleepchallenge.R;
import com.sleepchallenge.activities.ChallengeStatusActivity;
import com.sleepchallenge.fragments.MyChallengesIndividualFragment;
import com.sleepchallenge.holders.ChallengeIndividRunHolder;
import com.sleepchallenge.itemclicklisteners.ChallengeIndividRunItemClickListener;
import com.sleepchallenge.models.ChallengeIndividualRunningModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.UserSessionManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


public class ChallengeIndividRunAdapter extends RecyclerView.Adapter<ChallengeIndividRunHolder> {

    public ArrayList<ChallengeIndividualRunningModel> challngIndRunlistModels;
    MyChallengesIndividualFragment context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont, thinFont, specialFont;
    UserSessionManager session;
    String user_name, challenge_type;

    public ChallengeIndividRunAdapter(ArrayList<ChallengeIndividualRunningModel> challngIndRunlistModels, MyChallengesIndividualFragment context, int resource) {
        this.challngIndRunlistModels = challngIndRunlistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        regularFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getActivity().getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getActivity().getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getActivity().getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getActivity().getResources().getString(R.string.specialFont));

        session = new UserSessionManager(context.getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_name = userDetails.get(UserSessionManager.USER_NAME);

    }

    @Override
    public ChallengeIndividRunHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ChallengeIndividRunHolder slh = new ChallengeIndividRunHolder(layout);
        return slh;
    }


    @Override
    public void onBindViewHolder(final ChallengeIndividRunHolder holder, final int position) {
        holder.pause_challenge.setVisibility(View.GONE);

        String str_opponentname = challngIndRunlistModels.get(position).getOpponent_name();
        String converted_string = str_opponentname.substring(0, 1).toUpperCase() + str_opponentname.substring(1);

        //String usernem = user_name;
        String usernem = challngIndRunlistModels.get(position).getUser_name();
        String converted_user_naem = usernem.substring(0, 1).toUpperCase() + usernem.substring(1);


        holder.card_view.setCardBackgroundColor(Color.parseColor("#E0E0E0"));

        holder.challenger_names.setText(Html.fromHtml(converted_user_naem));
        holder.challenger_names.setTypeface(regularFont);
        holder.oponent_names.setText(Html.fromHtml(converted_string));
        holder.oponent_names.setTypeface(regularFont);
        holder.vsText.setTypeface(thinFont);

        holder.cash_prize_amount.setText(("$" + challngIndRunlistModels.get(position).getAmount()));
        holder.cash_prize_amount.setTypeface(boldFont);
        String timedate = parseDateToddMMyyyy(challngIndRunlistModels.get(position).getStart_on());
        holder.dateandtime.setText(Html.fromHtml(timedate));
        holder.dateandtime.setTextColor(context.getActivity().getResources().getColor(R.color.title_txt));
        holder.dateandtime.setTypeface(thinFont);
        challenge_type = challngIndRunlistModels.get(position).getChallenge_type();
        if (challenge_type.equals("DAILY")) {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTypeface(thinFont);
            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.members_color));
        }  else {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTypeface(thinFont);
            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.bbg));
        }


        holder.status.setText(Html.fromHtml(challngIndRunlistModels.get(position).getStatus()));
         holder.status.setTypeface(regularFont);
        if (challngIndRunlistModels.get(position).getStatus().equals("PENDING")) {
            holder.status.setText("PENDING");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("COMPLETED")) {
            holder.status.setText("COMPLETED");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("YETTOSTART")) {
            holder.status.setText("UPCOMING");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("PAUSED")) {
            holder.status.setText("PAUSED");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("RUNNING")) {
            holder.status.setText("ONGOING");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("REJECTED")) {
            holder.status.setText("REJECTED");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("TIE")) {
            holder.status.setText("TIE");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("DRAW")) {
            holder.status.setText("DRAW");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("MANUAL_CANCELLATION")) {
            holder.status.setText("MANUAL CANCELLATION");
        } else if (challngIndRunlistModels.get(position).getStatus().equals("AUTO_CANCELLATION")) {
            holder.status.setText("AUTO CANCELLATION");
        } else {
            Toast.makeText(context.getActivity(), "no types found ", Toast.LENGTH_SHORT).show();
        }
        holder.status.setBackgroundColor(Color.parseColor("#239843"));
        holder.status.setTextColor(Color.parseColor("#ffffff"));


        /*Picasso.with(context.getActivity())
                .load(challngIndRunlistModels.get(position).getActivity_image())
                .placeholder(R.drawable.no_image_found)
                .into(holder.activity_image);*/

        holder.share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take a Challenge with -");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I'm inviting you to join ActivityChallenge App. To stay active and earn money Why don't you join?. "+ AppUrls.SHARE_APP_URL);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });

       holder.setItemClickListener(new ChallengeIndividRunItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                if(challngIndRunlistModels.get(position).getChallenge_type().equalsIgnoreCase("WEEKLY")){
                    Intent it=new Intent(context.getActivity(), ChallengeStatusActivity.class);
                    it.putExtra("challengeid",challngIndRunlistModels.get(position).getChallenge_id());
                    it.putExtra("challengetype",challngIndRunlistModels.get(position).getChallenge_type());
                    context.startActivity(it);
                }

            }
        });



    }


    @Override
    public int getItemCount() {
        return this.challngIndRunlistModels.size();
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
