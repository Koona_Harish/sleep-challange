package com.sleepchallenge.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.activities.GroupChatDetailActivity;
import com.sleepchallenge.holders.GroupDetailHolder;
import com.sleepchallenge.holders.MessageAbuseHolder;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;
import com.sleepchallenge.models.DeactivateReasonModel;
import com.sleepchallenge.models.MessagesDetailModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class GroupDetailAdapter extends RecyclerView.Adapter<GroupDetailHolder> {
    public ArrayList<MessagesDetailModel> groupDetailModels;
    public GroupChatDetailActivity context;
    String recipient;
    LayoutInflater li;
    GridLayoutManager gridLayoutManager;
    int resource;
    String From_type,userid,usertype,device_id,access_token,send_check_Value;
    Typeface typeface;
    private boolean checkInternet;

    UserSessionManager session;
    ArrayList<DeactivateReasonModel> abuse_report_modelList;
    AlertDialog dialog;
    AbuseReportAdapter abuseReportAdapter;
    private RadioButton lastCheckedRB = null;

    public GroupDetailAdapter(ArrayList<MessagesDetailModel> groupDetailModels, GroupChatDetailActivity context, int resource, String from_type) {
        this.groupDetailModels = groupDetailModels;
        this.context = context;
        this.resource = resource;
        From_type=from_type;
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.specialFont));

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        userid = userDetails.get(UserSessionManager.USER_ID);
        usertype = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        abuse_report_modelList = new ArrayList<>();

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME" , className);
    }



    @Override
    public GroupDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        GroupDetailHolder slh = new GroupDetailHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(GroupDetailHolder holder, final int position)
    {


        recipient= groupDetailModels.get(position).recipient;

        if(recipient.equals("receiver_LHS"))
        {
//,
            String str = groupDetailModels.get(position).msg;
            String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
            holder.msg_left.setText(converted_string);
            holder.msg_left.setTypeface(typeface);
            holder.msg_left_date.setText(groupDetailModels.get(position).sent_on_txt);
            holder.sender_left_name.setText(groupDetailModels.get(position).from_name);
            holder.ll_left.setBackgroundResource(R.drawable.bg_msgdetail_rounded_corner_left);
            holder.report_abuse_left.setVisibility(View.VISIBLE);
            holder.report_abuse_left.setTypeface(typeface);

        }
        else if(recipient.equals("sender_RHS"))
        {
            String str = groupDetailModels.get(position).msg;
            String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
            holder.msg_right.setText(converted_string);
            holder.msg_right.setTypeface(typeface);
            holder.msg_right_date.setText(groupDetailModels.get(position).sent_on_txt);
            holder.sender_right_name.setText(groupDetailModels.get(position).from_name);
            holder.ll_right.setBackgroundResource(R.drawable.bg_msgdetail_rounded_corner_right);

        }

        holder.report_abuse_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
               getReportAbusetitle(groupDetailModels.get(position).id,groupDetailModels.get(position).from_id);
            }
        });


        holder.setItemClickListener(new AllItemClickListeners()
        {
            @Override
            public void onItemClick(View v, int pos)
            {

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.groupDetailModels.size();
    }


    private void getReportAbusetitle(final String msg_id, final String from_id) {
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            String url_deactivate = AppUrls.BASE_URL + AppUrls.ABUSE_TYPE;
            Log.d("REPABUSEURL", url_deactivate);
            StringRequest req_members = new StringRequest(Request.Method.GET, url_deactivate, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("ABUSERESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100"))
                        {
                            abuse_report_modelList = new ArrayList<>();
                            JSONArray jsonArray = jobcode.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++)
                            {

                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                DeactivateReasonModel dact = new DeactivateReasonModel(jdataobj);
                                abuse_report_modelList.add(dact);

                            }

                            abuseDialog(msg_id,from_id);

                        }
                        if (response_code.equals("10200")) {

                            Toast.makeText(context, "No Data Found..!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                  error.getMessage();
                }
            }) {
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(context, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    private void abuseDialog(final String msg_id, final String from_id)
    {
        Log.d("DFSDFSDFSDF","DFSDFSDFSDF");
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = context.getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.report_abuse_dialog_list,null);

        final EditText report_descriptin = dialog_layout.findViewById(R.id.report_descriptin);
        TextView sendReportButton_cancel =  dialog_layout.findViewById(R.id.sendReportButton_cancel);
        final TextView sendReportButton =  dialog_layout.findViewById(R.id.sendReportButton);
        TextView titl_txt = dialog_layout.findViewById(R.id.titl_txt);
        TextView title_second_txt = dialog_layout.findViewById(R.id.title_second_txt);
        TextView category_txt = dialog_layout.findViewById(R.id.category_txt);
        titl_txt.setTypeface(typeface);
        title_second_txt.setTypeface(typeface);
        category_txt.setTypeface(typeface);
        sendReportButton_cancel.setTypeface(typeface);
        sendReportButton.setTypeface(typeface);

        RecyclerView abuse_type_recyclerview = (RecyclerView) dialog_layout.findViewById(R.id.abuse_type_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        abuse_type_recyclerview.setLayoutManager(layoutManager);
        abuseReportAdapter = new AbuseReportAdapter(abuse_report_modelList, context,R.layout.row_abuse_type_list);
        gridLayoutManager = new GridLayoutManager(context, 2);
        abuse_type_recyclerview.setLayoutManager(gridLayoutManager);
        abuse_type_recyclerview.setAdapter(abuseReportAdapter);

        sendReportButton_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        sendReportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                String description_value = report_descriptin.getText().toString();
                Log.d("DATAAA",msg_id+"//////"+description_value+"//////"+send_check_Value);
                if (description_value.equals("") || description_value.equals("0")) {
                    Toast.makeText(context, "Please Enter valid Description", Toast.LENGTH_SHORT).show();
                } else {
                    sendAbusedReport(msg_id,description_value,send_check_Value,from_id);
                }

            }
        });

        builder.setView(dialog_layout).setNegativeButton("", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();
        dialog.show();

    }

    public class AbuseReportAdapter extends RecyclerView.Adapter<MessageAbuseHolder> {
        public ArrayList<DeactivateReasonModel> reasonlistModels;
        GroupChatDetailActivity context;
        LayoutInflater li;
        int resource;


        public AbuseReportAdapter(ArrayList<DeactivateReasonModel> reasonlistModels, GroupChatDetailActivity context, int resource) {
            this.reasonlistModels = reasonlistModels;
            this.context = context;
            this.resource = resource;
            li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regularFont));
        }

        @Override
        public MessageAbuseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = li.inflate(resource, parent, false);
            MessageAbuseHolder slh = new MessageAbuseHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(final MessageAbuseHolder holder, final int position) {
            String str_reason = reasonlistModels.get(position).reason;
            String converted_string = str_reason.substring(0, 1).toUpperCase() + str_reason.substring(1);
            holder.reasone_radio_dynamic_button.setText(converted_string);

            View.OnClickListener rbClick = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RadioButton checked_rb = (RadioButton) v;
                    if (lastCheckedRB != null)
                    {
                        lastCheckedRB.setChecked(false);

                        send_check_Value = holder.reasone_radio_dynamic_button.getText().toString();
                    }
                    lastCheckedRB = checked_rb;

                    send_check_Value = holder.reasone_radio_dynamic_button.getText().toString();
                }
            };
            holder.reasone_radio_dynamic_button.setOnClickListener(rbClick);

            holder.setItemClickListener(new AllItemClickListeners() {
                @Override
                public void onItemClick(View v, int pos) {


                }
            });
        }

        @Override
        public int getItemCount() {
            return this.reasonlistModels.size();
        }
    }

    private void sendAbusedReport(final String msg_id, final String description_value, final String send_check_value, final String from_id)
    {
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            String url_follow_members = AppUrls.BASE_URL + AppUrls.REPORT_ABUSE;
            Log.d("GRPREPORTABUSEURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("GRPREPORTABUSERESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("response_code");

                        if (response_code.equals("10100")) {

                            Toast.makeText(context, "Reported successfully", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        }
                        if (response_code.equals("10200")) {

                            Toast.makeText(context, "Invalid input.!", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                   error.getMessage();
                }
            }) {

                @Override
                protected Map<String, String> getParams()  {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", userid);
                    params.put("against_user_id", from_id);
                    params.put("chat_id", msg_id);
                    params.put("chat_type", From_type);
                    params.put("abused_type", send_check_value);
                    params.put("text_message", description_value);
                    Log.d("GRPABUSREPPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(context, "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }
}
