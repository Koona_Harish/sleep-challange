package com.sleepchallenge.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;

import com.sleepchallenge.R;
import com.sleepchallenge.activities.CreateGroupActivity;
import com.sleepchallenge.filter.CustomFilterforMembers;
import com.sleepchallenge.itemclicklisteners.AddMembersGroupClickListner;
import com.sleepchallenge.models.AddMembersGroupModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;


public class AddMemberGroupAdapter extends RecyclerView.Adapter<AddMemberGroupAdapter.AddMembersGroupHolder> implements Filterable
{

    public ArrayList<AddMembersGroupModel> gmemberListItem, filterList;
    public CreateGroupActivity context;
    CustomFilterforMembers filter;
    LayoutInflater li;
    int resource;
    ArrayList<String> gm_ids = new ArrayList<String>();
    ArrayList<String> gm_names = new ArrayList<String>();

    public AddMemberGroupAdapter(ArrayList<AddMembersGroupModel> gmemberListItem, CreateGroupActivity context, int resource) {
        this.gmemberListItem = gmemberListItem;
        this.context = context;
        this.resource = resource;
        this.filterList = gmemberListItem;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);
    }

    @Override
    public AddMembersGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, null);
        AddMembersGroupHolder slh = new AddMembersGroupHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final AddMembersGroupHolder holder, final int position)
    {
        holder.memCheck.setText(gmemberListItem.get(position).getName());
        holder.memCheck.setChecked(gmemberListItem.get(position).ischecked);




        holder.setItemClickListener(new AddMembersGroupClickListner() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return this.gmemberListItem.size();

    }

    @Override
    public Filter getFilter()
    {
        if (filter == null) {
            filter = new CustomFilterforMembers(filterList, this);
        }

        return filter;
    }





//    HOLDER

    public class AddMembersGroupHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public CheckBox memCheck;
        AddMembersGroupClickListner addMembersGroupClickListner;
        private Map<String, Boolean> checkBoxStates = new HashMap<>();

        public AddMembersGroupHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            memCheck = (CheckBox) itemView.findViewById(R.id.memCheck);

            memCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int pos = getAdapterPosition();
                    if (isChecked) {
                        gm_ids.add(gmemberListItem.get(pos).getId());
                        HashSet hs = new HashSet();
                        hs.addAll(gm_ids);
                        gm_ids.clear();
                        gm_ids.addAll(hs);

                        gm_names.add(gmemberListItem.get(pos).getName());
                        HashSet hs1 = new HashSet();
                        hs1.addAll(gm_names);
                        gm_names.clear();
                        gm_names.addAll(hs1);


                        context.setGmName(gm_ids, gm_names);
                        gmemberListItem.get(pos).setIschecked(true);
                        memCheck.isChecked();

                    } else {
                        gm_ids.remove(gmemberListItem.get(pos).getId());
                        gm_names.remove(gmemberListItem.get(pos).getName());
                        context.setGmName(gm_ids, gm_names);
                        gmemberListItem.get(pos).setIschecked(false);

                    }
                }
            });



        }

        @Override
        public void onClick(View view) {
            this.addMembersGroupClickListner.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(AddMembersGroupClickListner ic)
        {
            this.addMembersGroupClickListner =ic;
        }




    }



}



/*public class AddMemberGroupAdapter extends RecyclerView.Adapter<AddMembersGroupHolder>
{

    ArrayList<AddMembersGroupModel> listModels;
    CreateGroupActivity context;
    int resource;
    LayoutInflater li;

    public AddMemberGroupAdapter(CreateGroupActivity context, ArrayList<AddMembersGroupModel> listModels, int resource) {
        this.listModels = listModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public AddMembersGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        AddMembersGroupHolder slh = new AddMembersGroupHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(AddMembersGroupHolder holder, int position) {
            holder.memCheck.setText(listModels.get(position).getName());


    }

    @Override
    public int getItemCount() {
        return listModels.size();
    }


}*/


