package com.sleepchallenge.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;

import com.sleepchallenge.R;
import com.sleepchallenge.activities.UpdateGroupActivity;
import com.sleepchallenge.filter.CustomFilterforMembersStatus;
import com.sleepchallenge.itemclicklisteners.GetMemberUpdtStatusItemClickListener;
import com.sleepchallenge.models.GetMemUpdtStatusModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;


public class GetMemUpdateStatusGroupAdapter extends RecyclerView.Adapter<GetMemUpdateStatusGroupAdapter.GetMemInStatusGroupHolder> implements Filterable {
    public ArrayList<GetMemUpdtStatusModel> getMemStatusList, filterList;
    public UpdateGroupActivity context;
    CustomFilterforMembersStatus filter;
    ArrayList<String> gm_ids = new ArrayList<String>();
    ArrayList<String> gm_names = new ArrayList<String>();
    LayoutInflater li;
    int resource;


    public GetMemUpdateStatusGroupAdapter(ArrayList<GetMemUpdtStatusModel> getMemStatusList, UpdateGroupActivity context, int resource) {
       this.getMemStatusList = getMemStatusList;
       this.context = context;
       this.resource = resource;
        this.filterList = getMemStatusList;
       li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public GetMemInStatusGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, null);
        GetMemInStatusGroupHolder slh = new GetMemInStatusGroupHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final GetMemInStatusGroupHolder holder, final int position)
    {

        String status=getMemStatusList.get(position).getStatus();
        Log.d("SSSSSS",status);
        if(status.equals("1"))
        {
            holder.memStatusCheck.setText(getMemStatusList.get(position).getName());
            holder.memStatusCheck.setChecked(true);
        }
        holder.memStatusCheck.setText(getMemStatusList.get(position).getName());
        holder.memStatusCheck.setChecked(getMemStatusList.get(position).ischecked);


        holder.setItemClickListener(new GetMemberUpdtStatusItemClickListener()
        {
            @Override
            public void onItemClick(View view, int layoutPosition)
            {
               
            }
        });

    }


    @Override
    public Filter getFilter()
    {
        if (filter == null) {
            filter = new CustomFilterforMembersStatus(filterList, this);
        }

        return filter;
    }


    @Override
    public int getItemCount() {
        return getMemStatusList.size();
    }

    ////////HOLDER START///////////

    public class GetMemInStatusGroupHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {

        public CheckBox memStatusCheck;
        GetMemberUpdtStatusItemClickListener getMemUpdtStatusItemClickListener;
        private Map<String, Boolean> checkBoxStates = new HashMap<>();

        public GetMemInStatusGroupHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            memStatusCheck = (CheckBox) itemView.findViewById(R.id.memStatusCheck);

            memStatusCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int pos = getAdapterPosition();
                    if (isChecked) {
                        gm_ids.add(getMemStatusList.get(pos).getId());
                        HashSet hs = new HashSet();
                        hs.addAll(gm_ids);
                        gm_ids.clear();
                        gm_ids.addAll(hs);

                        gm_names.add(getMemStatusList.get(pos).getName());
                        HashSet hs1 = new HashSet();
                        hs1.addAll(gm_names);
                        gm_names.clear();
                        gm_names.addAll(hs1);


                        context.setGmName(gm_ids, gm_names);
                        getMemStatusList.get(pos).setIschecked(true);
                        memStatusCheck.isChecked();

                    } else {
                        gm_ids.remove(getMemStatusList.get(pos).getId());
                        gm_names.remove(getMemStatusList.get(pos).getName());
                        context.setGmName(gm_ids, gm_names);
                        getMemStatusList.get(pos).setIschecked(false);

                    }
                }
            });



        }

        @Override
        public void onClick(View view) {
            this.getMemUpdtStatusItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(GetMemberUpdtStatusItemClickListener ic)
        {
            this.getMemUpdtStatusItemClickListener =ic;
        }




    }

}
