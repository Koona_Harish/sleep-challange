package com.sleepchallenge.adapters;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.activities.MyChallengesActivity;
import com.sleepchallenge.fragments.MyChallengesGroupFragment;
import com.sleepchallenge.holders.ChallengeGroupDetailHolder;
import com.sleepchallenge.itemclicklisteners.ChallengeGroupDetailItemClickListener;
import com.sleepchallenge.models.ChallengeGroupDetailModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class MyChallengeGroupUpcomAdapter extends RecyclerView.Adapter<ChallengeGroupDetailHolder> {

    public ArrayList<ChallengeGroupDetailModel> challnggroupDetaillistModels;
    MyChallengesGroupFragment context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont, thinFont, specialFont;
    UserSessionManager session;
    String user_name, send_pauseVal, device_id, token, challange_id, Resume;
    Dialog dialog;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String Paused_by, challenge_type;


    public MyChallengeGroupUpcomAdapter(ArrayList<ChallengeGroupDetailModel> challnggroupDetaillistModels, MyChallengesGroupFragment context, int resource) {
        this.challnggroupDetaillistModels = challnggroupDetaillistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        regularFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getActivity().getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getActivity().getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getActivity().getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getActivity().getResources().getString(R.string.specialFont));
        session = new UserSessionManager(context.getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        checkInternet = NetworkChecking.isConnected(context.getActivity());
        pprogressDialog = new ProgressDialog(context.getActivity());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);


    }

    @Override
    public ChallengeGroupDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ChallengeGroupDetailHolder slh = new ChallengeGroupDetailHolder(layout);
        return slh;
    }


    @Override
    public void onBindViewHolder(final ChallengeGroupDetailHolder holder, final int position) {

        holder.card_view.setCardBackgroundColor(Color.parseColor("#E0E0E0"));
        // String PAUSE_ACCESS=challnggroupDetaillistModels.get(position).getPause_access();
        if (challnggroupDetaillistModels.get(position).getPause_access().equals("0")) {
            holder.pause_challenge.setVisibility(View.GONE);
        } else {
            holder.pause_challenge.setVisibility(View.VISIBLE);
        }
        holder.timing_layout.setVisibility(View.VISIBLE);
        String starttime=parseTime(challnggroupDetaillistModels.get(position).getStart_on());
        String endtime=parseTime(challnggroupDetaillistModels.get(position).getCompleted_on());
        holder.starttime_text.setText("Start time:"+starttime);
        holder.wakeup_time_text.setText("Wake up time:"+endtime);
        holder.starttime_text.setTypeface(regularFont);
        holder.wakeup_time_text.setTypeface(regularFont);
        Resume = withoutparseDateToddMMyyyy(challnggroupDetaillistModels.get(position).getResume_on());
        String str_opponentname = challnggroupDetaillistModels.get(position).getOpponent_name();
        String converted_string = str_opponentname.substring(0, 1).toUpperCase() + str_opponentname.substring(1);

        String usernem = challnggroupDetaillistModels.get(position).getUser_name();
        String converted_user_naem = usernem.substring(0, 1).toUpperCase() + usernem.substring(1);

        String paused_by = challnggroupDetaillistModels.get(position).getPaused_by();
        if (!paused_by.equals("")) {
            Paused_by = paused_by.substring(0, 1).toUpperCase() + paused_by.substring(1);
        }

        holder.challenger_names.setText(Html.fromHtml(converted_user_naem));
        holder.challenger_names.setTypeface(regularFont);
        holder.oponent_names.setText(Html.fromHtml(converted_string));
        holder.oponent_names.setTypeface(regularFont);
        holder.vsText.setTypeface(thinFont);

        holder.cash_prize_amount.setText(("$" + challnggroupDetaillistModels.get(position).getAmount()));
        holder.cash_prize_amount.setTypeface(boldFont);

        String timedate = parseDateToddMMyyyy(challnggroupDetaillistModels.get(position).getStart_on());
        holder.dateandtime.setText(Html.fromHtml(timedate));
        holder.dateandtime.setTextColor(context.getActivity().getResources().getColor(R.color.title_txt));
        holder.dateandtime.setTypeface(regularFont);
        challenge_type = challnggroupDetaillistModels.get(position).getChallenge_type();
        holder.challenge_type.setTypeface(regularFont);
        if (challenge_type.equals("DAILY")) {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.members_color));
        } else {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.bbg));
        }


        holder.status.setText(Html.fromHtml(challnggroupDetaillistModels.get(position).getStatus()));
        holder.status.setTypeface(regularFont);
        if (challnggroupDetaillistModels.get(position).getStatus().equals("PENDING")) {
            holder.status.setText("PENDING");
        } else if (challnggroupDetaillistModels.get(position).getStatus().equals("COMPLETED")) {
            holder.status.setText("COMPLETED");
        } else if (challnggroupDetaillistModels.get(position).getStatus().equals("YETTOSTART")) {
            holder.status.setText("UPCOMING");
        } else if (challnggroupDetaillistModels.get(position).getStatus().equals("PAUSED")) {
            holder.status.setText("PAUSED");
            holder.pause_challenge.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Paused by : " + Paused_by);
            holder.won_cash.setVisibility(View.GONE);
        } else if (challnggroupDetaillistModels.get(position).getStatus().equals("RUNNING")) {
            holder.status.setText("ONGOING");
        } else if (challnggroupDetaillistModels.get(position).getStatus().equals("REJECTED")) {
            holder.status.setText("REJECTED");
        } else if (challnggroupDetaillistModels.get(position).getStatus().equals("TIE")) {
            holder.status.setText("TIE");
        } else if (challnggroupDetaillistModels.get(position).getStatus().equals("DRAW")) {
            holder.status.setText("DRAW");
        } else if (challnggroupDetaillistModels.get(position).getStatus().equals("MANUAL_CANCELLATION")) {
            holder.status.setText("MANUAL CANCELLATION");
        } else if (challnggroupDetaillistModels.get(position).getStatus().equals("AUTO_CANCELLATION")) {
            holder.status.setText("AUTO CANCELLATION");
        } else {
            Toast.makeText(context.getActivity(), "no types found ", Toast.LENGTH_SHORT).show();
        }
        holder.status.setBackgroundColor(Color.parseColor("#DD6B55"));
        holder.status.setTextColor(Color.parseColor("#ffffff"));
        if (challenge_type.equalsIgnoreCase("SPECIAL_EVENT"))
            holder.pause_challenge.setVisibility(View.GONE);

        holder.share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take a Challenge with -");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I'm inviting you to join ActivityChallenge App. To stay active and earn money Why don't you join?. " + AppUrls.SHARE_APP_URL);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });


        holder.setItemClickListener(new ChallengeGroupDetailItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {


            }
        });

        holder.pause_challenge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = new Dialog(context.getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.pause_challenge_custom_dialog);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                TextView ok_pause_btn = (TextView) dialog.findViewById(R.id.ok_pause_btn);

                final RadioGroup radio_pause_group = (RadioGroup) dialog.findViewById(R.id.radio_pause_group);

                dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);
                ok_pause_btn.setOnClickListener(new View.OnClickListener() {
                    @Override

                    public void onClick(View v) {
                        if (checkInternet) {
                            RadioButton radioPPButton;
                            int selectedId = radio_pause_group.getCheckedRadioButtonId();
                            radioPPButton = (RadioButton) dialog.findViewById(selectedId);
                            switch (selectedId) {
                                case R.id.radio_buttn_onewk:

                                    send_pauseVal = "1_WEEK";
                                    break;

                                case R.id.radio_buttn_twowk:

                                    send_pauseVal = "2_WEEK";
                                    break;

                                case R.id.radio_buttn_onemonth:
                                    send_pauseVal = "1_MONTH";
                                    break;

                                case R.id.radio_buttn_threemonth:
                                    send_pauseVal = "3_MONTH";
                                    break;


                                case R.id.radio_buttn_sixmonth:
                                    send_pauseVal = "6_MONTH";
                                    break;

                                case R.id.radio_buttn_oneyr:
                                    send_pauseVal = "1_YEAR";
                                    break;
                            }
                            challange_id = challnggroupDetaillistModels.get(position).getChallenge_id();
                            sendReqToPauseChallng(challange_id, send_pauseVal);
                        } else {
                            Toast.makeText(context.getActivity(), "No Internet Connection..!", Toast.LENGTH_LONG).show();
                        }

                    }

                });
                dialog.show();

            }
        });


    }

    private void sendReqToPauseChallng(final String challange_id, final String send_pauseVal) {
        if (checkInternet) {
            Log.d("PAUSEURL:", AppUrls.BASE_URL + AppUrls.PAUSE_CHALLENGE);
            StringRequest forgetReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.PAUSE_CHALLENGE, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("PAUSERESP", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100")) {
                            Intent intent = new Intent(context.getActivity(), MyChallengesActivity.class);
                            context.getActivity().startActivity(intent);
                            context.getActivity().finish();
                            // context.getGroupChallenges();
                            pprogressDialog.dismiss();
                            Toast.makeText(context.getActivity(), "  Challenge Paused successfully,\n will be Resume on " + Resume, Toast.LENGTH_SHORT).show();
                            dialog.dismiss();

                        }
                        if (successResponceCode.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(context.getActivity(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                        }
                        if (successResponceCode.equals("10300")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(context.getActivity(), " Invalid Challenge..", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.cancel();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED", headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("challenge_id", challange_id);
                    params.put("time_span", send_pauseVal);

                    Log.d("PAUSEPARAM:", params.toString());
                    return params;

                }
            };

            forgetReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context.getActivity());
            requestQueue.add(forgetReq);
        } else {
            Toast.makeText(context.getActivity(), "No Internet Connection..!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public int getItemCount() {
        return this.challnggroupDetaillistModels.size();
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy hh:mm ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String withoutparseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
    private String parseTime(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
