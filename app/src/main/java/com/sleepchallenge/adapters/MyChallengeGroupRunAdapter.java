package com.sleepchallenge.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sleepchallenge.R;
import com.sleepchallenge.activities.ChallengeStatusActivity;
import com.sleepchallenge.fragments.MyChallengesGroupFragment;
import com.sleepchallenge.holders.ChallengeGroupDetailHolder;
import com.sleepchallenge.itemclicklisteners.ChallengeGroupDetailItemClickListener;
import com.sleepchallenge.models.ChallengeGroupDetailModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.UserSessionManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class MyChallengeGroupRunAdapter extends RecyclerView.Adapter<ChallengeGroupDetailHolder> {

    public ArrayList<ChallengeGroupDetailModel> challnggroupDetailRunlistModels;
    MyChallengesGroupFragment context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont, thinFont, specialFont;
    UserSessionManager session;
    String user_name, challenge_type;

    public MyChallengeGroupRunAdapter(ArrayList<ChallengeGroupDetailModel> challnggroupDetailRunlistModels, MyChallengesGroupFragment context, int resource) {
        this.challnggroupDetailRunlistModels = challnggroupDetailRunlistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        regularFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getActivity().getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getActivity().getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getActivity().getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getActivity().getResources().getString(R.string.specialFont));

        session = new UserSessionManager(context.getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_name = userDetails.get(UserSessionManager.USER_NAME);


    }

    @Override
    public ChallengeGroupDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ChallengeGroupDetailHolder slh = new ChallengeGroupDetailHolder(layout);
        return slh;
    }


    @Override
    public void onBindViewHolder(final ChallengeGroupDetailHolder holder, final int position) {

        holder.card_view.setCardBackgroundColor(Color.parseColor("#E0E0E0"));
       /* String EVEL_FACET=challnggroupDetailRunlistModels.get(position).getEvaluation_factor();
        Log.d("EVVVVVV",EVEL_FACET);*/
        String PAUSE_ACCESS = challnggroupDetailRunlistModels.get(position).getPause_access();
        if (PAUSE_ACCESS.equals("0")) {
            holder.pause_challenge.setVisibility(View.GONE);
        }

        String str_opponentname = challnggroupDetailRunlistModels.get(position).getOpponent_name();
        String converted_string = str_opponentname.substring(0, 1).toUpperCase() + str_opponentname.substring(1);

        String usernem = challnggroupDetailRunlistModels.get(position).getUser_name();
        ;
        String converted_user_naem = usernem.substring(0, 1).toUpperCase() + usernem.substring(1);

        holder.challenger_names.setText(Html.fromHtml(converted_user_naem));
        holder.challenger_names.setTypeface(regularFont);
        holder.oponent_names.setText(Html.fromHtml(converted_string));
        holder.oponent_names.setTypeface(regularFont);
        holder.vsText.setTypeface(thinFont);

        holder.cash_prize_amount.setText(("$" + challnggroupDetailRunlistModels.get(position).getAmount()));
        holder.cash_prize_amount.setTypeface(boldFont);
        String timedate = parseDateToddMMyyyy(challnggroupDetailRunlistModels.get(position).getStart_on());
        holder.dateandtime.setText(Html.fromHtml(timedate));
        holder.dateandtime.setTypeface(regularFont);
        holder.dateandtime.setTextColor(context.getActivity().getResources().getColor(R.color.title_txt));
        challenge_type = challnggroupDetailRunlistModels.get(position).getChallenge_type();
        holder.challenge_type.setTypeface(thinFont);
        if (challenge_type.equals("DAILY")) {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.members_color));
        } else {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.bbg));
        }

        holder.status.setText(Html.fromHtml(challnggroupDetailRunlistModels.get(position).getStatus()));
        holder.status.setTypeface(regularFont);
        if (challnggroupDetailRunlistModels.get(position).getStatus().equals("PENDING")) {
            holder.status.setText("PENDING");
        } else if (challnggroupDetailRunlistModels.get(position).getStatus().equals("COMPLETED")) {
            holder.status.setText("COMPLETED");
        } else if (challnggroupDetailRunlistModels.get(position).getStatus().equals("YETTOSTART")) {
            holder.status.setText("UPCOMING");
        } else if (challnggroupDetailRunlistModels.get(position).getStatus().equals("PAUSED")) {
            holder.status.setText("PAUSED");
        } else if (challnggroupDetailRunlistModels.get(position).getStatus().equals("RUNNING")) {
            holder.status.setText("ONGOING");
        } else if (challnggroupDetailRunlistModels.get(position).getStatus().equals("REJECTED")) {
            holder.status.setText("REJECTED");
        } else if (challnggroupDetailRunlistModels.get(position).getStatus().equals("TIE")) {
            holder.status.setText("TIE");
        } else if (challnggroupDetailRunlistModels.get(position).getStatus().equals("DRAW")) {
            holder.status.setText("DRAW");
        } else if (challnggroupDetailRunlistModels.get(position).getStatus().equals("MANUAL_CANCELLATION")) {
            holder.status.setText("MANUAL CANCELLATION");
        } else if (challnggroupDetailRunlistModels.get(position).getStatus().equals("AUTO_CANCELLATION")) {
            holder.status.setText("AUTO CANCELLATION");
        } else {
            Toast.makeText(context.getActivity(), "no types found ", Toast.LENGTH_SHORT).show();
        }
        holder.status.setBackgroundColor(Color.parseColor("#239843"));
        holder.status.setTextColor(Color.parseColor("#ffffff"));

        holder.share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take a Challenge with -");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I'm inviting you to join ActivityChallenge App. To stay active and earn money Why don't you join?. " + AppUrls.SHARE_APP_URL);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });


        holder.setItemClickListener(new ChallengeGroupDetailItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {
                Intent it=new Intent(context.getActivity(), ChallengeStatusActivity.class);
               it.putExtra("challengeid",challnggroupDetailRunlistModels.get(position).getChallenge_id());
             it.putExtra("challengetype",challnggroupDetailRunlistModels.get(position).getChallenge_type());
                context.startActivity(it);

            }
        });

    }

    @Override
    public int getItemCount() {
        return this.challnggroupDetailRunlistModels.size();
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
