package com.sleepchallenge.adapters;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.sleepchallenge.R;
import com.sleepchallenge.activities.GroupDetailActivity;
import com.sleepchallenge.filter.CustomFilterForMyGroupList;
import com.sleepchallenge.fragments.MyGroupsFragment;
import com.sleepchallenge.holders.MyGroupHolder;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;
import com.sleepchallenge.models.MyGroupModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyGroupsAdapter extends RecyclerView.Adapter<MyGroupHolder> implements Filterable {

    public ArrayList<MyGroupModel> allmemberlistModels, allmembersfilterList;
    MyGroupsFragment context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont,thinFont;
    CustomFilterForMyGroupList filter;


    public MyGroupsAdapter(ArrayList<MyGroupModel> allmemberlist, MyGroupsFragment context, int resource) {
        this.allmemberlistModels = allmemberlist;
        this.context = context;
        this.resource = resource;
        this.allmembersfilterList = allmemberlistModels;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        regularFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.thinFont));
    }




    @Override
    public MyGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        MyGroupHolder slh = new MyGroupHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final MyGroupHolder holder, final int position)
    {

        String str = allmemberlistModels.get(position).name;
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.group_name.setText(Html.fromHtml(converted_string));
        holder.group_name.setTypeface(boldFont);

        holder.group_ranking_text.setText(Html.fromHtml(allmemberlistModels.get(position).overall_rank));
        holder.group_totlapercentage.setText(Html.fromHtml(allmemberlistModels.get(position).winning_per+" %"));
        holder.group_win_per.setText(Html.fromHtml(allmemberlistModels.get(position).total_win));
        holder.group_los_per.setText(Html.fromHtml(allmemberlistModels.get(position).total_loss));
        holder.member_count.setText(Html.fromHtml(allmemberlistModels.get(position).group_members+" members"));

        String groupimg_profile=allmemberlistModels.get(position).group_pic;
        Log.d("PPPROFILE",groupimg_profile);
         if(groupimg_profile.equals("null") && groupimg_profile.equals(""))
         {
             Picasso.with(context.getActivity())
                     .load(R.drawable.group_dummy)
                     .placeholder(R.drawable.group_dummy)
                    // .resize(60,60)
                     .into(holder.group_image);
         }
         else
         {
             Picasso.with(context.getActivity())
                     .load(groupimg_profile)
                     .placeholder(R.drawable.group_dummy)
                     .into(holder.group_image);
         }

        holder.setItemClickListener(new AllItemClickListeners()
        {
            @Override
            public void onItemClick(View v, int pos)
            {

                Intent ii = new Intent(context.getActivity(), GroupDetailActivity.class);
                ii.putExtra("grp_id", allmemberlistModels.get(position).id);
                ii.putExtra("grp_name", allmemberlistModels.get(position).name);
                ii.putExtra("grp_admin_id", allmemberlistModels.get(position).admin_id);
                ii.putExtra("GROUP_CONVERSATION_TYPE", "GROUP_INTERNAL");
                context.startActivity(ii);
            }
        });

    }

    @Override
    public int getItemCount()
    {
        return this.allmemberlistModels.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterForMyGroupList(allmembersfilterList, this);
        }

        return filter;
    }
}
