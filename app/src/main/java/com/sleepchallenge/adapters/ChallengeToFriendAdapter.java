package com.sleepchallenge.adapters;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.sleepchallenge.R;
import com.sleepchallenge.activities.ChallengeAmountActivity;
import com.sleepchallenge.activities.ChallengeToFriendActivity;
import com.sleepchallenge.filter.CustomFilterForChallengeToFriend;
import com.sleepchallenge.holders.ChallengeToFriendHolder;
import com.sleepchallenge.itemclicklisteners.AllMembersItemClickListener;
import com.sleepchallenge.models.ChallengeToFriendModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ChallengeToFriendAdapter extends RecyclerView.Adapter<ChallengeToFriendHolder> implements Filterable,View.OnClickListener {

    public ArrayList<ChallengeToFriendModel> allmemberlistModels, allmembersfilterList;
    ChallengeToFriendActivity context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont, thinFont, specialFont;
    String challengedate,challengetype,hours,timestr,noofdays;
    private boolean checkInternet;
    String challengerName,remainderstr;
    CustomFilterForChallengeToFriend filter;


    public ChallengeToFriendAdapter(ArrayList<ChallengeToFriendModel> allmemberlistModels, ChallengeToFriendActivity context, int resource,
                    String challengedate,String challengetype,String hours,String timestr,String noofdays,String remainderstr) {
        this.allmemberlistModels = allmemberlistModels;
        this.context = context;
        this.resource = resource;
        this.allmembersfilterList = allmemberlistModels;
        this.challengedate = challengedate;
        this.challengetype = challengetype;
        this.hours = hours;
        this.timestr = timestr;
        this.noofdays = noofdays;
        this.remainderstr = remainderstr;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.specialFont));


    }

    @Override
    public ChallengeToFriendHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ChallengeToFriendHolder slh = new ChallengeToFriendHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final ChallengeToFriendHolder holder, final int position)
    {

        String str = allmemberlistModels.get(position).getName();
        challengerName=str;
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.all_member_user_name.setText(Html.fromHtml(converted_string));

        holder.all_member_ranking_text.setText(Html.fromHtml(allmemberlistModels.get(position).getOverall_rank()));
        holder.all_member_totlapercentage.setText(Html.fromHtml(allmemberlistModels.get(position).getWinning_per()+" %"));
        holder.all_member_win_per.setText(Html.fromHtml(allmemberlistModels.get(position).getTotal_win()));
        holder.all_member_los_per.setText(Html.fromHtml(allmemberlistModels.get(position).getTotal_loss()));
        if (!allmemberlistModels.get(position).getAlready_applied().equals("0")){
            holder.send_request_button.setVisibility(View.GONE);
        }else {
            holder.send_request_button.setVisibility(View.VISIBLE);
        }

         String img_profile=allmemberlistModels.get(position).getProfile_pic();

         if(img_profile.equals("null"))
         {
             Picasso.with(context)
                     .load(R.drawable.img_circle_placeholder)
                     .placeholder(R.drawable.members_dummy)
                     .into(holder.all_member_image);
         }
         else
         {
             Picasso.with(context)
                     .load(allmemberlistModels.get(position).getProfile_pic())
                     .placeholder(R.drawable.members_dummy)
                     .into(holder.all_member_image);
         }
        holder.send_request_button.setOnClickListener(this);
        holder.send_request_button.setTag(position);
        holder.setItemClickListener(new AllMembersItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {

            }
        });

    }

    @Override
    public int getItemCount()
    {
        return this.allmemberlistModels.size();
    }


    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterForChallengeToFriend(allmembersfilterList, this);
        }

        return filter;
    }

    @Override
    public void onClick(View v) {
        int pos= (int) v.getTag();
        Intent intent = new Intent(context, ChallengeAmountActivity.class);
        intent.putExtra("challengedate",challengedate);
        intent.putExtra("hours",hours);
        intent.putExtra("time",timestr);
        intent.putExtra("challengetype",challengetype);
        intent.putExtra("challengername",allmemberlistModels.get(pos).name);
        intent.putExtra("challengerid",allmemberlistModels.get(pos).id);
        intent.putExtra("challengertype","USER");
        intent.putExtra("remainder",remainderstr);
        intent.putExtra("activty","challengeto");
        if(challengetype.equalsIgnoreCase("Weekly")) {
            intent.putExtra("noofdays", noofdays);
        }
        context.startActivity(intent);
    }
}
