package com.sleepchallenge.adapters;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sleepchallenge.R;
import com.sleepchallenge.activities.GroupDetailActivity;
import com.sleepchallenge.activities.MemberDetailActivity;
import com.sleepchallenge.activities.MyFollowing;
import com.sleepchallenge.filter.CustomFilterForAllMembersList;
import com.sleepchallenge.holders.AllMembersHolder;
import com.sleepchallenge.itemclicklisteners.AllMembersItemClickListener;
import com.sleepchallenge.models.AllMembersModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyFollowingAdapter extends RecyclerView.Adapter<AllMembersHolder> {

    public ArrayList<AllMembersModel> myfollowinglistModels;
    MyFollowing context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont, thinFont;
    CustomFilterForAllMembersList filter;


    public MyFollowingAdapter(ArrayList<AllMembersModel> myfollowinglistModels, MyFollowing context, int resource) {
        this.myfollowinglistModels = myfollowinglistModels;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.thinFont));
    }


    @Override
    public AllMembersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        AllMembersHolder slh = new AllMembersHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final AllMembersHolder holder, final int position)
    {


        String str = myfollowinglistModels.get(position).getName();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.all_member_user_name.setText(Html.fromHtml(converted_string));
        holder.all_member_user_name.setTypeface(boldFont);

        holder.all_member_ranking_text.setText(Html.fromHtml(myfollowinglistModels.get(position).getOverall_rank()));
        holder.all_member_ranking_text.setTypeface(regularFont);
        holder.all_member_totlapercentage.setText(Html.fromHtml(myfollowinglistModels.get(position).getWinning_per()+" %"));
        holder.all_member_totlapercentage.setTypeface(regularFont);
        holder.all_member_win_per.setText(Html.fromHtml(myfollowinglistModels.get(position).getTotal_win()));
        holder.all_member_win_per.setTypeface(regularFont);
        holder.all_member_los_per.setText(Html.fromHtml(myfollowinglistModels.get(position).getTotal_loss()));
        holder.all_member_los_per.setTypeface(regularFont);


          final String user_type= myfollowinglistModels.get(position).getUser_type();
        holder.user_type.setTypeface(regularFont);
          if(user_type.equals("USER"))
          {
             holder.user_type.setText("USER");
          }
          else if(user_type.equals("GROUP"))
          {
              holder.user_type.setText("GROUP");
          }
          else{
              holder.user_type.setText("SPONSOR");
          }

         String img_profile=myfollowinglistModels.get(position).getProfile_pic();

         if(img_profile.equals("null"))
         {
             Picasso.with(context)
                     .load(R.drawable.profile_img)
                     .placeholder(R.drawable.members_dummy)
                     .resize(60,60)
                     .into(holder.all_member_image);
         }
         else
         {
             Picasso.with(context)
                     .load(myfollowinglistModels.get(position).getProfile_pic())
                     .placeholder(R.drawable.members_dummy)
                     .into(holder.all_member_image);
         }

        holder.setItemClickListener(new AllMembersItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos) {
               if(user_type.equalsIgnoreCase("USER")){
                   Intent intent=new Intent(context, MemberDetailActivity.class);
                   Log.d("IDDD:",myfollowinglistModels.get(position).getId());
                   intent.putExtra("MEMBER_ID",myfollowinglistModels.get(position).getId());
                   intent.putExtra("MEMBER_NAME",myfollowinglistModels.get(position).getName());
                   intent.putExtra("member_user_type",myfollowinglistModels.get(position).getUser_type());
                   context.startActivity(intent);
               }else {
                   Intent ii = new Intent(context, GroupDetailActivity.class);
                   ii.putExtra("grp_id", myfollowinglistModels.get(position).getId());
                   ii.putExtra("grp_name", myfollowinglistModels.get(position).getName());
                   ii.putExtra("grp_admin_id", myfollowinglistModels.get(position).getAdmin_id());
                   ii.putExtra("GROUP_CONVERSATION_TYPE", "GROUP_NOT_USER");
                   context.startActivity(ii);
               }


            }
        });

    }

    @Override
    public int getItemCount()
    {
        return this.myfollowinglistModels.size();
    }
}
