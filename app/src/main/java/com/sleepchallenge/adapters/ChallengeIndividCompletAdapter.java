package com.sleepchallenge.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sleepchallenge.R;
import com.sleepchallenge.activities.ChallengeStatusActivity;
import com.sleepchallenge.activities.WinScratchCardsActivity;
import com.sleepchallenge.fragments.MyChallengesIndividualFragment;
import com.sleepchallenge.holders.ChallengeIndividRunHolder;
import com.sleepchallenge.itemclicklisteners.ChallengeIndividRunItemClickListener;
import com.sleepchallenge.models.ChallengeIndividualRunningModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.UserSessionManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class ChallengeIndividCompletAdapter extends RecyclerView.Adapter<ChallengeIndividRunHolder> {

    public ArrayList<ChallengeIndividualRunningModel> challngIndRunlistModels;
    MyChallengesIndividualFragment context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont, thinFont, specialFont;
    UserSessionManager session;
    String user_name, challenge_type;

    public ChallengeIndividCompletAdapter(ArrayList<ChallengeIndividualRunningModel> challngIndRunlistModels, MyChallengesIndividualFragment context, int resource) {
        this.challngIndRunlistModels = challngIndRunlistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        regularFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getActivity().getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getActivity().getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getActivity().getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getActivity().getResources().getString(R.string.specialFont));

        session = new UserSessionManager(context.getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_name = userDetails.get(UserSessionManager.USER_NAME);
    }

    @Override
    public ChallengeIndividRunHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ChallengeIndividRunHolder slh = new ChallengeIndividRunHolder(layout);
        return slh;
    }


    @Override
    public void onBindViewHolder(final ChallengeIndividRunHolder holder, final int position) {

        holder.card_view.setCardBackgroundColor(Color.parseColor("#E0E0E0"));
        holder.pause_challenge.setVisibility(View.GONE);

        String str_opponentname = challngIndRunlistModels.get(position).getOpponent_name();
        final String converted_oppname_string = str_opponentname.substring(0, 1).toUpperCase() + str_opponentname.substring(1);

        String username = challngIndRunlistModels.get(position).getUser_name();
        final String converted_user_name = username.substring(0, 1).toUpperCase() + username.substring(1);

        final String winning_status = challngIndRunlistModels.get(position).getWinning_status();

        final String winning_reward_type = challngIndRunlistModels.get(position).getWinning_reward_type();
        final String winning_reward_value = challngIndRunlistModels.get(position).getWinning_reward_value();
        final String is_scratched = challngIndRunlistModels.get(position).getIs_scratched();


        holder.challenger_names.setText(Html.fromHtml(converted_user_name));
        holder.challenger_names.setTypeface(regularFont);
        holder.oponent_names.setText(Html.fromHtml(converted_oppname_string));
        holder.oponent_names.setTypeface(regularFont);
        holder.vsText.setTypeface(thinFont);

        holder.cash_prize_amount.setText(("$" + challngIndRunlistModels.get(position).getAmount()));
        holder.cash_prize_amount.setTypeface(boldFont);
        String timedate = parseDateToddMMyyyy(challngIndRunlistModels.get(position).getStart_on());
        holder.dateandtime.setText(Html.fromHtml(timedate));
        holder.dateandtime.setTextColor(context.getActivity().getResources().getColor(R.color.title_txt));
        holder.dateandtime.setTypeface(thinFont);

        challenge_type = challngIndRunlistModels.get(position).getChallenge_type();
        holder.challenge_type.setTypeface(thinFont);
        if (challenge_type.equals("DAILY")) {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.members_color));
        }else {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getActivity().getResources().getColor(R.color.bbg));
        }

        final String status = challngIndRunlistModels.get(position).getStatus();
        final String ws_winner = challngIndRunlistModels.get(position).getWinner();
        Log.d("ws_winner", "" + ws_winner);
        holder.won_cash.setTypeface(regularFont);
        holder.winner_name.setTypeface(regularFont);
        if (status.equals("COMPLETED")) {
            holder.ll_completed.setVisibility(View.VISIBLE);

            if (ws_winner.equals("challenger")) {
                holder.winner_name.setText("Won : " + converted_user_name);

                if (winning_status.equals("loss")) {
                    holder.won_cash.setText(("Lost : " + "$" + challngIndRunlistModels.get(position).getAmount()));
                    holder.won_cash.setTextColor(Color.parseColor("#DD6B55"));

                } else {

                    if (winning_reward_type.equals("SCRATCH_CARD")) {
                        if (is_scratched.equals("0")) {
                            holder.won_cash.setText(("Won,Scratch Card"));
                            holder.won_cash.setTextColor(Color.parseColor("#239843"));
                        } else {
                            holder.won_cash.setText(("Won : " + "$" + challngIndRunlistModels.get(position).getWinning_amount()));
                            holder.won_cash.setTextColor(Color.parseColor("#239843"));
                        }
                    } else {
                        holder.won_cash.setText(("Won : " + "$" + challngIndRunlistModels.get(position).getWinning_amount()));
                        holder.won_cash.setTextColor(Color.parseColor("#239843"));
                    }
                }

            } else if (ws_winner.equals("opponent")) {
                holder.winner_name.setText("Won : " + converted_oppname_string);

                if (winning_status.equals("loss")) {
                    holder.won_cash.setText(("Lost : " + "$" + challngIndRunlistModels.get(position).getAmount()));
                    holder.won_cash.setTextColor(Color.parseColor("#DD6B55"));
                } else {
                    holder.won_cash.setText(("Won : " + "$" + challngIndRunlistModels.get(position).getWinning_amount()));
                    holder.won_cash.setTextColor(Color.parseColor("#239843"));
                }


            } else if (ws_winner.equals("no")) {

                if (challngIndRunlistModels.get(position).getOpponent_name().equals("ADMIN")) {
                    holder.winner_name.setText("You Lost the challenge");
                } else {
                    holder.winner_name.setText("Both Lost the challenge");
                }
                holder.won_cash.setText(("Lost : " + "$" + challngIndRunlistModels.get(position).getAmount()));
                holder.won_cash.setTextColor(Color.parseColor("#DD6B55"));

            }
        } else if (status.equals("TIE")) {
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("You both have won scratch card");
            holder.won_cash.setVisibility(View.GONE);
        } else if (status.equals("AUTO_CANCELLATION")) {
            holder.won_cash.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Cancelled by ADMIN");
        } else if (status.equals("REJECTED")) {
            holder.won_cash.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Rejected by : " + converted_oppname_string);
        } else if (status.equals("MANUAL_CANCELLATION") || status.equals("CANCELLED"))//CANCELLATION
        {
            holder.won_cash.setVisibility(View.GONE);
            holder.ll_completed.setVisibility(View.VISIBLE);
            holder.winner_name.setText("Cancelled by : " + converted_user_name);
        }

        holder.status.setText(Html.fromHtml(status));
        holder.status.setTypeface(regularFont);
        holder.status.setBackgroundResource(R.drawable.gradient_toolbar_color);
        holder.status.setTextColor(Color.parseColor("#ffffff"));


        /*Picasso.with(context.getActivity())
                .load(challngIndRunlistModels.get(position).getActivity_image())
                .placeholder(R.drawable.no_image_found)
                .into(holder.activity_image);*/

        holder.share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take a Challenge with -");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I'm inviting you to join ActivityChallenge App. To stay active and earn money Why don't you join?. " + AppUrls.SHARE_APP_URL);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });


        holder.setItemClickListener(new ChallengeIndividRunItemClickListener() {
            @Override
            public void onItemClick(View v, int pos)
            {
               if (status.equals("REJECTED") || status.equals("MANUAL_CANCELLATION") || status.equals("CANCELLED")) {

                } else {
                    if (winning_status.equals("win")) {

                        if (winning_reward_type.equals("SCRATCH_CARD") && is_scratched.equals("0")) {

                            Intent intent = new Intent(context.getActivity(), WinScratchCardsActivity.class);
                            intent.putExtra("challenge_id", challngIndRunlistModels.get(position).getChallenge_id());
                            intent.putExtra("winner_name", converted_user_name);
                            intent.putExtra("win_status", "WON");
                            intent.putExtra("type", "USER");
                            intent.putExtra("amount", challngIndRunlistModels.get(position).getWinning_amount());
                            intent.putExtra("is_group_admin", challngIndRunlistModels.get(position).getIs_group_admin());
                            context.startActivity(intent);

                        } else {
                            Intent it=new Intent(context.getActivity(), ChallengeStatusActivity.class);
                            it.putExtra("challengeid",challngIndRunlistModels.get(position).getChallenge_id());
                            it.putExtra("challengetype",challngIndRunlistModels.get(position).getChallenge_type());
                            it.putExtra("opponentid",challngIndRunlistModels.get(position).getOpponent_id());
                            context.startActivity(it);
                        }

                    } else
                    {
                        Intent it=new Intent(context.getActivity(), ChallengeStatusActivity.class);
                        it.putExtra("challengeid",challngIndRunlistModels.get(position).getChallenge_id());
                        it.putExtra("challengetype",challngIndRunlistModels.get(position).getChallenge_type());
                        it.putExtra("opponentid",challngIndRunlistModels.get(position).getOpponent_id());
                        context.startActivity(it);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.challngIndRunlistModels.size();
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
