package com.sleepchallenge.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.activities.ChallengeRequestActivity;
import com.sleepchallenge.activities.PaymentDetailsActivity;
import com.sleepchallenge.holders.ChallengeRequestHolder;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;
import com.sleepchallenge.models.ChallegneRequestModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class ChallengeRequestAdapter extends RecyclerView.Adapter<ChallengeRequestHolder> implements View.OnClickListener {

    public ArrayList<ChallegneRequestModel> challengeRequestModels;
    ChallengeRequestActivity context;
    LayoutInflater li;
    int resource;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    Typeface thinFont,regularFont,boldFont,specialFont;
    String token = "", device_id = "", user_type="",user_id="",challenge_id,challengeAmount,challange_start_on,challengegoal;

    public ChallengeRequestAdapter(ArrayList<ChallegneRequestModel> challengeRequestModels, ChallengeRequestActivity context, int resource) {
        this.challengeRequestModels = challengeRequestModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.specialFont));

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        user_id = userDetails.get(UserSessionManager.USER_ID);
    }

    @Override
    public ChallengeRequestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ChallengeRequestHolder slh = new ChallengeRequestHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final ChallengeRequestHolder holder, final int position) {

      //  Log.d("CHECKING", challengeAmount + "\n" + walletAmount);

        if (challengeRequestModels.get(position).user_pic.equals(AppUrls.BASE_IMAGE_URL) && !challengeRequestModels.get(position).group_pic.equals(AppUrls.BASE_IMAGE_URL)) {
            Picasso.with(context)
                    .load(challengeRequestModels.get(position).group_pic)
                    .placeholder(R.drawable.group_dummy)
                    .resize(70, 70)
                    .into(holder.challenger_image);
        } else if (challengeRequestModels.get(position).group_pic.equals(AppUrls.BASE_IMAGE_URL) && !challengeRequestModels.get(position).user_pic.equals(AppUrls.BASE_IMAGE_URL)) {
            Picasso.with(context)
                    .load(challengeRequestModels.get(position).user_pic)
                    .placeholder(R.drawable.members_dummy)
                    .resize(70, 70)
                    .into(holder.challenger_image);
        }
          final String challengerName= challengeRequestModels.get(position).user_name;
        if (challengeRequestModels.get(position).group_name.equals("")) {
            holder.challenger_req_name.setText(challengerName);
            holder.challenger_req_name.setTypeface(boldFont);
        } else {
            holder.challenger_req_name.setText(challengeRequestModels.get(position).group_name);
            holder.challenger_req_name.setTypeface(boldFont);
        }

        if (challengeRequestModels.get(position).group_rank.equals("") && !challengeRequestModels.get(position).user_rank.equals("")) {
            holder.rank_rl.setVisibility(View.GONE);
            holder.challenger_rank.setText(challengeRequestModels.get(position).user_rank);
            holder.challenger_rank.setTypeface(specialFont);
        } else if (challengeRequestModels.get(position).user_rank.equals("") && !challengeRequestModels.get(position).group_rank.equals("")) {
            holder.rank_rl.setVisibility(View.GONE);
            holder.challenger_rank.setText(challengeRequestModels.get(position).group_rank);
            holder.challenger_rank.setTypeface(specialFont);
        }
        challange_start_on = parseDateToddMMyyyy(challengeRequestModels.get(position).start_on);

        holder.challenge_start_time.setText(challange_start_on);
        holder.challenge_start_time.setTypeface(specialFont);

        challengegoal = challengeRequestModels.get(position).challenge_goal;
        holder.challenge_goal_txt.setText(challengegoal);
        holder.challenge_goal_txt.setTypeface(specialFont);



        final String challenge_type = challengeRequestModels.get(position).challenge_type;
        final String challenge_to = challengeRequestModels.get(position).challenge_to;
        final String remainder = challengeRequestModels.get(position).remainder;
        final String start_time = challengeRequestModels.get(position).start_time;
        if (challenge_type.equals("DAILY"))
        {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.members_color));
            holder.challenge_type.setTypeface(specialFont);
        }
        else {
            holder.challenge_type.setText(challenge_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.members_color));
            holder.challenge_type.setTypeface(specialFont);
        }
        /*if (user_type.equals("USER")){
            holder.challenger_req_amount_doller.setText("$ "+challengeRequestModels.get(position).getUser_wallet());
        }else if (user_type.equals("GROUP")){
            holder.challenger_req_amount_doller.setText("$ "+challengeRequestModels.get(position).getGroup_wallet());
        }else {
            Toast.makeText(context, "Check User Type..!", Toast.LENGTH_SHORT).show();
        }*/
        challengeAmount = challengeRequestModels.get(position).amount;
        holder.challenger_req_amount_doller.setText("$"+challengeAmount);
        holder.challenger_req_amount_doller.setTypeface(specialFont);

        holder.accept_btn.setTypeface(specialFont);
        holder.reject_btn.setTypeface(specialFont);

        holder.challenge_goal_title.setTypeface(specialFont);
        holder.challenge_start_text.setTypeface(specialFont);
        holder.challenge_evaluation_unit.setTypeface(specialFont);
        holder.challenger_req_amount_title.setTypeface(specialFont);
          holder.accept_btn.setOnClickListener(this);
          holder.accept_btn.setTag(position);

        /*holder.accept_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {



            }
        });*/

        holder.reject_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context,R.style.MyDialogTheme);

                alertDialog.setTitle("Are You Sure?");

                alertDialog.setMessage("You Want To Reject This Challenge?");

                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which) {
                        challenge_id = challengeRequestModels.get(position).id;

                        rejectChallenge();
                    }
                });
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });

        holder.setItemClickListener(new AllItemClickListeners()
        {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });
    }


    public void rejectChallenge() {
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.REJECT_CHALLENGE_REQUEST+"?id="+challenge_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("RejectChallenge", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("message");
                                String response_code = jsonObject.getString("response_code");
                                if (response_code.equals("10100")) {


                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String challenge_id = jsonObject1.getString("challenge_id");

                                    Intent intent = new Intent(context, ChallengeRequestActivity.class);
                                    context.startActivity(intent);

                                }
                                if (response_code.equals("10200")) {

                                    Toast.makeText(context, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //   progressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");

                    Log.d("SAVEHEADERS", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        } else {
            // progressDialog.cancel();
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }



    @Override
    public int getItemCount() {
        return this.challengeRequestModels.size();
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy  HH:mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public void onClick(View view) {
        int pos= (int)view.getTag();
        Intent intent=new Intent(context, PaymentDetailsActivity.class);
        intent.putExtra("challengedate",challengeRequestModels.get(pos).start_on);
        intent.putExtra("hours",challengeRequestModels.get(pos).challenge_goal);
        intent.putExtra("time",challengeRequestModels.get(pos).start_time);
        intent.putExtra("challengetype",challengeRequestModels.get(pos).challenge_type);
        intent.putExtra("challengername",challengeRequestModels.get(pos).user_name);
        intent.putExtra("activty","AcceptChallengePayment");
        intent.putExtra("challengerid","");
        intent.putExtra("challengeid",challengeRequestModels.get(pos).id);
        intent.putExtra("challengertype",challengeRequestModels.get(pos).challenge_to);
        intent.putExtra("remainder",challengeRequestModels.get(pos).remainder);
        intent.putExtra("payamout",challengeRequestModels.get(pos).amount);
        intent.putExtra("noofdays",challengeRequestModels.get(pos).days);


      //  Log.d("Values",challange_start_on+"//"+challengegoal+"//"+challenge_type+"//"+challengerName+"//"+challenge_to+"//"+remainder+"//"+challengeAmount+"//"+challengeAmount);

        context.startActivity(intent);
    }
}
