package com.sleepchallenge.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.activities.UpdateGroupActivity;
import com.sleepchallenge.holders.GetMemInGroupHolder;
import com.sleepchallenge.itemclicklisteners.GetMemInGroupClickListner;
import com.sleepchallenge.models.GetGroupMembersModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class GetMemUpdateGroupAdapter extends RecyclerView.Adapter<GetMemInGroupHolder> {
    public ArrayList<GetGroupMembersModel> getMemList;
    public UpdateGroupActivity context;
    LayoutInflater li;
    int resource;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String member_id,fetch_group_id;
    UserSessionManager userSessionManager;
    String device_id, token;




    public GetMemUpdateGroupAdapter(ArrayList<GetGroupMembersModel> getMemList, UpdateGroupActivity context, int resource, String fetch_group_id_G) {
       this.getMemList = getMemList;
       this.context = context;
       this.resource = resource;
       this.fetch_group_id = fetch_group_id_G;
       li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        pprogressDialog = new ProgressDialog(context);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        Log.d("JJJJ",fetch_group_id+"//"+fetch_group_id_G);
        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

    }

    @Override
    public GetMemInGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, null);
        GetMemInGroupHolder slh = new GetMemInGroupHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final GetMemInGroupHolder holder, final int position)
    {

        Log.d("MEMEBRET", getMemList.get(position).getUser_name_M());
        holder.memNAME.setText(getMemList.get(position).getUser_name_M());


          member_id=getMemList.get(position).getId_M();
         Log.d("MMMID", member_id+"///"+fetch_group_id);



        holder.setItemClickListener(new GetMemInGroupClickListner() {
            @Override
            public void onItemClick(View view, int layoutPosition)
            {
              //  Toast.makeText(context,"CLLLLLLLIKKC",Toast.LENGTH_LONG).show();
               deleteMember();
            }
        });

    }

    private void deleteMember()
    {
        checkInternet= NetworkChecking.isConnected(context);
        if (checkInternet)
        {
            pprogressDialog.show();
            Log.d("DELETURL", AppUrls.BASE_URL + AppUrls.GROUP_DELETE+fetch_group_id+"/"+member_id);
//http://192.168.1.61:8090/api/v1/group/5a03dc43c4e3908af07b7e0c/5a327d47014cf51f94dfc4f8
            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, AppUrls.BASE_URL + AppUrls.GROUP_DELETE+fetch_group_id+"/"+member_id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            Log.d("DELTRESP", response);

                            try
                            {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if(successResponceCode.equals("10100"))
                                {//{"success":true,"message":"Member is removed successfully","response_code":10100}
                                    // pprogressDialog.dismiss();
                                     Toast.makeText(context,"Member is Removed", Toast.LENGTH_LONG);
                                     context.getDataForUpdate();

                                }
                                if (successResponceCode.equals("10200"))
                                {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(context, "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                pprogressDialog.cancel();
                            }
                        }

                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pprogressDialog.cancel();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    })
            {
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);

        } else {

            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public int getItemCount() {
        return getMemList.size();
    }

}
