package com.sleepchallenge.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.sleepchallenge.R;
import com.sleepchallenge.activities.GroupChatDetailActivity;
import com.sleepchallenge.filter.CustomFilterForGroupChat;
import com.sleepchallenge.fragments.GroupChatFragment;
import com.sleepchallenge.holders.GroupListHolder;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;
import com.sleepchallenge.models.GroupMessageModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class GroupListAdapter extends RecyclerView.Adapter<GroupListHolder> implements Filterable {
    public ArrayList<GroupMessageModel> groupListModels;
    public GroupChatFragment context;
    String isRead,group_type;
    LayoutInflater li;
    int resource;
    String from_chat_type;
    Typeface regularFont,boldFont;
    CustomFilterForGroupChat filter;
    public GroupListAdapter(ArrayList<GroupMessageModel> groupListModels, GroupChatFragment context, int resource, String group_type, String chat_type) {
        this.groupListModels = groupListModels;
        this.context = context;
        this.resource = resource;
        this.group_type = group_type;
        from_chat_type=chat_type;
        regularFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.specialFont));
        boldFont = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.boldFont));
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME" , className);
    }



    @Override
    public GroupListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        GroupListHolder slh = new GroupListHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(GroupListHolder holder, final int position)
    {



        String str = groupListModels.get(position).from_name;
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.group_name_text.setText(Html.fromHtml(converted_string));
        holder.group_name_text.setTypeface(boldFont);

       holder.mesage_desc_text.setText(Html.fromHtml(groupListModels.get(position).msg));
       holder.message_sent_time.setText(Html.fromHtml(groupListModels.get(position).sent_on_txt));

        holder.mesage_desc_text.setTypeface(regularFont);
        holder.message_sent_time.setTypeface(regularFont);

          Picasso.with(context.getActivity())
                .load(groupListModels.get(position).profile_pic)
                .placeholder(R.drawable.group_dummy)
                .resize(60,60)
                .into(holder.group_profile_pic);


        holder.setItemClickListener(new AllItemClickListeners()
        {
            @Override
            public void onItemClick(View v, int pos)
            {
               Intent ingrpdetail=new Intent(context.getActivity(), GroupChatDetailActivity.class);

                ingrpdetail.putExtra("group_id",groupListModels.get(position).id);
                ingrpdetail.putExtra("group_name",groupListModels.get(position).from_name);
                ingrpdetail.putExtra("profile_pic",groupListModels.get(position).profile_pic);
                ingrpdetail.putExtra("GROUP_TYPE",group_type);
                ingrpdetail.putExtra("GROUP_CONVERSATION_TYPE",groupListModels.get(position).conversation_type);
                ingrpdetail.putExtra("FROM_TYPE", from_chat_type);
                Log.d("TETAUA",group_type+"IDD:"+groupListModels.get(position).id+"fromname:"+groupListModels.get(position).from_name+"pic:"+groupListModels.get(position).profile_pic+" ctype:"+groupListModels.get(position).conversation_type);


               context.startActivity(ingrpdetail);


            }
        });
    }

    @Override
    public int getItemCount() {
        return this.groupListModels.size();
    }
    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterForGroupChat(groupListModels, this);
        }

        return filter;
    }
}
