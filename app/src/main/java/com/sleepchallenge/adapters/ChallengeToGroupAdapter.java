package com.sleepchallenge.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.sleepchallenge.R;
import com.sleepchallenge.activities.ChallengeAmountActivity;
import com.sleepchallenge.activities.ChallengeToGroupActivity;
import com.sleepchallenge.filter.CustomFilterForChallengeToGroup;
import com.sleepchallenge.holders.MyGroupHolder;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;
import com.sleepchallenge.models.MyGroupModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ChallengeToGroupAdapter extends RecyclerView.Adapter<MyGroupHolder> implements Filterable,View.OnClickListener {

    public ArrayList<MyGroupModel> allgrouplistModels, allgroupfilterList;
    ChallengeToGroupActivity context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont, thinFont;
    CustomFilterForChallengeToGroup filter;
    String challengedate,challengetype,hours,timestr,noofdays;
    String challengerName,remainderstr;


    public ChallengeToGroupAdapter(ArrayList<MyGroupModel> allgrouplistModels, ChallengeToGroupActivity context, int resource,
                                   String challengedate,String challengetype,String hours,String timestr,String noofdays,String remainderstr) {
        this.allgrouplistModels = allgrouplistModels;
        this.context = context;
        this.resource = resource;
        this.allgroupfilterList = allgrouplistModels;
        this.challengedate = challengedate;
        this.challengetype = challengetype;
        this.hours = hours;
        this.timestr = timestr;
        this.noofdays = noofdays;
        this.remainderstr = remainderstr;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.thinFont));
    }


    @Override
    public MyGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        MyGroupHolder slh = new MyGroupHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final MyGroupHolder holder, final int position) {

        String str = allgrouplistModels.get(position).name;
        challengerName=str;
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.group_name.setText(Html.fromHtml(converted_string));
        holder.group_name.setTypeface(boldFont);

        holder.group_ranking_text.setText(Html.fromHtml(allgrouplistModels.get(position).overall_rank));
        holder.group_totlapercentage.setText(Html.fromHtml(allgrouplistModels.get(position).winning_per + " %"));
        holder.group_win_per.setText(Html.fromHtml(allgrouplistModels.get(position).total_win));
        holder.group_los_per.setText(Html.fromHtml(allgrouplistModels.get(position).total_loss));
        holder.member_count.setText(Html.fromHtml(allgrouplistModels.get(position).group_members + " members"));

        String groupimg_profile = allgrouplistModels.get(position).group_pic;
        Log.d("PPPROFILE", groupimg_profile);
        if (groupimg_profile.equals("null") && groupimg_profile.equals("")) {
            Picasso.with(context)
                    .load(R.drawable.group_dummy)
                    .placeholder(R.drawable.group_dummy)
                    .resize(60, 60)
                    .into(holder.group_image);
        } else {
            Picasso.with(context)
                    .load(groupimg_profile)
                    .placeholder(R.drawable.group_dummy)
                    .into(holder.group_image);
        }
        holder.send_request_button.setVisibility(View.VISIBLE);
        holder.send_request_button.setOnClickListener(this);
        holder.send_request_button.setTag(position);
         holder.setItemClickListener(new AllItemClickListeners() {
            @Override
            public void onItemClick(View v, int pos) {

//                Intent intent=new Intent(context, MemberDetailActivity.class);
//                Log.d("IDDD:",allmemberlistModels.get(position).id);
//                intent.putExtra("MEMBER_ID",allmemberlistModels.get(position).id);
//                intent.putExtra("MEMBER_NAME",allmemberlistModels.get(position).name);
//                intent.putExtra("member_user_type",allmemberlistModels.get(position).user_type);
//                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.allgrouplistModels.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterForChallengeToGroup(allgrouplistModels, this);
        }

        return filter;
    }

    @Override
    public void onClick(View v) {
        int pos= (int) v.getTag();
        Intent intent = new Intent(context, ChallengeAmountActivity.class);
        intent.putExtra("challengedate",challengedate);
        intent.putExtra("hours",hours);
        intent.putExtra("time",timestr);
        intent.putExtra("challengetype",challengetype);
        intent.putExtra("challengername",allgrouplistModels.get(pos).name);
        intent.putExtra("challengerid",allgrouplistModels.get(pos).id);
        intent.putExtra("challengertype","GROUP");
        intent.putExtra("activty","challengeto");
        intent.putExtra("remainder",remainderstr);
        if(challengetype.equalsIgnoreCase("Weekly")) {
            intent.putExtra("noofdays", noofdays);
        }
        context.startActivity(intent);
    }
}

