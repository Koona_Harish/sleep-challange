package com.sleepchallenge.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sleepchallenge.R;
import com.sleepchallenge.activities.AllRunningChallengesActivity;
import com.sleepchallenge.holders.AllChallangesMoreHolder;
import com.sleepchallenge.itemclicklisteners.AllChalngMoreItemClickListener;
import com.sleepchallenge.models.AllChallengesStatusModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.UserSessionManager;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class AllChallangRunningAdapter extends RecyclerView.Adapter<AllChallangesMoreHolder> {

    public ArrayList<AllChallengesStatusModel> allChalngMorelistModels;
    AllRunningChallengesActivity context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont,thinFont;
    ProgressDialog progressDialog;
    UserSessionManager session;
    String user_name, challegne_type;


    public AllChallangRunningAdapter(ArrayList<AllChallengesStatusModel> allChalngMorelistModels, AllRunningChallengesActivity context, int resource) {
        this.allChalngMorelistModels = allChalngMorelistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.thinFont));

        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_name = userDetails.get(UserSessionManager.USER_NAME);
    }

    @Override
    public AllChallangesMoreHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        AllChallangesMoreHolder slh = new AllChallangesMoreHolder(layout);
        return slh;
    }


    @Override
    public void onBindViewHolder(final AllChallangesMoreHolder holder, final int position) {
        holder.pause_challenge.setVisibility(View.GONE);
        String str_opponentname = allChalngMorelistModels.get(position).getOpponent_name();
        String converted_string = str_opponentname.substring(0, 1).toUpperCase() + str_opponentname.substring(1);

        String usernem = allChalngMorelistModels.get(position).getUser_name();
        ;
        String converted_user_naem = usernem.substring(0, 1).toUpperCase() + usernem.substring(1);

        holder.all_challenger_names.setText(Html.fromHtml(converted_user_naem));
        holder.oponent_names.setText(Html.fromHtml(converted_string));
        holder.vsText.setTypeface(thinFont);

        holder.all_cash_prize_amount.setText(("$" + allChalngMorelistModels.get(position).getAmount()));


        String timedate = parseDateToddMMyyyy(allChalngMorelistModels.get(position).getStart_on());
        holder.all_dateandtime.setText(Html.fromHtml(timedate));

        challegne_type = allChalngMorelistModels.get(position).getChallenge_type();
        if (challegne_type.equals("DAILY")) {
            holder.challenge_type.setText(challegne_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.daily_bg));
        } else if (challegne_type.equals("LONGRUN")) {
            holder.challenge_type.setText(challegne_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.longrun_bg));
        } else {
            holder.challenge_type.setText(challegne_type);
            holder.challenge_type.setTextColor(context.getResources().getColor(R.color.week_bg));
        }

        if (allChalngMorelistModels.get(position).getStatus().equals("RUNNING"))
            holder.all_status.setText(Html.fromHtml("ONGOING"));
        holder.all_status.setBackgroundColor(Color.parseColor("#239843"));
        holder.all_status.setTextColor(Color.parseColor("#ffffff"));


        /*Picasso.with(context)
                .load(allChalngMorelistModels.get(position).getActivity_image())
                .placeholder(R.drawable.no_image_found)
                .into(holder.all_activity_image);*/

        holder.share_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Activity Challenge - Take a Challenge with -");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "I'm inviting you to join ActivityChallenge App. To stay active and earn money Why don't you join?. "+ AppUrls.SHARE_APP_URL);
                context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });


        holder.setItemClickListener(new AllChalngMoreItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                /*if (allChalngMorelistModels.get(pos).getEvaluation_factor().equals("STEPS")) {
                    Intent intent = new Intent(context, StepChallengeStatusActivity.class);
                    intent.putExtra("challenge_id", allChalngMorelistModels.get(position).getChallenge_id());
                    intent.putExtra("activity_id", allChalngMorelistModels.get(position).getActivity_id());
                    intent.putExtra("opponent_type", allChalngMorelistModels.get(position).getOpponent_type());
                    intent.putExtra("gps", allChalngMorelistModels.get(position).getOpponent_type());
                    context.startActivity(intent);
                } else if (allChalngMorelistModels.get(pos).getEvaluation_factor().equals("DISTANCE")) {
                    //  Toast.makeText(context, "KM", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, KmChallengeStatusActivity.class);
                    intent.putExtra("challenge_id", allChalngMorelistModels.get(position).getChallenge_id());
                    intent.putExtra("activity_id", allChalngMorelistModels.get(position).getActivity_id());
                    intent.putExtra("opponent_type", allChalngMorelistModels.get(position).getOpponent_type());
                    intent.putExtra("gps", allChalngMorelistModels.get(position).getOpponent_type());
                    context.startActivity(intent);
                } else if (allChalngMorelistModels.get(pos).getEvaluation_factor().equals("TIME")) {
                    // Toast.makeText(context, "MINUTE", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, MinuiteChallengeStatusActivity.class);
                    intent.putExtra("challenge_id", allChalngMorelistModels.get(position).getChallenge_id());
                    intent.putExtra("activity_id", allChalngMorelistModels.get(position).getActivity_id());
                    intent.putExtra("opponent_type", allChalngMorelistModels.get(position).getOpponent_type());
                    intent.putExtra("gps", allChalngMorelistModels.get(position).getOpponent_type());
                    context.startActivity(intent);
                } else if (allChalngMorelistModels.get(pos).getEvaluation_factor().equals("IMAGE")) {
                    // Toast.makeText(context, "HAMMING DISTANCE", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, PictureChallengeStatusActivity.class);
                    intent.putExtra("challenge_id", allChalngMorelistModels.get(position).getChallenge_id());
                    intent.putExtra("activity_id", allChalngMorelistModels.get(position).getActivity_id());
                    intent.putExtra("opponent_type", allChalngMorelistModels.get(position).getOpponent_type());
                    Log.d("IntentData", allChalngMorelistModels.get(position).getActivity_id() + "\n" + allChalngMorelistModels.get(position).getChallenge_id());
                    context.startActivity(intent);
                }else if (allChalngMorelistModels.get(pos).getEvaluation_factor().equals("COUNT")) {
                    // Toast.makeText(context, "HAMMING DISTANCE", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, PushupChallengeStatusActivity.class);
                    intent.putExtra("challenge_id", allChalngMorelistModels.get(position).getChallenge_id());
                    intent.putExtra("activity_id", allChalngMorelistModels.get(position).getActivity_id());
                    intent.putExtra("opponent_type", allChalngMorelistModels.get(position).getOpponent_type());
                    Log.d("IntentData", allChalngMorelistModels.get(position).getActivity_id() + "\n" + allChalngMorelistModels.get(position).getChallenge_id());
                    context.startActivity(intent);
                }*/

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.allChalngMorelistModels.size();
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


}
