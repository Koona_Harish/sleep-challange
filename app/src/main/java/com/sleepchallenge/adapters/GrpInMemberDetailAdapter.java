package com.sleepchallenge.adapters;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sleepchallenge.R;
import com.sleepchallenge.activities.GroupDetailActivity;
import com.sleepchallenge.activities.MemberDetailActivity;
import com.sleepchallenge.filter.CustomFilterForAllGroupList;
import com.sleepchallenge.holders.MyGroupHolder;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;
import com.sleepchallenge.models.GrpInMemberDetailModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GrpInMemberDetailAdapter extends RecyclerView.Adapter<MyGroupHolder>  {

    public ArrayList<GrpInMemberDetailModel> allgrouplistModels;
    MemberDetailActivity context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont,thinFont;
    CustomFilterForAllGroupList filter;


    public GrpInMemberDetailAdapter(ArrayList<GrpInMemberDetailModel> allgrouplistModels, MemberDetailActivity context, int resource) {
        this.allgrouplistModels = allgrouplistModels;
        this.context = context;
        this.resource = resource;

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.thinFont));
    }




    @Override
    public MyGroupHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        MyGroupHolder slh = new MyGroupHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final MyGroupHolder holder, final int position)
    {

        String str = allgrouplistModels.get(position).group_name;
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.group_name.setText(Html.fromHtml(converted_string));
        holder.group_name.setTypeface(boldFont);

        holder.group_ranking_text.setText(Html.fromHtml(allgrouplistModels.get(position).group_rank));
        holder.group_totlapercentage.setText(Html.fromHtml(allgrouplistModels.get(position).winning_percentage+" %"));
        holder.group_win_per.setText(Html.fromHtml(allgrouplistModels.get(position).win_challenges));
        holder.group_los_per.setText(Html.fromHtml(allgrouplistModels.get(position).lost_challenges));
        holder.member_count.setText(Html.fromHtml(allgrouplistModels.get(position).group_members+" members"));
        holder.send_request_button.setVisibility(View.GONE);
        String groupimg_profile=allgrouplistModels.get(position).group_pic;
        Log.d("PPPROFILE",groupimg_profile);
         if(groupimg_profile.equals("null") && groupimg_profile.equals(""))
         {
             Picasso.with(context)
                     .load(R.drawable.group_dummy)
                     .placeholder(R.drawable.group_dummy)
                     .resize(60,60)
                     .into(holder.group_image);
         }
         else
         {
             Picasso.with(context)
                     .load(groupimg_profile)
                     .placeholder(R.drawable.group_dummy)
                     .into(holder.group_image);
         }

        holder.setItemClickListener(new AllItemClickListeners()
        {
            @Override
            public void onItemClick(View v, int pos)
            {

               Intent ii = new Intent(context, GroupDetailActivity.class);
                ii.putExtra("grp_id" , allgrouplistModels.get(pos).id);
                ii.putExtra("grp_name" , allgrouplistModels.get(pos).group_name);
                ii.putExtra("grp_admin_id" , allgrouplistModels.get(pos).admin_id);
                ii.putExtra("GROUP_CONVERSATION_TYPE" , allgrouplistModels.get(pos).conversation_type);
                context.startActivity(ii);
            }
        });

    }

    @Override
    public int getItemCount()
    {
        return this.allgrouplistModels.size();
    }


}
