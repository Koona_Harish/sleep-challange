package com.sleepchallenge.adapters;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.sleepchallenge.R;
import com.sleepchallenge.activities.AllMembersActivity;
import com.sleepchallenge.activities.MemberDetailActivity;
import com.sleepchallenge.filter.CustomFilterForAllMembersList;
import com.sleepchallenge.holders.AllMembersHolder;
import com.sleepchallenge.itemclicklisteners.AllMembersItemClickListener;
import com.sleepchallenge.models.AllMemberModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AllMembersAdapter extends RecyclerView.Adapter<AllMembersHolder> implements Filterable {

    public ArrayList<AllMemberModel> allmemberlistModels, allmembersfilterList;
    AllMembersActivity context;
    LayoutInflater li;
    int resource;
    Typeface regularFont, boldFont,specialFont;
    CustomFilterForAllMembersList filter;

    String challengerName;
    String groupId;

    public AllMembersAdapter(ArrayList<AllMemberModel> allmemberlist, AllMembersActivity context, int resource,String groupId) {
        this.allmemberlistModels = allmemberlist;
        this.context = context;
        this.resource = resource;
        this.groupId=groupId;

        this.allmembersfilterList = allmemberlistModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        regularFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.boldFont));
        specialFont = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.specialFont));
    }

    @Override
    public AllMembersHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        AllMembersHolder slh = new AllMembersHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final AllMembersHolder holder, final int position)
    {
        if(groupId!=null && groupId.length()>0){
            String str = allmemberlistModels.get(position).user_name;
            challengerName=str;
            String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
            holder.all_member_user_name.setText(Html.fromHtml(converted_string));
            holder.all_member_user_name.setTypeface(boldFont);

            holder.all_member_ranking_text.setText(Html.fromHtml(allmemberlistModels.get(position).user_rank));
            holder.all_member_ranking_text.setTypeface(regularFont);
            holder.all_member_totlapercentage.setText(Html.fromHtml(allmemberlistModels.get(position).winning_percentage+" %"));
            holder.all_member_totlapercentage.setTypeface(specialFont);
            holder.all_member_win_per.setText(Html.fromHtml(allmemberlistModels.get(position).win_challenges));
            holder.all_member_win_per.setTypeface(regularFont);
            holder.all_member_los_per.setText(Html.fromHtml(allmemberlistModels.get(position).lost_challenges));
            holder.all_member_los_per.setTypeface(regularFont);

            String img_profile=allmemberlistModels.get(position).profile_pic;
            Log.d("PPPROFILE",img_profile);
            if(img_profile.equals("null") && img_profile.equals(""))
            {
                Picasso.with(context)
                        .load(R.drawable.members_dummy)
                        .placeholder(R.drawable.members_dummy)
                        .resize(60,60)
                        .into(holder.all_member_image);
            }
            else
            {
                Picasso.with(context)
                        .load(allmemberlistModels.get(position).profile_pic)
                        .placeholder(R.drawable.members_dummy)
                        .into(holder.all_member_image);
            }
        }else{
            String str = allmemberlistModels.get(position).name;
            challengerName=str;
            String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
            holder.all_member_user_name.setText(Html.fromHtml(converted_string));
            holder.all_member_user_name.setTypeface(boldFont);

            holder.all_member_ranking_text.setText(Html.fromHtml(allmemberlistModels.get(position).overall_rank));
            holder.all_member_ranking_text.setTypeface(regularFont);
            holder.all_member_totlapercentage.setText(Html.fromHtml(allmemberlistModels.get(position).winning_per+" %"));
            holder.all_member_totlapercentage.setTypeface(regularFont);
            holder.all_member_win_per.setText(Html.fromHtml(allmemberlistModels.get(position).total_win));
            holder.all_member_win_per.setTypeface(regularFont);
            holder.all_member_los_per.setText(Html.fromHtml(allmemberlistModels.get(position).total_loss));
            holder.all_member_los_per.setTypeface(regularFont);

            String img_profile=allmemberlistModels.get(position).profile_pic;
            Log.d("PPPROFILE",img_profile);
            if(img_profile.equals("null") && img_profile.equals(""))
            {
                Picasso.with(context)
                        .load(R.drawable.members_dummy)
                        .placeholder(R.drawable.members_dummy)
                        .resize(60,60)
                        .into(holder.all_member_image);
            }
            else
            {
                Picasso.with(context)
                        .load(allmemberlistModels.get(position).profile_pic)
                        .placeholder(R.drawable.members_dummy)
                        .into(holder.all_member_image);
            }

        }


        holder.setItemClickListener(new AllMembersItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {

               if(groupId.length()==0){
                   Intent intent=new Intent(context, MemberDetailActivity.class);
                   Log.d("IDDD:",allmemberlistModels.get(position).id);
                   intent.putExtra("MEMBER_ID",allmemberlistModels.get(position).id);
                   intent.putExtra("MEMBER_NAME",allmemberlistModels.get(position).name);
                   intent.putExtra("member_user_type",allmemberlistModels.get(position).user_type);
                   context.startActivity(intent);
               }

            }
        });

    }

    @Override
    public int getItemCount()
    {
        return this.allmemberlistModels.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new CustomFilterForAllMembersList(allmembersfilterList, this,groupId);
        }

        return filter;
    }
}
