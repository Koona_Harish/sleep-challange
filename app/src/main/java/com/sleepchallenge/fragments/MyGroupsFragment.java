package com.sleepchallenge.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.MyGroupsAdapter;
import com.sleepchallenge.models.MyGroupModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.DividerItemDecorator;
import com.sleepchallenge.utils.MovableFloatingActionButton;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyGroupsFragment extends Fragment implements View.OnClickListener  {

    View view;

    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String user_id, user_type, token, device_ID;
    RecyclerView my_groups_recyclerView;
    ArrayList<MyGroupModel> allMyGroupList;
    MyGroupsAdapter myGroupAdapter;
    RecyclerView.LayoutManager layoutManager;
    Typeface thinFont,regularFont;
    SearchView group_search;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        view= inflater.inflate(R.layout.fragment_my_groups, container, false);

        thinFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.thinFont));
        regularFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.regularFont));
        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_ID = userDetails.get(UserSessionManager.DEVICE_ID);
        checkInternet = NetworkChecking.isConnected(getActivity());
        group_search= view.findViewById(R.id.group_search);
        my_groups_recyclerView =  view.findViewById(R.id.my_groups_recyclerView);


        group_search.setOnClickListener(this);
        //View view = findViewById(R.id.custom_tab);
        group_search.setIconified(true);
        group_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                group_search.setIconified(false);
            }
        });
        group_search.clearFocus();
        group_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if(myGroupAdapter!=null)
                    myGroupAdapter.getFilter().filter(query);
                return false;
            }
        });

         getMyGroups();

        return view;
    }

    private void getMyGroups() {
        if (checkInternet) {
            Dialog.showProgressBar(getActivity(), "Loading");

             String myGroupurl = AppUrls.BASE_URL + "group/user?user_id=" + user_id + "&user_type=" + user_type;
            Log.d("MYGROUPURL", myGroupurl);

            StringRequest strAllMember = new StringRequest(Request.Method.GET, myGroupurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Dialog.hideProgressBar();
                    Log.d("MYGROUPRESP", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100"))
                        {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            allMyGroupList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                MyGroupModel mygroup = new MyGroupModel(jdataobj);
                                allMyGroupList.add(mygroup);
                            }
                            myGroupAdapter = new MyGroupsAdapter(allMyGroupList, MyGroupsFragment.this, R.layout.row_my_group);
                            layoutManager = new LinearLayoutManager(getActivity());
                            my_groups_recyclerView.setNestedScrollingEnabled(false);
                            my_groups_recyclerView.setLayoutManager(layoutManager);
                            RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.recycler_view_divider));
                            my_groups_recyclerView.addItemDecoration(dividerItemDecoration);

                            my_groups_recyclerView.setAdapter(myGroupAdapter);
                        }
                        if (successResponceCode.equals("10200"))
                        {
                            Toast.makeText(getActivity(), "Invalid Input..!", Toast.LENGTH_SHORT).show();
                        }
                        if (successResponceCode.equals("10300"))
                        {
                            Toast.makeText(getActivity(), "No Data Found..!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Dialog.hideProgressBar();
                    Log.e("Error", error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strAllMember.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(strAllMember);
        } else {
            Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {

    }
}
