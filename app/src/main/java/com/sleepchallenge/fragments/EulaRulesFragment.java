package com.sleepchallenge.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sleepchallenge.R;

public class EulaRulesFragment extends Fragment {


   TextView  eula_rules_text_title;
   View view;
    Typeface thinFont;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_eula_rules, container, false);

        thinFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.thinFont));

        eula_rules_text_title=view.findViewById(R.id.eula_rules_text_title);
        eula_rules_text_title.setTypeface(thinFont);

        return view;
    }


}
