package com.sleepchallenge.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;

public class ContactusFragment extends Fragment
{
   View view;
   WebView contactwebVw;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        view=inflater.inflate(R.layout.fragment_contactus, container, false);

        contactwebVw = view. findViewById(R.id.contactwebVw);

        contactwebVw.getSettings().setJavaScriptEnabled(true);
        contactwebVw.setWebViewClient(new MyBrowser());
        contactwebVw.loadUrl(AppUrls.BASE_IMAGE_URL + AppUrls.CONTACT_US_LINK);

        return view;
    }
    class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }



}
