package com.sleepchallenge.fragments;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.models.MySponsoringModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MySponsoringFragment extends Fragment {

    View view;

    private boolean checkInternet;

    String device_id, access_token, user_id,user_type;
    UserSessionManager session;
    ArrayList<MySponsoringModel> mySponsoringList;
    RecyclerView my_sponsoring_recyclerView;
    RecyclerView.LayoutManager layoutManager;
    SponsoringAdapter sponsorAdapter;
    Typeface thinFont,regularFont,boldFont;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_my_sponsoring, container, false);

        thinFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.thinFont));
        regularFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.boldFont));
        session = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);

        my_sponsoring_recyclerView =  view.findViewById(R.id.my_sponsoring_recyclerView);

        getMysponsoring();
        return view;
    }

    private void getMysponsoring() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet)
        {
            Log.d("UUUURLLL", AppUrls.BASE_URL + AppUrls.MY_SPONSORINGS +"?user_id="+user_id+ AppUrls.USER_TYPE + user_type);

            StringRequest strRe = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.MY_SPONSORINGS +"?user_id="+user_id+ AppUrls.USER_TYPE + user_type,

                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            Log.d("REPRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("response_code");
                                if (successResponceCode.equals("10100")) {




                                    JSONArray jArr = jsonObject.getJSONArray("data");
                                    mySponsoringList = new ArrayList<>();
                                    for (int i = 0; i < jArr.length(); i++)
                                    {
                                        JSONObject jdataobj = jArr.getJSONObject(i);
                                        MySponsoringModel sponreq = new MySponsoringModel(jdataobj);
                                        mySponsoringList.add(sponreq);
                                    }
                                    sponsorAdapter = new SponsoringAdapter(MySponsoringFragment.this,mySponsoringList,  R.layout.row_my_sponsorings);
                                    layoutManager = new LinearLayoutManager(getActivity());
                                    my_sponsoring_recyclerView.setNestedScrollingEnabled(false);
                                    my_sponsoring_recyclerView.setLayoutManager(layoutManager);
//                                    RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.recycler_view_divider));
//                                    my_sponsoring_recyclerView.addItemDecoration(dividerItemDecoration);
                                    my_sponsoring_recyclerView.setAdapter(sponsorAdapter);
                                }
                                if (successResponceCode.equals("10200")) {

                                    Toast.makeText(getActivity(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("10300")) {

                                    Toast.makeText(getActivity(), "No data Found", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                           error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders()  {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", access_token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strRe.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(strRe);
        } else {

            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }
    //ADAPTER

    class SponsoringAdapter extends RecyclerView.Adapter<MySponsoringHistoryHolder> {
        MySponsoringFragment context;
        int resource;
        ArrayList<MySponsoringModel> mysponingList;
        LayoutInflater lInfla;

        public SponsoringAdapter(MySponsoringFragment context, ArrayList<MySponsoringModel> mysponingList, int resource) {
            this.context = context;
            this.resource = resource;
            this.mysponingList = mysponingList;
            lInfla = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public MySponsoringHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View inf = lInfla.inflate(resource, parent, false);
            MySponsoringHistoryHolder rHol = new MySponsoringHistoryHolder(inf);
            return rHol;
        }

        @Override
        public void onBindViewHolder(MySponsoringHistoryHolder holder, int position) {

            holder.card.setCardBackgroundColor(Color.parseColor("#E0E0E0"));

            String sponsertypeto=mysponingList.get(position).to_sponsor_type;
            Log.d("sponsertypeto",mysponingList.get(position).to_sponsor_type);

            holder.name.setText(mysponingList.get(position).name);
            holder.name.setTypeface(boldFont);

            Log.d("PPPTAH",mysponingList.get(position).profile_pic);
            Picasso.with(getActivity())
                    .load(mysponingList.get(position).profile_pic)
                    .placeholder(R.drawable.members_dummy)
                    .into(holder.sponcering_image);

            holder.status.setText(mysponingList.get(position).status);
            holder.status.setTypeface(regularFont);
            holder.amount.setText("$"+mysponingList.get(position).amount);
            holder.amount.setTypeface(regularFont);

            String timedate=parseDateToddMMyyyy(mysponingList.get(position).date);

            holder.date.setText(Html.fromHtml(timedate));
            holder.date.setTypeface(regularFont);

            holder.sponser_type_to.setText(sponsertypeto);
            holder.sponser_type_to.setTypeface(regularFont);

            if(sponsertypeto.equals("USER"))
            {
                //  holder.sponser_type_to.setText("Type : USER");
                Picasso.with(getActivity())
                        .load(mysponingList.get(position).profile_pic)
                        .placeholder(R.drawable.members_dummy)
                        .into(holder.sponcering_image);
            }
            else
            {
                //  holder.sponser_type_to.setText("Type : GROUP");
                Picasso.with(getActivity())
                        .load(mysponingList.get(position).profile_pic)
                        .placeholder(R.drawable.group_dummy)
                        .into(holder.sponcering_image);
            }

            if (mysponingList.get(position).status.equals("ACCEPT")) {
                holder.status.setTextColor(Color.parseColor("#38ac59"));
            } else if (mysponingList.get(position).status.equals("PENDING")) {
                holder.status.setTextColor(Color.parseColor("#FF933A3A"));
            } else if (mysponingList.get(position).status.equals("REJECT")) {
                holder.status.setTextColor(Color.RED);
            }
        }

        @Override
        public int getItemCount() {
            return mysponingList.size();
        }
    }


    //Holder

    public class MySponsoringHistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public TextView name, status, amount,date,sponser_type_to;
        public CircleImageView sponcering_image;
        public CardView card;

        public MySponsoringHistoryHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            name =  itemView.findViewById(R.id.name);
            status = itemView.findViewById(R.id.status);
            amount =  itemView.findViewById(R.id.amount);
            date = itemView.findViewById(R.id.date);
            sponser_type_to =  itemView.findViewById(R.id.sponser_type_to);
            sponcering_image =  itemView.findViewById(R.id.sponcering_image);
            card =  itemView.findViewById(R.id.card);


        }
        @Override
        public void onClick(View view) {

        }
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}
