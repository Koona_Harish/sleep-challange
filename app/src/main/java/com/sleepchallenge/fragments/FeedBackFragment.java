package com.sleepchallenge.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class FeedBackFragment extends Fragment implements View.OnClickListener {

    View view;
    private boolean checkInternet;
    UserSessionManager session;
    String user_id, user_type,device_name, access_token, device_id,radioValue, title, details;
    RadioGroup rb_group;
    RadioButton rb_feedback, rb_suggest, rb_complain, radioButton;
    Button send_report;
    TextView help_text_title;
    Typeface regularFont;
    EditText report_message,report_title;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {

        view= inflater.inflate(R.layout.fragment_feed_back, container, false);

        device_name = android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL;
        regularFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.regularFont));
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        access_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        help_text_title=view.findViewById(R.id.help_text_title);
        help_text_title.setTypeface(regularFont);
        send_report=view.findViewById(R.id.send_report);
        send_report.setTypeface(regularFont);
        send_report.setOnClickListener(this);

        report_message = view. findViewById(R.id.report_message);
        report_title = view. findViewById(R.id.report_title);

        rb_group = view. findViewById(R.id.rb_group);
        rb_feedback = view. findViewById(R.id.rb_feedback);
        rb_feedback.setTypeface(regularFont);
        rb_suggest = view. findViewById(R.id.rb_suggest);
        rb_suggest.setTypeface(regularFont);
        rb_complain =view. findViewById(R.id.rb_complain);
        rb_complain.setTypeface(regularFont);


        return view;
    }

    @Override
    public void onClick(View v)
    {
     if(v==send_report)
     {
         checkInternet = NetworkChecking.isConnected(getActivity());
         if (checkInternet)
         {
             Dialog.showProgressBar(getActivity(),"Loading");
             if (validate())
             {

                 int selectedId = rb_group.getCheckedRadioButtonId();
                 radioButton =view.findViewById(selectedId);
                 radioValue = radioButton.getText().toString();
                 Log.d("xdfsd", radioValue);
                 title = report_title.getText().toString().trim();
                 details = report_message.getText().toString().trim();
                 Log.d("vfdurl", AppUrls.BASE_URL + AppUrls.FEEDBACK);
                 StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FEEDBACK,
                         new Response.Listener<String>() {
                             @Override
                             public void onResponse(String response) {
                                 Dialog.hideProgressBar();
                                 Log.d("REPORTRESP", response);

                                 try {
                                     JSONObject jsonObject = new JSONObject(response);
                                     String successResponceCode = jsonObject.getString("response_code");
                                     if (successResponceCode.equals("10100")) {
                                         Dialog.hideProgressBar();
                                         Toast.makeText(getActivity(), "Report has Sent Successfully", Toast.LENGTH_SHORT).show();
                                         getActivity().finish();
                                     }
                                     if (successResponceCode.equals("10200")) {
                                         Dialog.hideProgressBar();
                                         Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                     }
                                 } catch (JSONException e) {
                                     e.printStackTrace();
                                     Dialog.hideProgressBar();
                                 }
                             }
                         },
                         new Response.ErrorListener() {
                             @Override
                             public void onErrorResponse(VolleyError error)
                             {
                                 Dialog.hideProgressBar();

                                error.getMessage();
                             }
                         }) {

                     @Override
                     public Map<String, String> getHeaders() {
                         Map<String, String> headers = new HashMap<>();
                         headers.put("x-access-token", access_token);
                         headers.put("x-device-id", device_id);
                         headers.put("x-device-platform", "ANDROID");
                         Log.d("REPORT_HEADER", "HEADDER " + headers.toString());
                         return headers;
                     }

                     @Override
                     protected Map<String, String> getParams() {
                         Map<String, String> params = new HashMap<String, String>();
                         params.put("user_id", user_id);
                         params.put("user_type", user_type);
                         params.put("type", radioValue);
                         params.put("title", title);
                         params.put("details", details);
                         params.put("device_name", device_name);
                         Log.d("REPORT_PARAM:", "PARMS" + params.toString());
                         return params;
                     }
                 };
                 stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                 RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                 requestQueue.add(stringRequest);
             }

         } else {

             Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
         }
     }
    }


    public boolean validate() {
        boolean result = true;
        String msg = report_message.getText().toString().trim();
        if (msg.isEmpty() || msg.equals("") || msg.equals(null)) {
            report_message.setError("Please Enter  Report Message");
            result = false;
        } else {
            report_message.setError(null);
        }
        String titl = report_title.getText().toString().trim();
        if (titl.isEmpty() || titl.equals("") || titl.equals(null)) {
            report_title.setError("Please Enter  Report Subject");
            result = false;
        } else {
            report_title.setError(null);
        }
        return result;
    }
}
