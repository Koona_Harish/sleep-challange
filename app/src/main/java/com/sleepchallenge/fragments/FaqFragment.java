package com.sleepchallenge.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sleepchallenge.R;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class FaqFragment extends Fragment {

    View view;
    WebView webVw;

    public FaqFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)

    {
        view= inflater.inflate(R.layout.fragment_faq, container, false);

         Dialog.showProgressBar(getActivity(),"Loading...");
        webVw = view. findViewById(R.id.webVw);
        webVw.getSettings().setJavaScriptEnabled(true);
        webVw.setWebViewClient(new MyBrowser());
        webVw.loadUrl(AppUrls.BASE_IMAGE_URL + AppUrls.HELP_LINK);
         Dialog.hideProgressBar();


        return view;
    }
    class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//            pprogressDialog.dismiss();
            view.loadUrl(url);
            return true;
        }
    }

}
