package com.sleepchallenge.fragments;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sleepchallenge.R;
import com.sleepchallenge.activities.AllMembersActivity;
import com.sleepchallenge.activities.GroupsActivity;
import com.sleepchallenge.activities.PaymentDetailsActivity;

public class MeAsASponsorFragment extends Fragment implements  View.OnClickListener {

    View view;
    TextView sponsor_user,sponsor_group,sponsor_app;
    Typeface thinFont,regularFont,boldFont;
    android.app.Dialog addMoneydialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view= inflater.inflate(R.layout.fragment_me_as_asponsor, container, false);

        thinFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.thinFont));
        regularFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.boldFont));

        sponsor_user = view.findViewById(R.id.sponsor_user);
        sponsor_user.setTypeface(boldFont);
        sponsor_group = view.findViewById(R.id.sponsor_group);
        sponsor_group.setTypeface(boldFont);
        sponsor_app =  view.findViewById(R.id.sponsor_app);
        sponsor_app.setTypeface(boldFont);

        sponsor_user.setOnClickListener(this);
        sponsor_group.setOnClickListener(this);
        sponsor_app.setOnClickListener(this);



        return view;
    }

    @Override
    public void onClick(View view)
    {

        if(view==sponsor_user)
        {
            Intent intent = new Intent(getActivity(), AllMembersActivity.class);
            startActivity(intent);
        }
        if(view==sponsor_group)
        {
            Intent intent = new Intent(getActivity(), GroupsActivity.class);
            startActivity(intent);
        }
        if(view==sponsor_app)
        {
            addMoneydialog=new android.app.Dialog(getActivity());
            addMoneydialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            addMoneydialog.setContentView(R.layout.custom_dialog_send_money);
            TextView titleDialog = addMoneydialog.findViewById(R.id.titleDialog);
            final EditText edt_send_amount = addMoneydialog.findViewById(R.id.edt_send_amount);
            Button addMoneyButton = addMoneydialog.findViewById(R.id.sendMoneyButton);
            titleDialog.setText("Sponsor To APP");
            addMoneyButton.setText("Sponsor Money");
            titleDialog.setTypeface(regularFont);
            addMoneyButton.setTypeface(regularFont);
            addMoneyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    String send_amount=edt_send_amount.getText().toString();
                    if(send_amount == null || send_amount.equals("") || send_amount.length() < 0)
                    {
                        edt_send_amount.setError("Please Enter Amount..!");
                    }
                    else
                    {
                        sendWalletAmount(send_amount);

                    }
                }
            });
            addMoneydialog.show();
        }

    }

    private void sendWalletAmount(String send_amount)
    {
       Intent intent = new Intent(getActivity(), PaymentDetailsActivity.class);
        intent.putExtra("activty", "MeAsaSponsorAPP");
        intent.putExtra("sponsorAmount", send_amount);

        startActivity(intent);
    }
}
