package com.sleepchallenge.fragments;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.activities.MessageDetailActivity;
import com.sleepchallenge.filter.CustomFilterForSponsorChat;
import com.sleepchallenge.holders.MessagesHolder;
import com.sleepchallenge.itemclicklisteners.AllItemClickListeners;
import com.sleepchallenge.models.MessagesModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class SponsorChatFragment extends Fragment {


    View view;
    ImageView voice_search;

    RecyclerView recycler_messages;
    MessagesSponsorAdapter mesageAdapter;
    ArrayList<MessagesModel> msgModalList;
    LinearLayoutManager layoutManager;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String user_id,user_type,token,device_id;
    SearchView activity_search;
    EditText searchEditText;
    String chat_type="USER";
    private final int REQ_CODE_SPEECH_INPUT = 100;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.fragment_sponsor_chat, container, false);

        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        activity_search = view. findViewById(R.id.members_search);
        activity_search.setFocusable(false);
        activity_search.setIconified(false);
        activity_search.setIconifiedByDefault(false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        searchEditText =  activity_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setHint("Search a Sponsor");
        searchEditText.setHintTextColor(getResources().getColor(R.color.gray));
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        ImageView voiceIcon = activity_search.findViewById(android.support.v7.appcompat.R.id.search_voice_btn);
        voiceIcon.setImageResource(R.drawable.voice_icon);
        voice_search =  view.findViewById(R.id.voice_search);
        Log.d("DETAILL",user_id+"//"+user_type+"//"+token+"//"+device_id);
        recycler_messages=view.findViewById(R.id.recycler_messages);

        activity_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if(mesageAdapter!=null)
                    mesageAdapter.getFilter().filter(query);
                return false;
            }
        });
        activity_search.clearFocus();
        voice_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                        "Search Activities");
                try {
                    startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
                } catch (ActivityNotFoundException a) {
                    Toast.makeText(getContext(),
                            "Search not Supported",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        getSponsorMessages();
        return view;
    }

    private void getSponsorMessages()
    {

        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet)
        {
            String userMSGURL= AppUrls.BASE_URL+AppUrls.GET_NOTIFICATION_MESSAGE+"?entity_id="+user_id+"&entity_type="+user_type+"&flag=sponsor";
            Log.d("MSGURL", userMSGURL);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, userMSGURL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try
                            {
                                msgModalList=new ArrayList<>();
                                Log.d("MESSAGRESP",response);
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("response_code");
                                if (responceCode.equals("10100"))
                                {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++)
                                    {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        MessagesModel msg = new MessagesModel(jsonObject1);
                                        msgModalList.add(msg);
                                    }
                                    mesageAdapter = new MessagesSponsorAdapter(msgModalList, SponsorChatFragment.this, R.layout.row_messages,chat_type);
                                    layoutManager = new LinearLayoutManager(getActivity());
                                    recycler_messages.setNestedScrollingEnabled(false);
                                    recycler_messages.setLayoutManager(layoutManager);
                                    recycler_messages.setAdapter(mesageAdapter);
                                }

                                if (responceCode.equals("10200")) {

                                    Toast.makeText(getActivity(), "Invlid Input..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10300")) {

                                    Toast.makeText(getActivity(), "No Data Found..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {

                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.getMessage();
                }
            }){

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("RDHEAED",headers.toString());
                    return headers;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else
        {
            Toast.makeText(getActivity(),"No Internet Connection..!",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    searchEditText.setText("" + result.get(0));
                    mesageAdapter.getFilter().filter("" + result.get(0));
                }
                break;
            }

        }
    }

    public class MessagesSponsorAdapter extends RecyclerView.Adapter<MessagesHolder> implements Filterable {
        public ArrayList<MessagesModel> msgModels,sponsorfilter;
        public SponsorChatFragment context;
        String isRead;
        LayoutInflater li;
        int resource;
        String from_chat_type;
        Typeface typeface, typeface2;
        CustomFilterForSponsorChat filter;

        public MessagesSponsorAdapter(ArrayList<MessagesModel> msgModel, SponsorChatFragment ctx, int layout,String chat_type) {
            msgModels = msgModel;
            context = ctx;
            resource = layout;
            sponsorfilter=msgModel;
            from_chat_type=chat_type;
            typeface = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.regularFont));
            typeface2 = Typeface.createFromAsset(context.getActivity().getAssets(), context.getResources().getString(R.string.boldFont));
            li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            String className = this.getClass().getCanonicalName();
            Log.d("CURRENTCLASSNAME", className);
        }


        @Override
        public MessagesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = li.inflate(resource, parent, false);
            MessagesHolder slh = new MessagesHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(MessagesHolder holder, final int position) {


            String str = msgModels.get(position).from_name;
            String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
            holder.message_name_text.setText(Html.fromHtml(converted_string));
            holder.message_name_text.setTypeface(typeface);

            String str_desc = msgModels.get(position).msg;
            String converted_desc_string = str_desc.substring(0, 1).toUpperCase() + str_desc.substring(1);
            holder.mesage_desc_text.setText(Html.fromHtml(converted_desc_string));
            holder.mesage_desc_text.setTypeface(typeface);

            holder.message_sent_time.setText(Html.fromHtml(msgModels.get(position).sent_on_txt));
            holder.message_sent_time.setTypeface(typeface);

            if (msgModels.get(position).is_read.equals("0") || msgModels.get(position).is_read.equals("")) {

                holder.parent_layout.setBackgroundColor(Color.parseColor("#F1F1F1"));
            } else {
                holder.parent_layout.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }

            Picasso.with(context.getActivity())
                    .load(msgModels.get(position).profile_pic)
                    .placeholder(R.drawable.members_dummy)
                    .into(holder.profile_pic);


            holder.setItemClickListener(new AllItemClickListeners() {
                @Override
                public void onItemClick(View v, int pos) {
                    if(msgModels.size()>0 && msgModels!=null) {
                        Log.d("IDIDIIDID", msgModels.get(pos).from_id);
                        Intent inten = new Intent(context.getActivity(), MessageDetailActivity.class);
                        inten.putExtra("FROM_ID", msgModels.get(pos).from_id);
                        inten.putExtra("FROM_NAME", msgModels.get(pos).from_name);
                        inten.putExtra("FROM_TYPE", msgModels.get(pos).from_user_type);
                        inten.putExtra("FROM_PROFILEPIC", msgModels.get(pos).profile_pic);
                        inten.putExtra("FROM_TYPE", from_chat_type);

                        context.startActivity(inten);
                    }


                }
            });
        }

        @Override
        public int getItemCount() {
            return this.msgModels.size();
        }

        @Override
        public Filter getFilter() {
            if (filter == null) {
                filter = new CustomFilterForSponsorChat(msgModels, this);
            }

            return filter;
        }
    }
}
