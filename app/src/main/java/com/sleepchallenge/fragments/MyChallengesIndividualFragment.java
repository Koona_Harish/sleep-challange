package com.sleepchallenge.fragments;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.activities.AllCompletedChallengesActivity;
import com.sleepchallenge.activities.AllRunningChallengesActivity;
import com.sleepchallenge.activities.AllUpcomingChallengesActivity;
import com.sleepchallenge.adapters.ChallengeIndividCompletAdapter;
import com.sleepchallenge.adapters.ChallengeIndividRunAdapter;
import com.sleepchallenge.adapters.ChallengeIndividUpcomAdapter;
import com.sleepchallenge.models.ChallengeIndividualRunningModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.ChallengeDatesDB;
import com.sleepchallenge.utils.ChallengeIsAvailable;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.IndividualCompletedChallengesDB;
import com.sleepchallenge.utils.IndividualRunningChallengesDB;
import com.sleepchallenge.utils.IndividualUpcomingChallengesDB;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MyChallengesIndividualFragment extends Fragment implements View.OnClickListener {

    View view;
    ImageView no_data_image;
    RecyclerView recycler_individual_running, recycler_individual_upcoming, recycler_individual_completed;
    LinearLayoutManager layoutManager, layoutManager1, layoutManager2;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token, user_id, user_type, device_id;
    UserSessionManager userSessionManager;
    Typeface regularFont, boldFont, thinFont, specialFont;
    ChallengeIsAvailable challengeIsAvailable;
    //Running Adapter
    ChallengeIndividRunAdapter challengIndvidRunAdpter;
    ArrayList<ChallengeIndividualRunningModel> challengIndvidRunList = new ArrayList<>();
    //Upcoming Adapter
    ChallengeIndividUpcomAdapter challengIndvidUppAdpter;
    ArrayList<ChallengeIndividualRunningModel> challengIndvidUpcomList = new ArrayList<>();
    //Complet Adapter
    ChallengeIndividCompletAdapter challengIndvidCompletAdpter;
    ArrayList<ChallengeIndividualRunningModel> challengIndvidCompletcomList = new ArrayList<>();

    LinearLayout ll_running, ll_upcoming, ll_completed;
    TextView running_more_text, upcoming_more_text, complet_more_text;
    IndividualRunningChallengesDB individualRunningChallengesDB;
    IndividualUpcomingChallengesDB individualUpcomingChallengesDB;
    IndividualCompletedChallengesDB individualCompletedChallengesDB;
    ChallengeDatesDB challengeDatesDB;
    TextView ongoing_text, upcoming_text, completed_text;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_challenges_individual, container, false);

        regularFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.specialFont));

        individualRunningChallengesDB = new IndividualRunningChallengesDB(getActivity());
        individualUpcomingChallengesDB = new IndividualUpcomingChallengesDB(getActivity());
        individualCompletedChallengesDB = new IndividualCompletedChallengesDB(getActivity());
        challengeDatesDB = new ChallengeDatesDB(getActivity());
        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);

        pprogressDialog = new ProgressDialog(getActivity());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        ongoing_text = view.findViewById(R.id.ongoing_text);
        ongoing_text.setTypeface(boldFont);
        upcoming_text = view.findViewById(R.id.upcoming_text);
        upcoming_text.setTypeface(boldFont);
        completed_text = view.findViewById(R.id.completed_text);
        completed_text.setTypeface(boldFont);


        no_data_image = view.findViewById(R.id.no_data_image);

        ll_running = view.findViewById(R.id.ll_running);
        ll_upcoming = view.findViewById(R.id.ll_upcoming);
        ll_completed = view.findViewById(R.id.ll_completed);


        running_more_text = view.findViewById(R.id.running_more_text);
        running_more_text.setOnClickListener(this);
        running_more_text.setTypeface(boldFont);
        upcoming_more_text = view.findViewById(R.id.upcoming_more_text);
        upcoming_more_text.setOnClickListener(this);
        upcoming_more_text.setTypeface(boldFont);
        complet_more_text = view.findViewById(R.id.complet_more_text);
        complet_more_text.setOnClickListener(this);
        complet_more_text.setTypeface(boldFont);

        recycler_individual_running = view.findViewById(R.id.recycler_individual_running);
        recycler_individual_upcoming = view.findViewById(R.id.recycler_individual_upcoming);
        recycler_individual_completed = view.findViewById(R.id.recycler_individual_completed);


        challengIndvidRunAdpter = new ChallengeIndividRunAdapter(challengIndvidRunList, MyChallengesIndividualFragment.this, R.layout.row_my_challenges);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recycler_individual_running.setNestedScrollingEnabled(false);
        recycler_individual_running.setLayoutManager(layoutManager);

        challengIndvidUppAdpter = new ChallengeIndividUpcomAdapter(challengIndvidUpcomList, MyChallengesIndividualFragment.this, R.layout.row_my_challenges);
        layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recycler_individual_upcoming.setNestedScrollingEnabled(false);
        recycler_individual_upcoming.setLayoutManager(layoutManager1);

        challengIndvidCompletAdpter = new ChallengeIndividCompletAdapter(challengIndvidCompletcomList, MyChallengesIndividualFragment.this, R.layout.row_my_challenges);
        layoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recycler_individual_completed.setNestedScrollingEnabled(false);
        recycler_individual_completed.setLayoutManager(layoutManager2);
        getIndividualChallenges();
        return view;
    }

    public void getIndividualChallenges() {
        checkInternet = NetworkChecking.isConnected(getContext());
        if (checkInternet) {
            challengIndvidRunList.clear();
            challengIndvidUpcomList.clear();
            challengIndvidCompletcomList.clear();
            individualRunningChallengesDB.emptyDBBucket();
            individualCompletedChallengesDB.emptyDBBucket();
            Dialog.showProgressBar(getActivity(),"Get the Challenges...");
            String url = AppUrls.BASE_URL + AppUrls.MY_CHALLENGES + "user_id=" + user_id + "&user_type=" + user_type + "&type=INDIVIDUAL";
            Log.v("URL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("response_code");
                        if (status.equals("10100")) {
                            Dialog.hideProgressBar();
                            JSONObject jObjData = jsonObject.getJSONObject("data");
                            int running_cnt = jObjData.getInt("running_cnt");
                            int upcomming_cnt = jObjData.getInt("upcomming_cnt");
                            int completed_cnt = jObjData.getInt("completed_cnt");
                            if (running_cnt != 0) {
                                ll_running.setVisibility(View.VISIBLE);
                                JSONArray jsonArrayrunning = jObjData.getJSONArray("running");
                                ContentValues values = new ContentValues();
                                for (int i = 0; i < jsonArrayrunning.length(); i++) {
                                    JSONObject jsonObject1 = jsonArrayrunning.getJSONObject(i);
                                    values.put(IndividualRunningChallengesDB.CHALLENGE_ID, jsonObject1.getString("challenge_id"));
                                    values.put(IndividualRunningChallengesDB.USER_NAME, jsonObject1.getString("user_name"));
                                    values.put(IndividualRunningChallengesDB.OPPONENT_NAME, jsonObject1.getString("opponent_name"));
                                    values.put(IndividualRunningChallengesDB.WINNING_STATUS, jsonObject1.getString("winning_status"));
                                    values.put(IndividualRunningChallengesDB.IS_GROUP_ADMIN, jsonObject1.getString("is_group_admin"));
                                    values.put(IndividualRunningChallengesDB.STATUS, jsonObject1.getString("status"));
                                    values.put(IndividualRunningChallengesDB.AMOUNT, jsonObject1.getString("amount"));
                                    values.put(IndividualRunningChallengesDB.WINNING_AMOUNT, jsonObject1.getString("winning_amount"));
                                    values.put(IndividualRunningChallengesDB.START_ON, jsonObject1.getString("start_on"));
                                    values.put(IndividualRunningChallengesDB.PAUSED_ON, jsonObject1.getString("paused_on"));
                                    values.put(IndividualRunningChallengesDB.RESUME_ON, jsonObject1.getString("resume_on"));
                                    values.put(IndividualRunningChallengesDB.COMPLETED_ON_TXT, jsonObject1.getString("completed_on_txt"));
                                    values.put(IndividualRunningChallengesDB.COMPLETED_ON, jsonObject1.getString("completed_on"));
                                    values.put(IndividualRunningChallengesDB.WINNER, jsonObject1.getString("winner"));
                                    values.put(IndividualRunningChallengesDB.PAUSED_BY, jsonObject1.getString("paused_by"));
                                    values.put(IndividualRunningChallengesDB.CHALLENGE_TYPE, jsonObject1.getString("challenge_type"));
                                    values.put(IndividualRunningChallengesDB.IS_SCRATCHED, jsonObject1.getString("is_scratched"));
                                    if(jsonObject1.has("opponent_id")){
                                        values.put(IndividualRunningChallengesDB.OPPONENT_ID, jsonObject1.getString("opponent_id"));
                                    }
                                    boolean isExit = individualRunningChallengesDB.CheckIsDataAlreadyInDBorNot(jsonObject1.getString("challenge_id"));
                                    if (!isExit) {
                                        individualRunningChallengesDB.addRunningChallengesList(values);
                                       // if(jsonObject1.getString("challenge_type").equalsIgnoreCase("WEEKLY"))
                                         //challengeStatus(jsonObject1.getString("challenge_id"));
                                    }

                                }
                                individualRunningChallenges();

                            }
//upconing
                            if (upcomming_cnt != 0) {
                               ContentValues values = new ContentValues();
                                values.put(IndividualUpcomingChallengesDB.UPCOMING_COUNT, upcomming_cnt);
                                JSONArray jsonArrayupcom = jObjData.getJSONArray("upcomming");
                                for (int j = 0; j < jsonArrayupcom.length(); j++) {
                                    JSONObject jsonObject1 = jsonArrayupcom.getJSONObject(j);
                                    values.put(IndividualUpcomingChallengesDB.CHALLENGE_ID, jsonObject1.getString("challenge_id"));
                                    values.put(IndividualUpcomingChallengesDB.USER_NAME, jsonObject1.getString("user_name"));
                                    values.put(IndividualUpcomingChallengesDB.OPPONENT_NAME, jsonObject1.getString("opponent_name"));
                                    values.put(IndividualUpcomingChallengesDB.WINNING_STATUS, jsonObject1.getString("winning_status"));
                                    values.put(IndividualUpcomingChallengesDB.IS_GROUP_ADMIN, jsonObject1.getString("is_group_admin"));
                                    values.put(IndividualUpcomingChallengesDB.STATUS, jsonObject1.getString("status"));
                                    values.put(IndividualUpcomingChallengesDB.AMOUNT, jsonObject1.getString("amount"));
                                    values.put(IndividualUpcomingChallengesDB.WINNING_AMOUNT, jsonObject1.getString("winning_amount"));
                                    values.put(IndividualUpcomingChallengesDB.START_ON, jsonObject1.getString("start_on"));
                                    values.put(IndividualUpcomingChallengesDB.PAUSED_ON, jsonObject1.getString("paused_on"));
                                    values.put(IndividualUpcomingChallengesDB.RESUME_ON, jsonObject1.getString("resume_on"));
                                    values.put(IndividualUpcomingChallengesDB.COMPLETED_ON_TXT, jsonObject1.getString("completed_on_txt"));
                                    values.put(IndividualUpcomingChallengesDB.COMPLETED_ON, jsonObject1.getString("completed_on"));
                                    values.put(IndividualUpcomingChallengesDB.WINNER, jsonObject1.getString("winner"));
                                    values.put(IndividualUpcomingChallengesDB.PAUSED_BY, jsonObject1.getString("paused_by"));
                                    values.put(IndividualUpcomingChallengesDB.CHALLENGE_TYPE, jsonObject1.getString("challenge_type"));
                                    values.put(IndividualUpcomingChallengesDB.WIN_REWARD_TYPE, jsonObject1.getString("winning_reward_type"));
                                    values.put(IndividualUpcomingChallengesDB.WIN_REWARD_VALUE, jsonObject1.getString("winning_reward_value"));
                                    values.put(IndividualUpcomingChallengesDB.IS_SCRATCHED, jsonObject1.getString("is_scratched"));
                                    if(jsonObject1.has("opponent_id")){
                                        values.put(IndividualUpcomingChallengesDB.OPPONENT_ID, jsonObject1.getString("opponent_id"));
                                    }
                                    boolean isExit = individualUpcomingChallengesDB.CheckIsDataAlreadyInDBorNot(jsonObject1.getString("challenge_id"));
                                    if (!isExit) {
                                        individualUpcomingChallengesDB.addUpcomingChallengesList(values);
                                        challengeStatus(jsonObject1.getString("challenge_id"));
                                    }

                                }
                                individualUpcomingChallenges();

                            }
//complete
                            if (completed_cnt != 0)
                            {
                                ll_completed.setVisibility(View.VISIBLE);
                                ContentValues values = new ContentValues();
                                JSONArray jsonArraycomplet = jObjData.getJSONArray("completed");
                                for (int k = 0; k < jsonArraycomplet.length(); k++) {
                                    JSONObject jsonObject1 = jsonArraycomplet.getJSONObject(k);
                                    values.put(IndividualCompletedChallengesDB.CHALLENGE_ID, jsonObject1.getString("challenge_id"));
                                    values.put(IndividualCompletedChallengesDB.USER_NAME, jsonObject1.getString("user_name"));
                                    values.put(IndividualCompletedChallengesDB.OPPONENT_NAME, jsonObject1.getString("opponent_name"));
                                    values.put(IndividualCompletedChallengesDB.WINNING_STATUS, jsonObject1.getString("winning_status"));
                                    values.put(IndividualCompletedChallengesDB.IS_GROUP_ADMIN, jsonObject1.getString("is_group_admin"));
                                    values.put(IndividualCompletedChallengesDB.STATUS, jsonObject1.getString("status"));
                                    values.put(IndividualCompletedChallengesDB.AMOUNT, jsonObject1.getString("amount"));
                                    values.put(IndividualCompletedChallengesDB.WINNING_AMOUNT, jsonObject1.getString("winning_amount"));
                                    values.put(IndividualCompletedChallengesDB.START_ON, jsonObject1.getString("start_on"));
                                    values.put(IndividualCompletedChallengesDB.PAUSED_ON, jsonObject1.getString("paused_on"));
                                    values.put(IndividualCompletedChallengesDB.RESUME_ON, jsonObject1.getString("resume_on"));
                                    values.put(IndividualCompletedChallengesDB.COMPLETED_ON_TXT, jsonObject1.getString("completed_on_txt"));
                                    values.put(IndividualCompletedChallengesDB.COMPLETED_ON, jsonObject1.getString("completed_on"));
                                    values.put(IndividualCompletedChallengesDB.WINNER, jsonObject1.getString("winner"));
                                    values.put(IndividualCompletedChallengesDB.PAUSED_BY, jsonObject1.getString("paused_by"));
                                    values.put(IndividualCompletedChallengesDB.CHALLENGE_TYPE, jsonObject1.getString("challenge_type"));
                                    values.put(IndividualCompletedChallengesDB.WIN_REWARD_TYPE, jsonObject1.getString("winning_reward_type"));
                                    values.put(IndividualCompletedChallengesDB.WIN_REWARD_VALUE, jsonObject1.getString("winning_reward_value"));
                                    values.put(IndividualCompletedChallengesDB.IS_SCRATCHED, jsonObject1.getString("is_scratched"));
                                    if(jsonObject1.has("opponent_id")){
                                        values.put(IndividualCompletedChallengesDB.OPPONENT_ID, jsonObject1.getString("opponent_id"));
                                    }
                                    boolean isExit = individualCompletedChallengesDB.CheckIsDataAlreadyInDBorNot(jsonObject1.getString("challenge_id"));
                                    if (!isExit) {
                                        individualCompletedChallengesDB.addCompletedChallengesList(values);

                                    }

                                }
                                individualCompletedChallenges();

                            }
                        }
                        if (status.equals("10200")) {
                            Dialog.hideProgressBar();
                            no_data_image.setVisibility(View.VISIBLE);

                        }
                        if (status.equals("10300")) {
                            Dialog.hideProgressBar();
                            no_data_image.setVisibility(View.VISIBLE);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("Headers","//"+headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);

        } else {
            individualRunningChallenges();
            individualUpcomingChallenges();
            individualCompletedChallenges();

        }
    }

    @Override
    public void onClick(View view) {
        if (view == running_more_text) {
            Intent irunmore = new Intent(getActivity(), AllRunningChallengesActivity.class);
            irunmore.putExtra("condition", "INDIVIDUAL");
            startActivity(irunmore);
        }
        if (view == upcoming_more_text) {
            Intent irunmore = new Intent(getActivity(), AllUpcomingChallengesActivity.class);
            irunmore.putExtra("condition", "INDIVIDUAL");
            startActivity(irunmore);
        }
        if (view == complet_more_text) {
            Intent irunmore = new Intent(getActivity(), AllCompletedChallengesActivity.class);
            irunmore.putExtra("condition", "INDIVIDUAL");
            startActivity(irunmore);
        }
    }


    private void challengeStatus(final String id) {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            Dialog.showProgressBar(getActivity(), "Loading");
            String url = AppUrls.BASE_URL + AppUrls.CHALLENGE_STATUS + id + "&user_id=" + user_id;
            Log.d("STATUSURL", url);
            StringRequest statusstr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Dialog.hideProgressBar();
                    Log.d("STATUSRES", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100")) {
                            ContentValues values = new ContentValues();
                            JSONObject responseObj = jsonObject.optJSONObject("data");
                            JSONArray challenger_daily_status = responseObj.optJSONArray("challenger_daily_status");
                            JSONArray opponent_daily_status = responseObj.optJSONArray("opponent_daily_status");
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            for (int j = 0; j < challenger_daily_status.length(); j++) {
                                JSONObject jsonObject1 = challenger_daily_status.getJSONObject(j);
                                String starton = jsonObject1.getString("start_on");
                                String endon = jsonObject1.getString("completed_on");
                                String challengedate = jsonObject1.getString("challenge_date");
                                Date start = ConvertToDate(starton);
                                Calendar startcalendar = Calendar.getInstance();
                                startcalendar.setTime(start);
                                startcalendar.add(Calendar.HOUR, -2);
                                long start_time = startcalendar.getTimeInMillis();
                                Date end = ConvertToDate(endon);
                                Calendar endcalendar = Calendar.getInstance();
                                endcalendar.setTime(end);
                                endcalendar.add(Calendar.HOUR, 2);
                                long end_time = endcalendar.getTimeInMillis();
                                int start_seconds = (int) TimeUnit.MILLISECONDS.toSeconds(start_time);
                                int end_seconds = (int) TimeUnit.MILLISECONDS.toSeconds(end_time);
                                values.put(ChallengeDatesDB.CHALLENGE_DATE,challengedate);
                                values.put(ChallengeDatesDB.CHALLENGE_ID, id);
                                values.put(ChallengeDatesDB.START_TIME, start_seconds);
                                values.put(ChallengeDatesDB.END_TIME, end_seconds);
                                values.put(ChallengeDatesDB.CHALLENGE, "INDIVIDUAL");
                                challengeDatesDB.interstChallengeDates(values);
                                Log.v("Insert","///"+"Individual");
                            }
                            challengeIsAvailable=new ChallengeIsAvailable(getActivity());
                            challengeIsAvailable.challengeCheckRemainder();
                           /* Intent it = new Intent(getActivity(), BackgroundStatus.class);
                            getActivity().startService(it);*/
                                      }

                        if (successResponceCode.equals("10200")) {
                            Toast.makeText(getActivity(), "Invalid Input..!", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Dialog.hideProgressBar();
                    Log.e("Error", error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            statusstr.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(statusstr);
        } else {
            Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void individualUpcomingChallenges() {

        challengIndvidUpcomList.clear();


        List<String> challenge_id = individualUpcomingChallengesDB.getChallengeID();
        if (challenge_id.size() > 0) {
            List<String> user_name = individualUpcomingChallengesDB.getUserName();
            List<String> opponent_name = individualUpcomingChallengesDB.getOpponentName();
            List<String> winning_status = individualUpcomingChallengesDB.getWinningStatus();
            List<String> is_group_admin = individualUpcomingChallengesDB.getIsGroupAdmin();
            List<String> status = individualUpcomingChallengesDB.getStatus();
            List<String> amount = individualUpcomingChallengesDB.getAmount();
            List<String> winning_amount = individualUpcomingChallengesDB.getWinningAmount();
            List<String> start_on = individualUpcomingChallengesDB.getStartOn();
            List<String> paused_on = individualUpcomingChallengesDB.getPausedOn();
            List<String> resume_on = individualUpcomingChallengesDB.getResumeOn();
            List<String> completed_on_txt = individualUpcomingChallengesDB.getCompletedOnTxt();
            List<String> completed_on = individualUpcomingChallengesDB.getCompletedOn();
            List<String> winner = individualUpcomingChallengesDB.getWinner();
            List<String> paused_by = individualUpcomingChallengesDB.getPaused_by();
            List<String> challenge_type = individualUpcomingChallengesDB.getChallengeType();
            List<String> winning_reward_type = individualUpcomingChallengesDB.win_RewardType();
            List<String> winning_reward_value = individualUpcomingChallengesDB.win_RewardValue();
            List<String> is_scratched = individualUpcomingChallengesDB.isScratched();
            List<String> opponentids = individualUpcomingChallengesDB.getOpponetIds();
            int upcoming_count = individualUpcomingChallengesDB.getUpcomingingCount();
            Log.d("upcomingcountSS", "" + upcoming_count);
            for (int i = 0; i < status.size(); i++) {
                ChallengeIndividualRunningModel am_runn = new ChallengeIndividualRunningModel();
                am_runn.setChallenge_id(challenge_id.get(i));

                am_runn.setUser_name(user_name.get(i));
                am_runn.setOpponent_name(opponent_name.get(i));
                am_runn.setWinning_status(winning_status.get(i));
                am_runn.setIs_group_admin(is_group_admin.get(i));
                am_runn.setStatus(status.get(i));
                am_runn.setAmount(amount.get(i));
                am_runn.setWinning_amount(winning_amount.get(i));
                am_runn.setStart_on(start_on.get(i));
                am_runn.setPaused_on(paused_on.get(i));
                am_runn.setResume_on(resume_on.get(i));
                am_runn.setCompleted_on_txt(completed_on_txt.get(i));
                am_runn.setCompleted_on(completed_on.get(i));

                am_runn.setWinner(winner.get(i));
                am_runn.setPaused_by(paused_by.get(i));
                am_runn.setChallenge_type(challenge_type.get(i));
                am_runn.setWinning_reward_type(winning_reward_type.get(i));
                am_runn.setWinning_reward_value(winning_reward_value.get(i));
                am_runn.setIs_scratched(is_scratched.get(i));
                if(opponentids.size()>0)
                    am_runn.setOpponent_id(opponentids.get(i));
               /* challengIndvidUpcomList.add(am_runn);
                recycler_individual_upcoming.setAdapter(challengIndvidUppAdpter);
                ll_upcoming.setVisibility(View.VISIBLE);*/
               if(status.get(i).equalsIgnoreCase("YETTOSTART")){
                    challengIndvidUpcomList.add(am_runn);
                    recycler_individual_upcoming.setAdapter(challengIndvidUppAdpter);
                    ll_upcoming.setVisibility(View.VISIBLE);
                }
                if(status.get(i).equalsIgnoreCase("RUNNING")){
                    challengIndvidRunList.add(am_runn);
                    recycler_individual_running.setAdapter(challengIndvidRunAdpter);
                    ll_running.setVisibility(View.VISIBLE);
                }
                /*if(status.get(i).equalsIgnoreCase("COMPLETED")){
                    challengIndvidCompletcomList.add(am_runn);
                    recycler_individual_completed.setAdapter(challengIndvidCompletAdpter);
                    ll_completed.setVisibility(View.VISIBLE);
                }*/
             }


        }
    }

    private void individualCompletedChallenges() {
        List<String> challenge_id = individualCompletedChallengesDB.getChallengeID();
        if (challenge_id.size() > 0) {
            ll_completed.setVisibility(View.VISIBLE);
            List<String> user_name = individualCompletedChallengesDB.getUserName();
            List<String> opponent_name = individualCompletedChallengesDB.getOpponentName();
            List<String> winning_status = individualCompletedChallengesDB.getWinningStatus();
            List<String> is_group_admin = individualCompletedChallengesDB.getIsGroupAdmin();
            List<String> status = individualCompletedChallengesDB.getStatus();
            List<String> amount = individualCompletedChallengesDB.getAmount();
            List<String> winning_amount = individualCompletedChallengesDB.getWinningAmount();
            List<String> start_on = individualCompletedChallengesDB.getStartOn();
            List<String> paused_on = individualCompletedChallengesDB.getPausedOn();
            List<String> resume_on = individualCompletedChallengesDB.getResumeOn();
            List<String> completed_on_txt = individualCompletedChallengesDB.getCompletedOnTxt();
            List<String> completed_on = individualCompletedChallengesDB.getCompletedOn();
            List<String> winner = individualCompletedChallengesDB.getWinner();
            List<String> paused_by = individualCompletedChallengesDB.getPaused_by();
            List<String> challenge_type = individualCompletedChallengesDB.getChallengeType();
            List<String> winning_reward_type = individualCompletedChallengesDB.win_RewardType();
            List<String> winning_reward_value = individualCompletedChallengesDB.win_RewardValue();
            List<String> is_scratched = individualCompletedChallengesDB.isScratched();
            int completed_count = individualCompletedChallengesDB.getCompletedCount();
            List<String> opponentids = individualCompletedChallengesDB.getOpponentids();
           for (int i = 0; i < challenge_id.size(); i++) {
                ChallengeIndividualRunningModel am_runn = new ChallengeIndividualRunningModel();
                am_runn.setChallenge_id(challenge_id.get(i));

                am_runn.setUser_name(user_name.get(i));
                am_runn.setOpponent_name(opponent_name.get(i));
                am_runn.setWinning_status(winning_status.get(i));
                am_runn.setIs_group_admin(is_group_admin.get(i));
                am_runn.setStatus(status.get(i));
                am_runn.setAmount(amount.get(i));
                am_runn.setWinning_amount(winning_amount.get(i));
                am_runn.setStart_on(start_on.get(i));
                am_runn.setPaused_on(paused_on.get(i));
                am_runn.setResume_on(resume_on.get(i));
                am_runn.setCompleted_on_txt(completed_on_txt.get(i));
                am_runn.setCompleted_on(completed_on.get(i));

                am_runn.setWinner(winner.get(i));
                am_runn.setPaused_by(paused_by.get(i));
                am_runn.setChallenge_type(challenge_type.get(i));
                am_runn.setWinning_reward_type(winning_reward_type.get(i));
                am_runn.setWinning_reward_value(winning_reward_value.get(i));
                am_runn.setIs_scratched(is_scratched.get(i));
                if(opponentids.size()>0)
                am_runn.setOpponent_id(opponentids.get(i));

                challengIndvidCompletcomList.add(am_runn);
                Log.d("sdgsdfdgjsdfg", challengIndvidCompletcomList.toString());

            }
            recycler_individual_completed.setAdapter(challengIndvidCompletAdpter);

        } else {

        }
    }

    private void individualRunningChallenges() {

        challengIndvidRunList.clear();

        List<String> challenge_id = individualRunningChallengesDB.getChallengeID();
        if (challenge_id.size() > 0) {
            ll_running.setVisibility(View.VISIBLE);
            List<String> user_name = individualRunningChallengesDB.getUserName();
            List<String> opponent_name = individualRunningChallengesDB.getOpponentName();
            List<String> winning_status = individualRunningChallengesDB.getWinningStatus();
            List<String> is_group_admin = individualRunningChallengesDB.getIsGroupAdmin();
            List<String> status = individualRunningChallengesDB.getStatus();
            List<String> amount = individualRunningChallengesDB.getAmount();
            List<String> winning_amount = individualRunningChallengesDB.getWinningAmount();
            List<String> start_on = individualRunningChallengesDB.getStartOn();
            List<String> paused_on = individualRunningChallengesDB.getPausedOn();
            List<String> resume_on = individualRunningChallengesDB.getResumeOn();
            List<String> completed_on_txt = individualRunningChallengesDB.getCompletedOnTxt();
            List<String> completed_on = individualRunningChallengesDB.getCompletedOn();
            List<String> winner = individualRunningChallengesDB.getWinner();
            List<String> paused_by = individualRunningChallengesDB.getPaused_by();
            List<String> challenge_type = individualRunningChallengesDB.getChallengeType();
            List<String> winning_reward_type = individualRunningChallengesDB.getWinningRewardType();
            List<String> winning_reward_value = individualRunningChallengesDB.getWinningRewardValue();
            List<String> is_scratched = individualRunningChallengesDB.getIsScratched();
            List<String> opponentids = individualRunningChallengesDB.getOpponentId();

            for (int i = 0; i < challenge_id.size(); i++) {
                ChallengeIndividualRunningModel am_runn = new ChallengeIndividualRunningModel();
                am_runn.setChallenge_id(challenge_id.get(i));

                am_runn.setUser_name(user_name.get(i));
                am_runn.setOpponent_name(opponent_name.get(i));
                am_runn.setWinning_status(winning_status.get(i));
                am_runn.setIs_group_admin(is_group_admin.get(i));
                am_runn.setStatus(status.get(i));
                am_runn.setAmount(amount.get(i));
                am_runn.setWinning_amount(winning_amount.get(i));
                am_runn.setStart_on(start_on.get(i));
                am_runn.setPaused_on(paused_on.get(i));
                am_runn.setResume_on(resume_on.get(i));
                am_runn.setCompleted_on_txt(completed_on_txt.get(i));
                am_runn.setCompleted_on(completed_on.get(i));

                am_runn.setWinner(winner.get(i));
                am_runn.setPaused_by(paused_by.get(i));
                am_runn.setChallenge_type(challenge_type.get(i));
                am_runn.setWinning_reward_type(winning_reward_type.get(i));
                am_runn.setWinning_reward_value(winning_reward_value.get(i));
                am_runn.setIs_scratched(is_scratched.get(i));
                if(opponentids.size()>0)
                am_runn.setOpponent_id(opponentids.get(i));


                challengIndvidRunList.add(am_runn);
                Log.d("sdgsdfdgjsdfg", challengIndvidUpcomList.toString());

            }
            recycler_individual_running.setAdapter(challengIndvidRunAdpter);

        } else {

        }
    }

    private Date ConvertToDate(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return convertedDate;
    }
}
