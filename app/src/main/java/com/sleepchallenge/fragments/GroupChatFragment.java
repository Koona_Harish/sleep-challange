package com.sleepchallenge.fragments;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.GroupListAdapter;
import com.sleepchallenge.models.GroupMessageModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class GroupChatFragment extends Fragment  {

    View view;
    ImageView voice_search;

    RecyclerView recycler_groupchat_messages;
    GroupListAdapter groupListAdapter;
    ArrayList<GroupMessageModel> groupModalList;
    LinearLayoutManager layoutManager;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String user_id,user_type,token,device_id,group_type;
    SearchView  activity_search;;
    EditText searchEditText;
    String chat_type="GROUP";
    private final int REQ_CODE_SPEECH_INPUT = 100;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        view = inflater.inflate(R.layout.fragment_group_chat, container, false);

        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        activity_search = view. findViewById(R.id.group_search);
        activity_search.setFocusable(false);
        activity_search.setIconified(false);
        activity_search.setIconifiedByDefault(false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        searchEditText =activity_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setHint("Search a Group");
        searchEditText.setHintTextColor(getResources().getColor(R.color.gray));
        searchEditText.setTextColor(getResources().getColor(R.color.black));
        ImageView voiceIcon = activity_search.findViewById(android.support.v7.appcompat.R.id.search_voice_btn);
        voiceIcon.setImageResource(R.drawable.voice_icon);
        Log.d("DETAILL",user_id+"//"+user_type+"//"+token+"//"+device_id);
        voice_search = (ImageView) view.findViewById(R.id.voice_search);
        group_type = getActivity().getIntent().getStringExtra("condition");
        Log.d("hgskhgsdjjksd", group_type);
        recycler_groupchat_messages= view.findViewById(R.id.recycler_groupchat_messages);

        activity_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if(groupListAdapter!=null)
                    groupListAdapter.getFilter().filter(query);
                return false;
            }
        });
        voice_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                        "Search Activities");
                try {
                    startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
                } catch (ActivityNotFoundException a) {
                    Toast.makeText(getContext(),
                            "Search not Supported",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });


       // getGroupMessage();

        return view ;

    }

    private void getGroupMessage()
    {

        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet)
        {
            Log.d("GRPURL", AppUrls.BASE_URL+AppUrls.GROUP_LIST+"?entity_id="+user_id+"&entity_type="+user_type);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL+AppUrls.GROUP_LIST+"?entity_id="+user_id+"&entity_type="+user_type,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try
                            {
                                groupModalList=new  ArrayList<>();
                                Log.d("RESPGRP",response);
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("response_code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++)
                                    {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        GroupMessageModel grouplist = new GroupMessageModel(jsonObject1);
                                        groupModalList.add(grouplist);
                                    }
                                    groupListAdapter = new GroupListAdapter(groupModalList, GroupChatFragment.this, R.layout.row_group_list,group_type,chat_type);
                                    layoutManager = new LinearLayoutManager(getActivity());
                                    recycler_groupchat_messages.setNestedScrollingEnabled(false);
                                    recycler_groupchat_messages.setLayoutManager(layoutManager);
                                    recycler_groupchat_messages.setAdapter(groupListAdapter);
                                }
                                if (responceCode.equals("10200")) {
                                    recycler_groupchat_messages.setVisibility(View.GONE);
                                    Toast.makeText(getActivity(), "Invlid Input..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10300")) {
                                    recycler_groupchat_messages.setVisibility(View.GONE);
                                  // Toast.makeText(getActivity(), "No data Found.!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {

                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                   error.getMessage();
                }
            }){

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("GRPHEADER",headers.toString());
                    return headers;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else
        {
            Toast.makeText(getActivity(),"No Internet Connection..!",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
       // groupModalList.clear();
        getGroupMessage();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    searchEditText.setText("" + result.get(0));
                    groupListAdapter.getFilter().filter("" + result.get(0));
                }
                break;
            }

        }
    }

}
