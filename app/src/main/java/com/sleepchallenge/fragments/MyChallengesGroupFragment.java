package com.sleepchallenge.fragments;


import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.activities.AllCompletedChallengesActivity;
import com.sleepchallenge.activities.AllRunningChallengesActivity;
import com.sleepchallenge.activities.AllUpcomingChallengesActivity;
import com.sleepchallenge.adapters.MyChallengeGroupCompletAdapter;
import com.sleepchallenge.adapters.MyChallengeGroupRunAdapter;
import com.sleepchallenge.adapters.MyChallengeGroupUpcomAdapter;
import com.sleepchallenge.models.ChallengeGroupDetailModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.ChallengeDatesDB;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.GroupCompletedChallengesDB;
import com.sleepchallenge.utils.GroupRunningChallengesDB;
import com.sleepchallenge.utils.GroupUpcomingChallengesDB;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MyChallengesGroupFragment extends Fragment implements View.OnClickListener {

    ImageView no_data_image;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    String token, user_id, device_id;
    UserSessionManager userSessionManager;
    Typeface regularFont, boldFont, thinFont, specialFont;
    LinearLayoutManager layoutManager, layoutManager1, layoutManager2;
    View view;
    TextView running_more_text, upcoming_more_text, complet_more_text;
    LinearLayout ll_running, ll_upcoming, ll_completed;
    TextView group_ongoing_text, group_upcoming_text, group_completed_text;

    RecyclerView grp_running_recycler, grp_upcoming_recycler, grp_completed_recycler;
   //Running Adapter
    MyChallengeGroupRunAdapter challengGroupRunAdpter;
    ArrayList<ChallengeGroupDetailModel> challengGroupRunList = new ArrayList<>();
    //Upcoming Adapter
    MyChallengeGroupUpcomAdapter challengGroupUppAdpter;
    ArrayList<ChallengeGroupDetailModel> challengGroupUpcomList = new ArrayList<>();
    //Complet Adapter
    MyChallengeGroupCompletAdapter challengGroupCompletAdpter;
    ArrayList<ChallengeGroupDetailModel> challengGroupCompletcomList = new ArrayList<>();
    GroupRunningChallengesDB groupRunningChallengesDB;
    GroupUpcomingChallengesDB groupUpcomingChallengesDB;
    GroupCompletedChallengesDB groupCompletedChallengesDB;
   ChallengeDatesDB challengeDatesDB;

   @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_challenges_group, container, false);
        challengGroupRunList.clear();
        challengGroupUpcomList.clear();
        challengGroupCompletcomList.clear();
        groupRunningChallengesDB = new GroupRunningChallengesDB(getActivity());
        groupUpcomingChallengesDB = new GroupUpcomingChallengesDB(getActivity());
        groupCompletedChallengesDB = new GroupCompletedChallengesDB(getActivity());
        userSessionManager = new UserSessionManager(getActivity());
         challengeDatesDB = new ChallengeDatesDB(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_id = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        regularFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.regularFont));
        boldFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.boldFont));
        thinFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.thinFont));
        specialFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.specialFont));

        pprogressDialog = new ProgressDialog(getActivity());
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        no_data_image = view.findViewById(R.id.no_data_image);

        ll_running = view.findViewById(R.id.ll_running);
        ll_upcoming = view.findViewById(R.id.ll_upcoming);
        ll_completed = view.findViewById(R.id.ll_completed);

        group_ongoing_text = view.findViewById(R.id.group_ongoing_text);
        group_ongoing_text.setTypeface(boldFont);
        group_upcoming_text = view.findViewById(R.id.group_upcoming_text);
        group_upcoming_text.setTypeface(boldFont);
        group_completed_text = view.findViewById(R.id.group_completed_text);
        group_completed_text.setTypeface(boldFont);

        running_more_text = view.findViewById(R.id.running_more_text);
        running_more_text.setOnClickListener(this);
        running_more_text.setTypeface(boldFont);
        upcoming_more_text = view.findViewById(R.id.upcoming_more_text);
        upcoming_more_text.setOnClickListener(this);
        upcoming_more_text.setTypeface(boldFont);
        complet_more_text = view.findViewById(R.id.complet_more_text);
        complet_more_text.setOnClickListener(this);
        complet_more_text.setTypeface(boldFont);

        grp_running_recycler = view.findViewById(R.id.grp_running_recycler);
        grp_upcoming_recycler = view.findViewById(R.id.grp_upcoming_recycler);
        grp_completed_recycler = view.findViewById(R.id.grp_completed_recycler);

        challengGroupRunAdpter = new MyChallengeGroupRunAdapter(challengGroupRunList, MyChallengesGroupFragment.this, R.layout.row_my_challenges_group_detail);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        grp_running_recycler.setNestedScrollingEnabled(false);
        grp_running_recycler.setLayoutManager(layoutManager);

        challengGroupUppAdpter = new MyChallengeGroupUpcomAdapter(challengGroupUpcomList, MyChallengesGroupFragment.this, R.layout.row_my_challenges_group_detail);
        layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        grp_upcoming_recycler.setNestedScrollingEnabled(false);
        grp_upcoming_recycler.setLayoutManager(layoutManager1);


        challengGroupCompletAdpter = new MyChallengeGroupCompletAdapter(challengGroupCompletcomList, MyChallengesGroupFragment.this, R.layout.row_my_challenges_group_detail);
        layoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        grp_completed_recycler.setNestedScrollingEnabled(false);
        grp_completed_recycler.setLayoutManager(layoutManager2);


        getGroupChallenges();
        return view;
    }

    public void getGroupChallenges() {
        checkInternet = NetworkChecking.isConnected(getContext());

        if (checkInternet) {
            challengGroupRunList.clear();
            challengGroupUpcomList.clear();
            challengGroupCompletcomList.clear();
            groupRunningChallengesDB.emptyDBBucket();
            groupCompletedChallengesDB.emptyDBBucket();
            String url = AppUrls.BASE_URL + AppUrls.MY_CHALLENGES + "user_id=" + user_id + "&user_type=USER&type=" + "GROUP";

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("response_code");
                        if (status.equals("10100")) {
                            JSONObject jObjData = jsonObject.getJSONObject("data");
                            int running_cnt = jObjData.getInt("running_cnt");
                            int upcomming_cnt = jObjData.getInt("upcomming_cnt");

                            int completed_cnt = jObjData.getInt("completed_cnt");
                            if (running_cnt != 0) {
                                ContentValues values = new ContentValues();
                                JSONArray jsonArrayrunning = jObjData.getJSONArray("running");
                                for (int i = 0; i < jsonArrayrunning.length(); i++) {
                                    JSONObject jsonObject1 = jsonArrayrunning.getJSONObject(i);
                                    values.put(GroupRunningChallengesDB.CHALLENGE_ID, jsonObject1.getString("challenge_id"));
                                    values.put(GroupRunningChallengesDB.USER_NAME, jsonObject1.getString("user_name"));
                                    values.put(GroupRunningChallengesDB.OPPONENT_NAME, jsonObject1.getString("opponent_name"));
                                    values.put(GroupRunningChallengesDB.WINNING_STATUS, jsonObject1.getString("winning_status"));
                                    values.put(GroupRunningChallengesDB.IS_GROUP_ADMIN, jsonObject1.getString("is_group_admin"));
                                    values.put(GroupRunningChallengesDB.STATUS, jsonObject1.getString("status"));
                                    values.put(GroupRunningChallengesDB.AMOUNT, jsonObject1.getString("amount"));
                                    values.put(GroupRunningChallengesDB.WINNING_AMOUNT, jsonObject1.getString("winning_amount"));
                                    values.put(GroupRunningChallengesDB.START_ON, jsonObject1.getString("start_on"));
                                    values.put(GroupRunningChallengesDB.PAUSED_ON, jsonObject1.getString("paused_on"));
                                    values.put(GroupRunningChallengesDB.RESUME_ON, jsonObject1.getString("resume_on"));
                                    values.put(GroupRunningChallengesDB.COMPLETED_ON_TXT, jsonObject1.getString("completed_on_txt"));
                                    values.put(GroupRunningChallengesDB.COMPLETED_ON, jsonObject1.getString("completed_on"));
                                    values.put(GroupRunningChallengesDB.WINNER, jsonObject1.getString("winner"));
                                    values.put(GroupRunningChallengesDB.PAUSED_BY, jsonObject1.getString("paused_by"));
                                    values.put(GroupRunningChallengesDB.CHALLENGE_TYPE, jsonObject1.getString("challenge_type"));
                                    values.put(GroupRunningChallengesDB.WIN_REWARD_TYPE, jsonObject1.getString("winning_reward_type"));
                                    values.put(GroupRunningChallengesDB.WIN_REWARD_VALUE, jsonObject1.getString("winning_reward_value"));
                                    values.put(GroupRunningChallengesDB.IS_SCRATCHED, jsonObject1.getString("is_scratched"));
                                    values.put(GroupRunningChallengesDB.PAUSE_ACCESSS, jsonObject1.getString("pause_access"));
                                    if(jsonObject1.has("opponent_id")){
                                        values.put(GroupRunningChallengesDB.OPPONENT_ID, jsonObject1.getString("opponent_id"));
                                    }
                                    boolean isExit = groupRunningChallengesDB.CheckIsDataAlreadyInDBorNot(jsonObject1.getString("challenge_id"));
                                    if (!isExit) {
                                        groupRunningChallengesDB.addGroupRunningChallengesList(values);
                                    }

                                }
                                groupRunningChallenges();

                            } else {
                                ll_running.setVisibility(View.GONE);
                            }
//upconing
                            if (upcomming_cnt != 0) {
                                ContentValues values = new ContentValues();
                                JSONArray jsonArrayupcom = jObjData.getJSONArray("upcomming");
                                for (int j = 0; j < jsonArrayupcom.length(); j++) {
                                    JSONObject jsonObject1 = jsonArrayupcom.getJSONObject(j);
                                    values.put(GroupUpcomingChallengesDB.CHALLENGE_ID, jsonObject1.getString("challenge_id"));
                                    values.put(GroupUpcomingChallengesDB.USER_NAME, jsonObject1.getString("user_name"));
                                    values.put(GroupUpcomingChallengesDB.OPPONENT_NAME, jsonObject1.getString("opponent_name"));
                                    values.put(GroupUpcomingChallengesDB.WINNING_STATUS, jsonObject1.getString("winning_status"));
                                    values.put(GroupUpcomingChallengesDB.IS_GROUP_ADMIN, jsonObject1.getString("is_group_admin"));
                                    values.put(GroupUpcomingChallengesDB.STATUS, jsonObject1.getString("status"));
                                    values.put(GroupUpcomingChallengesDB.AMOUNT, jsonObject1.getString("amount"));
                                    values.put(GroupUpcomingChallengesDB.WINNING_AMOUNT, jsonObject1.getString("winning_amount"));
                                    values.put(GroupUpcomingChallengesDB.START_ON, jsonObject1.getString("start_on"));
                                    values.put(GroupUpcomingChallengesDB.PAUSED_ON, jsonObject1.getString("paused_on"));
                                    values.put(GroupUpcomingChallengesDB.RESUME_ON, jsonObject1.getString("resume_on"));
                                    values.put(GroupUpcomingChallengesDB.COMPLETED_ON_TXT, jsonObject1.getString("completed_on_txt"));
                                    values.put(GroupUpcomingChallengesDB.COMPLETED_ON, jsonObject1.getString("completed_on"));
                                    values.put(GroupUpcomingChallengesDB.WINNER, jsonObject1.getString("winner"));
                                    values.put(GroupUpcomingChallengesDB.PAUSED_BY, jsonObject1.getString("paused_by"));
                                    values.put(GroupUpcomingChallengesDB.CHALLENGE_TYPE, jsonObject1.getString("challenge_type"));
                                    values.put(GroupUpcomingChallengesDB.WIN_REWARD_TYPE, jsonObject1.getString("winning_reward_type"));
                                    values.put(GroupUpcomingChallengesDB.WIN_REWARD_VALUE, jsonObject1.getString("winning_reward_value"));
                                    values.put(GroupUpcomingChallengesDB.IS_SCRATCHED, jsonObject1.getString("is_scratched"));
                                    values.put(GroupUpcomingChallengesDB.PAUSE_ACCESSS, jsonObject1.getString("pause_access"));
                                    if(jsonObject1.has("opponent_id")){
                                        values.put(GroupUpcomingChallengesDB.OPPONENT_ID, jsonObject1.getString("opponent_id"));
                                    }
                                    boolean isExit = groupUpcomingChallengesDB.CheckIsDataAlreadyInDBorNot(jsonObject1.getString("challenge_id"));
                                    if (!isExit) {
                                        groupUpcomingChallengesDB.addGroupUpconingChallengesList(values);
                                        challengeStatus(jsonObject1.getString("challenge_id"));
                                  }

                                }
                                groupUpcomingChallenges();
                            } else {

                                ll_upcoming.setVisibility(View.GONE);
                            }
//complete
                            if (completed_cnt != 0) {
                                ContentValues values = new ContentValues();
                                JSONArray jsonArraycomplet = jObjData.getJSONArray("completed");
                                for (int k = 0; k < jsonArraycomplet.length(); k++) {
                                    JSONObject jsonObject1 = jsonArraycomplet.getJSONObject(k);
                                    values.put(GroupCompletedChallengesDB.CHALLENGE_ID, jsonObject1.getString("challenge_id"));
                                    values.put(GroupCompletedChallengesDB.USER_NAME, jsonObject1.getString("user_name"));
                                    values.put(GroupCompletedChallengesDB.OPPONENT_NAME, jsonObject1.getString("opponent_name"));
                                    values.put(GroupCompletedChallengesDB.WINNING_STATUS, jsonObject1.getString("winning_status"));
                                    values.put(GroupCompletedChallengesDB.IS_GROUP_ADMIN, jsonObject1.getString("is_group_admin"));
                                    values.put(GroupCompletedChallengesDB.STATUS, jsonObject1.getString("status"));
                                    values.put(GroupCompletedChallengesDB.AMOUNT, jsonObject1.getString("amount"));
                                    values.put(GroupCompletedChallengesDB.WINNING_AMOUNT, jsonObject1.getString("winning_amount"));
                                    values.put(GroupCompletedChallengesDB.START_ON, jsonObject1.getString("start_on"));
                                    values.put(GroupCompletedChallengesDB.PAUSED_ON, jsonObject1.getString("paused_on"));
                                    values.put(GroupCompletedChallengesDB.RESUME_ON, jsonObject1.getString("resume_on"));
                                    values.put(GroupCompletedChallengesDB.COMPLETED_ON_TXT, jsonObject1.getString("completed_on_txt"));
                                    values.put(GroupCompletedChallengesDB.COMPLETED_ON, jsonObject1.getString("completed_on"));
                                    values.put(GroupCompletedChallengesDB.WINNER, jsonObject1.getString("winner"));
                                    values.put(GroupCompletedChallengesDB.PAUSED_BY, jsonObject1.getString("paused_by"));
                                    values.put(GroupCompletedChallengesDB.CHALLENGE_TYPE, jsonObject1.getString("challenge_type"));
                                    values.put(GroupCompletedChallengesDB.WIN_REWARD_TYPE, jsonObject1.getString("winning_reward_type"));
                                    values.put(GroupCompletedChallengesDB.WIN_REWARD_VALUE, jsonObject1.getString("winning_reward_value"));
                                    values.put(GroupCompletedChallengesDB.IS_SCRATCHED, jsonObject1.getString("is_scratched"));
                                    values.put(GroupCompletedChallengesDB.PAUSE_ACCESSS, jsonObject1.getString("pause_access"));
                                    if(jsonObject1.has("opponent_id")){
                                        values.put(GroupCompletedChallengesDB.OPPONENT_ID, jsonObject1.getString("opponent_id"));
                                    }
                                    boolean isExit = groupCompletedChallengesDB.CheckIsDataAlreadyInDBorNot(jsonObject1.getString("challenge_id"));
                                    if (!isExit) {
                                        groupCompletedChallengesDB.addGroupCompletedChallengesList(values);

                                    }
                                }
                                groupCompletedChallenges();

                            } else {

                                ll_completed.setVisibility(View.GONE);
                            }

                        }
                        if (status.equals("10200")) {
                            Toast.makeText(getActivity(), "No Challenges Found...!", Toast.LENGTH_LONG).show();
                        }
                        if (status.equals("10300")) {
                            no_data_image.setVisibility(View.VISIBLE);
                            Toast.makeText(getActivity(), "No data Found..!", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);

        } else {
            groupRunningChallenges();
            groupUpcomingChallenges();
            groupCompletedChallenges();
            //Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == running_more_text) {
            Intent irunmore = new Intent(getActivity(), AllRunningChallengesActivity.class);
            irunmore.putExtra("condition", "GROUP");
            startActivity(irunmore);
        }
        if (view == upcoming_more_text) {
            Intent irunmore = new Intent(getActivity(), AllUpcomingChallengesActivity.class);
            irunmore.putExtra("condition", "GROUP");
            startActivity(irunmore);
        }
        if (view == complet_more_text) {
            Intent irunmore = new Intent(getActivity(), AllCompletedChallengesActivity.class);
            irunmore.putExtra("condition", "GROUP");
            startActivity(irunmore);
        }
    }

    private void groupCompletedChallenges() {
        List<String> challenge_id = groupCompletedChallengesDB.getChallengeID();
        if (challenge_id.size() > 0) {
            List<String> user_name = groupCompletedChallengesDB.getUserName();
            List<String> opponent_name = groupCompletedChallengesDB.getOpponentName();
            List<String> winning_status = groupCompletedChallengesDB.getWinningStatus();
            List<String> is_group_admin = groupCompletedChallengesDB.getIsGroupAdmin();
            List<String> status = groupCompletedChallengesDB.getStatus();
            List<String> amount = groupCompletedChallengesDB.getAmount();
            List<String> winning_amount = groupCompletedChallengesDB.getWinningAmount();
            List<String> start_on = groupCompletedChallengesDB.getStartOn();
            List<String> paused_on = groupCompletedChallengesDB.getPausedOn();
            List<String> resume_on = groupCompletedChallengesDB.getResumeOn();
            List<String> completed_on_txt = groupCompletedChallengesDB.getCompletedOnTxt();
            List<String> completed_on = groupCompletedChallengesDB.getCompletedOn();
            List<String> winner = groupCompletedChallengesDB.getWinner();
            List<String> paused_by = groupCompletedChallengesDB.getPaused_by();
            List<String> challenge_type = groupCompletedChallengesDB.getChallengeType();
            List<String> winning_reward_type = groupCompletedChallengesDB.win_RewardType();
            List<String> winning_reward_value = groupCompletedChallengesDB.win_RewardValue();
            List<String> is_scratched = groupCompletedChallengesDB.isScratched();
            int completed_count = groupCompletedChallengesDB.getCompletedCount();
            List<String> pause_aceess = groupCompletedChallengesDB.pauseaccess();
            Log.d("goup", "" + completed_count);
            for (int i = 0; i < challenge_id.size(); i++) {
                ChallengeGroupDetailModel am_runn = new ChallengeGroupDetailModel();
                am_runn.setChallenge_id(challenge_id.get(i));

                am_runn.setUser_name(user_name.get(i));
                am_runn.setOpponent_name(opponent_name.get(i));
                am_runn.setWinning_status(winning_status.get(i));
                am_runn.setIs_group_admin(is_group_admin.get(i));
                am_runn.setStatus(status.get(i));
                am_runn.setAmount(amount.get(i));
                am_runn.setWinning_amount(winning_amount.get(i));
                am_runn.setStart_on(start_on.get(i));
                am_runn.setPaused_on(paused_on.get(i));
                am_runn.setResume_on(resume_on.get(i));
                am_runn.setCompleted_on_txt(completed_on_txt.get(i));
                am_runn.setCompleted_on(completed_on.get(i));

                am_runn.setWinner(winner.get(i));
                am_runn.setPaused_by(paused_by.get(i));
                am_runn.setChallenge_type(challenge_type.get(i));
                am_runn.setWinning_reward_type(winning_reward_type.get(i));
                am_runn.setWinning_reward_value(winning_reward_value.get(i));
                am_runn.setIs_scratched(is_scratched.get(i));
                am_runn.setPause_access(pause_aceess.get(i));

                challengGroupCompletcomList.add(am_runn);
                Log.d("sdgsdfdgjsdfg", challengGroupCompletcomList.toString());

            }
            grp_completed_recycler.setAdapter(challengGroupCompletAdpter);

        } else {
             ll_completed.setVisibility(View.GONE);
        }
    }

    private void groupRunningChallenges() {
        List<String> challenge_id = groupRunningChallengesDB.getChallengeID();
        if (challenge_id.size() > 0) {
            List<String> user_name = groupRunningChallengesDB.getUserName();
            List<String> opponent_name = groupRunningChallengesDB.getOpponentName();
            List<String> winning_status = groupRunningChallengesDB.getWinningStatus();
            List<String> is_group_admin = groupRunningChallengesDB.getIsGroupAdmin();
            List<String> status = groupRunningChallengesDB.getStatus();
            List<String> amount = groupRunningChallengesDB.getAmount();
            List<String> winning_amount = groupRunningChallengesDB.getWinningAmount();
            List<String> start_on = groupRunningChallengesDB.getStartOn();
            List<String> paused_on = groupRunningChallengesDB.getPausedOn();
            List<String> resume_on = groupRunningChallengesDB.getResumeOn();
            List<String> completed_on_txt = groupRunningChallengesDB.getCompletedOnTxt();
            List<String> completed_on = groupRunningChallengesDB.getCompletedOn();
            List<String> winner = groupRunningChallengesDB.getWinner();
            List<String> paused_by = groupRunningChallengesDB.getPaused_by();
            List<String> challenge_type = groupRunningChallengesDB.getChallengeType();
            List<String> winning_reward_type = groupRunningChallengesDB.win_RewardType();
            List<String> winning_reward_value = groupRunningChallengesDB.win_RewardValue();
            List<String> is_scratched = groupRunningChallengesDB.isScratched();
            int completed_count = groupRunningChallengesDB.getRunningCount();
            List<String> pause_aceess = groupRunningChallengesDB.pauseaccess();
            List<String> opponentids = groupRunningChallengesDB.opponentids();
            Log.d("goup", "" + completed_count);
            for (int i = 0; i < challenge_id.size(); i++) {
                ChallengeGroupDetailModel am_runn = new ChallengeGroupDetailModel();
                am_runn.setChallenge_id(challenge_id.get(i));

                am_runn.setUser_name(user_name.get(i));
                am_runn.setOpponent_name(opponent_name.get(i));
                am_runn.setWinning_status(winning_status.get(i));
                am_runn.setIs_group_admin(is_group_admin.get(i));
                am_runn.setStatus(status.get(i));
                am_runn.setAmount(amount.get(i));
                am_runn.setWinning_amount(winning_amount.get(i));
                am_runn.setStart_on(start_on.get(i));
                am_runn.setPaused_on(paused_on.get(i));
                am_runn.setResume_on(resume_on.get(i));
                am_runn.setCompleted_on_txt(completed_on_txt.get(i));
                am_runn.setCompleted_on(completed_on.get(i));

                am_runn.setWinner(winner.get(i));
                am_runn.setPaused_by(paused_by.get(i));
                am_runn.setChallenge_type(challenge_type.get(i));
                am_runn.setWinning_reward_type(winning_reward_type.get(i));
                am_runn.setWinning_reward_value(winning_reward_value.get(i));
                am_runn.setIs_scratched(is_scratched.get(i));
                am_runn.setPause_access(pause_aceess.get(i));
                if(opponentids.size()>0)
                    am_runn.setOpponent_id(opponentids.get(i));
                challengGroupRunList.add(am_runn);
                Log.d("sdgsdfdgjsdfg", challengGroupRunList.toString());

            }
            grp_running_recycler.setAdapter(challengGroupRunAdpter);

        } else {
            ll_running.setVisibility(View.GONE);
        }
    }

    private void groupUpcomingChallenges() {
        List<String> challenge_id = groupUpcomingChallengesDB.getChallengeID();
        if (challenge_id.size() > 0) {
            List<String> user_name = groupUpcomingChallengesDB.getUserName();
            List<String> opponent_name = groupUpcomingChallengesDB.getOpponentName();
            List<String> winning_status = groupUpcomingChallengesDB.getWinningStatus();
            List<String> is_group_admin = groupUpcomingChallengesDB.getIsGroupAdmin();
            List<String> status = groupUpcomingChallengesDB.getStatus();
            List<String> amount = groupUpcomingChallengesDB.getAmount();
            List<String> winning_amount = groupUpcomingChallengesDB.getWinningAmount();
            List<String> start_on = groupUpcomingChallengesDB.getStartOn();
            List<String> paused_on = groupUpcomingChallengesDB.getPausedOn();
            List<String> resume_on = groupUpcomingChallengesDB.getResumeOn();
            List<String> completed_on_txt = groupUpcomingChallengesDB.getCompletedOnTxt();
            List<String> completed_on = groupUpcomingChallengesDB.getCompletedOn();
            List<String> winner = groupUpcomingChallengesDB.getWinner();
            List<String> paused_by = groupUpcomingChallengesDB.getPaused_by();
            List<String> challenge_type = groupUpcomingChallengesDB.getChallengeType();
            List<String> winning_reward_type = groupUpcomingChallengesDB.win_RewardType();
            List<String> winning_reward_value = groupUpcomingChallengesDB.win_RewardValue();
            List<String> is_scratched = groupUpcomingChallengesDB.isScratched();
            int completed_count = groupUpcomingChallengesDB.getCompletedCount();
            List<String> pause_aceess = groupUpcomingChallengesDB.pauseaccess();
            List<String> opponetids = groupUpcomingChallengesDB.opponentids();
            Log.d("goup", "" + completed_count);
            for (int i = 0; i < status.size(); i++) {
                ChallengeGroupDetailModel am_runn = new ChallengeGroupDetailModel();
                am_runn.setChallenge_id(challenge_id.get(i));

                am_runn.setUser_name(user_name.get(i));
                am_runn.setOpponent_name(opponent_name.get(i));
                am_runn.setWinning_status(winning_status.get(i));
                am_runn.setIs_group_admin(is_group_admin.get(i));
                am_runn.setStatus(status.get(i));
                am_runn.setAmount(amount.get(i));
                am_runn.setWinning_amount(winning_amount.get(i));
                am_runn.setStart_on(start_on.get(i));
                am_runn.setPaused_on(paused_on.get(i));
                am_runn.setResume_on(resume_on.get(i));
                am_runn.setCompleted_on_txt(completed_on_txt.get(i));
                am_runn.setCompleted_on(completed_on.get(i));

                am_runn.setWinner(winner.get(i));
                am_runn.setPaused_by(paused_by.get(i));
                am_runn.setChallenge_type(challenge_type.get(i));
                am_runn.setWinning_reward_type(winning_reward_type.get(i));
                am_runn.setWinning_reward_value(winning_reward_value.get(i));
                am_runn.setIs_scratched(is_scratched.get(i));
                am_runn.setPause_access(pause_aceess.get(i));
                if(opponetids.size()>0)
                    am_runn.setOpponent_id(opponetids.get(i));
                if(status.get(i).equalsIgnoreCase("YETTOSTART")){

                    challengGroupUpcomList.add(am_runn);
                    grp_upcoming_recycler.setAdapter(challengGroupUppAdpter);
                    ll_upcoming.setVisibility(View.VISIBLE);

                }
                if(status.get(i).equalsIgnoreCase("RUNNING")){
                    challengGroupRunList.add(am_runn);
                    grp_running_recycler.setAdapter(challengGroupRunAdpter);
                    ll_running.setVisibility(View.VISIBLE);
                }


            }


        } else {
            ll_upcoming.setVisibility(View.GONE);
        }
    }
     private void challengeStatus(final String id) {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            Dialog.showProgressBar(getActivity(), "Loading");
            String url = AppUrls.BASE_URL + AppUrls.CHALLENGE_STATUS + id + "&user_id=" + user_id;
            Log.d("STATUSURL", url);
            StringRequest statusstr = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Dialog.hideProgressBar();
                    Log.d("STATUSRES", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100")) {
                            ContentValues values = new ContentValues();
                            JSONObject responseObj = jsonObject.optJSONObject("data");
                            JSONArray challenger_daily_status = responseObj.optJSONArray("challenger_daily_status");
                            JSONArray opponent_daily_status = responseObj.optJSONArray("opponent_daily_status");
                            java.text.SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            for (int j = 0; j < challenger_daily_status.length(); j++) {
                                JSONObject jsonObject1 = challenger_daily_status.getJSONObject(j);
                                String starton = jsonObject1.getString("start_on");
                                String endon = jsonObject1.getString("completed_on");
                                String challengedate = jsonObject1.getString("challenge_date");
                                Date start = ConvertToDate(starton);
                                Calendar startcalendar = Calendar.getInstance();
                                startcalendar.setTime(start);
                                startcalendar.add(Calendar.HOUR, -2);
                                long start_time = startcalendar.getTimeInMillis();
                                Date end = ConvertToDate(endon);
                                Calendar endcalendar = Calendar.getInstance();
                                endcalendar.setTime(end);
                                endcalendar.add(Calendar.HOUR, 2);
                                long end_time = endcalendar.getTimeInMillis();
                                int start_seconds = (int) TimeUnit.MILLISECONDS.toSeconds(start_time);
                                int end_seconds = (int) TimeUnit.MILLISECONDS.toSeconds(end_time);
                                values.put(ChallengeDatesDB.CHALLENGE_ID, id);
                                values.put(ChallengeDatesDB.START_TIME, start_seconds);
                                values.put(ChallengeDatesDB.END_TIME, end_seconds);
                                values.put(ChallengeDatesDB.CHALLENGE, "GROUP");
                                values.put(ChallengeDatesDB.CHALLENGE_DATE, "GROUP");
                                challengeDatesDB.interstChallengeDates(values);
                                Log.v("Insert","///"+"Group");
                            }
                           /* Intent it = new Intent(getActivity(), BackgroundStatus.class);
                            getActivity().startService(it);*/
                                      }

                        if (successResponceCode.equals("10200")) {
                            Toast.makeText(getActivity(), "Invalid Input..!", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Dialog.hideProgressBar();
                    Log.e("Error", error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_id);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };

            statusstr.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(statusstr);
        } else {
            Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
      private Date ConvertToDate(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return convertedDate;
    }

}
