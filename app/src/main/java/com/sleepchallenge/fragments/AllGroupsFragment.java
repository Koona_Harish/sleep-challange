package com.sleepchallenge.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sleepchallenge.R;
import com.sleepchallenge.adapters.AllGroupsAdapter;
import com.sleepchallenge.models.MyGroupModel;
import com.sleepchallenge.utils.AppUrls;
import com.sleepchallenge.utils.Dialog;
import com.sleepchallenge.utils.DividerItemDecorator;
import com.sleepchallenge.utils.NetworkChecking;
import com.sleepchallenge.utils.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AllGroupsFragment extends Fragment implements  View.OnClickListener {

    View view;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    RecyclerView.LayoutManager layoutManager;
    Typeface thinFont,regularFont;
    String user_id, user_type, token, device_ID;
    RecyclerView all_groups_recyclerView;
    SearchView all_group_search;
    AllGroupsAdapter allGroupAdapter;
    ArrayList<MyGroupModel> allGroupList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {

        view= inflater.inflate(R.layout.fragment_all_groups, container, false);

        thinFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.thinFont));
        regularFont = Typeface.createFromAsset(getActivity().getAssets(), getResources().getString(R.string.regularFont));
        userSessionManager = new UserSessionManager(getActivity());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_type = userDetails.get(UserSessionManager.USER_TYPE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        device_ID = userDetails.get(UserSessionManager.DEVICE_ID);
        checkInternet = NetworkChecking.isConnected(getActivity());


        all_group_search= view.findViewById(R.id.all_group_search);
        all_groups_recyclerView = (RecyclerView) view.findViewById(R.id.all_groups_recyclerView);

        all_group_search.setIconified(false);
        all_group_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                all_group_search.setIconified(false);
            }
        });
        all_group_search.clearFocus();
        all_group_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                allGroupAdapter.getFilter().filter(query);
                return false;
            }
        });

        getAllGroups();

        return view;
    }

    private void getAllGroups() {
        if (checkInternet) {
            Dialog.showProgressBar(getActivity(), "Loading");

            String allGroupurl =   AppUrls.BASE_URL + AppUrls.GET_GROUPS;
            Log.d("ALLGROUPURL", allGroupurl);

            StringRequest strAllMember = new StringRequest(Request.Method.GET, allGroupurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Dialog.hideProgressBar();
                    Log.d("ALLGROUPRESP", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("response_code");
                        if (successResponceCode.equals("10100"))
                        {

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            allGroupList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                MyGroupModel mygroup = new MyGroupModel(jdataobj);
                                allGroupList.add(mygroup);
                            }
                            allGroupAdapter = new AllGroupsAdapter(allGroupList, AllGroupsFragment.this, R.layout.row_my_group);
                            layoutManager = new LinearLayoutManager(getActivity());
                            all_groups_recyclerView.setNestedScrollingEnabled(false);
                            all_groups_recyclerView.setLayoutManager(layoutManager);
                            RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(getActivity(), R.drawable.recycler_view_divider));
                            all_groups_recyclerView.addItemDecoration(dividerItemDecoration);

                            all_groups_recyclerView.setAdapter(allGroupAdapter);
                        }
                        if (successResponceCode.equals("10200"))
                        {
                            Toast.makeText(getActivity(), "Invalid Input..!", Toast.LENGTH_SHORT).show();
                        }
                        if (successResponceCode.equals("10300"))
                        {
                            Toast.makeText(getActivity(), "No Data Found..!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Dialog.hideProgressBar();
                    Log.e("Error", error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders(){
                    Map<String, String> headers = new HashMap<>();
                    headers.put("x-access-token", token);
                    headers.put("x-device-id", device_ID);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strAllMember.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(strAllMember);
        } else {
            Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(View view) {

    }

}
