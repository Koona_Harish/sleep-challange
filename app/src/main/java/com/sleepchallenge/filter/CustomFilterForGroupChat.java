package com.sleepchallenge.filter;

import android.widget.Filter;

import com.sleepchallenge.adapters.GroupListAdapter;
import com.sleepchallenge.models.GroupMessageModel;

import java.util.ArrayList;



public class CustomFilterForGroupChat extends Filter {

    GroupListAdapter adapter;
    ArrayList<GroupMessageModel> filterList;

    public CustomFilterForGroupChat(ArrayList<GroupMessageModel> filterList, GroupListAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<GroupMessageModel> filteredPlayers=new ArrayList<GroupMessageModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).from_name.toUpperCase().contains(constraint) )
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.groupListModels = (ArrayList<GroupMessageModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
