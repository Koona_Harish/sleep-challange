package com.sleepchallenge.filter;

import android.widget.Filter;

import com.sleepchallenge.fragments.SponsorChatFragment;
import com.sleepchallenge.models.MessagesModel;

import java.util.ArrayList;




public class CustomFilterForSponsorChat extends Filter {

    SponsorChatFragment.MessagesSponsorAdapter adapter;
    ArrayList<MessagesModel> filterList;

    public CustomFilterForSponsorChat(ArrayList<MessagesModel> filterList, SponsorChatFragment.MessagesSponsorAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<MessagesModel> filteredPlayers=new ArrayList<MessagesModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).from_name.toUpperCase().contains(constraint) )
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.msgModels = (ArrayList<MessagesModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
