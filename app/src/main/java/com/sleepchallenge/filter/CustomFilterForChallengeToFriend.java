package com.sleepchallenge.filter;

import android.widget.Filter;

import com.sleepchallenge.adapters.ChallengeToFriendAdapter;
import com.sleepchallenge.models.ChallengeToFriendModel;

import java.util.ArrayList;



public class CustomFilterForChallengeToFriend extends Filter {

    ChallengeToFriendAdapter adapter;
    ArrayList<ChallengeToFriendModel> filterList;

    public CustomFilterForChallengeToFriend(ArrayList<ChallengeToFriendModel> filterList, ChallengeToFriendAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<ChallengeToFriendModel> filteredPlayers=new ArrayList<ChallengeToFriendModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getName().toUpperCase().contains(constraint) )
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.allmemberlistModels = (ArrayList<ChallengeToFriendModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
