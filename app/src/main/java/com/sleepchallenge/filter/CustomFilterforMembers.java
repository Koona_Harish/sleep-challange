package com.sleepchallenge.filter;

import android.widget.Filter;

import com.sleepchallenge.adapters.AddMemberGroupAdapter;
import com.sleepchallenge.models.AddMembersGroupModel;

import java.util.ArrayList;




public class CustomFilterforMembers extends Filter {

    AddMemberGroupAdapter adapter;
    ArrayList<AddMembersGroupModel> filterList;

    public CustomFilterforMembers(ArrayList<AddMembersGroupModel> filterList, AddMemberGroupAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {

            constraint = constraint.toString().toUpperCase();
            ArrayList<AddMembersGroupModel> filteredPlayers = new ArrayList<>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getName().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.gmemberListItem = (ArrayList<AddMembersGroupModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
