package com.sleepchallenge.filter;

import android.widget.Filter;

import com.sleepchallenge.adapters.AllMembersAdapter;
import com.sleepchallenge.models.AllMemberModel;
import java.util.ArrayList;

public class CustomFilterForAllMembersList extends Filter
{
    AllMembersAdapter adapter;
    ArrayList<AllMemberModel> filterList;
    String groupid,comaprString;

    public CustomFilterForAllMembersList(ArrayList<AllMemberModel> filterList, AllMembersAdapter adapter,String groupid) {
        this.adapter = adapter;
        this.filterList = filterList;
        this.groupid = groupid;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<AllMemberModel> filteredPlayers=new ArrayList<AllMemberModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(groupid.length()==0)
                {
                   comaprString= filterList.get(i).name;
                }
                else
                {
                    comaprString= filterList.get(i).user_name;
                }
                if(comaprString.toUpperCase().contains(constraint) )
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.allmemberlistModels = (ArrayList<AllMemberModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
