package com.sleepchallenge.filter;

import android.widget.Filter;

import com.sleepchallenge.activities.AllSponsorsActivity;
import com.sleepchallenge.models.AllMemberModel;

import java.util.ArrayList;


public class CustomFilterForAllSponsorsList extends Filter {

    AllSponsorsActivity.AllSponsorsAdapter adapter;
    ArrayList<AllMemberModel> filterList;

    public CustomFilterForAllSponsorsList(ArrayList<AllMemberModel> filterList, AllSponsorsActivity.AllSponsorsAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<AllMemberModel> filteredPlayers=new ArrayList<AllMemberModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).name.toUpperCase().contains(constraint) )
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.allsponsorMoldelList = (ArrayList<AllMemberModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
