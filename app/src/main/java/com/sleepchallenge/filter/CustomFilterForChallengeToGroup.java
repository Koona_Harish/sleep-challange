package com.sleepchallenge.filter;

import android.widget.Filter;

import com.sleepchallenge.adapters.AllGroupsAdapter;
import com.sleepchallenge.adapters.ChallengeToGroupAdapter;
import com.sleepchallenge.models.MyGroupModel;

import java.util.ArrayList;

public class CustomFilterForChallengeToGroup  extends Filter
{
    ChallengeToGroupAdapter adapter;
    ArrayList<MyGroupModel> filterList;

    public CustomFilterForChallengeToGroup(ArrayList<MyGroupModel> filterList, ChallengeToGroupAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<MyGroupModel> filteredPlayers=new ArrayList<MyGroupModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).name.toUpperCase().contains(constraint) )
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.allgrouplistModels = (ArrayList<MyGroupModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}